set -e
if [ $# -eq 0 ]; then
  echo 'using default account '
  username=nmhung109
  password=12345678
else
  username=$1
  echo "username: $username"
  echo 'Please input password:'
  read -s password;
fi

BRANCH=master
if [ "$ER_ENV" == "DEV" ]; then
  BRANCH=DEV
fi

echo "pulling from $BRANCH..."
git pull "https://$username:$password@bitbucket.org/EraseRisk/erase-risk-api" $BRANCH

echo 'building...'
mvn clean package

echo 'copying resources to bin...'
mkdir -p bin
rm -f bin/eapi-0.0.1-SNAPSHOT.jar
rm -fR bin/email
cp target/eapi-0.0.1-SNAPSHOT.jar bin/
cp -R src/main/resources/email bin/

set +e
echo 'kill the current one. If fail please run command "./run_as_daemon.sh"...'
ps -ef | grep "bin/eapi-0.0.1-SNAPSHOT.jar" | grep -v grep | awk '{print $2}' | xargs kill

echo 'starting...'
./run_as_daemon.sh
