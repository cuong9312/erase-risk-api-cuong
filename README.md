#Erase Risk - API

## Requirements
1. Java 8
2. Maven 3.x.x
3. Lombok

### Started:
`mvn spring-boot:run`  
- `localhost:8080`  
- health check: `http://localhost:8088/admin_1s5ytd/health`
- swagger: `http://localhost:8080/swagger-ui.html`

### Running in production
`mvn package`
`./erase-risk-api.jar`


## API standard
- request and response parameter is in camelCase for both body and url parameter
### Success
1. code 200
2. body:   
```
{
  "data": {
    
  }
}
```

### Fail by validation
1. code 400
2. body:   
```
{
  "status": {
    "id": "unique_id",
    "message": "content of error",
    "code": "ABC_EFG_GHJ",
    "errors": [ // incase has sub errors
      "code": "",
      "param": "param name",
      "message": "content"
    ],
  }
}
```

### Partial success
If our main task is success but additional tasks may be fail then we can return partial success
1. http code 202
2. body
```
{
  "data": {
    ...  
  },
  "status": {
    ...
  }
}
```