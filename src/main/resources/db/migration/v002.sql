create table if not exists strategy_last_price (
  strategy_id INT not null,
  price decimal(18, 4) not null,
  updated_at timestamp not null,
  primary key (strategy_id)
) ENGINE=InnoDB;

INSERT INTO jobs
(
  TYPE,
  name,
  started_at,
  state,
  state_expiry_at,
  running_on_node
)
VALUES
(
  'STORE_PRICE_UPDATE',
  'JOB TO Store last price',
  null,
  0,
  null,
  null
);


