package com.fintasoft.eraserisk.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fintasoft.eraserisk.model.db.FaqGroup;

import java.util.stream.Stream;

@Repository
public interface FaqGroupRepository extends JpaRepository<FaqGroup, Long> {
    Stream<FaqGroup> findByLanguage(String language);
}
