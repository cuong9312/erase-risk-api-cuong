package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.constances.ReportStatusEnum;
import com.fintasoft.eraserisk.model.db.ReportRequest;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRequestRepository extends JpaRepository<ReportRequest, Long> {
	List<ReportRequest> findByStatus(ReportStatusEnum status);
}
