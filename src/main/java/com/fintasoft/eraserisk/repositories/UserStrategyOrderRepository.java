package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.UserStrategyOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.stream.Stream;

@Repository
public interface UserStrategyOrderRepository extends JpaRepository<UserStrategyOrder, Long> {
    Stream<UserStrategyOrder> findBySignalIdInAndSignalDataDeletedIsFalse(Collection<Long> signalIds);

    UserStrategyOrder findByOrderNoAndAccountNumberAndUserId(String orderNo, String accountNumber, long userId);

    UserStrategyOrder findByOrderNoAndAccountNumberAndUserIdAndSecuritiesCompanyId(String orderNo, String accountNumber, long userId, long secId);
}
