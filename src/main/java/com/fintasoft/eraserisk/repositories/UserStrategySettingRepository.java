package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.UserStrategySetting;
import com.fintasoft.eraserisk.model.UserStrategySetting;
import com.fintasoft.eraserisk.model.db.Strategy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserStrategySettingRepository extends JpaRepository<UserStrategySetting, UserStrategySetting.Pk> {
	@Query("select p from UserStrategySetting p where p.pk.strategyId = ?1 and p.pk.userId = ?2 and p.pk.secId = ?3 and p.pk.auto = ?4")
	UserStrategySetting findByPk(long strategyId, long userId, long secId, boolean auto);
}
