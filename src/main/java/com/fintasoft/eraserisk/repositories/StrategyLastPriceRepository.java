package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.StrategyLastPrice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StrategyLastPriceRepository extends JpaRepository<StrategyLastPrice, Long> {
}
