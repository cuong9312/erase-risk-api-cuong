package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.SystemConf;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SystemConfRepository extends JpaRepository<SystemConf, SystemConf.Pk> {
    @Query("select p from SystemConf p where p.pk.name = :name and p.pk.lang = :lang")
    SystemConf findByPk(@Param("name") String name, @Param("lang") String lang);

    @Query("select p from SystemConf p where p.pk.name = :name and p.pk.lang in :listLang")
    List<SystemConf> findByNameAndLangs(@Param("name") String name, @Param("listLang") List<String> listLang);


    @Query("select s from SystemConf s where s.pk.name like :name and s.pk.lang like :lang")
    Page<SystemConf> findAll(@Param("name") String name, @Param("lang") String lang, Pageable pageable);
}
