package com.fintasoft.eraserisk.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fintasoft.eraserisk.model.db.UserPurchaseHistory;

import java.util.stream.Stream;

@Repository
public interface UserPurchaseHistoryRepository extends JpaRepository<UserPurchaseHistory, Long> {
	Page<UserPurchaseHistory> findByUserId(long userId, Pageable pageable);
	@Query("from UserPurchaseHistory p where p.expirationDate < current_timestamp() and p.payment.status = 'COMPLETE' and p.settleExpiry = false")
	Stream<UserPurchaseHistory> findExpired();
}
