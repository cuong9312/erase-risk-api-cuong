package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.constances.SignalEnum;
import com.fintasoft.eraserisk.model.db.StrategySignalData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface StrategySignalDataRepository extends JpaRepository<StrategySignalData, Long> {
	List<StrategySignalData> findByStrategyIdAndTradingAtBetweenAndDeletedFalseOrderByTradingAtAsc(long StrategyId, Timestamp start, Timestamp end);

	List<StrategySignalData> findTop2ByStrategyIdAndDeletedFalseOrderByTradingAtDesc(long StrategyId);

	@Transactional
	@Modifying
	@Query("update StrategySignalData s set s.deleted = true where s.strategyId = ?1")
	void markDeleteByStrategyId(long StrategyId);

	StrategySignalData findTopByCloseForIdIsNullAndCloseByIdIsNullAndStrategyIdAndSignalAndDeletedFalseOrderByIdAsc(Long strategyId, SignalEnum signalEnum);

	@Query(value = "select s.* from strategy_signal_data s where s.id < ?1 and s.strategy_id = ?2 and s.deleted = false order by id desc limit 1", nativeQuery = true)
	StrategySignalData findLastSignalBefore(Long signalId, Long strategyId);

	@Query(value = "select s.* from strategy_signal_data s where s.strategy_id = ?1 and s.deleted = false order by id desc limit 1", nativeQuery = true)
	StrategySignalData findLastSignal(Long strategyId);

	@Query(value = "select s from StrategySignalData s where s.signal = :signal and s.strategyId = :strategyId and s.deleted = false and s.closeById is null and s.closeForId is null order by id asc")
	List<StrategySignalData> findSignalOpen(@Param("signal") SignalEnum signalEnum, @Param("strategyId") Long strategyId);

	StrategySignalData findByTempId(Long tempId);
}
