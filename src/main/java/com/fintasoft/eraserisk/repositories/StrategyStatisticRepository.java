package com.fintasoft.eraserisk.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fintasoft.eraserisk.model.db.StrategyStatistic;

@Repository
public interface StrategyStatisticRepository extends JpaRepository<StrategyStatistic, Long> {
}
