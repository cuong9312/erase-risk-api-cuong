package com.fintasoft.eraserisk.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fintasoft.eraserisk.model.db.DerivativeProductPlatform;

import java.util.List;

@Repository
public interface DerivativeProductPlatformRepository extends JpaRepository<DerivativeProductPlatform, DerivativeProductPlatform.Pk> {
    List<DerivativeProductPlatform> findByPkSignalPlatformIdAndTradingCode(Long platformId, String code);
}
