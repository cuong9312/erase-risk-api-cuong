package com.fintasoft.eraserisk.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fintasoft.eraserisk.model.db.StrategySecuritiesCompany;

@Repository
public interface StrategySecuritiesCompanyRepository extends JpaRepository<StrategySecuritiesCompany, StrategySecuritiesCompany.Pk> {
    StrategySecuritiesCompany findByPkStrategyIdAndPkSecId(long strategyId, long secId);
}
