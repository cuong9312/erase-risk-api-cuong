package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.UserWishList;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserWishListRepository extends JpaRepository<UserWishList, Long> {
	Page<UserWishList> findByUserId(long userId, Pageable pageable);
	
	UserWishList findByUserIdAndStrategyId(long userId, long strategyId);
	
	void deleteByUserIdAndStrategyId(long userId, long strategyId);
}
