package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.UserAuditLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAuditLogRepository extends JpaRepository<UserAuditLog, Long> {
    Page<UserAuditLog> findByUserIdAndSecIdOrderByLoginTimeDesc(long userId, long secId, Pageable pageable);
}
