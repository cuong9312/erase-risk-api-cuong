package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.DailyProfit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

@Repository
public interface DailyProfitRepository extends JpaRepository<DailyProfit, DailyProfit.Pk> {
    List<DailyProfit> findByPkStrategyIdInAndPkDateBetweenOrderByPkDateAsc(List<Long> strategyIds, Timestamp start, Timestamp end);
    List<DailyProfit> findByPkStrategyIdInAndPkDateBetweenOrderByPkStrategyIdAscPkDateDesc(Collection<Long> strategyIds, Timestamp start, Timestamp end);
    List<DailyProfit> findByPkStrategyIdAndPkDateBetweenOrderByPkDateAsc(Long strategyId, Timestamp start, Timestamp end);
    void deleteByPkStrategyId(Long strategyId);
}
