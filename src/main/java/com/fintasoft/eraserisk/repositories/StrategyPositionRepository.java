package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.StrategyPosition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.stream.Stream;

@Repository
public interface StrategyPositionRepository extends JpaRepository<StrategyPosition, StrategyPosition.Pk> {
	void deleteByPkCategory(String category);
	Stream<StrategyPosition> findByPkCategoryAndPkStrategyIdIn(String category, Collection<Long> ids);
}
