package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    
    User findByEmail(String email);

    List<User> findByVerifyEmailToken(String verifyEmailToken);

    User findByResetPasswordToken(String resetPasswordToken);

    User findByUserInfoPhoneNumber(String phoneNumber);
    
    User findByUserInfoPhoneNumberAndIdNot(String phoneNumber, long userId);
    @Query("select distinct u from User u join u.purchases p join p.payment pm where p.strategyId = ?1 and p.expirationDate >= current_timestamp()")
    Stream<User> findUserBoughtStrategy(long strategyId);
}
