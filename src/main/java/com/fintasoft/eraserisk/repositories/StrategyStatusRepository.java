package com.fintasoft.eraserisk.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fintasoft.eraserisk.model.db.StrategyStatus;

@Repository
public interface StrategyStatusRepository extends JpaRepository<StrategyStatus, Long> {

}
