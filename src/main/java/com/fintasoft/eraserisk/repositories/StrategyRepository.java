package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.Strategy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface StrategyRepository extends JpaRepository<Strategy, Long> {
	List<Strategy> findByStrategyNameOrStrategyCode(String strategyName, String strategyCode);
	List<Strategy> findByIdIn(Collection<Long> ids);
	List<Strategy> findByProductIdIn(Collection<Long> ids);
	Strategy findByStrategyName(String strategyName);
	Strategy findByStrategyCode(String strategyCode);
}
