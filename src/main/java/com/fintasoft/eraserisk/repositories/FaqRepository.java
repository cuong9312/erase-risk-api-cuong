package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.Faq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FaqRepository extends JpaRepository<Faq, Long> {
}
