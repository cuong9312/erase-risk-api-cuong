package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.SecuritiesCompany;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SecuritiesCompanyRepository extends JpaRepository<SecuritiesCompany, Long> {
	List<SecuritiesCompany> findByStrategiesStrategyId(long strategyId);
	List<SecuritiesCompany> findByIdIn(List<Long> secIds);
	SecuritiesCompany findBySecCode(String secCode);
}
