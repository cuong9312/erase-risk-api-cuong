package com.fintasoft.eraserisk.repositories.impl;

import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.response.PageExt;
import com.fintasoft.eraserisk.model.db.UserPurchaseHistory;
import com.fintasoft.eraserisk.model.query.ExportRevenue;
import com.fintasoft.eraserisk.model.query.RevenueByDay;
import com.fintasoft.eraserisk.model.query.RevenueByMonth;
import com.fintasoft.eraserisk.model.query.RevenueSummary;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import com.fintasoft.eraserisk.utils.CriterialHelper;
import com.fintasoft.eraserisk.utils.NativeHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserPurchaseRepositoryImpl {
    @PersistenceContext
    EntityManager entityManager;

    public Page<UserPurchaseHistory> findValidOnly(
            long userId,
            Pageable pageable
    ) {
        return this.findValidOnly(userId, null, pageable);
    }

    public Page<UserPurchaseHistory> findValidOnly(
            long userId,
            List<Long> strategyIds,
            Pageable pageable
    ) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserPurchaseHistory> query = cb.createQuery(UserPurchaseHistory.class);
        Root<UserPurchaseHistory> root = query.from(UserPurchaseHistory.class);
        query.select(root);

        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<UserPurchaseHistory> countRoot = countQuery.from(UserPurchaseHistory.class);
        countQuery.select(cb.count(countRoot));

        Predicate predicate = cb.conjunction();
        predicate.getExpressions().add(cb.equal(root.get("user").get("id"), userId));
        
        predicate.getExpressions().add(cb.greaterThan(root.get("expirationDate"), cb.currentTimestamp()));

        if (!CollectionUtils.isEmpty(strategyIds)) {
            predicate.getExpressions().add(root.get("strategy").get("id").in(strategyIds));
        }
       
        CriterialHelper.applySort(new HashMap<>(), new HashMap<>(), pageable, query, cb, root);

        query.where(predicate);
        countQuery.where(predicate);

        TypedQuery<UserPurchaseHistory> typedQuery = entityManager.createQuery(query);
        if (pageable != null) {
            typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize()).setMaxResults(pageable.getPageSize());
        }

        List<UserPurchaseHistory> userPurchaseHistories = typedQuery.getResultList();
        Long count = entityManager.createQuery(countQuery).getSingleResult();
        return new PageImpl<>(userPurchaseHistories, pageable, count);
    }

    public Page<UserPurchaseHistory> findValidOnly(
            List<Long> userIds,
            Long strategyId,
            Pageable pageable
    ) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserPurchaseHistory> query = cb.createQuery(UserPurchaseHistory.class);
        Root<UserPurchaseHistory> root = query.from(UserPurchaseHistory.class);
        query.select(root);

        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<UserPurchaseHistory> countRoot = countQuery.from(UserPurchaseHistory.class);
        countQuery.select(cb.count(countRoot));

        Predicate predicate = cb.conjunction();
        if (!CollectionUtils.isEmpty(userIds)) {
            predicate.getExpressions().add(root.get("user").get("id").in(userIds));
        }

        predicate.getExpressions().add(cb.greaterThan(root.get("expirationDate"), cb.currentTimestamp()));

        predicate.getExpressions().add(cb.equal(root.get("strategy").get("id"), strategyId));

        CriterialHelper.applySort(new HashMap<>(), new HashMap<>(), pageable, query, cb, root);

        query.where(predicate);
        countQuery.where(predicate);

        TypedQuery<UserPurchaseHistory> typedQuery = entityManager.createQuery(query);
        if (pageable != null) {
            typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize()).setMaxResults(pageable.getPageSize());
        }

        List<UserPurchaseHistory> userPurchaseHistories = typedQuery.getResultList();
        Long count = entityManager.createQuery(countQuery).getSingleResult();
        return new PageImpl<>(userPurchaseHistories, pageable, count);
    }

    public Page<UserPurchaseHistory> findPurchaseHistories(
            List<QueryParam> params,
            Pageable pageable
    ) throws ParseException {
        return CriterialHelper.findEntityByParamAndPageable(entityManager, params, pageable, UserPurchaseHistory.class);
    }

    public PageExt<RevenueByDay, RevenueSummary> queryRevenueByDay(
            List<Long> paymentMethodIds,
            List<Long> providerIds,
            List<Long> secIds,
            Timestamp minPurchaseDate,
            Timestamp maxPurchaseDate,
            Pageable pageable
    ) {
        String queryString = "Select DATE(p.purchaseDate) as purchaseDate, sum(p.autoYn) as countAuto, " +
                "count(p.id) as countTotal, sum(p.totalPrice) as amount from UserPurchaseHistory p " +
                "join p.payment py ";
        String countQueryString = "select count(s.purchase_date), sum(s.total_price), sum(s.count_auto), sum(s.count_total) from ( " +
                "select DATE(p.purchase_date) as purchase_date, sum(p.total_price) as total_price, \n" +
                "sum(p.auto_yn) as count_auto, count(p.purchase_date) as count_total " +
                "from user_purchase_history p " +
                "join user_purchase_payment py on p.payment_id = py.id ";
        StringBuilder condition = new StringBuilder(" where py.status = 'COMPLETE' "); // condition here
        StringBuilder countCondition = new StringBuilder(" where py.status = 'COMPLETE' "); // condition here
        String groupBy = " group by DATE(p.purchaseDate) ";
        StringBuilder orderBy = new StringBuilder();
        StringBuilder paging = new StringBuilder();
        if (pageable != null && pageable.getSort() != null) {
            pageable.getSort().forEach(order -> {
                orderBy.append(
                        (orderBy.length() == 0 ? " order by p." : ",p.") + order.getProperty() + (order.isAscending() ? " asc" : " desc")
                );
            });
        }

        Map<String, Object> parameters = new HashMap<>();

        if (!CollectionUtils.isEmpty(paymentMethodIds)) {
            condition.append(" and py.paymentMethod.id in (:pmIds)");
            countCondition.append(" and py.payment_method in (" + StringUtils.arrayToDelimitedString(paymentMethodIds.toArray(), ",") + ")");
            parameters.put("pmIds", paymentMethodIds);
        }

        if (!CollectionUtils.isEmpty(providerIds)) {
            queryString += " join p.strategy st";
            countQueryString += " join strategy st on st.id = p.strategy_id";
            condition.append(" and st.strategyProviderId in (:spIds)");
            countCondition.append(" and st.strategy_provider_id in (" + StringUtils.arrayToDelimitedString(providerIds.toArray(), ",") + ")");
            parameters.put("spIds", providerIds);
        }

        if (!CollectionUtils.isEmpty(secIds)) {
            queryString += " join p.securitiesCompany sc";
            condition.append(" and sc.id in (:scIds)");
            countCondition.append(" and p.sec_id in (" + StringUtils.arrayToDelimitedString(secIds.toArray(), ",") + ")");
            parameters.put("scIds", secIds);
        }

        if (minPurchaseDate != null) {
            condition.append(" and p.purchaseDate >= :minPurchaseDate");
            countCondition.append(" and p.purchase_date >= '" + minPurchaseDate + "'");
            parameters.put("minPurchaseDate", minPurchaseDate);
        }

        if (maxPurchaseDate != null) {
            condition.append(" and p.purchaseDate <= :maxPurchaseDate");
            countCondition.append(" and p.purchase_date <= '" + maxPurchaseDate + "'");
            parameters.put("maxPurchaseDate", maxPurchaseDate);
        }

        queryString = queryString + condition.toString() + groupBy.toString() + orderBy.toString() + paging.toString();
        countQueryString = countQueryString + countCondition.toString() + " group by DATE(p.purchase_date)) as s";

        Query query = entityManager.createQuery(queryString);
        Query countQuery = entityManager.createNativeQuery(countQueryString);
        if (pageable != null) {
            query.setMaxResults(pageable.getPageSize()).setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        }

        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        List results = query.getResultList();
        List<RevenueByDay> list = new ArrayList<>();
        results.forEach(i -> {
            Object[] ls = (Object[]) i;
            list.add(new RevenueByDay((java.sql.Date)ls[0], (Long)ls[1], (Long)ls[2], (Double)ls[3]));
        });
        Object[] summary = ((Object[]) countQuery.getSingleResult());
        RevenueSummary revenueSummary = new RevenueSummary(NativeHelper.toLong(summary[2]), NativeHelper.toLong(summary[3]), NativeHelper.toDouble(summary[1]));
        return new PageExt(list, pageable, NativeHelper.toLong(summary[0]), revenueSummary);
    }
    
    public List<ExportRevenue> exportRevenueByDay(
            List<Long> paymentMethodIds,
            List<Long> providerIds,
            List<Long> secIds,
            Timestamp minPurchaseDate,
            Timestamp maxPurchaseDate
    ) {
        String queryString = "Select DATE(p.purchaseDate) as purchaseDate, sum(p.autoYn) as countAuto, " +
                "count(p.id) as countTotal, sum(p.totalPrice) as amount from UserPurchaseHistory p " +
                "join p.payment py ";

        StringBuilder condition = new StringBuilder(" where py.status = 'COMPLETE' "); // condition here
        String groupBy = " group by DATE(p.purchaseDate) ";


        Map<String, Object> parameters = new HashMap<>();

        if (!CollectionUtils.isEmpty(paymentMethodIds)) {
            condition.append(" and py.paymentMethod.id in (:pmIds)");
            parameters.put("pmIds", paymentMethodIds);
        }

        if (!CollectionUtils.isEmpty(providerIds)) {
            queryString += " join p.strategy st";
            condition.append(" and st.strategyProviderId in (:spIds)");
            parameters.put("spIds", providerIds);
        }

        if (!CollectionUtils.isEmpty(secIds)) {
            queryString += " join p.securitiesCompany sc";
            condition.append(" and sc.id in (:scIds)");
            parameters.put("scIds", secIds);
        }

        if (minPurchaseDate != null) {
            condition.append(" and p.purchaseDate >= :minPurchaseDate");
            parameters.put("minPurchaseDate", minPurchaseDate);
        }

        if (maxPurchaseDate != null) {
            condition.append(" and p.purchaseDate <= :maxPurchaseDate");
            parameters.put("maxPurchaseDate", maxPurchaseDate);
        }

        queryString = queryString + condition.toString() + groupBy.toString();
        Query query = entityManager.createQuery(queryString);

        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        
        List results = query.getResultList();
        List<ExportRevenue> list = new ArrayList<>();
        results.forEach(i -> {
            Object[] ls = (Object[]) i;
            list.add(new ExportRevenue(ConvertUtils.fromDate((java.sql.Date)ls[0]), (Long)ls[1], (Long)ls[2], (Double)ls[3]));
        });
        
        return list;
    }

    public PageExt<RevenueByMonth, RevenueSummary> queryRevenueByMonth(
            List<Long> paymentMethodIds,
            List<Long> providerIds,
            List<Long> secIds,
            Timestamp minPurchaseDate,
            Timestamp maxPurchaseDate,
            Pageable pageable
    ) {
        String queryString = "Select EXTRACT( YEAR_MONTH FROM p.purchaseDate) as purchaseDate, sum(p.autoYn) as countAuto, " +
                "count(p.id) as countTotal, sum(p.totalPrice) as amount from UserPurchaseHistory p " +
                "join p.payment py ";
        String countQueryString = "select count(s.purchase_date) as countMonths, sum(s.total_price) as totalPrice, " +
                "sum(s.count_auto) as countAuto, sum(s.count_total) as countTotal from ( " +
                "select EXTRACT( YEAR_MONTH FROM p.purchase_date) as purchase_date, sum(p.total_price) as total_price, \n" +
                "  sum(p.auto_yn) as count_auto, count(p.purchase_date) as count_total  from user_purchase_history p " +
                "join user_purchase_payment py on p.payment_id = py.id ";
        StringBuilder condition = new StringBuilder(" where py.status = 'COMPLETE' "); // condition here
        StringBuilder countCondition = new StringBuilder(" where py.status = 'COMPLETE' "); // condition here
        String groupBy = " group by EXTRACT( YEAR_MONTH FROM p.purchaseDate) ";
        StringBuilder orderBy = new StringBuilder();
        StringBuilder paging = new StringBuilder();
        if (pageable != null && pageable.getSort() != null) {
            pageable.getSort().forEach(order -> {
                orderBy.append(
                        (orderBy.length() == 0 ? " order by p." : ",p.") + order.getProperty() + (order.isAscending() ? " asc" : " desc")
                );
            });
        }

        Map<String, Object> parameters = new HashMap<>();

        if (!CollectionUtils.isEmpty(paymentMethodIds)) {
            queryString += " join p.payment py";
            countQueryString += " join user_purchase_payment py on p.payment_id = py.id";
            condition.append(" and py.paymentMethod.id in (:pmIds)");
            countCondition.append(" and py.payment_method in (" + StringUtils.arrayToDelimitedString(paymentMethodIds.toArray(), ",") + ")");
            parameters.put("pmIds", paymentMethodIds);
        }

        if (!CollectionUtils.isEmpty(providerIds)) {
            queryString += " join p.strategy st";
            countQueryString += " join strategy st on st.id = p.strategy_id";
            condition.append(" and st.strategyProviderId in (:spIds)");
            countCondition.append(" and st.strategy_provider_id in (" + StringUtils.arrayToDelimitedString(providerIds.toArray(), ",") + ")");
            parameters.put("spIds", providerIds);
        }

        if (!CollectionUtils.isEmpty(secIds)) {
            queryString += " join p.securitiesCompany sc";
            condition.append(" and sc.id in (:scIds)");
            countCondition.append(" and p.sec_id in (" + StringUtils.arrayToDelimitedString(secIds.toArray(), ",") + ")");
            parameters.put("scIds", secIds);
        }

        if (minPurchaseDate != null) {
            condition.append(" and p.purchaseDate >= :minPurchaseDate");
            countCondition.append(" and p.purchase_date >= '"+ minPurchaseDate + "'");
            parameters.put("minPurchaseDate", minPurchaseDate);
        }

        if (maxPurchaseDate != null) {
            condition.append(" and p.purchaseDate <= :maxPurchaseDate");
            countCondition.append(" and p.purchase_date <= '" + maxPurchaseDate + "'");
            parameters.put("maxPurchaseDate", maxPurchaseDate);
        }

        queryString = queryString + condition.toString() + groupBy.toString() + orderBy.toString() + paging.toString();
        countQueryString = countQueryString + countCondition.toString() + " group by EXTRACT( YEAR_MONTH FROM p.purchase_date)) as s";

        Query query = entityManager.createQuery(queryString);
        Query countQuery = entityManager.createNativeQuery(countQueryString);
        if (pageable != null) {
            query.setMaxResults(pageable.getPageSize()).setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        }

        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        List results = query.getResultList();
        List<RevenueByMonth> list = new ArrayList<>();
        results.forEach(i -> {
            Object[] ls = (Object[]) i;
            list.add(new RevenueByMonth((Integer)ls[0], (Long)ls[1], (Long)ls[2], (Double)ls[3]));
        });
        Object[] summary = ((Object[]) countQuery.getSingleResult());
        RevenueSummary revenueSummary = new RevenueSummary(NativeHelper.toLong(summary[2]), NativeHelper.toLong(summary[3]), NativeHelper.toDouble(summary[1]));
        return new PageExt<>(list, pageable, NativeHelper.toLong(summary[0]), revenueSummary);
    }
    
    public List<ExportRevenue> exportRevenueByMonth(
            List<Long> paymentMethodIds,
            List<Long> providerIds,
            List<Long> secIds,
            Timestamp minPurchaseDate,
            Timestamp maxPurchaseDate
    ) {
        String queryString = "Select EXTRACT( YEAR_MONTH FROM p.purchaseDate) as purchaseDate, sum(p.autoYn) as countAuto, " +
                "count(p.id) as countTotal, sum(p.totalPrice) as amount from UserPurchaseHistory p " +
                "join p.payment py ";

        StringBuilder condition = new StringBuilder(" where py.status = 'COMPLETE' "); // condition here
        String groupBy = " group by EXTRACT( YEAR_MONTH FROM p.purchaseDate) ";
        
        Map<String, Object> parameters = new HashMap<>();

        if (!CollectionUtils.isEmpty(paymentMethodIds)) {
            queryString += " join p.payment py";
            condition.append(" and py.paymentMethod.id in (:pmIds)");
            parameters.put("pmIds", paymentMethodIds);
        }

        if (!CollectionUtils.isEmpty(providerIds)) {
            queryString += " join p.strategy st";
            condition.append(" and  st.strategyProviderId in (:spIds)");
            parameters.put("spIds", providerIds);
        }

        if (!CollectionUtils.isEmpty(secIds)) {
            queryString += " join p.securitiesCompany sc";
            condition.append(" and sc.id in (:scIds)");
            parameters.put("scIds", secIds);
        }

        if (minPurchaseDate != null) {
            condition.append(" and p.purchaseDate >= :minPurchaseDate");
            parameters.put("minPurchaseDate", minPurchaseDate);
        }

        if (maxPurchaseDate != null) {
            condition.append(" and p.purchaseDate <= :maxPurchaseDate");
            parameters.put("maxPurchaseDate", maxPurchaseDate);
        }

        queryString = queryString + condition.toString() + groupBy.toString();

        Query query = entityManager.createQuery(queryString);
        

        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }

        List results = query.getResultList();
        List<ExportRevenue> list = new ArrayList<>();
        results.forEach(i -> {
            Object[] ls = (Object[]) i;
            list.add(new ExportRevenue( ((Integer)ls[0]).toString(), (Long)ls[1], (Long)ls[2], (Double)ls[3]));
        });
        
        return list;
    }
}
