package com.fintasoft.eraserisk.repositories.impl;

import com.fintasoft.eraserisk.constances.PaymentStatusEnum;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.db.Strategy;
import com.fintasoft.eraserisk.utils.CriterialHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Repository
public class StrategyRepositoryImpl {
    @PersistenceContext
    EntityManager entityManager;

    public Page<Strategy> searchStrategies(
            Long providerId,
            String strategyName,
            Pageable pageable
    ) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Strategy> query = cb.createQuery(Strategy.class);
        Root<Strategy> root = query.from(Strategy.class);
        query.select(root);

        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<Strategy> countRoot = countQuery.from(Strategy.class);
        countQuery.select(cb.count(countRoot));

        Predicate predicate = cb.conjunction();
        if (providerId != null) {
            predicate.getExpressions().add(cb.equal(root.get("strategyProviderId"), providerId));
        }

        if (!StringUtils.isEmpty(strategyName)) {
            predicate.getExpressions().add(cb.like(cb.upper(root.get("strategyName")), "%" + strategyName + "%"));
        }

        CriterialHelper.applySort(new HashMap<>(), new HashMap<>(), pageable, query, cb, root);

        query.where(predicate);
        countQuery.where(predicate);

        TypedQuery<Strategy> typedQuery = entityManager.createQuery(query);
        if (pageable != null) {
            typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize()).setMaxResults(pageable.getPageSize());
        }

        List<Strategy> strategies = typedQuery.getResultList();
        Long count = entityManager.createQuery(countQuery).getSingleResult();
        return new PageImpl<>(strategies, pageable, count);
    }

    public Page<Strategy> findStrategies(
            List<QueryParam> params,
            Pageable pageable
    ) throws ParseException {
        return CriterialHelper.findEntityByParamAndPageable(entityManager, params, pageable, Strategy.class);
    }
    
    public Page<Strategy> searchStrategiesByUserId(
            long userId,
            Pageable pageable
    ) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Strategy> query = cb.createQuery(Strategy.class);
        Root<Strategy> root = query.from(Strategy.class);
        query.select(root);

        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<Strategy> countRoot = countQuery.from(Strategy.class);
        countQuery.select(cb.count(countRoot));

        Predicate predicate = cb.conjunction();
        Join join = root.join("userPurchaseHistories");
        
        predicate.getExpressions().add(cb.equal(join.join("user").get("id"), userId));
        
        predicate.getExpressions().add(cb.equal(join.join("payment").get("status"), PaymentStatusEnum.COMPLETE));
        predicate.getExpressions().add(cb.greaterThanOrEqualTo(join.get("expirationDate"), cb.currentTimestamp()));
        
        Predicate countPredicate = cb.conjunction();
        Join countJoin = countRoot.join("userPurchaseHistories");
        
        countPredicate.getExpressions().add(cb.equal(countJoin.join("user").get("id"), userId));
        
        countPredicate.getExpressions().add(cb.equal(countJoin.join("payment").get("status"), PaymentStatusEnum.COMPLETE));
        countPredicate.getExpressions().add(cb.greaterThanOrEqualTo(countJoin.get("expirationDate"), cb.currentTimestamp()));

        // TODO need to consider put join to map, and field to map
        CriterialHelper.applySort(new HashMap<>(), new HashMap<>(), pageable, query, cb, root);

        query.where(predicate);
        countQuery.where(countPredicate);

        TypedQuery<Strategy> typedQuery = entityManager.createQuery(query);
        if (pageable != null) {
            typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize()).setMaxResults(pageable.getPageSize());
        }

        List<Strategy> strategies = typedQuery.getResultList();
        Long count = entityManager.createQuery(countQuery).getSingleResult();
        return new PageImpl<>(strategies, pageable, count);
    }

    public List<Strategy> getAllStrategiesBelongToUser(
            long userId
    ) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Strategy> query = cb.createQuery(Strategy.class);
        Root<Strategy> root = query.from(Strategy.class);
        query.select(root);

        Predicate predicate = cb.conjunction();
        Join join = root.join("userPurchaseHistories");

        predicate.getExpressions().add(cb.equal(join.join("user").get("id"), userId));

        predicate.getExpressions().add(cb.equal(join.join("payment").get("status"), PaymentStatusEnum.COMPLETE));
        predicate.getExpressions().add(cb.greaterThanOrEqualTo(join.get("expirationDate"), cb.currentTimestamp()));

        query.where(predicate);

        TypedQuery<Strategy> typedQuery = entityManager.createQuery(query);
        return typedQuery.getResultList();
    }

    public Page<Strategy> getAllStrategiesBelongToUser(
            long userId,
            Pageable pageable
    ) throws ParseException {
        List<QueryParam> queryParams = new ArrayList<>();
        queryParams.add(QueryParam.create("userPurchaseHistories.user.id", userId, Long.class));
        queryParams.add(QueryParam.create("userPurchaseHistories.payment.status", PaymentStatusEnum.COMPLETE.name(), PaymentStatusEnum.class));
        queryParams.add(QueryParam.create("userPurchaseHistories.expirationDate", QueryParam.CURRENT_TIMESTAMP, null, Timestamp.class).functionParam());

        return CriterialHelper.findEntityByParamAndPageable(this.entityManager, queryParams, pageable, Strategy.class);
    }
}
