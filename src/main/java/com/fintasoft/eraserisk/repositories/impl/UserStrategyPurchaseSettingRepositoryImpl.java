package com.fintasoft.eraserisk.repositories.impl;

import com.fintasoft.eraserisk.model.query.UserStrategySettingQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserStrategyPurchaseSettingRepositoryImpl {
    @PersistenceContext
    EntityManager entityManager;

    public Page<UserStrategySettingQuery> queryActive(
            long userId,
            Pageable pageable
    ) {
        String joinAndCondition =
                "from user_purchase_history u\n" +
                "inner join strategy s on u.strategy_id = s.id\n" +
                "inner join user_strategy_setting us on us.strategy_id = u.strategy_id and us.user_id = u.user_id and us.auto = u.auto_yn\n" +
                "where u.user_id = :userId and u.expiration_date > NOW()\n" +
                "group by us.user_id, us.strategy_id, us.auto, us.sec_id";
        return queryActive(userId, pageable, joinAndCondition);
    }

    public Page<UserStrategySettingQuery> queryExpired(
            long userId,
            Pageable pageable
    ) {
        String joinAndCondition =
                "from user_purchase_history u\n" +
                "inner join strategy s on u.strategy_id = s.id\n" +
                "inner join user_strategy_setting us on us.strategy_id = u.strategy_id and us.user_id = u.user_id and us.auto = u.auto_yn\n" +
                "inner join (\n" +
                "  select up.user_id as user_id, up.strategy_id as strategy_id, up.auto_yn as auto, max(up.expiration_date) as expiration_date\n" +
                "  from user_purchase_history up where up.user_id = 146\n" +
                "  group by up.user_id, up.strategy_id, up.auto_yn\n" +
                ") as max_purchase on max_purchase.strategy_id = u.strategy_id and max_purchase.user_id = u.user_id and max_purchase.auto = u.auto_yn\n" +
                "where u.user_id = :userId and max_purchase.expiration_date <= NOW()\n" +
                "group by us.user_id, us.strategy_id, us.auto, us.sec_id";
        return queryActive(userId, pageable, joinAndCondition);
    }

    private Page<UserStrategySettingQuery> queryActive(
            long userId,
            Pageable pageable,
            String joinAndCondition
    ) {
        String query = "select s.strategy_name, us.user_id, us.strategy_id, us.auto, us.auto_disabled, us.position_type, us.balance, us.lot_size, sum(u.quantity), max(u.expiration_date), us.sec_id \n" + joinAndCondition;
        String countQuery = "select count(*) as total from (\n" +
                "select us.user_id " + joinAndCondition +
                ") as a";
        if (pageable != null) {
            String order = "";
            if (pageable.getSort().isSorted()) {
                Iterator<Sort.Order> it = pageable.getSort().iterator();
                while (it.hasNext()) {
                    Sort.Order o = it.next();
                    order += (order.length() == 0 ? "" : ",") + o.getProperty() + " " + o.getDirection().name();
                }
                order = " order by " + order;
            }
            query += order;
//            query += " limit " + pageable.getPageSize() + " offset " + (pageable.getPageSize() * pageable.getPageNumber());
        }

        Query q = entityManager.createNativeQuery(query);
        Query cq = entityManager.createNativeQuery(countQuery);

        if (pageable != null) {
            q.setMaxResults(pageable.getPageSize()).setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        }

        q.setParameter("userId", userId);
        cq.setParameter("userId", userId);

        int count = ((BigInteger)cq.getSingleResult()).intValue();
        List<UserStrategySettingQuery> data = (List<UserStrategySettingQuery>) q.getResultList().stream().map(
                ar -> UserStrategySettingQuery.from((Object[])ar)
        ).collect(Collectors.toList());
        return new PageImpl(data, pageable, (long) count);
    }
}
