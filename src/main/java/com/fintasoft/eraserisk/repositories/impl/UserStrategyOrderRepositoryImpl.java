package com.fintasoft.eraserisk.repositories.impl;


import com.fintasoft.eraserisk.model.api.response.PageExt;
import com.fintasoft.eraserisk.model.query.TradeAutoReport;
import com.fintasoft.eraserisk.model.query.TradeReport;
import com.fintasoft.eraserisk.utils.NativeHelper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserStrategyOrderRepositoryImpl {
    @PersistenceContext
    EntityManager entityManager;

    public PageExt<TradeReport, TradeReport.Summary> queryTradeReport(
            List<Long> secIds,
            List<Long> providerIds,
            Timestamp minTradingAt,
            Timestamp maxTradingAt,
            String username,
            Pageable pageable
    ) {
        String query = "select ua.user_id, ua.sec_id, ua.login_time, ua.logout_time, count(us.id), sum(us.auto_yn), u.username, ui.full_name, s.sec_name \n" +
                "from user_audit_log ua \n" +
                "inner join user_strategy_order us on ua.sec_id = us.sec_id and ua.login_time <= us.trading_at and (ua.logout_time is null or ua.logout_time >= us.trading_at)\n" +
                "inner join `user` u on us.user_id = u.id\n" +
                "inner join `user_info` ui on us.user_id = ui.id\n" +
                "inner join securities_company s on s.id = us.sec_id\n" +
                "{moreJoin}\n" +
                "where 1=1 {condition}\n" +
                "group by ua.user_id, ua.sec_id, ua.login_time, ua.logout_time";
        String condition = "";
        String moreJoin = "";

        String summaryQuery = "select count(e.login_time), sum(e.total), sum(e.total_auto) from (\n" +
                "select ua.user_id, ua.sec_id, ua.login_time as login_time, ua.logout_time, count(us.id) as total, sum(us.auto_yn) as total_auto from user_audit_log ua \n" +
                "inner join user_strategy_order us on ua.sec_id = us.sec_id and ua.login_time <= us.trading_at and (ua.logout_time is null or ua.logout_time >= us.trading_at)" +
                "inner join `user` u on us.user_id = u.id\n" +
                "inner join `user_info` ui on us.user_id = ui.id\n" +
                "{moreJoin}\n" +
                "where 1=1 {condition}\n" +
                "group by ua.user_id, ua.sec_id, ua.login_time, ua.logout_time\n" +
                ") as e";
        Map<String, Object> parameters = new HashMap<>();
        if (!CollectionUtils.isEmpty(providerIds)) {
            moreJoin += "inner join strategy st on st.id = us.strategy_id\n";
            condition += " and st.strategy_provider_id in (:providerIds) ";
            parameters.put("providerIds", providerIds);
        }

        if (!CollectionUtils.isEmpty(secIds)) {
            condition += " and us.sec_id in (:secIds) ";
            parameters.put("secIds", secIds);
        }

        if (minTradingAt!= null) {
            condition += " and us.trading_at >= :min";
            parameters.put("min", minTradingAt);
        }

        if (maxTradingAt!= null) {
            condition += " and us.trading_at <= :max";
            parameters.put("max", maxTradingAt);
        }

        if (!StringUtils.isEmpty(username)) {
            condition += " and upper(u.username) like :username";
            parameters.put("username", "%" + username.toUpperCase() + "%");
        }

        query = query.replace("{condition}", condition).replace("{moreJoin}", moreJoin);
        summaryQuery = summaryQuery.replace("{condition}", condition).replace("{moreJoin}", moreJoin);

        Query q = entityManager.createNativeQuery(query);
        Query sq = entityManager.createNativeQuery(summaryQuery);

        if (pageable != null) {
            q.setMaxResults(pageable.getPageSize()).setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        }

        for (Map.Entry<String, Object> entry:parameters.entrySet()) {
            q.setParameter(entry.getKey(), entry.getValue());
            sq.setParameter(entry.getKey(), entry.getValue());
        }

        List<Object> list = q.getResultList();
        Object[] summary = (Object[]) sq.getSingleResult();

        List<TradeReport> reports = new ArrayList<>();
        list.forEach(row -> {
            Object[] r = (Object[]) row;
            reports.add(new TradeReport(
                    (Integer) r[0], (Integer) r[1], (Timestamp) r[2], (Timestamp) r[3],
                    ((BigInteger) r[4]).intValue(), ((BigDecimal) r[5]).intValue(), (String) r[6], (String) r[7], (String) r[8]
            ));
        });

        TradeReport.Summary s = new TradeReport.Summary(NativeHelper.toInt(summary[1]), NativeHelper.toInt(summary[2]));

        PageExt result = new PageExt(reports, pageable, NativeHelper.toInt(summary[0]), s);
        return result;
    }
    
    public List<TradeReport> exportTradeReport(
            List<Long> secIds,
            Timestamp minTradingAt,
            Timestamp maxTradingAt,
            String username
    ) {
        String query = "select ua.user_id, ua.sec_id, ua.login_time, ua.logout_time, count(us.id), sum(us.auto_yn), u.username, ui.full_name, s.sec_name a \n" +
                "from user_audit_log ua \n" +
                "inner join user_strategy_order us on ua.sec_id = us.sec_id and ua.login_time <= us.trading_at and (ua.logout_time is null or ua.logout_time >= us.trading_at)\n" +
                "inner join `user` u on us.user_id = u.id\n" +
                "inner join `user_info` ui on us.user_id = ui.id\n" +
                "inner join securities_company s on s.id = us.sec_id\n" +
                "where 1=1 {condition}\n" +
                "group by ua.user_id, ua.sec_id, ua.login_time, ua.logout_time";
        String condition = "";

        Map<String, Object> parameters = new HashMap<>();
        if (!CollectionUtils.isEmpty(secIds)) {
            condition += " and us.sec_id in (:secIds) ";
            parameters.put("secIds", secIds);
        }

        if (minTradingAt!= null) {
            condition += " and us.trading_at >= :min";
            parameters.put("min", minTradingAt);
        }

        if (maxTradingAt!= null) {
            condition += " and us.trading_at <= :max";
            parameters.put("max", maxTradingAt);
        }

        if (!StringUtils.isEmpty(username)) {
            condition += " and upper(u.username) like :username";
            parameters.put("username", "%" + username.toUpperCase() + "%");
        }

        query = query.replace("{condition}", condition);

        Query q = entityManager.createNativeQuery(query);

        for (Map.Entry<String, Object> entry:parameters.entrySet()) {
            q.setParameter(entry.getKey(), entry.getValue());
        }

        List<Object> list = q.getResultList();

        List<TradeReport> reports = new ArrayList<>();
        list.forEach(row -> {
            Object[] r = (Object[]) row;
            reports.add(new TradeReport(
                    (Integer) r[0], (Integer) r[1], (Timestamp) r[2], (Timestamp) r[3],
                    ((BigInteger) r[4]).intValue(), ((BigDecimal) r[5]).intValue(), (String) r[6], (String) r[7], (String) r[8]
            ));
        });


        return reports;
    }


    public PageExt<TradeAutoReport, TradeAutoReport.Summary> queryTradeAutoReport(
            List<Long> secIds,
            List<Long> providersIds,
            Timestamp minTradingAt,
            Timestamp maxTradingAt,
            String username,
            String strategyName,
            Pageable pageable
    ) {
        String query = "select u.id as user_id, u.username, ui.full_name, ua.created_at, s.id as strategy_id, s.strategy_name, p.id as provider_id, p.company_name, sc.id as securities_id, sc.sec_name, ua.quantity\n" +
                "from user_strategy_order ua\n" +
//                "inner join user_purchase_history up on up.id = ua.purchase_id\n" +
                "inner join user u on ua.user_id = u.id\n" +
                "inner join user_info ui on u.id = ui.id\n" +
                "inner join strategy s on ua.strategy_id = s.id\n" +
                "inner join strategy_provider p on s.strategy_provider_id = p.id\n" +
                "inner join securities_company sc on ua.sec_id = sc.id\n" +
                "where 1=1 {condition}";
        String condition = "";

        String summaryQuery = "select count(ua.id), sum(ua.quantity)\n" +
                "from user_strategy_order ua\n" +
//                "inner join user_purchase_history up on up.id = ua.purchase_id\n" +
                "inner join user u on ua.user_id = u.id\n" +
                "inner join user_info ui on u.id = ui.id\n" +
                "inner join strategy s on ua.strategy_id = s.id\n" +
                "inner join strategy_provider p on s.strategy_provider_id = p.id\n" +
                "inner join securities_company sc on ua.sec_id = sc.id\n" +
                "where 1=1 {condition}";

        Map<String, Object> parameters = new HashMap<>();
        if (!CollectionUtils.isEmpty(secIds)) {
            condition += " and ua.sec_id in (:secIds) ";
            parameters.put("secIds", secIds);
        }


        if (!CollectionUtils.isEmpty(providersIds)) {
            condition += " and p.id in (:pIds)";
            parameters.put("pIds", providersIds);
        }

        if (minTradingAt!= null) {
            condition += " and ua.created_at >= :min";
            parameters.put("min", minTradingAt);
        }

        if (maxTradingAt!= null) {
            condition += " and ua.created_at <= :max";
            parameters.put("max", maxTradingAt);
        }

        if (!StringUtils.isEmpty(username)) {
            condition += " and upper(ui.full_name) like :username";
            parameters.put("username", "%" + username.toUpperCase() + "%");
        }

        if (!StringUtils.isEmpty(strategyName)) {
            condition += " and upper(s.strategy_name) like :sName";
            parameters.put("sName", "%" + strategyName.toUpperCase() + "%");
        }

        query = query.replace("{condition}", condition);
        summaryQuery = summaryQuery.replace("{condition}", condition);

        Query q = entityManager.createNativeQuery(query);
        Query sq = entityManager.createNativeQuery(summaryQuery);

        if (pageable != null) {
            q.setMaxResults(pageable.getPageSize()).setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        }

        for (Map.Entry<String, Object> entry : parameters.entrySet()) {
            q.setParameter(entry.getKey(), entry.getValue());
            sq.setParameter(entry.getKey(), entry.getValue());
        }

        List<Object> list = q.getResultList();
        Object[] summary = (Object[]) sq.getSingleResult();

        List<TradeAutoReport> reports = new ArrayList<>();
        list.forEach(row -> {
            Object[] r = (Object[]) row;
            reports.add(new TradeAutoReport(
                    (Integer) r[0], (String) r[1], (String) r[2], (Timestamp) r[3],
                    (Integer) r[4], (String) r[5], (Integer) r[6], (String) r[7],
                    (Integer) r[8], (String) r[9], (Integer) r[10]
            ));
        });

        TradeAutoReport.Summary s = new TradeAutoReport.Summary(NativeHelper.toInt(summary[1]));

        PageExt result = new PageExt(reports, pageable, NativeHelper.toInt(summary[0]), s);
        return result;
    }
}
