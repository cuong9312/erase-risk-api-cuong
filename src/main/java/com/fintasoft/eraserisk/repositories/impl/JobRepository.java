package com.fintasoft.eraserisk.repositories.impl;

import com.fintasoft.eraserisk.Application;
import com.fintasoft.eraserisk.annotations.NewTransactional;
import com.fintasoft.eraserisk.annotations.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;
import java.sql.Timestamp;

@Repository
public class JobRepository {
    @Autowired
    EntityManager em;

    @NewTransactional
    public boolean getJob(String type, Integer expiryInSecond) {
        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("Job.queryJob");
        query.setParameter("type", type);
        query.setParameter("nodeId", Application.getInstanceId());
        query.setParameter("expiryMS", expiryInSecond);
        boolean success = query.execute();
        if (!success) {
            return false;
        }
        Timestamp startedAt = (Timestamp) query.getSingleResult();
        return startedAt != null;
    }

    @Transactional
    public void finishJob(String type) {
        javax.persistence.Query query = em.createNativeQuery("update jobs set state = 0 where `type` = ?1");
        query.setParameter(1, type);
        query.executeUpdate();
    }
}
