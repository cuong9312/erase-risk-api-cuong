package com.fintasoft.eraserisk.repositories.impl;

import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.db.StrategySignalData;
import com.fintasoft.eraserisk.utils.CriterialHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Stream;

@Repository
public class StrategySignalDataRepositoryImpl {
    @PersistenceContext
    EntityManager entityManager;

    public List<StrategySignalData> findByStrategyIdAndLimit(
            int maxNumber, Long ...strategyIds
    ) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < strategyIds.length; i++) {
            if (i != 0) {
                s.append(" union all ");
            }
            s.append("(select s.* from strategy_signal_data s where s.deleted = FALSE and s.strategy_id=" + strategyIds[i] + " order by s.trading_at DESC limit " + maxNumber + " offset 0)");
        }

        return entityManager.createNativeQuery(s.toString(), StrategySignalData.class).getResultList();
    }

    public Page<StrategySignalData> find(List<QueryParam> queryParams, Pageable pageable) throws ParseException {
        queryParams.add(QueryParam.create("deleted", "false", Boolean.class));
        return CriterialHelper.findEntityByParamAndPageable(entityManager, queryParams, pageable, StrategySignalData.class);
    }

    public Stream<com.fintasoft.eraserisk.model.db.StrategySignalData> find(List<QueryParam> params) throws ParseException {
        params.add(QueryParam.create("deleted", "false", Boolean.class));
        return CriterialHelper.findEntityByParam(entityManager, params, StrategySignalData.class);
    }
}
