package com.fintasoft.eraserisk.repositories.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;

@Repository
public class ReportRepository {
    @Autowired
    EntityManager em;

    public Long getNextPendingId() {
        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("ReportRequest.getNextReportId");
        boolean success = query.execute();
        if (!success) {
            return null;
        }

        Integer id = (Integer) query.getSingleResult();
        return id == null ? null : (long) id;
    }
}
