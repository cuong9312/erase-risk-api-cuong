package com.fintasoft.eraserisk.repositories.impl;

import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.utils.CriterialHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.ParseException;
import java.util.List;
import java.util.stream.Stream;

@Repository
public class QueryParamRepository {
    @PersistenceContext
    EntityManager entityManager;

    public <E> Page<E> find(List<QueryParam> queryParams, Pageable pageable, Class<E> clazz) throws ParseException {
        return CriterialHelper.findEntityByParamAndPageable(entityManager, queryParams, pageable, clazz);
    }

    public <E> Stream<E> find(List<QueryParam> params, Class<E> clazz) throws ParseException {
        return CriterialHelper.findEntityByParam(entityManager, params, clazz);
    }

    public <E> Stream<E> find(List<QueryParam> params, Sort sort, Class<E> clazz) throws ParseException {
        return CriterialHelper.findEntityByParam(entityManager, params, sort, clazz);
    }
}
