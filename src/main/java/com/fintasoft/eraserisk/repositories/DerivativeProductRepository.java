package com.fintasoft.eraserisk.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fintasoft.eraserisk.model.db.DerivativeProduct;

@Repository
public interface DerivativeProductRepository extends JpaRepository<DerivativeProduct, Long> {
	DerivativeProduct findByDerivativeCode(String derivativeCode);
	List<DerivativeProduct> findByDerivativeNameOrDerivativeCode(String derivativeName, String derivativeCode);

}
