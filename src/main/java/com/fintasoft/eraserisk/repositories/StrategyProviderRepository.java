package com.fintasoft.eraserisk.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fintasoft.eraserisk.model.db.StrategyProvider;

import java.util.List;

@Repository
public interface StrategyProviderRepository extends JpaRepository<StrategyProvider, Long> {
    List<StrategyProvider> findByCompanyName(String companyName);
    List<StrategyProvider> findByCompanyNameKr(String companyNameKr);
    List<StrategyProvider> findByIdIn(List<Long> ids);
}
