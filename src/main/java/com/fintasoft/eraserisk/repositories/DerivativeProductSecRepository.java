package com.fintasoft.eraserisk.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fintasoft.eraserisk.model.db.DerivativeProductSec;

@Repository
public interface DerivativeProductSecRepository extends JpaRepository<DerivativeProductSec, DerivativeProductSec.Pk> {

}
