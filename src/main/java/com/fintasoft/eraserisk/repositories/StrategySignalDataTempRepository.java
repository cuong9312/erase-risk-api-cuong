package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.StrategySignalDataTemp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StrategySignalDataTempRepository extends JpaRepository<StrategySignalDataTemp, Long> {

}
