package com.fintasoft.eraserisk.repositories;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.fintasoft.eraserisk.model.db.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
	Page<Cart> findByUserId(long userId, Pageable pageable);

	@Query("select c from Cart c where c.userId = ?1 and c.strategy.status = 1")
	Page<Cart> findActiveByUserId(long userId, Pageable pageable);

	@Query("select sum(c.quantity*c.price) from Cart as c where c.user.id=?1 and c.strategy.status = 1")
	Double queryTotalPriceByUser(long userId);

	void deleteByUserIdAndId(long userId, long id);

	Cart findByUserIdAndStrategyIdAndSecuritiesCompanyIdAndPriceAndAutoYn(long userId, long strategyId, long secId, double price, int autoYn);
	
	Cart findByIdAndUserId(long Id, long userId);
	
	Page<Cart> findByUserIdAndIdIn(long userId, Collection<Long> cartIds, Pageable pageable);

	@Query("select c from Cart c where c.userId = ?1 and c.strategy.status = 1 and c.id in (?2)")
	Page<Cart> findActiveByUserIdAndIdIn(long userId, Collection<Long> cartIds, Pageable pageable);
}
