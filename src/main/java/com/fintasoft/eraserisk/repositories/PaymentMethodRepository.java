package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.PaymentMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentMethodRepository extends JpaRepository<PaymentMethod, Long> {
	
	PaymentMethod findByMethodCode(String methodCode);
}
