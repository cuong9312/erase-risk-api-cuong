package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
}
