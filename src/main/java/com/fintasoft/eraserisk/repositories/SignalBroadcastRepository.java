package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.SignalBroadcast;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SignalBroadcastRepository extends JpaRepository<SignalBroadcast, Long> {

}
