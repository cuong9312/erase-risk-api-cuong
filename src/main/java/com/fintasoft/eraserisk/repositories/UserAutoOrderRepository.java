package com.fintasoft.eraserisk.repositories;

import com.fintasoft.eraserisk.model.db.UserAutoOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAutoOrderRepository extends JpaRepository<UserAutoOrder, Long> {
	
	UserAutoOrder findByOrderNoAndAccountNumber(String orderNo, String accountNumber);
}
