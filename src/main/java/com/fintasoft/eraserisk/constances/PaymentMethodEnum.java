package com.fintasoft.eraserisk.constances;


public enum PaymentMethodEnum {
    CARD, BANK_TRANSFER
}
