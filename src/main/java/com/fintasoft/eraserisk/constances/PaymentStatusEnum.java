package com.fintasoft.eraserisk.constances;


public enum PaymentStatusEnum {
    PENDING, COMPLETE, CANCELLED;
}
