package com.fintasoft.eraserisk.constances;

public interface Constants {
    String ROI_YEAR_TO_DAY = "ROI_YEAR_TO_DAY";

    String SYSTEM_CONFIG_LANG_NONE = "NONE";
    String SYSTEM_CONFIG_LANG_KO = "KO";
    String SYSTEM_CONFIG_LANG_EN = "EN";

    int BATCH_SIZE_SEND_EMAIL = 49;

}