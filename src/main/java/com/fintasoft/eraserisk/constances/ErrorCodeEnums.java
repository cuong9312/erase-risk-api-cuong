package com.fintasoft.eraserisk.constances;

public enum ErrorCodeEnums {
    INTERNAL_SERVER_ERRROR,
    INVALID_PARAMETER_ERROR,
    TARGET_NOT_FOUND,
    UNAUTHORIZED,
    LOGIN_FAILED,


    EMPTY_VALUE,
    INVALID_FORMAT,
    INVALID_VALUE,
    INVALID_STATE,
    CHECKOUT_FAILED,
    ORDER_FAILED
    ;
}
