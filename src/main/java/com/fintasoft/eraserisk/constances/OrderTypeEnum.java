package com.fintasoft.eraserisk.constances;


public enum OrderTypeEnum {
    Market, Limit;
}
