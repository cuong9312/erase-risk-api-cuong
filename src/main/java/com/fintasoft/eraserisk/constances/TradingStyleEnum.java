package com.fintasoft.eraserisk.constances;


public enum TradingStyleEnum {
    TREND_FOLLOWING, PATTERN, COUNTER_TREND, SWING;
}
