package com.fintasoft.eraserisk.constances;


public enum TradingDurationEnum {
    LONG, SHORT_SWING, SHORT_DAY;
}
