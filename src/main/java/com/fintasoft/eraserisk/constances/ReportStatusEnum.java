package com.fintasoft.eraserisk.constances;


public enum ReportStatusEnum {
    PENDING, IN_PROGRESS, COMPLETED, FAILED;
}
