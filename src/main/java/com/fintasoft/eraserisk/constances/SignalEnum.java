package com.fintasoft.eraserisk.constances;

public enum SignalEnum {
    Long,
    Short,
    ExitLong,
    ExitShort
    ;
}
