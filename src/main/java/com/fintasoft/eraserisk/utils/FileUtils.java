package com.fintasoft.eraserisk.utils;


import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.function.Function;

public class FileUtils {
    public static <T> T processTempFile(Function<File, T> processor) throws IOException {
        File f = File.createTempFile(UUID.randomUUID().toString(), "tmp");
        try {
            return processor.apply(f);
        } finally {
            f.deleteOnExit();
        }
    }
}
