package com.fintasoft.eraserisk.utils;


import java.util.ArrayList;
import java.util.List;

public class Collections {
    public static <T> List<T> toList(T... ts) {
        List<T> result = new ArrayList(ts.length);
        for (T t : ts) {
            result.add(t);
        }
        return result;
    }
}
