package com.fintasoft.eraserisk.utils.lambda;

@FunctionalInterface
public interface RethrowTripleFunction<P1, P2, P3, R, E extends Exception> {
    R apply(P1 p1, P2 p2, P3 p3) throws E;
}
