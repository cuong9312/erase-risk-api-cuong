package com.fintasoft.eraserisk.utils;


import java.util.Base64;

import org.springframework.http.ResponseEntity;

import com.fintasoft.eraserisk.model.api.Response;

public class ResponseUtil {
    public static <T> ResponseEntity<Response<T>> from(T body) {
        return ResponseEntity.ok(new Response<T>(body));
    }

    public static String generateTransactionId() {
        return new String(Base64.getEncoder().encode(String.valueOf(Thread.currentThread().getId()).getBytes()));
    }
}
