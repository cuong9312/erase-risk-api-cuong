package com.fintasoft.eraserisk.utils;


import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {
    public static final long MILISECONDS_OF_A_DAY = 86400000l;
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");

    public static Date yesterdayBegin () {
        return startOfPrevious(1);
    }

    public static Date yesterdayEnd () {
        return endOfPrevious(1);
    }

    public static Date previous(int range) {
        return new Date(System. currentTimeMillis() - ((long)range) * MILISECONDS_OF_A_DAY);
    }

    public static Date endOfPrevious(int range) {
        return getEndOfDate(previous(range));
    }

    public static Date startOfPrevious(int range) {
        return getStartOfDate(previous(range));
    }

    public static Date getEndOfDate(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        return c.getTime();
    }

    public static Date getStartOfDate(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }


    public static Date convertFullDate(String date) {
        try {
            return dateFormat.parse(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date convertMinDateToDate(String minDate) throws ParseException {
        Date date = convertNow(minDate);
        if (date == null) date = convertYesterday(minDate);
        if (date == null) date = convertStartYesterday(minDate);
        if (date == null) date = convertEndYesterday(minDate);
        if (date == null) date = convertFullDate(minDate);
        if (date == null) date = convertMinDateToTime(minDate);
        return date;
    }

    public static String convertMinDate(String minDate) throws ParseException {
        Date date = convertMinDateToDate(minDate);
        if (date == null) {
            return null;
        }
        return dateFormat.format(date);
    }

    public static Date convertMinDateToTime(String minDate) throws ParseException {
        if (StringUtils.isEmpty(minDate)) {
            return null;
        }

        return TimeUtils.getStartOfDate(new SimpleDateFormat("yyyyMMdd").parse(minDate));
    }

    public static String convertMaxDate(String maxDate) throws ParseException {
        Date date = convertMaxDateAsDate(maxDate);
        if (date == null) return null;
        return dateFormat.format(date);
    }

    public static Date convertMaxDateAsDate(String maxDate) throws ParseException {
        Date date = convertNow(maxDate);
        if (date == null) date = convertYesterday(maxDate);
        if (date == null) date = convertStartYesterday(maxDate);
        if (date == null) date = convertEndYesterday(maxDate);
        if (date == null) date = convertFullDate(maxDate);
        if (date == null) date = convertMaxDateToTime(maxDate);
        if (date == null) {
            return null;
        }

        return date;
    }

    public static Date convertMaxDateToTime(String maxDate) throws ParseException {
        if (StringUtils.isEmpty(maxDate)) {
            return null;
        }

        return TimeUtils.getEndOfDate(new SimpleDateFormat("yyyyMMdd").parse(maxDate));
    }

    public static Date convertNow(String date) {
        if ("NOW".equalsIgnoreCase(date)) {
            return new Date();
        }

        return null;
    }

    public static Date convertYesterday(String date) throws ParseException {
        if ("Y".equalsIgnoreCase(date)) {
            return new Date(System.currentTimeMillis() - MILISECONDS_OF_A_DAY);
        }

        return null;
    }

    public static Date convertStartYesterday(String date) throws ParseException {
        if ("SY".equalsIgnoreCase(date)) {
            long time = System.currentTimeMillis() - MILISECONDS_OF_A_DAY;
            time = time - time % MILISECONDS_OF_A_DAY;
            return new Date(time);
        }

        return null;
    }

    public static Date convertEndYesterday(String date) throws ParseException {
        if ("EY".equalsIgnoreCase(date)) {
            long time = System.currentTimeMillis();
            time = time - time % MILISECONDS_OF_A_DAY - 1;
            return new Date(time);
        }

        return null;
    }
}
