package com.fintasoft.eraserisk.utils;


import java.math.BigDecimal;
import java.math.BigInteger;

public class NativeHelper {
    public static Long toLong(Object o) {
        if (o == null) return null;
        if (o instanceof BigDecimal)
            return ((BigDecimal)o).longValue();
        if (o instanceof BigInteger)
            return ((BigInteger)o).longValue();
        return null;
    }

    public static Integer toInt(Object o) {
        return toInt(o, 0);
    }

    public static Integer toInt(Object o, Integer defaultValue) {
        if (o == null) return defaultValue  ;
        if (o instanceof BigDecimal)
            return ((BigDecimal)o).intValue();
        if (o instanceof BigInteger)
            return ((BigInteger)o).intValue();
        return defaultValue;
    }

    public static Double toDouble(Object o) {
        if (o == null) return null;
        if (o instanceof BigDecimal)
            return ((BigDecimal)o).doubleValue();
        if (o instanceof BigInteger)
            return ((BigInteger)o).doubleValue();
        return null;
    }
}
