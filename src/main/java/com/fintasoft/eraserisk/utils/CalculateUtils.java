package com.fintasoft.eraserisk.utils;


import com.fintasoft.eraserisk.constances.SignalEnum;
import com.fintasoft.eraserisk.model.db.Strategy;
import com.fintasoft.eraserisk.model.db.StrategySignalData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CalculateUtils {
	private static final Logger log = LoggerFactory.getLogger(CalculateUtils.class);

    public static void calculateProfit(List<StrategySignalData> openSignals, StrategySignalData nextSignal, StrategySignalData lastSignal, double unitValue){
        nextSignal.setProfit(null);
    	if (openSignals.isEmpty()
				&& (
						nextSignal.getSignal() == SignalEnum.ExitLong
								|| nextSignal.getSignal() == SignalEnum.ExitShort)
				) {
    		log.error("Ignore a due no open signals but has signal type is {}", nextSignal.getSignal());
            calculateStatistic(nextSignal, lastSignal);
    		return;
		}
		if (openSignals.isEmpty()) {
    		openSignals.add(nextSignal);
		} else {
			StrategySignalData open = null;
			if (nextSignal.getSignal() == SignalEnum.Long || nextSignal.getSignal() == SignalEnum.ExitShort) {
				open = openSignals.stream().filter((s) -> s.getSignal() == SignalEnum.Short).findFirst().orElse(null);
			} else {
				open = openSignals.stream().filter((s) -> s.getSignal() == SignalEnum.Long).findFirst().orElse(null);
			}
			if (open == null) {
				nextSignal.setProfit(null);
				if (nextSignal.getSignal() == SignalEnum.Long || nextSignal.getSignal() == SignalEnum.Short) {
					openSignals.add(nextSignal);
				}
			} else {
				open.setCloseBySignal(nextSignal);
				openSignals.remove(open);
				if (open.getSignal() ==  SignalEnum.Long) {
					nextSignal.setProfit((nextSignal.getPrice() - open.getPrice()) * unitValue);
				} else {
					nextSignal.setProfit(-(nextSignal.getPrice() - open.getPrice()) * unitValue);
				}
			}
		}
        calculateStatistic(nextSignal, lastSignal);
    }

    public static void calculateStatistic(StrategySignalData nextSignal, StrategySignalData lastSignal){
        if (lastSignal != null) {
            nextSignal.setCumulativeProfit(lastSignal.getCumulativeProfit() + nextSignal.getSafetyProfit());
            if (nextSignal.getCumulativeProfit() > lastSignal.getHighestProfit()) {
                nextSignal.setHighestProfit(nextSignal.getCumulativeProfit());
            } else {
                nextSignal.setHighestProfit(lastSignal.getHighestProfit());
            }
            nextSignal.setDrawdown(nextSignal.getHighestProfit() - nextSignal.getCumulativeProfit());
        } else {
            nextSignal.setCumulativeProfit(nextSignal.getSafetyProfit());
            nextSignal.setHighestProfit(nextSignal.getCumulativeProfit());
            nextSignal.setDrawdown(nextSignal.getHighestProfit() - nextSignal.getCumulativeProfit());
        }
    }
}
