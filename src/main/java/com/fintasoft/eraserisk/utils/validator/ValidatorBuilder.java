package com.fintasoft.eraserisk.utils.validator;


import lombok.Data;

@Data
public abstract class ValidatorBuilder {
    private ValidatorBuilder checkIfPassed;

    public void checkIf(ValidatorBuilder other) {
        this.checkIfPassed = other;
    }

    public Object check() {
        if (this.checkIfPassed != null && this.checkIfPassed.check() == null) {
            return null;
        }

        return this.doCheck();
    }

    public abstract Object doCheck();
}
