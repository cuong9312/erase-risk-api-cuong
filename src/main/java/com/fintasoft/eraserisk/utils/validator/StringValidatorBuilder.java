package com.fintasoft.eraserisk.utils.validator;

import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import lombok.Data;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Data
public class StringValidatorBuilder extends ValidatorBuilder {
    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
    public static Validate<String> DATE_FORMAT_CHECK = (s) -> {
        try {
            return DATE_FORMAT.parse(s);
        } catch (ParseException e) {
            return null;
        }
    };

    private Errors errors;
    private String fieldName;
    private String fieldValue;
    private boolean empty = false;
    private Validate<String> format;
    private Validate<String> value;

    public StringValidatorBuilder(Errors errors, String fieldName, String fieldValue) {
        this.errors = errors;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    public StringValidatorBuilder empty() {
        this.empty = true;
        return this;
    }

    public StringValidatorBuilder format(Validate validate) {
        this.format = validate;
        return this;
    }

    public StringValidatorBuilder value(Validate validate) {
        this.value = validate;
        return this;
    }

    private boolean passedEmpty() {
        return this.empty || !StringUtils.isEmpty(this.fieldValue);
    }

    public Object doCheck() {
        Object result = 0;
        if (this.empty) {
            if (StringUtils.isEmpty(this.fieldValue)) {
                errors.rejectValue(this.fieldName, ErrorCodeEnums.EMPTY_VALUE.name(),
                        new Object[]{this.fieldName}, "error.param.not_blank");
                return null;
            }
        }
        if (this.format != null && this.passedEmpty()) {
            result = this.format.valid(this.fieldValue);
            if (result == null) {
                errors.rejectValue(this.fieldName, ErrorCodeEnums.INVALID_FORMAT.name(),
                        new Object[]{this.fieldName}, "error.param.invalid_format");
                return null;
            }
        }
        if (this.value != null && this.passedEmpty()) {
            result = this.value.valid(this.fieldValue);
            if (result == null) {
                errors.rejectValue(this.fieldName, ErrorCodeEnums.INVALID_VALUE.name(),
                        new Object[]{this.fieldValue, this.fieldName}, "error.param.value_invalid");
                return null;
            }
        }
        return result;
    }

    public interface Validate<T> {
        Object valid(T t);
    }
}
