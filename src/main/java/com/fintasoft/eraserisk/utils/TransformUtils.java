package com.fintasoft.eraserisk.utils;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintasoft.eraserisk.exceptions.InvalidParameterException;

public class TransformUtils {
    public static <T> T castFrom(Object o, ObjectMapper objectMapper, Class<T> clazz) {
        if (o == null) throw new InvalidParameterException();
        if (o.getClass().isAssignableFrom(clazz)) {
            return (T) o;
        }
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(o), clazz);
        } catch (Exception e) {
            throw new InvalidParameterException().source(e);
        }
    }

    public static <T> T castFrom(Object o, ObjectMapper objectMapper, TypeReference<T> typeReference) {
        if (o == null) throw new InvalidParameterException();
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(o), typeReference);
        } catch (Exception e) {
            throw new InvalidParameterException().source(e);
        }
    }
}
