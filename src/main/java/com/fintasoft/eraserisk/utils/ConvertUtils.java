package com.fintasoft.eraserisk.utils;


import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.configurations.AppContextStatic;
import com.fintasoft.eraserisk.exceptions.InvalidFormatException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import org.springframework.data.domain.*;
import org.springframework.util.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class ConvertUtils {
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    public static SimpleDateFormat dataDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static SimpleDateFormat dateFormatDisplay = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
    public static SimpleDateFormat dateOnlyFormatDisplay = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat dataDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static Date toDate(String date, String fieldName) {
        try {
        	if (date == null) return null;
            return dateFormat.parse(date);
        } catch (ParseException e) {
            throw new InvalidFormatException(fieldName);
        }
    }

    public static String fromTime(Timestamp timestamp) {
        if (timestamp == null) return null;
        return dateFormatDisplay.format(timestamp);
    }

    public static String fromDate(Date date) {
        if (date == null) return null;
        return dateOnlyFormatDisplay.format(date);
    }
    
    public static Timestamp addDay(Timestamp timestamp, long days){
    	Calendar calendar = Calendar.getInstance();
        calendar.setTime(new java.sql.Date(timestamp.getTime()));
        calendar.add(Calendar.DATE, (int) days);
    	return new Timestamp(calendar.getTimeInMillis());
    }

    public static QueryParam<Timestamp> getMinMaxTimestamp(
            String fieldName,
            String minTime,
            String maxTime,
            String minTimeFieldName,
            String maxTimeFieldName
    ) {
        try {
            String minCreated = TimeUtils.convertMinDate(minTime);
            try {
                String maxCreated = TimeUtils.convertMaxDate(maxTime);
                return QueryParam.create(fieldName, minCreated, maxCreated, Timestamp.class);
            } catch (ParseException e) {
                throw new InvalidFormatException(maxTimeFieldName);
            }
        } catch (ParseException e) {
            throw new InvalidFormatException(minTimeFieldName);
        }
    }
    
    public static Timestamp convertDateTime(String date, String time){
    	try {
    		return new Timestamp(dataDateFormat.parse(date + " " + time).getTime());
        } catch (ParseException e) {
            throw new InvalidFormatException(date + " " + time);
        }
    }
    
    public static Map<String, String> splitQuery(String query) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "EUC-KR"), URLDecoder.decode(pair.substring(idx + 1), "EUC-KR"));
        }
        return query_pairs;
    }
    
    public static Timestamp fromString(String datetime){
    	try {
    		return new Timestamp(dataDateTimeFormat.parse(datetime).getTime());
        } catch (ParseException e) {
            throw new InvalidFormatException(datetime);
        }
    }

    public static <T, S> Page<T> convert(Page<S> p, IConvertType<T, S> convertFunction) {
        return new PageImpl<T>(p.getContent().stream().map(s -> convertFunction.from(s)).collect(Collectors.toList()));
    }

    public interface IConvertType<T, S> {
        T from(S s);
    }

    public static Pageable addMoreSort(Pageable pageable, Sort sort, boolean first) {
        if (pageable.getSort().isUnsorted()) {
            return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), sort);
        } else {
            Sort s = sort;
            if (first) {
                s = sort.and(pageable.getSort());
            } else {
                s = pageable.getSort().and(sort);
            }

            return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), s);
        }
    }

    public static String translateResource(String url) {
        if (StringUtils.isEmpty(url)) return url;
        int index =  url.indexOf(":");
        if (index < 1 || index == url.length() - 1) return url;
        String resourceName = url.substring(0, index);
        String path = url.substring(index + 1);
        AppConf appConf = AppContextStatic.ctx.getBean(AppConf.class);
        if (appConf.getResources().containsKey(resourceName)) {
            return appConf.getResources().get(resourceName) + path;
        }
        return url;
    }

    public static <T> T coalesce(T... ts) {
        for (T t : ts) {
            if (t != null) {
                return t;
            }
        }
        return null;
    }

    public static String fromIntlToLocal(String phoneNumber) {
        if (!StringUtils.isEmpty(phoneNumber) && phoneNumber.startsWith("82")) {
            return "0" + phoneNumber.substring(2);
        }
        return phoneNumber;
    }

    public static String fromLocalToInt(String phoneNumber) {
        if (!StringUtils.isEmpty(phoneNumber) && phoneNumber.startsWith("0")) {
            return "82" + phoneNumber.substring(1);
        }
        return phoneNumber;
    }
}
