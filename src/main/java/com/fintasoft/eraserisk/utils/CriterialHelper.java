package com.fintasoft.eraserisk.utils;


import com.fintasoft.eraserisk.model.api.QueryParam;

import org.hibernate.query.Query;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class CriterialHelper {
    private static List<Map<String, Expression>> createFieldCache(int numberOfRoots) {
        List<Map<String, Expression>> res = new ArrayList<>();
        for (int i = 0; i < numberOfRoots; i++) {
            res.add(new HashMap<>());
        }
        return res;
    }

    private static List<Map<String, Join>> createJoinCache(int numberOfRoots) {
        List<Map<String, Join>> res = new ArrayList<>();
        for (int i = 0; i < numberOfRoots; i++) {
            res.add(new HashMap<>());
        }
        return res;
    }

    public static <T> PageImpl<T> findEntityByParamAndPageable(
            EntityManager entityManager,
            List<QueryParam> params,
            Pageable pageable,
            Class<T> aClass
    ) throws ParseException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(aClass);
        Root<T> root = query.from(aClass);
        query.select(root).distinct(true);

        CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
        Root<T> countRoot = countQuery.from(aClass);
        countQuery.select(cb.countDistinct(countRoot));

        Predicate predicate = cb.conjunction();
        List<Map<String, Join>> joinCache = createJoinCache(2);
        List<Map<String, Expression>> fieldCache = createFieldCache(2);
        List<Pair<ParameterExpression, Object>> parameters = applyCondition(fieldCache, joinCache, cb, predicate, params, root, countRoot);

        applySort(fieldCache.get(0), joinCache.get(0), pageable, query, cb, root);

        query.where(predicate);
        countQuery.where(predicate);

        TypedQuery<T> typedQuery = entityManager.createQuery(query);
        TypedQuery<Long> countTypedQuery = entityManager.createQuery(countQuery);
        if (pageable != null) {
            typedQuery.setFirstResult(pageable.getPageNumber() * pageable.getPageSize()).setMaxResults(pageable.getPageSize());
        }

        parameters.forEach((pair) -> {
            typedQuery.setParameter(pair.getFirst(), pair.getSecond());
            countTypedQuery.setParameter(pair.getFirst(), pair.getSecond());
        });

        List<T> data = typedQuery.getResultList();
        Long count = countTypedQuery.getSingleResult();
        return new PageImpl<T>(data, pageable, count);
    }

    public static String getSQLByParam(
            EntityManager entityManager,
            List<QueryParam> params,
            Class aClass
    ) throws ParseException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery(aClass);
        Root root = query.from(aClass);
        query.select(root).distinct(true);

        Predicate predicate = cb.conjunction();
        List<Map<String, Join>> joinCache = createJoinCache(2);
        List<Map<String, Expression>> fieldCache = createFieldCache(2);
        List<Pair<ParameterExpression, Object>> parameters = applyCondition(fieldCache, joinCache, cb, predicate, params, root);

        query.where(predicate);

        TypedQuery typedQuery = entityManager.createQuery(query);

        String sql = typedQuery.unwrap(Query.class).getQueryString();

        parameters.forEach((pair) -> {
            typedQuery.setParameter(pair.getFirst(), pair.getSecond());
        });

        return sql;
    }

    public static <T> Stream<T> findEntityByParam(
            EntityManager entityManager,
            List<QueryParam> params,
            Class aClass
    ) throws ParseException {
        return findEntityByParam(entityManager, params, null, aClass);
    }

    public static <T> Stream<T> findEntityByParam(
            EntityManager entityManager,
            List<QueryParam> params,
            Sort sort,
            Class aClass
    ) throws ParseException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery(aClass);
        Root root = query.from(aClass);
        query.select(root).distinct(true);

        Predicate predicate = cb.conjunction();
        List<Map<String, Join>> joinCache = createJoinCache(2);
        List<Map<String, Expression>> fieldCache = createFieldCache(2);
        List<Pair<ParameterExpression, Object>> parameters = applyCondition(fieldCache, joinCache, cb, predicate, params, root);

        query.where(predicate).distinct(true);

        applySort(fieldCache.get(0), joinCache.get(0), sort, query, cb, root);

        TypedQuery typedQuery = entityManager.createQuery(query);

        parameters.forEach((pair) -> {
            typedQuery.setParameter(pair.getFirst(), pair.getSecond());
        });

        Query<T> convertedQuery = (Query<T>) typedQuery;
        convertedQuery.setFetchSize(5000);

        return convertedQuery.stream();
    }

    public static <T> List<T> findListEntityByParam(
            EntityManager entityManager,
            List<QueryParam> params,
            Class aClass
    ) throws ParseException {
        return findListEntityByParam(entityManager, params, null, aClass);
    }

    public static <T> List<T> findListEntityByParam(
            EntityManager entityManager,
            List<QueryParam> params,
            Sort sort,
            Class aClass
    ) throws ParseException {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery(aClass);
        Root root = query.from(aClass);
        query.select(root).distinct(true);

        Predicate predicate = cb.conjunction();
        List<Map<String, Join>> joinCache = createJoinCache(2);
        List<Map<String, Expression>> fieldCache = createFieldCache(2);
        List<Pair<ParameterExpression, Object>> parameters = applyCondition(fieldCache, joinCache, cb, predicate, params, root);

        query.where(predicate).distinct(true);

        applySort(fieldCache.get(0), joinCache.get(0), sort, query, cb, root);

        TypedQuery typedQuery = entityManager.createQuery(query);

        parameters.forEach((pair) -> {
            typedQuery.setParameter(pair.getFirst(), pair.getSecond());
        });

        return typedQuery.getResultList();
    }

    public static <T> void applySort(
            Map<String, Expression> fieldCache,
            Map<String, Join> joinCache,
            Pageable pageable, CriteriaQuery<T> query,
            CriteriaBuilder cb, Root<T> root) {
        if (pageable != null) {
            applySort(fieldCache, joinCache, pageable.getSort(), query, cb, root);
        }
    }

    public static <T> void applySort(
            Map<String, Expression> fieldCache,
            Map<String, Join> joinCache,
            Sort sort, CriteriaQuery<T> query,
            CriteriaBuilder cb, Root<T> root
    ) {

        if (sort != null) {
            List<Order> orders = new ArrayList<>();
            sort.forEach(order -> {
                String orderProperty = order.getProperty();
                String fieldKey = "origin:" + orderProperty;
                Expression ex;
                if (fieldCache.containsKey(fieldKey)) {
                    ex = fieldCache.get(fieldKey);
                } else {
                    String[] parts = orderProperty.split("\\.");
                    Join join = null;
                    if (parts.length > 1) {
                        String originalKey = "";
                        for (int i = 0; i < parts.length - 1; i++) {
                            originalKey += originalKey.isEmpty() ? parts[i] : ("." + parts[i]);
                            String currentKey = "origin:" + originalKey;
                            if (joinCache.containsKey(currentKey)) {
                                join = joinCache.get(currentKey);
                            } else {
                                join = join == null ? root.join(parts[i], JoinType.LEFT) : join.join(parts[i], JoinType.LEFT);
                                joinCache.put(currentKey, join);
                            }
                        }
                    }
                    ex = join == null ? root.get(parts[parts.length - 1]) : join.get(parts[parts.length - 1]);
                }
                fieldCache.put(fieldKey, ex);
                if (order.isAscending()) orders.add(cb.asc(ex));
                else orders.add(cb.desc(ex));
            });
            if (!orders.isEmpty()) {
                query.orderBy(orders);
            }
        }
    }

    public static <T> List<Pair<ParameterExpression, Object>> applyCondition(
            List<Map<String, Expression>> fieldCache,
            List<Map<String, Join>> joinCache,
            CriteriaBuilder cb,
            Predicate predicate,
            List<QueryParam> params,
            Root root,
            Root... others
    ) throws ParseException {
        List<Pair<ParameterExpression, Object>> parameters = new ArrayList();
        for (int i = 0; i < params.size(); i++) {
            QueryParam param = params.get(i);
            Predicate p = applyCondition(fieldCache, joinCache, cb, param, root, parameters, others);
            if (p != null) predicate.getExpressions().add(p);
        }
        return parameters;
    }

    public static Predicate applyCondition(
            List<Map<String, Expression>> fieldCache,
            List<Map<String, Join>> joinCache,
            CriteriaBuilder cb,
            QueryParam param,
            Root root,
            List<Pair<ParameterExpression, Object>> parameters,
            Root... others
    ) throws ParseException {
        if (param.getManual() != null) {
            return param.getManual().create(fieldCache.get(0), joinCache.get(0), cb, param, root, parameters, others);
        }
        if (param.isNull()) {
            for (int i = 0; i < others.length; i++) {
                param.getFieldExpression(fieldCache.get(i + 1), joinCache.get(i + 1), others[i]);
            }
            return cb.isNull(param.getFieldExpression(fieldCache.get(0), joinCache.get(0), root));
        }
        if (!CollectionUtils.isEmpty(param.getChilds())) {
            List<Predicate> ps = new ArrayList<>();
            for (int j = 0; j < param.getChilds().size(); j++) {
                Predicate p = applyCondition(fieldCache, joinCache, cb, (QueryParam) param.getChilds().get(j), root, parameters, others);
                if (p != null) ps.add(p);
            }
            if (param.isOr()) {
                return cb.or(ps.toArray(new Predicate[ps.size()]));
            } else {
                return cb.and(ps.toArray(new Predicate[ps.size()]));
            }
        }
        if (!StringUtils.isEmpty(param.getMinValue())) {
            Expression expression = param.getValueExpresssion(cb);
            for (int i = 0; i < others.length; i++) {
                param.getFieldExpression(fieldCache.get(i + 1), joinCache.get(i + 1), others[i]);
            }
            if (!param.isFunctionParam())
                parameters.add(Pair.of((ParameterExpression) expression, param.getObject(param.getMinValue())));
            return cb.greaterThanOrEqualTo(
                    param.getFieldExpression(fieldCache.get(0), joinCache.get(0), root),
                    expression
            );
        }
        if (!StringUtils.isEmpty(param.getMaxValue())) {
            Expression expression = param.getValueExpresssion(cb);
            for (int i = 0; i < others.length; i++) {
                param.getFieldExpression(fieldCache.get(i + 1), joinCache.get(i + 1), others[i]);
            }
            if (!param.isFunctionParam())
                parameters.add(Pair.of((ParameterExpression) expression, param.getObject(param.getMaxValue())));
            return cb.lessThanOrEqualTo(
                    param.getFieldExpression(fieldCache.get(0), joinCache.get(0), root),
                    expression
            );
        }
        if (!StringUtils.isEmpty(param.getValue())) {
            Expression expression = param.getValueExpresssion(cb);
            for (int i = 0; i < others.length; i++) {
                param.getFieldExpression(fieldCache.get(i + 1), joinCache.get(i + 1), others[i]);
            }
            if (String.class.equals(param.getDataClass())) {
                if (param.isExtract()) {
                    if (!param.isFunctionParam())
                        parameters.add(Pair.of((ParameterExpression) expression, param.getObject(param.getValue()).toString().toUpperCase()));
                    return cb.equal(
                            cb.upper(param.getFieldExpression(fieldCache.get(0), joinCache.get(0), root)),
                            expression
                    );
                } else {
                    if (!param.isFunctionParam())
                        parameters.add(Pair.of((ParameterExpression) expression, "%" + param.getObject(param.getValue()).toString().toUpperCase() + "%"));
                    return cb.like(
                            cb.upper(param.getFieldExpression(fieldCache.get(0), joinCache.get(0), root)),
                            expression
                    );
                }
            } else {
                if (!param.isFunctionParam())
                    parameters.add(Pair.of((ParameterExpression) expression, param.getObject(param.getValue())));
                return cb.equal(
                        param.getFieldExpression(fieldCache.get(0), joinCache.get(0), root),
                        expression
                );
            }
        }

        if (!CollectionUtils.isEmpty(param.getValues())) {
            for (int i = 0; i < others.length; i++) {
                param.getFieldExpression(fieldCache.get(i + 1), joinCache.get(i + 1), others[i]);
            }
            return param.getFieldExpression(fieldCache.get(0), joinCache.get(0), root).in(param.getValues());
        }
        return null;
    }
}
