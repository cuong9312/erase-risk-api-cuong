package com.fintasoft.eraserisk.utils;

import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;

@Data
public class ValidatorHelper {
    private String fieldName;
    private Object fieldValue;
    private Errors errors;
    private boolean failed = false;

    public ValidatorHelper(String fieldName, Object value, Errors errors) {
        this.fieldName = fieldName;
        this.fieldValue = value;
        this.errors = errors;
    }

    private boolean isEmpty() {
        return this.fieldValue == null ||
                (this.fieldValue instanceof String && StringUtils.isBlank((String) this.fieldValue));
    }

    public ValidatorHelper notBlank() {
        if (this.isEmpty()) {
            errors.rejectValue(
                    this.fieldName,
                    ErrorCodeEnums.EMPTY_VALUE.name(),
                    "error.empty_value"
            );
            this.failed = true;
        }
        return this;
    }

    public ValidatorHelper formatCheck(IValidate check) {
        if (!this.isEmpty()) {
            if (!check.isValid(this.fieldValue)) {
                errors.rejectValue(
                        this.fieldName,
                        ErrorCodeEnums.INVALID_FORMAT.name(),
                        "error.invalid_format"
                );
            }
            this.failed = true;
        }
        return this;
    }

    public ValidatorHelper valueCheck(IValidate check) {
        if (!this.isEmpty() && !this.failed) {
            if (!check.isValid(this.fieldValue)) {
                errors.rejectValue(
                        this.fieldName,
                        ErrorCodeEnums.INVALID_VALUE.name(),
                        "error.invalid_value"
                );
            }
        }
        return this;
    }

    public interface IValidate {
        boolean isValid (Object value);
    }
    
}
