package com.fintasoft.eraserisk.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FieldError {
    private String code;
    private String param;
    private String message;
    private Object[] messageParams;
}
