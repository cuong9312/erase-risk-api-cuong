package com.fintasoft.eraserisk.exceptions;

import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeneralException extends RuntimeException {
    private String code = ErrorCodeEnums.INTERNAL_SERVER_ERRROR.name();
    private String message = "error.unknown";
    private Object[] messageParams;
    private Throwable source;

    public GeneralException(String code, String message, Object[] messageParams) {
        this.code = code;
        this.message = message;
        this.messageParams = messageParams;
    }

    public String getMessage() {
        return this.message;
    }

    public GeneralException source(Throwable source) {
        this.source = source;
        return this;
    }

    public Throwable getCause() {
        return this.source;
    }
}
