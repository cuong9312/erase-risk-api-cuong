package com.fintasoft.eraserisk.exceptions;


import com.fintasoft.eraserisk.constances.ErrorCodeEnums;

public class InvalidStateException extends GeneralException {
    public InvalidStateException(String message, Object[] messageParams) {
        super(ErrorCodeEnums.INVALID_STATE.name(), message, messageParams);
    }
}
