package com.fintasoft.eraserisk.exceptions;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class SubErrorsException extends GeneralException {
    private List<FieldError> errors = new ArrayList<>();

    public SubErrorsException add(FieldError error) {
        this.errors.add(error);
        return this;
    }

    public SubErrorsException(String code, String message) {
        super(code, message, null);
    }

    public SubErrorsException(String code, String message, Object[] messageParams) {
        super(code, message, messageParams);
    }
}
