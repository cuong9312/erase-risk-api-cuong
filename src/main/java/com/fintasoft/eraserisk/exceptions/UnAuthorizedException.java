package com.fintasoft.eraserisk.exceptions;

import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import lombok.Data;

@Data
public class UnAuthorizedException extends GeneralException {
    public UnAuthorizedException() {
        super(ErrorCodeEnums.UNAUTHORIZED.name(), "error.unauthorized", null);
    }
}
