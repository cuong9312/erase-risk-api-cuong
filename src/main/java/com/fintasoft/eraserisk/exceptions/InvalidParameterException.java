package com.fintasoft.eraserisk.exceptions;

import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import lombok.Data;

@Data
public class InvalidParameterException extends SubErrorsException {
    public InvalidParameterException() {
        super(ErrorCodeEnums.INVALID_PARAMETER_ERROR.name(), "error.invalid.parameter");
    }
}
