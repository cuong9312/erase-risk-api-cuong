package com.fintasoft.eraserisk.exceptions;


import com.fintasoft.eraserisk.constances.ErrorCodeEnums;

public class TargetNotFoundException extends GeneralException {
    public TargetNotFoundException() {
        super(ErrorCodeEnums.TARGET_NOT_FOUND.name(), "error.target_not_found", null);
    }
}
