package com.fintasoft.eraserisk.exceptions;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fintasoft.eraserisk.model.api.Response;

@ControllerAdvice
public class ExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    @Autowired
    private MessageSource messageSource;

    @org.springframework.web.bind.annotation.ExceptionHandler(SubErrorsException.class)
    @ResponseBody
    public ResponseEntity<Response> handleSubErrorsException(SubErrorsException e) {
        return log(e, ResponseEntity.badRequest().body(Response.from(e, messageSource)));
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(GeneralException.class)
    @ResponseBody
    public ResponseEntity<Response> handleGeneralException(GeneralException e) {
        return log(e, ResponseEntity.badRequest().body(Response.from(e, messageSource)));
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<Response> handleGeneralException(Exception e) {
        return log(e, new ResponseEntity<>(
                Response.from(new GeneralException(), messageSource),
                HttpStatus.INTERNAL_SERVER_ERROR
        ));
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(PartialException.class)
    @ResponseBody
    public ResponseEntity<Response> handlePartialException(PartialException e) {
        Response response = null;
        if (e.getE() instanceof GeneralException) {
            response = Response.from((GeneralException) e.getE(), messageSource);
        } else {
            response = Response.from(new GeneralException(), messageSource);
        }
        response.setData(e.getData());
        return log(e, new ResponseEntity<>(response, HttpStatus.ACCEPTED));
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseEntity<Response> handleValidationErrorException(MethodArgumentNotValidException ex) {
        return log(ex, new ResponseEntity(
                Response.from(ex, messageSource),
                HttpStatus.BAD_REQUEST
        ));
    }

    private ResponseEntity<Response> log(Exception e, ResponseEntity<Response> r) {
        logger.error("<" + r.getBody().getStatus().getId() + ">" + e.getMessage(), e);
        return r;
    }
}
