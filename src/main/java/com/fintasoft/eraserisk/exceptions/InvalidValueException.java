package com.fintasoft.eraserisk.exceptions;


import com.fintasoft.eraserisk.constances.ErrorCodeEnums;

public class InvalidValueException extends InvalidParameterException {
    public InvalidValueException(String fieldName) {
        super();
        this.add(new FieldError(
                ErrorCodeEnums.INVALID_VALUE.name(),
                fieldName,
                "error.invalid_value",
                null
        ));
    }


    public InvalidValueException(String fieldName, String message, Object[] params) {
        super();
        this.add(new FieldError(
                ErrorCodeEnums.INVALID_FORMAT.name(),
                fieldName,
                message,
                params
        ));
    }
}
