package com.fintasoft.eraserisk.exceptions;


import com.fintasoft.eraserisk.constances.ErrorCodeEnums;

public class InvalidFormatException extends InvalidParameterException {
    public InvalidFormatException(String fieldName) {
        super();
        this.add(new FieldError(
                ErrorCodeEnums.INVALID_FORMAT.name(),
                fieldName,
                "error.invalid_format",
                null
        ));
    }
}
