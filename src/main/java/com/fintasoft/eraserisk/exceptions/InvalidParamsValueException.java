package com.fintasoft.eraserisk.exceptions;


import java.util.Arrays;

public class InvalidParamsValueException extends InvalidValueException {
    public InvalidParamsValueException(String fieldName, Object... params) {
        super(fieldName, "error.invalid_params_value", new Object[]{Arrays.toString(params)});
    }
}
