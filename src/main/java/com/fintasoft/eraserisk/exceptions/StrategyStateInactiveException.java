package com.fintasoft.eraserisk.exceptions;


public class StrategyStateInactiveException extends InvalidStateException {
    public StrategyStateInactiveException(Long strategyId) {
        super("error.strategy.inactive", new Object[]{strategyId});
    }
}
