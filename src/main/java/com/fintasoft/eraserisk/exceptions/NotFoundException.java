package com.fintasoft.eraserisk.exceptions;

import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class NotFoundException extends GeneralException {
    public NotFoundException (String objectName) {
        super(ErrorCodeEnums.TARGET_NOT_FOUND.name(), "error.object_not_found", new Object[]{objectName});
    }
}
