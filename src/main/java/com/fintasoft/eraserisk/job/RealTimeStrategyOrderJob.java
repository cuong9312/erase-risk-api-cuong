package com.fintasoft.eraserisk.job;


import com.fintasoft.eraserisk.services.job.JobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/*
DROP PROCEDURE update_strategy_position;

DELIMITER //

CREATE PROCEDURE update_strategy_position ()
BEGIN
    DECLARE sRank INT DEFAULT 0;
    DECLARE strategyId INT;
    DECLARE bDone INT;
    DECLARE curs CURSOR FOR  select s.id as id from strategy as s left join strategy_statistic as ss on ss.id = s.id where s.status = 1 order by COALESCE(ss.roi, -1) desc;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;
    OPEN curs;
    SET sRank = 0;
    SET bDone = 0;
    REPEAT
        FETCH curs INTO strategyId;
        Set sRank = sRank + 1;
        Update strategy SET rank = sRank where id = strategyId;
    UNTIL bDone END REPEAT;
    CLOSE curs;
    Update strategy SET rank = 2147483646 where ADDDATE(created_at, valid_period) < NOW() and rank != 2147483646;
END; //
DELIMITER ;
 */


@Component
public class RealTimeStrategyOrderJob implements JobService.JobWorker{
    private static final Logger log = LoggerFactory.getLogger(RealTimeStrategyOrderJob.class);

    @Autowired
    JdbcTemplate jdbcTemplate;

    private boolean isRunning = false;

    @Scheduled(cron = "0 0 * * * *")
    public void updateStrategyPosition() {
        this.doCalculate();
    }

    @Override
    public void doJob() {
        this.doCalculate();
    }

    public void doCalculate() {
        if (isRunning) return;
        isRunning = true;
        jdbcTemplate.execute("CALL update_strategy_position");
        isRunning = false;
    }
}
