package com.fintasoft.eraserisk.job;

import com.fintasoft.eraserisk.model.db.DerivativeProduct;
import com.fintasoft.eraserisk.model.db.StrategySignalData;
import com.fintasoft.eraserisk.repositories.StrategyRepository;
import com.fintasoft.eraserisk.services.job.JobService;
import com.fintasoft.eraserisk.utils.DoubleUtils;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class RangeProfitCalculateJob implements JobService.JobWorker{
    private static final Logger log = LoggerFactory.getLogger(RangeProfitCalculateJob.class);
    private Stream<StrategySignalData> stream;
    private List<ProcessUnit> processUnits = new ArrayList<>();
    private int total = 0;
    private StrategyRepository strategyRepository;
    private boolean safetyHandleException = true;

    public RangeProfitCalculateJob(StrategyRepository strategyRepository) {
        this.strategyRepository = strategyRepository;
    }

    public void setStream(Stream<StrategySignalData> stream) {
        this.stream = stream;
    }

    public void setSafetyHandleException(boolean safetyHandleException) {
        this.safetyHandleException = safetyHandleException;
    }

    public RangeProfitCalculateJob resetUnits() {
        processUnits = new ArrayList<>();
        return this;
    }

    public RangeProfitCalculateJob addUnit(SavingProcess saveProcess, SignalChecker checker) {
        ProcessUnit processUnit = new ProcessUnit(saveProcess, checker, strategyRepository);
        processUnit.setSaveProcess(saveProcess);
        processUnit.setChecker(checker);
        processUnits.add(processUnit);
        return this;
    }

    @Override
    public void doJob() {
        processUnits.forEach(p -> p.reset());
        try {
            stream.forEachOrdered(s -> {
                total ++;
                processUnits.forEach(p -> p.processSignal(s));
            });
            processUnits.forEach(p -> p.finish());
        } catch (Exception e) {
            log.error("exception while processing stream", e);
            if (!this.safetyHandleException) {
                throw e;
            }
        } finally {
            log.info("process total {} signals", total);
            try {stream.close();} catch (Exception e){}
        }
    }

    @Data
    public static class ProcessUnit {
//        private List<Double> cumulativeProfits;
        private Long currentStrategyId = null;
        private Double cumulativeProfit = null;
        private Double highestCumulativeProfit = null;
        private Double maxDrawDown = null;
        private Double roi = null;
        private Double bestTrade = null;
        private Double worstTrade = null;
        private Double requiredCapital = null;
        private Double netProfit = null;
        private Double winRate = null;
        private int totalWin = 0;
        private int totalEven = 0;
        private int totalTrade = 0;
        private int totalSignal = 0;
//        private StrategySignalData lastSignal;

        private SavingProcess saveProcess;
        private SignalChecker checker;
        private StrategyRepository strategyRepository;

        public ProcessUnit(SavingProcess saveProcess, SignalChecker checker, StrategyRepository strategyRepository) {
            this.saveProcess = saveProcess;
            this.checker = checker;
            this.strategyRepository = strategyRepository;
        }

        private void reset() {
//            cumulativeProfits = new ArrayList<>();
            currentStrategyId = null;
            cumulativeProfit = null;
            highestCumulativeProfit = null;
            maxDrawDown = null;
            requiredCapital = null;
            netProfit = null;
            winRate = null;
            roi = null;
            bestTrade = null;
            worstTrade = null;
            totalWin = 0;
            totalEven = 0;
            totalTrade = 0;
            totalSignal = 0;
//            lastSignal = null;
        }

        public void finish() {
            if (currentStrategyId != null) {
                this.save();
            }
        }

        public void processSignal(StrategySignalData s) {
            if (checker != null && !checker.check(s)) return;
            if (currentStrategyId != null && s.getStrategyId() != currentStrategyId) { // move to another strategy
                this.save();
                // reset for new strategy
                this.reset();
            }
            boolean isCloseSignal = s.getCloseForId() != null;
            if (isCloseSignal) {
                bestTrade = DoubleUtils.max(bestTrade, s.getProfit());
                worstTrade = DoubleUtils.min(worstTrade, s.getProfit());
                if (s.getSafetyProfit() > 0d) totalWin++;
                else if (s.getSafetyProfit() == 0d) totalEven++;
                totalTrade++;
            }
            cumulativeProfit = DoubleUtils.plusOrZero(cumulativeProfit, s.getProfit());
            highestCumulativeProfit = DoubleUtils.max(highestCumulativeProfit, cumulativeProfit);
            Double dd = highestCumulativeProfit - cumulativeProfit;
            maxDrawDown = DoubleUtils.max(maxDrawDown, dd);
//            cumulativeProfits.add(cumulativeProfit);
            totalSignal++;
            currentStrategyId = s.getStrategyId();
//            lastSignal = s;
        }

        public void save() {
            // calculate drawdown first
            DerivativeProduct derivativeProduct = strategyRepository.getOne(this.currentStrategyId).getDerivativeProduct();
//            if (highestCumulativeProfit != null) {
//                cumulativeProfits.forEach(p -> {
//                    if (p == null) return;
//                    Double dd = highestCumulativeProfit - p;
//                    maxDrawDown = DoubleUtils.max(maxDrawDown, dd);
//                });
//            }
            if (maxDrawDown != null) {
                requiredCapital = derivativeProduct.getMargin() + maxDrawDown.doubleValue()  * derivativeProduct.getMddRate();
                roi = cumulativeProfit / requiredCapital * 100d;
            }
            if (cumulativeProfit != null) {
                netProfit = cumulativeProfit;
            }
            if (totalTrade > 0) {
                winRate = (double) totalWin /(double) totalTrade * 100d;
            }
            saveProcess.save(this);
        }

    }

    public interface SavingProcess {
        void save(ProcessUnit currentData);
    }

    public interface SignalChecker {
        boolean check(StrategySignalData data);
    }
}
