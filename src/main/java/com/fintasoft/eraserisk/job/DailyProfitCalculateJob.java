package com.fintasoft.eraserisk.job;

import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.model.db.DailyProfit;
import com.fintasoft.eraserisk.model.db.StrategySignalData;
import com.fintasoft.eraserisk.repositories.DailyProfitRepository;
import com.fintasoft.eraserisk.repositories.StrategyRepository;
import com.fintasoft.eraserisk.services.job.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.stream.Stream;

@Component
public class DailyProfitCalculateJob implements JobService.JobWorker{
    private Stream<StrategySignalData> stream;
    private Timestamp start = null;

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public void setStream(Stream<StrategySignalData> stream) {
        this.stream = stream;
    }

    @Autowired
    AppConf appConf;

    @Autowired
    DailyProfitRepository dailyProfitRepository;

    @Autowired
    StrategyRepository strategyRepository;

    @Override
    public void doJob() {
        RangeProfitCalculateJob rangeProfitCalculateJob = new RangeProfitCalculateJob(strategyRepository);
        rangeProfitCalculateJob.setStream(this.stream);
        rangeProfitCalculateJob.resetUnits().addUnit(this::save, null);
        rangeProfitCalculateJob.doJob();
    }

    public void save(RangeProfitCalculateJob.ProcessUnit data) {
        DailyProfit dailyProfit = dailyProfitRepository.findById(new DailyProfit.Pk(new Date(start.getTime()), data.getCurrentStrategyId())).orElse(null);
        if (dailyProfit == null)
        dailyProfit = new DailyProfit(
                data.getCurrentStrategyId(),
                new Date(start.getTime()),
                data.getCumulativeProfit(),
                data.getRoi(),
                data.getMaxDrawDown()
        );
        else {
            dailyProfit.setMdd(data.getMaxDrawDown());
            dailyProfit.setProfit(data.getCumulativeProfit());
            dailyProfit.setRoi(data.getRoi());
        }
        dailyProfit.setRequiredCapital(data.getRequiredCapital());
        dailyProfit.setBestTrade(data.getBestTrade());
        dailyProfit.setWorstTrade(data.getWorstTrade());
        dailyProfit.setTotalWin(data.getTotalWin());
        dailyProfit.setTotalCount(data.getTotalSignal());
        dailyProfit.setTotalTrade(data.getTotalTrade());
        dailyProfit.setTotalBreakEven(data.getTotalEven());
        dailyProfit.setWinRate(data.getWinRate());
        dailyProfitRepository.save(dailyProfit);
    }
}
