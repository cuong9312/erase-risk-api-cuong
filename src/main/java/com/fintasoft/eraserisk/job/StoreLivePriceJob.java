package com.fintasoft.eraserisk.job;

import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.model.db.StrategyLastPrice;
import com.fintasoft.eraserisk.repositories.StrategyLastPriceRepository;
import com.fintasoft.eraserisk.repositories.impl.JobRepository;
import com.fintasoft.eraserisk.services.queue.StrategyPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Optional;

@Component
public class StoreLivePriceJob {
    @Autowired
    JobRepository jobRepository;

    @Autowired
    StrategyLastPriceRepository strategyLastPriceRepository;

    @Autowired
    StrategyPriceService strategyPriceService;

    @Scheduled(cron = "0 0,5,10,15,20,25,30,35,40,45,50,55 * * * *")
    @Transactional
    public void doJob() {
        if (jobRepository.getJob("STORE_PRICE_UPDATE", 250)) {
            strategyPriceService.getPriceUpdateMap().forEach((id, time) -> this.update(id, strategyPriceService.getPrice(id), time));
            jobRepository.finishJob("STORE_PRICE_UPDATE");
        }
    }

    private void update(Long strategyId, Double price, Long time) {
        Optional<StrategyLastPrice> s = strategyLastPriceRepository.findById(strategyId);
        if (s.isPresent()) {
            s.get().setPrice(price);
            s.get().setUpdatedAt(new Timestamp(time));
        } else {
            strategyLastPriceRepository.save(new StrategyLastPrice(strategyId, price, new Timestamp(time)));
        }
    }
}
