package com.fintasoft.eraserisk.job;

import com.fintasoft.eraserisk.repositories.impl.JobRepository;
import com.fintasoft.eraserisk.services.order.UserPurchaseHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ExpirePurchaseJob {
    @Autowired
    UserPurchaseHistoryService userPurchaseHistoryService;
    @Autowired
    JobRepository jobRepository;

    @Scheduled(cron = "0 0 7 * * *")
    public void expirePurchases() {
        if (jobRepository.getJob("PURCHASE_EXPIRY", 1200)) {
            userPurchaseHistoryService.expirePurchases();
            jobRepository.finishJob("PURCHASE_EXPIRY");
        }
    }
}
