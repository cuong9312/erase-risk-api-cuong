//package com.fintasoft.eraserisk.job;
//
//import com.fintasoft.eraserisk.model.api.QueryParam;
//import com.fintasoft.eraserisk.model.db.StrategySignalData;
//import com.fintasoft.eraserisk.repositories.StrategySignalDataRepository;
//import com.fintasoft.eraserisk.services.job.JobService;
//import com.fintasoft.eraserisk.utils.TimeUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//
//import javax.transaction.Transactional;
//import java.sql.Timestamp;
//import java.text.ParseException;
//import java.util.stream.Stream;
//
///**
// * job to calculate daily profit. Should be run in early of next day.
// */
//@Component
//public class DailyProfitJob implements JobService.JobWorker{
//    private Timestamp start = null;
//    private Timestamp end = null;
//
//    @Autowired
//    StrategySignalDataRepository strategySignalDataRepository;
//
//    @Autowired
//    DailyProfitCalculateJob dailyProfitCalculateJob;
//
//    /**
//     * should call this function every time
//     */
//    public void reset() {
//        this.start = null;
//        this.end = null;
//    }
//
//    public Timestamp getStart() {
//        return start;
//    }
//
//    public void setStart(Timestamp start) {
//        this.start = start;
//    }
//
//    public Timestamp getEnd() {
//        return end;
//    }
//
//    public void setEnd(Timestamp end) {
//        this.end = end;
//    }
//
//    /**
//     * execute by spring boot scheduler
//     */
//    @Scheduled(cron = "0 0 1 * * *")
//    @Transactional(Transactional.TxType.REQUIRES_NEW)
//    public void schedulerExecute() {
//        this.reset();
//        this.doJob();
//    }
//
//    @Override
//    @Transactional(Transactional.TxType.REQUIRES_NEW)
//    public void doJob() {
//        if (this.start==null) {
//            try {
//                start = new Timestamp(TimeUtils.convertStartYesterday("SY").getTime());
//            } catch (ParseException e) {
//                throw new RuntimeException(e);
//            }
//        }
//        if (this.start==null) {
//            try {
//                end = new Timestamp(TimeUtils.convertEndYesterday("EY").getTime());
//            } catch (ParseException e) {
//                throw new RuntimeException(e);
//            }
//        }
//
//        this.dailyProfitCalculateJob.setStart(start);
//
//        /**
//         * query all signal data of yesterday and order by strategy and trading at
//         */
//        Stream<StrategySignalData> stream =
//                strategySignalDataRepository.findByTradingAtBetweenOrderByStrategyIdAscTradingAtAscIdAsc(start, end);
//
//        this.dailyProfitCalculateJob.setStream(stream);
//        this.dailyProfitCalculateJob.doJob();
//    }
//
//
//}
