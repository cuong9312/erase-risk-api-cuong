//package com.fintasoft.eraserisk.job;
//
//
//import com.fintasoft.eraserisk.configurations.AppConf;
//import com.fintasoft.eraserisk.model.db.StrategyPosition;
//import com.fintasoft.eraserisk.model.db.StrategySignalData;
//import com.fintasoft.eraserisk.repositories.StrategyPositionRepository;
//import com.fintasoft.eraserisk.repositories.StrategyRepository;
//import com.fintasoft.eraserisk.repositories.StrategySignalDataRepository;
//import com.fintasoft.eraserisk.utils.TimeUtils;
//import lombok.Data;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.util.CollectionUtils;
//
//import javax.transaction.Transactional;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//
//@Configuration
//public class CalculateStrategyPositionJob {
//    private static final Logger log = LoggerFactory.getLogger(CalculateStrategyPositionJob.class);
//    @Autowired
//    AppConf appConf;
//
//    @Autowired
//    StrategyPositionRepository strategyPositionRepository;
//
//    @Autowired
//    StrategyRepository strategyRepository;
//
//    @Autowired
//    StrategySignalDataRepository strategySignalDataRepository;
//
//    @Scheduled(cron = "0 1 0 * * *")
//    public void schedule() {
//        this.doCalculate();
//    }
//
//    private volatile boolean doing = false;
//    private volatile boolean reserved = false;
//
//    @Transactional(Transactional.TxType.REQUIRES_NEW)
//    public void doCalculate() {
//        if (doing) {
//            reserved = true;
//            return;
//        }
//        doing = true;
//        try {
//            log.info("start doing calculate");
//            if (!CollectionUtils.isEmpty(appConf.getCalculateStrategyPosition())) {
//                RangeProfitCalculateJob rangeProfitCalculateJob = new RangeProfitCalculateJob(strategyRepository);
//                List<OrderCategory> categories = appConf.getCalculateStrategyPosition().stream().
//                        map(s -> new OrderCategory(s)).collect(Collectors.toList());
//                categories.sort((t, s) ->
//                        t.getCategory().getRange() < s.getCategory().getRange() ? 1 :
//                                (t.getCategory().getRange() == s.getCategory().getRange() ? 0 : -1));
//                int maxRange = categories.get(0).getCategory().getRange();
//                Timestamp takeDataDay = new Timestamp(
//                        TimeUtils.getStartOfDate(
//                                new Date(System.currentTimeMillis() - (((long) maxRange + 1) * MILISECONDS_OF_A_DAY))
//                        ).getTime());
//                Timestamp to = new Timestamp(TimeUtils.convertEndYesterday("EY").getTime());
//                Stream<StrategySignalData> datas = strategySignalDataRepository.
//                        findByTradingAtBetweenOrderByStrategyIdAscTradingAtAscIdAsc(takeDataDay, to);
//                rangeProfitCalculateJob.setStream(datas);
//                rangeProfitCalculateJob.resetUnits();
//                rangeProfitCalculateJob.setSafetyHandleException(false);
//                categories.forEach(c -> rangeProfitCalculateJob.addUnit(c, c));
//                rangeProfitCalculateJob.doJob();
//                strategyPositionRepository.deleteAll();
//                categories.forEach(c -> {
//                    c.results.sort((l, r) -> {
//                        if (r.getRoi() == null) return 1;
//                        if (l.getRoi() == null) return -1;
//                        if (l.getRoi() > r.getRoi()) return 1;
//                        else if (l.getRoi().equals(r.getRoi())) return 0;
//                        return -1;
//                    });
//                    for (int i = 0; i < c.results.size(); i++) {
//                        c.results.get(i).setPositionNo(i);
//                    }
//                    strategyPositionRepository.saveAll(c.results);
//                });
//            }
//        } catch (Exception e) {
//            log.error("Exception while calculate strategy positions", e);
//        }
//        doing = false;
//        if (reserved) {
//            this.reserved = false;
//            this.doCalculate();
//        }
//    }
//
//    @Data
//    public static class OrderCategory implements RangeProfitCalculateJob.SignalChecker, RangeProfitCalculateJob.SavingProcess {
//        private AppConf.StrategyPositionCategory category;
//        private Timestamp minTime;
//        private List<StrategyPosition> results = new ArrayList<>();
//
//        public OrderCategory(AppConf.StrategyPositionCategory category) {
//            this.category = category;
//            this.minTime = new Timestamp(System.currentTimeMillis() - ((long) category.getRange() * MILISECONDS_OF_A_DAY));
//        }
//
//        @Override
//        public boolean check(StrategySignalData data) {
//            return !data.getTradingAt().before(this.minTime);
//        }
//
//        @Override
//        public void save(RangeProfitCalculateJob.ProcessUnit unit) {
//            StrategyPosition sp = new StrategyPosition(
//                    unit.getCurrentStrategyId(),
//                    this.getCategory().getName(),
//                    0,
//                    String.format("%.6f", unit.getRoi()),
//                    unit.getRoi(),
//                    unit.getCumulativeProfit(),
//                    unit.getMaxDrawDown()
//            );
//            sp.setRequiredCapital(unit.getRequiredCapital());
//            sp.setNetProfit(unit.getNetProfit());
//            sp.setBestTrade(unit.getBestTrade());
//            sp.setWorstTrade(unit.getWorstTrade());
//            sp.setTotalWin(unit.getTotalWin());
//            sp.setTotalCount(unit.getTotalSignal());
//            sp.setTotalTrade(unit.getTotalTrade());
//            sp.setWinRate(unit.getWinRate());
//            this.results.add(sp);
//        }
//    }
//}
