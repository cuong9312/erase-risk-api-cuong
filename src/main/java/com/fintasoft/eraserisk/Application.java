package com.fintasoft.eraserisk;

import com.fintasoft.eraserisk.configurations.AppConf;
import org.apache.camel.spring.boot.CamelSpringBootApplicationController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.UUID;

/**
 * Endpoint class to run by spring-boot
 */
@SpringBootApplication
@EnableConfigurationProperties({ AppConf.class })
@EnableScheduling
@EnableAsync
public class Application {
    public static ApplicationContext applicationContext;
    private static String INSTANCE_ID;
    public  static String getInstanceId() {
        return INSTANCE_ID;
    }
	
    public static void main(String[] args) {
	    INSTANCE_ID = "api_" + UUID.randomUUID().toString();
        applicationContext = new SpringApplication(Application.class).run(args);
        CamelSpringBootApplicationController applicationController =
                applicationContext.getBean(CamelSpringBootApplicationController.class);
        applicationController.run();
    }
}
