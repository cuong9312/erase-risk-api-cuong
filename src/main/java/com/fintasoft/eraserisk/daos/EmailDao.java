package com.fintasoft.eraserisk.daos;

import com.fintasoft.eraserisk.configurations.AppConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import javax.mail.BodyPart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;

@Repository
public class EmailDao {
    @Autowired
    AppConf appConf;
    private static final Logger log = LoggerFactory.getLogger(EmailDao.class);


    String encoding = "UTF8";

    @Async
    public CompletableFuture sendEmailAsync(String to, String subject, String content) {
        sendEmail(to, subject, content);
        return new CompletableFuture();
    }

    public void sendEmail(String to, String subject, String content) {
        JavaMailSenderImpl javaMailSender = getMailSender();

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = null;

        try {

            BodyPart messageBodyPart = new MimeBodyPart();

            helper = new MimeMessageHelper(mimeMessage, false, encoding);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setFrom(appConf.getEmail().getSender());

            messageBodyPart.setText(content);
            mimeMessage.setText(content, encoding, "html");

            javaMailSender.send(helper.getMimeMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException("E-mail could not be sent to user ");
        }
    }

    @Async
    public void sendEmailAsync(List<String> to, String subject, String content) {
        JavaMailSenderImpl javaMailSender = getMailSender();

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = null;

        try {
            String[] recipients = to.toArray(new String[0]);
            BodyPart messageBodyPart = new MimeBodyPart();
            helper = new MimeMessageHelper(mimeMessage, false, encoding);
            helper.setTo(appConf.getEmail().getSupport());
            helper.setBcc(recipients);
            helper.setSubject(subject);
            helper.setFrom(appConf.getEmail().getSender());

            messageBodyPart.setText(content);
            mimeMessage.setText(content, encoding, "html");

            javaMailSender.send(helper.getMimeMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException("E-mail could not be sent to user ");
        }
    }

    private JavaMailSenderImpl getMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(appConf.getEmail().getEndpoint());
        mailSender.setPort(appConf.getEmail().getPort());
//        mailSender.setDefaultEncoding(mailConfiguration.getEncoding());
        mailSender.setUsername(appConf.getEmail().getSmtpUsername());
        mailSender.setPassword(appConf.getEmail().getSmtpPassword());

        Properties prop = mailSender.getJavaMailProperties();
        prop.put("mail.transport.protocol", "smtp");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.ssl.trust", appConf.getEmail().getEndpoint());
//        prop.put("mail.debug", "true");

        return mailSender;
    }
}
