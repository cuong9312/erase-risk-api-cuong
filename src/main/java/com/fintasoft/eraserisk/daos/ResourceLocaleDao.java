package com.fintasoft.eraserisk.daos;

import com.fintasoft.eraserisk.configurations.FmConfiguration;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Locale;
import java.util.Map;

/**
 * this class contains method to load resource file based on locale
 */
@Repository
public class ResourceLocaleDao {
    @Autowired
    private FmConfiguration fmConfiguration;

    @Autowired
    private HttpServletRequest request;

    public String getTextFileResource(String resourceName, Map<String, Object> context) throws IOException, TemplateException {
        Locale locale = Locale.forLanguageTag(request.getHeader("Accept-Language"));
        if (locale == null || StringUtils.isEmpty(locale.getDisplayLanguage())) {
            locale = LocaleContextHolder.getLocale();
        }
        if (StringUtils.isEmpty(locale.getDisplayLanguage())) {
            locale = Locale.getDefault();
        }
        return getTextFileResource(resourceName, context, locale);
    }

    public String getTextFileResource(String resourceName, Map<String, Object> context, Locale locale) throws IOException, TemplateException {
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        Writer bos = new OutputStreamWriter(bs);
        fmConfiguration.fmConfiguration().getTemplate(resourceName, locale).process(context, bos);
        return bs.toString();
    }

}
