package com.fintasoft.eraserisk.annotations;

import org.springframework.transaction.annotation.Propagation;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD,ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@org.springframework.transaction.annotation.Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
@Documented
public @interface NewTransactional {
}
