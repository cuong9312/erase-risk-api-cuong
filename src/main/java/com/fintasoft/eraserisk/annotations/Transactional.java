package com.fintasoft.eraserisk.annotations;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD,ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@org.springframework.transaction.annotation.Transactional(rollbackFor = Exception.class)
@Documented
public @interface Transactional {
}
