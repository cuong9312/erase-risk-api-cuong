package com.fintasoft.eraserisk.cli;


import sun.net.www.protocol.http.HttpURLConnection;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class CliApplication {
    private static final String USER_AGENT = "eEraseRisk/1.0";
    private static final String BASE_PATH = "http://localhost:8080/api/v1/internal/jobs/";
//    private static final String BASE_PATH = "http://13.124.2.212:8080/api/v1/internal/jobs/";

    public static void main(String[] args) throws Exception {
        String command = args[0];
        if ("strategy-order".equalsIgnoreCase(command)) {
            sendPost(BASE_PATH + "strategy-pos");
        } else if ("daily-profit".equalsIgnoreCase(command)) {
            sendPost(BASE_PATH + "daily-profit");
        }
    }

    // HTTP POST request
    private static void sendPost(String url) throws Exception {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes("");
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        System.out.println(response.toString());

    }
}
