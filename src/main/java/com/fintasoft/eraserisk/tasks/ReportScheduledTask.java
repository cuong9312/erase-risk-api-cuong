package com.fintasoft.eraserisk.tasks;


import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.ReportStatusEnum;
import com.fintasoft.eraserisk.repositories.impl.ReportRepository;
import com.fintasoft.eraserisk.services.queue.RequestResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fintasoft.eraserisk.model.api.response.ReportRequestResponse;
import com.fintasoft.eraserisk.model.db.ReportRequest;
import com.fintasoft.eraserisk.model.query.RevenueByDay;
import com.fintasoft.eraserisk.model.query.RevenueByMonth;
import com.fintasoft.eraserisk.model.query.TradeReport;
import com.fintasoft.eraserisk.repositories.ReportRequestRepository;
import com.fintasoft.eraserisk.services.ReportService;



/*
*****STORE PROCEDURE*****

DROP PROCEDURE IF EXISTS `query_report`;
DELIMITER //

CREATE PROCEDURE query_report (OUT reportId INT)
BEGIN
    DECLARE reportId INT;
    DECLARE bDone INT;
    DECLARE curs CURSOR FOR  select r.id as id from report_request as r where r.status = 'PENDING' order by id desc limit 1;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;
    OPEN curs;
    SET bDone = 0;
    REPEAT
        FETCH curs INTO reportId;
        Update report_request SET status = 'IN_PROGRESS' where id = reportId;
    UNTIL bDone END REPEAT;
    SELECT reportId;
    CLOSE curs;
END//


DELIMITER ;

*************************
 */


@Component
public class ReportScheduledTask {
	private static final Logger log = LoggerFactory.getLogger(ReportScheduledTask.class);

	@Value("${app.report.max_thread:5}")
	private int maxThread;

	@Autowired
	ReportRequestRepository reportRepository;
	@Autowired
	ReportRepository reportRepositoryImpl;
	
	@Autowired
	ReportService reportService;

	@Autowired
	RequestResponseHandler queueHandler;

	@Autowired
	AppConf appConf;

	private Short usingSlot = 0;

	@Scheduled(fixedRate = 5000) // every 5 seconds
	public void scanReport() {
		System.out.println(String.format("report: %d", this.usingSlot));
		synchronized (this.usingSlot) {
			if (usingSlot >= maxThread) {
				return;
			}
			this.usingSlot++;
		}
		while(true) {
			Long reportId = reportRepositoryImpl.getNextPendingId();
			if (reportId == null) {
				break;
			}
			log.info("start running report {}", reportId);
			ReportRequest reportRequest = reportRepository.findById(reportId).orElse(null);
			if (reportRequest.getStatus() == ReportStatusEnum.COMPLETED || reportRequest.getStatus() == ReportStatusEnum.FAILED) {
				return;
			}
			ReportRequestResponse response;
			try {
				if (RevenueByDay.class.getCanonicalName().equals(reportRequest.getRootClass())
						|| RevenueByMonth.class.getCanonicalName().equals(reportRequest.getRootClass())) {
					response = reportService.generateRevenueReport(reportRequest);
				} else if (TradeReport.class.getCanonicalName().equals(reportRequest.getRootClass())) {
					response = reportService.generateTradeReport(reportRequest);
				} else {
					response = reportService.generate(reportRequest);
				}

				// notify that report is finished
				if (response != null) {
					queueHandler.sendMessageSafe(appConf.getQueue().getTopics().getReportUpdate(), "REPORT_FINISH", response);
				}
				log.info("finish report {}", reportId);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		synchronized (this.usingSlot) {
			this.usingSlot--;
		}
	}
}
