package com.fintasoft.eraserisk.services;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncService {

    @Async
    public void doJobNoReturn(Runnable runnable) {
        runnable.run();
    }
}
