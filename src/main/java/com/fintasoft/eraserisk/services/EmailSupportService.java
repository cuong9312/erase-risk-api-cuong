package com.fintasoft.eraserisk.services;

import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.Constants;
import com.fintasoft.eraserisk.daos.EmailDao;
import com.fintasoft.eraserisk.daos.ResourceLocaleDao;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.exceptions.TargetNotFoundException;
import com.fintasoft.eraserisk.model.api.EmailSupportRequest;
import com.fintasoft.eraserisk.model.api.request.validator.EmailValidator;
import com.fintasoft.eraserisk.model.db.User;
import com.fintasoft.eraserisk.repositories.UserRepository;
import freemarker.template.TemplateException;
import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Service
public class EmailSupportService {
    private static final Logger log = LoggerFactory.getLogger(EmailSupportService.class);

    @Autowired
    EmailDao emailDao;

    @Autowired
    AppConf appConf;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private ResourceLocaleDao resourceLocaleDao;


    public void createEmailSupport(EmailSupportRequest request, Long userId) throws IOException, TemplateException {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new TargetNotFoundException();
        }

        HashMap<String,Object> dataMap = new HashMap<>();
        dataMap.put("userId", user.getId());
        dataMap.put("name", user.getUserInfo().getFullName());
        dataMap.put("phoneNumber", user.getUserInfo().getPhoneNumber());
        dataMap.put("email", user.getEmail());
        dataMap.put("question", request.getContent());

        String content = resourceLocaleDao.getTextFileResource(appConf.getEmailSupport(),dataMap);

        emailDao.sendEmailAsync(
                appConf.getEmail().getSupport(),
                request.getSubject(),
                content
        );
    }

    public void createEmailMarketing(List<String> emails, String content, String subject) {
        EmailValidator emailValidator = new EmailValidator();
        emails.forEach(email -> {
            if (!emailValidator.isValid(email)) {
                throw new InvalidValueException("additionEmails", "error.invalid_format", new String[]{ "additionEmails" });
            }
        });
        List<List<String>> emailsPartitions = ListUtils.partition(emails, Constants.BATCH_SIZE_SEND_EMAIL);
        emailsPartitions.forEach(emailsPartition -> {
            emailDao.sendEmailAsync(emailsPartition, subject, content);
        });
    }
}
