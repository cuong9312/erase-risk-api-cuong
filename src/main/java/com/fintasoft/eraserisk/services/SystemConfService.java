package com.fintasoft.eraserisk.services;

import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.constances.Constants;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.exceptions.TargetNotFoundException;
import com.fintasoft.eraserisk.model.api.request.SystemConfUpdate;
import com.fintasoft.eraserisk.model.api.response.*;
import com.fintasoft.eraserisk.model.db.*;
import com.fintasoft.eraserisk.repositories.*;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SystemConfService {

	@Autowired
	private SystemConfRepository systemConfRepository;

	@Transactional
	public SystemConfResponse findByPk(String name, String lang) {
		List<String> listLang = new ArrayList<>();
		listLang.add(lang);
		if (!lang.equalsIgnoreCase(Constants.SYSTEM_CONFIG_LANG_NONE)) {
			listLang.add(Constants.SYSTEM_CONFIG_LANG_NONE);
		}
		List<SystemConf> listSytemConf = systemConfRepository.findByNameAndLangs(name, listLang);
		if (CollectionUtils.isEmpty(listSytemConf)) {
				throw new TargetNotFoundException();
		}

		SystemConf systemConf = new SystemConf();
		for (SystemConf tempSystemConf : listSytemConf) {
			if (tempSystemConf.getPk().getLang().equalsIgnoreCase(lang)) {
				return SystemConfResponse.from(tempSystemConf);
			}
			if (tempSystemConf.getPk().getLang().equalsIgnoreCase(Constants.SYSTEM_CONFIG_LANG_NONE)) {
				systemConf = tempSystemConf;
			}
		}
		return SystemConfResponse.from(systemConf);
	}

	@Transactional
	public Page<SystemConfResponse> findAll(String name, String lang, Pageable pageable) {
		Page<SystemConf> systemConfs = systemConfRepository.findAll("%"+name+"%", "%"+lang+"%", pageable);
        List<SystemConfResponse> listSystemConfResponse = new ArrayList<>();
        systemConfs.forEach(p -> listSystemConfResponse.add(SystemConfResponse.from(p)));
        return new PageImpl(listSystemConfResponse, systemConfs.getPageable(), systemConfs.getTotalElements());

    }

	@Transactional
	public SystemConfResponse save(SystemConf systemConf) {
		SystemConf systemConfDb = systemConfRepository.findById(systemConf.getPk()).orElse(null);
		if (systemConfDb != null) {
			throw new InvalidValueException("pk", "error.already_in_use", new String[]{ "pk" });
		}
		systemConfRepository.save(systemConf);
		return SystemConfResponse.from(systemConfRepository.save(systemConf));
	}

	@Transactional
	public SystemConfResponse update(String name, String lang, SystemConfUpdate systemConfUpdate) {
		SystemConf systemConfDb = systemConfRepository.findByPk(name, lang);
		if (systemConfDb == null) {
			throw new TargetNotFoundException();
		}
		systemConfDb.setValue(systemConfUpdate.getValue());
		systemConfDb.setDataType(systemConfUpdate.getDataType());
		systemConfDb.setDescription(systemConfUpdate.getDescription());
		return SystemConfResponse.from(systemConfRepository.save(systemConfDb));
	}
}
