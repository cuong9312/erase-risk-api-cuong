package com.fintasoft.eraserisk.services.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fintasoft.eraserisk.model.api.response.AuthorityResponse;
import com.fintasoft.eraserisk.model.api.response.SecuritiesCompanyResponse;
import com.fintasoft.eraserisk.repositories.AuthorityRepository;
import com.fintasoft.eraserisk.repositories.SecuritiesCompanyRepository;

@Service
public class AuthorityService {
    @Autowired
    AuthorityRepository authorityRepository;

    public List<AuthorityResponse> getAll() {
        List<AuthorityResponse> result = new ArrayList<>();
        authorityRepository.findAll().forEach(s -> result.add(AuthorityResponse.from(s)));
        return result;
    }
}
