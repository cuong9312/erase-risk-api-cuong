package com.fintasoft.eraserisk.services.user;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import com.fintasoft.eraserisk.constances.ReportStatusEnum;
import com.fintasoft.eraserisk.constances.RoleEnums;
import com.fintasoft.eraserisk.daos.EmailDao;
import com.fintasoft.eraserisk.daos.ResourceLocaleDao;
import com.fintasoft.eraserisk.exceptions.GeneralException;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.exceptions.TargetNotFoundException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.request.*;
import com.fintasoft.eraserisk.model.api.response.AdminResponse;
import com.fintasoft.eraserisk.model.api.response.UserResponse;
import com.fintasoft.eraserisk.model.db.*;
import com.fintasoft.eraserisk.repositories.*;
import com.fintasoft.eraserisk.repositories.impl.QueryParamRepository;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import freemarker.template.TemplateException;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Stream;

@Service
public class UserService {

    @Autowired
    private AppConf appConf;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QueryParamRepository queryParamRepository;

    @Autowired
    private SecuritiesCompanyRepository securitiesCompanyRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private StrategyProviderRepository strategyProviderRepository;
    
    @Autowired
    private AuthorityRepository authorityRepository;
    
    @Autowired
    private ReportRequestRepository reportRequestRepository;

    @Autowired
    private ResourceLocaleDao resourceLocaleDao;

    @Autowired
    private EmailDao emailDao;

    private final String separator = ":::";

    public UserResponse getUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new TargetNotFoundException();
        }

        return UserResponse.fromCustomer(user);
    }

    public UserResponse getUserByUserId(Long id, boolean isAdmin) {
        User user = userRepository.findById(id).orElse(null);
        if (user == null) {
            throw new TargetNotFoundException();
        }
        if (isAdmin) {
        	return UserResponse.fromAdmin(user);
        } else {
        	return UserResponse.fromCustomer(user);
        }
    }

    public Page<UserResponse> getAllUser(Pageable pageable) {
    	Page<User> users = userRepository.findAll(pageable);    	
        List<UserResponse> list = new ArrayList<>();
    	users.getContent().forEach(up -> list.add(UserResponse.fromCustomer(up)));
        return new PageImpl<>(list, users.getPageable(), users.getTotalElements());
    }

    public Long exportUsers(List<QueryParam> params, boolean isAdmin, String createdByUserId)
            throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(params);
        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setSqlParam(json);
        reportRequest.setCreatedByUserId(createdByUserId);
        reportRequest.setRootClass(User.class.getCanonicalName());

        reportRequest.setStatus(ReportStatusEnum.PENDING);

        Map<String, String> exportFields = new LinkedHashMap<String, String>();
        if (isAdmin) {
            reportRequest.setResponseClass(AdminResponse.class.getCanonicalName());
            exportFields.put("fullName", "Full Name");
            exportFields.put("username", "Username");
            exportFields.put("secNames", "Partner");
            exportFields.put("providerNames", "Strategy Provider");
            exportFields.put("phoneNumber", "Phone Number");
            exportFields.put("cellNumber", "Cell Number");
            exportFields.put("createdAt", "Registration Date");
        } else {
            reportRequest.setResponseClass(UserResponse.class.getCanonicalName());
            exportFields.put("username", "Username");
            exportFields.put("email", "Email");
            exportFields.put("phoneNumber", "Phone Number");
            exportFields.put("createdAt", "Join Date");
            exportFields.put("secNames", "Partner");
        }

        reportRequest.setExportField(mapper.writeValueAsString(exportFields));
        reportRequestRepository.save(reportRequest);

        return reportRequest.getId();
    }

    public Page<UserResponse> findUsers(List<QueryParam> params, Pageable pageable, boolean isAdmin)
            throws ParseException, JsonProcessingException {
        Page<User> users = queryParamRepository.find(params, pageable, User.class);
        List<UserResponse> list = new ArrayList<>();
        if (isAdmin){
            users.getContent().forEach(up -> list.add(UserResponse.fromAdmin(up)));
        } else {
            users.getContent().forEach(up -> list.add(UserResponse.fromCustomer(up)));
        }

        return new PageImpl(list, users.getPageable(), users.getTotalElements());
    }
    public List<String> findEmails(List<QueryParam> params) throws ParseException {
        Stream<User> users = queryParamRepository.find(params, User.class);
        List<String> emails = new ArrayList<>();
        users.forEach(user -> emails.add(user.getEmail()));
        return emails;
    }

    @com.fintasoft.eraserisk.annotations.Transactional
    public UserResponse registerUser(UserRegister userRegister) throws IOException, TemplateException {
        if (userRepository.findByUsername(userRegister.getEmail()) != null) {
            throw new InvalidValueException("email", "error.already_in_use", new String[]{ "Email" });
        }
        
        if (userRepository.findByUserInfoPhoneNumber(userRegister.getPhoneNumber()) != null) {
            throw new InvalidValueException("phoneNumber", "error.already_in_use", new String[]{ "Phone Number" });
        }
        
        byte[] b = new byte[32];
        new Random().nextBytes(b);
        UserInfo userInfo = new UserInfo();
        Date parsedDate = ConvertUtils.toDate(userRegister.getBirthday(), "birthday");
        if (parsedDate != null) {
        	userInfo.setBirthday(new java.sql.Date(parsedDate.getTime()));
        }
        
        userInfo.setFullName(userRegister.getFullName());
        userInfo.setPhoneNumber(userRegister.getPhoneNumber());
        if (userRegister.getEmailNotification() != null) {
            userInfo.setEmailNotification(userRegister.getEmailNotification());
        }
        if (userRegister.getMobileNotification() != null) {
            userInfo.setMobileNotification(userRegister.getMobileNotification());
        }

        List<SecuritiesCompany> securitiesCompanies = new ArrayList<>();
        if (userRegister.getSecIds() != null) {
            for (int i = 0; i < userRegister.getSecIds().length; i++) {
                SecuritiesCompany securitiesCompany = securitiesCompanyRepository.findById(userRegister.getSecIds()[i]).orElse(null);
                if (securitiesCompany != null) {
                    securitiesCompanies.add(securitiesCompany);
                } else {
                    throw new InvalidValueException("secIds[" + i + "]");
                }
            }
        }
        
        List<Role> roles = new ArrayList<>();
        
        Role customerRole = roleRepository.findByRoleName(RoleEnums.CUSTOMER.toString());
        if (customerRole!=null){
        	roles.add(customerRole);
        }

        User user = User.builder()
                .username(userRegister.getEmail())
                .email(userRegister.getEmail())
                .passwordHash(BCrypt.hashpw(userRegister.getPassword(), BCrypt.gensalt()))
                .authKey(Base64.getUrlEncoder().encodeToString(b).substring(0, 32))
                .status(User.USER_STATUS__INACTIVE)
                .userInfo(userInfo)
                .securitiesCompanies(securitiesCompanies)
                .roles(roles)
                .verifyEmailToken(createUserToken(userRegister.getEmail()))
                .build();
        userInfo.setUser(user);
            userRepository.save(user);

        String confirmUrl = String.format("%s%s%s", appConf.getFrontend().getBasePath(), appConf.getFrontend().getVerifyEmail(), user.getVerifyEmailToken());
        // sending email
        emailDao.sendEmailAsync(
                user.getEmail(),
                "Email Confirmation",
                resourceLocaleDao.getTextFileResource(appConf.getEmailVerificationTemplate(),
                        new HashMap<String, Object>(){{put("confirmLink", confirmUrl);}})
        );
        return UserResponse.fromCustomer(user);
    }

    @com.fintasoft.eraserisk.annotations.Transactional
    public UserResponse verifyUserEmail(String token) {
        String combine = null;
        try {
            combine = new String(Base64.getDecoder().decode(token.getBytes()));
        } catch (Exception e) {
            throw new InvalidValueException("token");
        }

        if (!combine.contains(separator)) {
            throw new InvalidValueException("token");
        }

        String[] parts = combine.split(separator);
        String email = parts[0];
        List<User> users = userRepository.findByVerifyEmailToken(token);
        User matched = null;
        for (User user : users) {
            if (email.equals(user.getEmail())) {
                matched = user;
                break;
            }
        }

        if (matched == null) {
            throw new InvalidValueException("token");
        }

        matched.setStatus(User.USER_STATUS__ACTIVE);
        matched.setVerifyEmailToken(null);

        return UserResponse.fromCustomer(matched);
    }

    public String createUserToken(String key) {
        return createUserToken(key, false);
    }

    public String createUserToken(String key, boolean withTime) {
        String uuid = UUID.randomUUID().toString();
        String combine = key + (withTime ? separator + new Date().getTime() : "") + separator + uuid;
        return new String(Base64.getEncoder().encode(combine.getBytes()));
    }

    public UserResponse loginUser(Login login) {
        User user = userRepository.findByUsername(login.getUsername());
        if (user == null) {
            throw new GeneralException(ErrorCodeEnums.LOGIN_FAILED.name(), "error.login_failed", null);
        }

        if (user.getStatus() == User.USER_STATUS__INACTIVE) {
            throw new InvalidValueException("username", "error.account_inactive", null);
        }


        if (!BCrypt.checkpw(login.getPassword(), user.getPasswordHash())) {
            throw new GeneralException(ErrorCodeEnums.LOGIN_FAILED.name(), "error.login_failed", null);
        }

        return UserResponse.fromCustomer(user);
    }
    
    public UserResponse loginAdmin(Login login) {
        User user = userRepository.findByUsername(login.getUsername());
        if (user == null) {
            throw new GeneralException(ErrorCodeEnums.LOGIN_FAILED.name(), "error.login_failed", null);
        }

        if (user.getStatus() == User.USER_STATUS__INACTIVE) {
            throw new InvalidValueException("username", "error.account_inactive", null);
        }


        if (!BCrypt.checkpw(login.getPassword(), user.getPasswordHash())) {
            throw new GeneralException(ErrorCodeEnums.LOGIN_FAILED.name(), "error.login_failed", null);
        }
        
        boolean isAdmin = false;
        
        List<Role> roles = user.getRoles();
        
        for (int i = 0; i < roles.size(); i++) {
			if (roles.get(i).getRoleName().equals(RoleEnums.ADMIN.toString())){
				isAdmin = true;
				break;
			}
		}
        
        if (!isAdmin){
        	throw new GeneralException(ErrorCodeEnums.LOGIN_FAILED.name(), "error.login_role_failed", null);
        }

        return UserResponse.fromAdmin(user);
    }
    
    @com.fintasoft.eraserisk.annotations.Transactional
    public UserResponse updateUser(long userId, UserUpdate userUpdate, boolean isAdmin) {
    	User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new TargetNotFoundException();
        }
        
        if (!isAdmin && !BCrypt.checkpw(userUpdate.getPassword(), user.getPasswordHash())) {
            throw new InvalidValueException(ErrorCodeEnums.LOGIN_FAILED.name(), "error.wrong_password", null);
        }
        
        if (userRepository.findByUserInfoPhoneNumberAndIdNot(userUpdate.getPhoneNumber(),  userId) != null) {
            throw new InvalidValueException("phoneNumber", "error.already_in_use", new String[]{ "Phone Number" });
        }
        
        UserInfo userInfo = user.getUserInfo();
        Date parsedDate = ConvertUtils.toDate(userUpdate.getBirthday(), "birthday");
        if (parsedDate != null) {
        	userInfo.setBirthday(new java.sql.Date(parsedDate.getTime()));
        }

        userInfo.setFullName(userUpdate.getFullName());
        userInfo.setPhoneNumber(userUpdate.getPhoneNumber());
        if (userUpdate.getEmailNotification() != null) {
            userInfo.setEmailNotification(userUpdate.getEmailNotification());
        }
        if (userUpdate.getMobileNotification() != null) {
            userInfo.setMobileNotification(userUpdate.getMobileNotification());
        }
        
        if (isAdmin && userUpdate.getStatus() != null){
        	user.setStatus(userUpdate.getStatus());
        }

        List<SecuritiesCompany> securitiesCompanies = new ArrayList<>();
        if (userUpdate.getSecIds() != null) {
            for (int i = 0; i < userUpdate.getSecIds().length; i++) {
                SecuritiesCompany securitiesCompany = securitiesCompanyRepository.findById(userUpdate.getSecIds()[i]).orElse(null);
                if (securitiesCompany != null) {
                    securitiesCompanies.add(securitiesCompany);
                } else {
                    throw new InvalidValueException("secIds[" + i + "]");
                }
            }
        }

        user.setUserInfo(userInfo);
        user.setSecuritiesCompanies(securitiesCompanies);
        userInfo.setUser(user);
        userRepository.save(user);
        return UserResponse.fromCustomer(user);
    }
    
    public UserResponse updatePassword(long userId, UpdatePassword updatePassword) {
        return this.updatePassword(userId, updatePassword, true);
    }

    @com.fintasoft.eraserisk.annotations.Transactional
    public UserResponse updatePassword(long userId, UpdatePassword updatePassword, boolean checkOld) {
    	User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new TargetNotFoundException();
        }

        if (updatePassword instanceof EmailResetPasswordRequest) {
            if (!user.getResetPasswordToken().equals(((EmailResetPasswordRequest) updatePassword).getToken())) {
                throw new InvalidValueException("token");
            }
        }
        
        if (checkOld && !BCrypt.checkpw(updatePassword.getOldPassword(), user.getPasswordHash())) {
            throw new InvalidValueException(ErrorCodeEnums.LOGIN_FAILED.name(), "error.wrong_password", null);
        }
        
        
        byte[] b = new byte[32];
        new Random().nextBytes(b);
        
        user.setPasswordHash(BCrypt.hashpw(updatePassword.getNewPassword(), BCrypt.gensalt()));
        
        userRepository.save(user);
        return UserResponse.fromCustomer(user);
    }

    @com.fintasoft.eraserisk.annotations.Transactional
    public UserResponse registerAdmin(AdminRegister adminRegister) {
        if (userRepository.findByEmail(adminRegister.getEmail()) != null) {
            throw new InvalidValueException("email", "error.already_in_use", new String[]{ "Email" });
        }
        
        if (userRepository.findByUsername(adminRegister.getUsername()) != null) {
            throw new InvalidValueException("username", "error.already_in_use", new String[]{ "Username" });
        }
        
        if (userRepository.findByUserInfoPhoneNumber(adminRegister.getPhoneNumber()) != null) {
            throw new InvalidValueException("phoneNumber", "error.already_in_use", new String[]{ "Phone Number" });
        }
        
        byte[] b = new byte[32];
        new Random().nextBytes(b);
        AdminInfo adminInfo = new AdminInfo();
        
        adminInfo.setFullName(adminRegister.getFullName());
        adminInfo.setPhoneNumber(adminRegister.getPhoneNumber());
        adminInfo.setCellNumber(adminRegister.getCellNumber());
        

        List<SecuritiesCompany> securitiesCompanies = new ArrayList<>();
        if (adminRegister.getSecIds() != null) {
            for (int i = 0; i < adminRegister.getSecIds().length; i++) {
                SecuritiesCompany securitiesCompany = securitiesCompanyRepository.findById(adminRegister.getSecIds()[i]).orElse(null);
                if (securitiesCompany != null) {
                    securitiesCompanies.add(securitiesCompany);
                } else {
                    throw new InvalidValueException("secIds[" + i + "]");
                }
            }
        }
        
        List<StrategyProvider> strategyProviders = new ArrayList<>();
        if (adminRegister.getProviderIds() != null) {
            for (int i = 0; i < adminRegister.getProviderIds().length; i++) {
                StrategyProvider strategyProvider = strategyProviderRepository.findById(adminRegister.getProviderIds()[i]).orElse(null);
                if (strategyProvider != null) {
                	strategyProviders.add(strategyProvider);
                } else {
                    throw new InvalidValueException("providerIds[" + i + "]");
                }
            }
        }
        
        List<Authority> authorities = new ArrayList<>();
        if (adminRegister.getAuthIds() != null) {
            for (int i = 0; i < adminRegister.getAuthIds().length; i++) {
            	Authority authority = authorityRepository.findById(adminRegister.getAuthIds()[i]).orElse(null);
                if (authority != null) {
                	authorities.add(authority);
                } else {
                    throw new InvalidValueException("authIds[" + i + "]");
                }
            }
        }
        
        List<Role> roles = new ArrayList<>();
        
        Role adminRole = roleRepository.findByRoleName(RoleEnums.ADMIN.toString());
        if (adminRole!=null){
        	roles.add(adminRole);
        }

        User user = User.builder()
                .username(adminRegister.getUsername())
                .email(adminRegister.getEmail())
                .passwordHash(BCrypt.hashpw(adminRegister.getPassword(), BCrypt.gensalt()))
                .authKey(Base64.getUrlEncoder().encodeToString(b).substring(0, 32))
                .status(User.USER_STATUS__ACTIVE)
                .adminInfo(adminInfo)
                .adminSecuritiesCompanies(securitiesCompanies)
                .adminStrategyProviders(strategyProviders)
                .authorities(authorities)
                .roles(roles)
                .build();
        adminInfo.setUser(user);
        userRepository.save(user);
        return UserResponse.fromAdmin(user);
    }
    
    @com.fintasoft.eraserisk.annotations.Transactional
    public UserResponse updateAdmin(long userId, AdminUpdate adminUpdate, boolean isAdmin) {
    	User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new TargetNotFoundException();
        }
        
        if (!isAdmin && !BCrypt.checkpw(adminUpdate.getPassword(), user.getPasswordHash())) {
            throw new InvalidValueException(ErrorCodeEnums.LOGIN_FAILED.name(), "error.wrong_password", null);
        }
        
        if (userRepository.findByUserInfoPhoneNumberAndIdNot(adminUpdate.getPhoneNumber(),  userId) != null) {
            throw new InvalidValueException("phoneNumber", "error.already_in_use", new String[]{ "Phone Number" });
        }
        
        AdminInfo adminInfo = user.getAdminInfo();
        adminInfo.setFullName(adminUpdate.getFullName());
        adminInfo.setPhoneNumber(adminUpdate.getPhoneNumber());
        adminInfo.setCellNumber(adminUpdate.getCellNumber());
        
        if (isAdmin && adminUpdate.getStatus() != null){
        	user.setStatus(adminUpdate.getStatus());
        }
        
        if (isAdmin && adminUpdate.getPassword()!=null){
        	user.setPasswordHash(BCrypt.hashpw(adminUpdate.getPassword(), BCrypt.gensalt()));
        }
        
        user.setEmail(adminUpdate.getEmail());
        
        if (isAdmin) {
	        List<SecuritiesCompany> securitiesCompanies = new ArrayList<>();
	        if (adminUpdate.getSecIds() != null) {
	            for (int i = 0; i < adminUpdate.getSecIds().length; i++) {
	                SecuritiesCompany securitiesCompany = securitiesCompanyRepository.findById(adminUpdate.getSecIds()[i]).orElse(null);
	                if (securitiesCompany != null) {
	                    securitiesCompanies.add(securitiesCompany);
	                } else {
	                    throw new InvalidValueException("secIds[" + i + "]");
	                }
	            }
	        }
	        
	        List<StrategyProvider> strategyProviders = new ArrayList<>();
	        if (adminUpdate.getProviderIds() != null) {
	            for (int i = 0; i < adminUpdate.getProviderIds().length; i++) {
	                StrategyProvider strategyProvider = strategyProviderRepository.findById(adminUpdate.getProviderIds()[i]).orElse(null);
	                if (strategyProvider != null) {
	                	strategyProviders.add(strategyProvider);
	                } else {
	                    throw new InvalidValueException("providerIds[" + i + "]");
	                }
	            }
	        }
	        
	        List<Authority> authorities = new ArrayList<>();
	        if (adminUpdate.getAuthIds() != null) {
	            for (int i = 0; i < adminUpdate.getAuthIds().length; i++) {
	            	Authority authority = authorityRepository.findById(adminUpdate.getAuthIds()[i]).orElse(null);
	                if (authority != null) {
	                	authorities.add(authority);
	                } else {
	                    throw new InvalidValueException("authIds[" + i + "]");
	                }
	            }
	        }
	        
	        user.setAdminSecuritiesCompanies(securitiesCompanies);
	        user.setAuthorities(authorities);
	        user.setAdminStrategyProviders(strategyProviders);
        }

        user.setAdminInfo(adminInfo);
        
        adminInfo.setUser(user);
        userRepository.save(user);
        return UserResponse.fromAdmin(user);
    }

    public UserResponse findByUserName(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new InvalidValueException("username");
        }

        return UserResponse.fromCustomer(user);
    }

    public UserResponse findByResetPasswordToken(String token) {
        User user = userRepository.findByResetPasswordToken(token);
        if (user == null) {
            throw new InvalidValueException("token");
        }

        return UserResponse.fromCustomer(user);
    }

    @com.fintasoft.eraserisk.annotations.Transactional
    public void sendResetPasswordViaEmail(Long userId) throws IOException, TemplateException {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new InvalidValueException("userId");
        }

        user.setResetPasswordToken(createUserToken(user.getEmail(), true));
        String resetPasswordUrl = String.format("%s%s%s", appConf.getFrontend().getBasePath(), appConf.getFrontend().getResetPassword(), user.getResetPasswordToken());

        emailDao.sendEmailAsync(
                user.getEmail(),
                "Reset Password",
                resourceLocaleDao.getTextFileResource(appConf.getResetPasswordTemplate(),
                        new HashMap<String, Object>(){{put("resetPasswordUrl", resetPasswordUrl);}})
        );
    }
}
