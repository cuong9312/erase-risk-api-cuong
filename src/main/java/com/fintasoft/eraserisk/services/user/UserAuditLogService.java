package com.fintasoft.eraserisk.services.user;

import com.fintasoft.eraserisk.model.db.UserAuditLog;
import com.fintasoft.eraserisk.repositories.UserAuditLogRepository;
import com.fintasoft.eraserisk.utils.ConvertUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;


import java.sql.Timestamp;

@Service
public class UserAuditLogService {

    @Autowired
    private UserAuditLogRepository userAuditLogRepository;

    @com.fintasoft.eraserisk.annotations.Transactional
    public String securityLoginUpdate(long userId, long secId) {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        Page<UserAuditLog> lastestLogs = userAuditLogRepository.findByUserIdAndSecIdOrderByLoginTimeDesc(userId, secId, PageRequest.of(0, 1));
        if (!CollectionUtils.isEmpty(lastestLogs.getContent()) && lastestLogs.getContent().get(0).getLogoutTime() == null) {
            lastestLogs.getContent().get(0).setLogoutTime(now);
        }

        UserAuditLog userAuditLog = new UserAuditLog();
        userAuditLog.setUserId(userId);
        userAuditLog.setSecId(secId);
        userAuditLog.setLoginTime(now);
        userAuditLogRepository.save(userAuditLog);
        
        if (!CollectionUtils.isEmpty(lastestLogs.getContent())) {
        	return ConvertUtils.fromTime(lastestLogs.getContent().get(0).getLoginTime());
        } else {
        	return "";
        }
        
    }

    @com.fintasoft.eraserisk.annotations.Transactional
    public void securityLogoutUpdate(long userId, long secId) {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        Page<UserAuditLog> lastestLogs = userAuditLogRepository.findByUserIdAndSecIdOrderByLoginTimeDesc(userId, secId, PageRequest.of(0, 1));
        if (!CollectionUtils.isEmpty(lastestLogs.getContent()) && lastestLogs.getContent().get(0).getLogoutTime() == null) {
            lastestLogs.getContent().get(0).setLogoutTime(now);
        }
    }
}
