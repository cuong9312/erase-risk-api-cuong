package com.fintasoft.eraserisk.services.queue;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.configurations.RouteConfiguration;
import com.fintasoft.eraserisk.model.kafka.Message;
import com.fintasoft.eraserisk.model.kafka.MessageTypeEnum;
import com.fintasoft.eraserisk.model.kafka.ResponseDestination;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rx.subjects.AsyncSubject;
import rx.subjects.Subject;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class RequestResponseHandler implements Processor {
    public static final Logger log = LoggerFactory.getLogger(RequestResponseHandler.class);
    public static final String SOURCE_ID = "API";
    private ConcurrentHashMap<String, Subject> pendingRequests = new ConcurrentHashMap<>();

    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    RouteConfiguration routeConfiguration;

    @Autowired
    AppConf appConf;

    @Override
    public void process(Exchange exchange) throws Exception {
        Message message = exchange.getIn().getBody(Message.class);
        if (message.getMessageType() == MessageTypeEnum.RESPONSE) {
            if (this.pendingRequests.containsKey(message.getMessageId())) {
                Subject subject = this.pendingRequests.get(message.getMessageId());
                subject.onNext(message.getData());
                subject.onCompleted();
            }
        }
    }

    public Subject<?, ?> sendRequestSafe(String topic, String uri, Object content) {
        try {
            return this.sendRequest(topic, uri, content);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public Subject<?, ?> sendRequest(String topic, String uri, Object content) throws JsonProcessingException {
        AsyncSubject subject = AsyncSubject.create();
        Message message = createMessage(topic, MessageTypeEnum.REQUEST, uri, content);
        pendingRequests.put(message.getMessageId(), subject);
        producerTemplate.asyncSendBody(getResponseTopic(topic), objectMapper.writeValueAsString(message));
        return subject;
    }

    public void sendRequestNoResponse(String topic, String uri, Object content) throws JsonProcessingException {
        Message message = createMessage(topic, MessageTypeEnum.REQUEST, uri, content, false);
        producerTemplate.asyncSendBody(getResponseTopic(topic), objectMapper.writeValueAsString(message));
    }

    public void sendRequestNoResponseSafe(String topic, String uri, Object content) {
        try {
            this.sendRequestNoResponse(topic, uri, content);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
    }

    public Message sendMessageSafe(String topic, String uri, Object content) {
        try {
            return sendMessage(topic, uri, content);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public Message sendMessage(String topic, String uri, Object content) throws JsonProcessingException {
        Message message = createMessage(topic, MessageTypeEnum.MESSAGE, uri, content);
        producerTemplate.asyncSendBody(getResponseTopic(topic), objectMapper.writeValueAsString(message));
        return message;
    }

    private Message createMessage(String topic, MessageTypeEnum type, String uri, Object content) {
        return createMessage(topic, type, uri, content, true);
    }

    private Message createMessage(String topic, MessageTypeEnum type, String uri, Object content, boolean requireResponse) {
        Message message = new Message();
        message.setData(content);
        message.setUri(uri);
        message.setMessageType(type);
        message.setSourceId(SOURCE_ID);
        if (requireResponse) {
            message.setResponseDestination(new ResponseDestination(routeConfiguration.getResponseListenerTopic(), uri));
        }
        message.setMessageId(UUID.randomUUID().toString());
        return message;
    }

    public String getResponseTopic(String topic) {
        return appConf.getQueue().getCommonSendingOut().replace("{topic}", topic);
    }

}
