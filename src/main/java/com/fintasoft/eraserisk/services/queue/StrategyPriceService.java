package com.fintasoft.eraserisk.services.queue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.model.api.message.PriceUpdate;
import com.fintasoft.eraserisk.model.kafka.Message;
import com.fintasoft.eraserisk.model.kafka.MessageTypeEnum;
import com.fintasoft.eraserisk.repositories.StrategyLastPriceRepository;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Controller
public class StrategyPriceService implements Processor {
    private static final Logger log = LoggerFactory.getLogger(StrategyPriceService.class);
    private ConcurrentHashMap<Long, Double> priceMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Long, Long> priceUpdateMap = new ConcurrentHashMap<>();

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    RequestResponseHandler requestResponseHandler;

    @Autowired
    StrategyLastPriceRepository strategyLastPriceRepository;

    @Autowired
    AppConf appConf;

    @PostConstruct
    public void initData() {
        strategyLastPriceRepository.findAll().forEach(s -> {
            if (!priceMap.containsKey(s.getStrategyId())) {
                priceMap.put(s.getStrategyId(), s.getPrice());
            }
            if (!priceUpdateMap.containsKey(s.getStrategyId())) {
                priceUpdateMap.put(s.getStrategyId(), s.getUpdatedAt().getTime());
            }
        });
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        Message message = exchange.getIn().getBody(Message.class);
        if (message.getMessageType() == MessageTypeEnum.MESSAGE) {
            try {
                if (message.getUri().equals("UPDATE-PRICE")) {
                    List<PriceUpdate> response = message.getData(objectMapper, new TypeReference<List<PriceUpdate>>(){});
                    Long time = System.currentTimeMillis();
                    response.forEach(p -> {
                        priceMap.put(p.getId(), p.getPrice());
                        priceUpdateMap.put(p.getId(), time);
                    });
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    public ConcurrentHashMap<Long, Double> getPriceMap() {
        return priceMap;
    }

    public ConcurrentHashMap<Long, Long> getPriceUpdateMap() {
        return priceUpdateMap;
    }

    public void init() throws JsonProcessingException {
        // send request init data
        requestResponseHandler.sendRequest(appConf.getQueue().getTopics().getLiveData(), "STRATEGIES", null);
    }

    public Double getPrice(Long strategyId) {
        return priceMap.get(strategyId);
    }

    public Timestamp getUpdatedTime(Long strategyId) {
        Long time = priceUpdateMap.get(strategyId);
        return time == null ? null : new Timestamp(time);
    }
}
