package com.fintasoft.eraserisk.services.order;

import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import com.fintasoft.eraserisk.constances.PaymentMethodEnum;
import com.fintasoft.eraserisk.constances.PaymentStatusEnum;
import com.fintasoft.eraserisk.exceptions.GeneralException;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.model.api.request.CheckoutFeedback;
import com.fintasoft.eraserisk.model.api.request.CheckoutRequest;
import com.fintasoft.eraserisk.model.api.response.UserPurchasePaymentResponse;
import com.fintasoft.eraserisk.model.db.*;
import com.fintasoft.eraserisk.repositories.*;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class CheckoutService {
	private static final Logger log = LoggerFactory.getLogger(CheckoutService.class);

	@Autowired
	private CartRepository cartRepository;
	
	@Autowired
	private PaymentMethodRepository paymentMethodRepository;
	
	@Autowired
	private UserPurchasePaymentRepository userPurchasePaymentRepository;
	
	@Autowired
	private UserPurchaseHistoryRepository userPurchaseHistoryRepository;
	
	@Autowired
	private StrategySecuritiesCompanyRepository sscRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private StrategyRepository strategyRepository;

	@Autowired
	private AppConf appConf;

	@Autowired
	private UserStrategySettingRepository spsRepository;

	@Autowired
	private UserPurchaseHistoryService userPurchaseHistoryService;

	private final String USER_AGENT = "Mozilla/5.0";

	@com.fintasoft.eraserisk.annotations.Transactional
    public UserPurchasePaymentResponse checkout(long userId, CheckoutRequest request) {
		
		User user = userRepository.findById(userId).orElse(null);
		if (user == null) {
			throw new InvalidValueException("userId");
		}
		
		UserPurchasePayment userPurchasePayment = new UserPurchasePayment();
		userPurchasePayment.setUser(user);
		userPurchasePayment.setNote(request.getNote());
		
		double checkoutAmount = 0;
		String strategyNameList = "";
		
		List<UserPurchaseHistory> userPurchaseHistories = new ArrayList<>();
		List<Cart> carts = new ArrayList<>();
		for (int i = 0; i < request.getCartIds().length; i++) {
			Cart cart = cartRepository.findByIdAndUserId(request.getCartIds()[i], userId);
			if (cart != null){
				Strategy strategy = cart.getStrategy();

				// Check remaining unit and valid securities company to purchase
				List<StrategySecuritiesCompany> securitiesCompanies = strategy.getSecuritiesCompanies();
				boolean hasSec = false;
				for (StrategySecuritiesCompany securitiesCompany : securitiesCompanies) {
					if (securitiesCompany.getSecuritiesCompany().getId() == cart.getSecuritiesCompany().getId()){
						hasSec = true;
						if (cart.getAutoYn() == 1) {

							if (securitiesCompany.getRemainingSellingUnit() < cart.getQuantity()){
								throw new GeneralException(ErrorCodeEnums.CHECKOUT_FAILED.name(),"error.invalid_quantity", null);
							} else {
								securitiesCompany.setRemainingSellingUnit(securitiesCompany.getRemainingSellingUnit() - cart.getQuantity());
								sscRepository.save(securitiesCompany);
								break;
							}
						}
					}
				}
				
				
				if (!hasSec) {
					throw new InvalidValueException("secIds");
				}
				
				if (cart.getAutoYn() == 1){
					if (strategy.getRemainingAuto() < cart.getQuantity()){
						throw new GeneralException(ErrorCodeEnums.CHECKOUT_FAILED.name(),"error.invalid_quantity", null);
					} else {
						strategy.setRemainingAuto(strategy.getRemainingAuto() - cart.getQuantity());
					}
				} else {
					if (strategy.getRemainingManual() < cart.getQuantity()){
						throw new GeneralException(ErrorCodeEnums.CHECKOUT_FAILED.name(),"error.invalid_quantity", null);
					} else {
						strategy.setRemainingManual(strategy.getRemainingManual() - cart.getQuantity());
					}
				}
				
				strategyRepository.save(strategy);
				
				// End check
				
				checkoutAmount += cart.getQuantity() * cart.getPrice();
				if (i != 0) {
					strategyNameList += ", " + cart.getStrategy().getStrategyName();
				} else {
					strategyNameList += cart.getStrategy().getStrategyName();
				}

				UserPurchaseHistory userPurchaseHistory = new UserPurchaseHistory();
				userPurchaseHistory.setAutoYn(cart.isAuto() ? 1 : 0);
				userPurchaseHistory.setUser(user);
				userPurchaseHistory.setQuantity(cart.getQuantity());
				userPurchaseHistory.setRemainingQuantity(cart.getQuantity());
				userPurchaseHistory.setSecuritiesCompany(cart.getSecuritiesCompany());
				userPurchaseHistory.setUnitPrice(cart.getPrice());
				userPurchaseHistory.setTotalPrice(cart.getPrice()*cart.getQuantity());
				userPurchaseHistory.setStrategy(strategy);
				userPurchaseHistory.setValidPeriod(strategy.getValidPeriod());
				userPurchaseHistories.add(userPurchaseHistory);
				carts.add(cart);
			} else {
				throw new GeneralException(ErrorCodeEnums.CHECKOUT_FAILED.name(),"error.invalid_cart", null);
			}
		}
		
		userPurchasePayment.setPaidAmount(checkoutAmount);
		userPurchasePayment.setStatus(PaymentStatusEnum.PENDING);
		
		PaymentMethod paymentMethod = paymentMethodRepository.findByMethodCode(request.getPaymentMethod());
		if (paymentMethod != null){
			userPurchasePayment.setPaymentMethod(paymentMethod);
		} else {
			throw new GeneralException(ErrorCodeEnums.CHECKOUT_FAILED.name(),"error.invalid_payment_method", null);
		}
		
		userPurchasePayment = userPurchasePaymentRepository.save(userPurchasePayment);
		
		//Remove all carts item after create checkout payment
		cartRepository.deleteAll(carts);
		
		String payUrl = null;
		if (paymentMethod.getMethodCode().equalsIgnoreCase(PaymentMethodEnum.CARD.name())){
			payUrl = checkoutViaPayApp(userPurchasePayment, strategyNameList, checkoutAmount,
					request.getFeedbackParams(), request.getReturnParams());
			userPurchasePayment.setPayUrl(payUrl);
			userPurchasePaymentRepository.save(userPurchasePayment);
		}
		
		for (UserPurchaseHistory userPurchaseHistory : userPurchaseHistories) {
			userPurchaseHistory.setPayment(userPurchasePayment);
			userPurchaseHistoryRepository.save(userPurchaseHistory);
		}
		
		return UserPurchasePaymentResponse.from(userPurchasePayment);
    }
	
	private String checkoutViaPayApp(UserPurchasePayment userPurchasePayment, String strategyNameList,
									 double checkoutAmount, Map<String, String> feedbackParams,
									 Map<String, String> returnParams){
		try {
			String stringUrl = "http://api.payapp.kr/oapi/apiLoad.html";
			URL url = new URL(stringUrl);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			
			//add request header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept", "text/html,application/xhtml+xml,*/*");
			con.setRequestProperty("Accept-Language", "ko-KR");
			
			String urlParameters = "";

			StringBuilder feedbackUrl = new StringBuilder(appConf.getCheckout().getWsUrl() + appConf.getCheckout().getFeedbackPayapp() + "?ref="+userPurchasePayment.getId());
			if (feedbackParams != null) {
				feedbackParams.forEach((k, v) -> feedbackUrl.append("&" + k + "=" + v));
			}
			StringBuilder returnUrl = new StringBuilder(appConf.getCheckout().getFrontUrl() + "/purchase-history/"+userPurchasePayment.getId());
			if (returnParams != null) {
				returnParams.forEach((k, v) -> returnUrl.append("&" + k + "=" + v));
			}
			urlParameters += "cmd=payrequest";
			urlParameters += "&userid=eraserisk";
			urlParameters += "&goodname=" + URLEncoder.encode(strategyNameList, "UTF-8");
			urlParameters += "&price=" + Math.round(checkoutAmount);
			urlParameters += "&recvphone=" + userPurchasePayment.getUser().getUserInfo().getLocalPhoneNumber().replaceAll( "[^\\d]", "" ).replaceAll("^0*", "");
			urlParameters += "&memo=" + userPurchasePayment.getNote();
			urlParameters += "&reqaddr=0";
			urlParameters += "&feedbackurl=" + URLEncoder.encode(feedbackUrl.toString(),"UTF-8");
			urlParameters += "&var1=";
			urlParameters += "&var2=";
			urlParameters += "&smsuse=y";
			urlParameters += "&reqmode=";
			urlParameters += "&vccode=";
			urlParameters += "&returnurl=" + URLEncoder.encode(returnUrl.toString(),"UTF-8");
			urlParameters += "&openpaytype=" + userPurchasePayment.getPaymentMethod().getMethodCode();
			urlParameters += "&checkretry=n";
			
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			log.warn("send to payapp with url {} and params {}", stringUrl, urlParameters);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			StringBuilder response = new StringBuilder();

			if (responseCode != 200) {
				throw new GeneralException(ErrorCodeEnums.CHECKOUT_FAILED.name(), "error.checkout_failed", null);
			} else {
				BufferedReader in = new BufferedReader(
				        new InputStreamReader(con.getInputStream(),"EUC-KR"));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				
				in.close();
			}

			log.warn("receive from payapp with response {}", response);
			
		    Map<String, String> map = ConvertUtils.splitQuery(response.toString());
		    
		    if ("0".equals(map.get("state"))){
		    	throw new GeneralException(ErrorCodeEnums.CHECKOUT_FAILED.name(), "error.checkout_failed", null);
		    } else {
		    	String payUrl = map.get("payurl");
		    	userPurchasePayment.setTransactionNo(map.get("mul_no"));
		    	userPurchasePaymentRepository.save(userPurchasePayment);
		    	return payUrl;
		    }
		} catch (Exception e) {
			throw new GeneralException(ErrorCodeEnums.CHECKOUT_FAILED.name(), "error.checkout_failed", null).source(e);
		}
	}
	
	@com.fintasoft.eraserisk.annotations.Transactional
    public UserPurchasePaymentResponse feedback(CheckoutFeedback feedback) {
		log.warn("got feedback from payapp {}", feedback);
		UserPurchasePayment userPurchasePayment = userPurchasePaymentRepository.getOne(feedback.getPaymentId());
		
		if (!appConf.getCheckout().getAccountId().equals(feedback.getAccountId())){
			throw new InvalidValueException("userid[" + feedback.getAccountId() + "]");
		}
		
		if (!appConf.getCheckout().getLinkKey().equals(feedback.getLinkKey())){
			throw new InvalidValueException("linkKey");
		}
		
		if (!appConf.getCheckout().getLinkValue().equals(feedback.getLinkVal())){
			throw new InvalidValueException("linkVal");
		}
		
		if (feedback.getPrice() != userPurchasePayment.getPaidAmount()){
			throw new InvalidValueException("price");
		}
		
		switch (feedback.getPayState()) {
		case 1:
			break;
		case 4:
			if (userPurchasePayment.getTransactionNo().equals(feedback.getMulNo())) {
				userPurchasePayment.setTransactionNo(feedback.getMulNo());
				userPurchasePayment.setStatus(PaymentStatusEnum.COMPLETE);
				userPurchasePayment.setPurchaseDate(new Timestamp(new Date().getTime()));
				
				userPurchaseHistoryService.completePayment(userPurchasePayment);
			} else {
                userPurchaseHistoryService.cancelPayment(userPurchasePayment);
			}
			
			userPurchasePaymentRepository.save(userPurchasePayment);

			break;
		
		case 8:
        case 16:
        case 32:
        case 9:
        case 64:
        	userPurchaseHistoryService.cancelPayment(userPurchasePayment);
        	userPurchasePaymentRepository.save(userPurchasePayment);
        	break;
        	
        case 10:
        	break;
		default:
			break;
		}
		
		return UserPurchasePaymentResponse.from(userPurchasePayment);
	}
}
