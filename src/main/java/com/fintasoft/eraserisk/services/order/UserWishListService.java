package com.fintasoft.eraserisk.services.order;

import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import com.fintasoft.eraserisk.exceptions.GeneralException;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.request.WishListRequest;
import com.fintasoft.eraserisk.model.api.response.UserWishListResponse;
import com.fintasoft.eraserisk.model.db.Strategy;
import com.fintasoft.eraserisk.model.db.User;
import com.fintasoft.eraserisk.model.db.UserWishList;
import com.fintasoft.eraserisk.repositories.StrategyRepository;
import com.fintasoft.eraserisk.repositories.UserRepository;
import com.fintasoft.eraserisk.repositories.UserWishListRepository;
import com.fintasoft.eraserisk.repositories.impl.QueryParamRepository;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserWishListService {

	@Autowired
	private UserWishListRepository userWishListRepository;

	@Autowired
	private QueryParamRepository queryParamRepository;
	
	@Autowired
	private StrategyRepository strategyRepository;
	
	@Autowired
	private UserRepository userRepository;

	public Page<UserWishListResponse> findByUserId(long userId, Pageable pageable) {
		Page<UserWishList> userWishLists = userWishListRepository.findByUserId(userId, pageable);
		return userWishLists.map(UserWishListResponse::from);
	}

	public Page<UserWishListResponse> findByUserIdOrderByRoi(long userId, String orderCategory, Pageable pageable) throws ParseException {
		List<QueryParam> params = new ArrayList<>();
		params.add(QueryParam.create("userId", userId + "", Long.class));
		QueryParam.create(
				QueryParam.create("strategy.strategyPositions.pk.category", orderCategory).leftJoin(1),
				QueryParam.create("strategy.strategyPositions.pk.category", "").leftJoin(1).nullValue()
		);
		Sort sort = Sort.by(Sort.Direction.DESC, "strategy.strategyPositions.positionNo");
		if (pageable == null) {
			pageable = PageRequest.of(0, 20, sort);
		} else {
			pageable = ConvertUtils.addMoreSort(pageable, sort, true);
		}

		return queryParamRepository.find(params, pageable, UserWishList.class).map(UserWishListResponse::from);
	}
	
	@com.fintasoft.eraserisk.annotations.Transactional
    public UserWishListResponse addToWishList(long userId, WishListRequest wishListRequest) {
		Strategy strategy = strategyRepository.findById(wishListRequest.getStrategyId()).orElse(null);
		if (strategy == null) {
			throw new InvalidValueException("strategyId");
		}

		User user = userRepository.findById(userId).orElse(null);
		if (user == null) {
			throw new InvalidValueException("userId");
		}
				
		UserWishList userWishList = userWishListRepository.findByUserIdAndStrategyId(userId,strategy.getId());
		
		if (userWishList == null) {
			userWishList = UserWishList.builder()
		            .strategy(strategy)
		            .user(user)
		            .build();
		} else {
			throw new GeneralException(ErrorCodeEnums.INVALID_VALUE.name(), "error.existed_wishlist", null);
		}
		
	    
		userWishListRepository.save(userWishList);
        return UserWishListResponse.from(userWishList);
    }
	
	@com.fintasoft.eraserisk.annotations.Transactional
    public void removeItemInWishList(long userId, WishListRequest wishListRequest) {
		userWishListRepository.deleteByUserIdAndStrategyId(userId, wishListRequest.getStrategyId());
	}
}
