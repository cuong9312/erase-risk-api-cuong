package com.fintasoft.eraserisk.services.order;


import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import com.fintasoft.eraserisk.constances.OrderTypeEnum;
import com.fintasoft.eraserisk.constances.SignalEnum;
import com.fintasoft.eraserisk.exceptions.GeneralException;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.model.UserStrategySetting;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.request.AutoOrderRequest;
import com.fintasoft.eraserisk.model.api.request.ImportOrderRequest;
import com.fintasoft.eraserisk.model.api.request.OrderRequest;
import com.fintasoft.eraserisk.model.api.request.UpdateOrderRequest;
import com.fintasoft.eraserisk.model.api.response.StrategyPurchaseStatisticResponse;
import com.fintasoft.eraserisk.model.api.response.UserAutoOrderResponse;
import com.fintasoft.eraserisk.model.db.*;
import com.fintasoft.eraserisk.repositories.*;
import com.fintasoft.eraserisk.repositories.impl.QueryParamRepository;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;

@Service
public class OrderService {

    private static final Logger log = LoggerFactory.getLogger(OrderService.class);

    @Autowired
    private QueryParamRepository queryParamRepository;

    @Autowired
    private UserStrategyOrderRepository userStrategyOrderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserPurchaseHistoryRepository userPurchaseHistoryRepository;

    @Autowired
    private UserStrategySettingRepository spsRepository;

    @Autowired
    private SecuritiesCompanyRepository securitiesCompanyRepository;

    @Autowired
    private StrategySignalDataRepository strategySignalDataRepository;

    @Transactional
    public StrategyPurchaseStatisticResponse saveOrder(long userId, AutoOrderRequest request) {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new InvalidValueException("userId");
        }

        UserStrategySetting userStrategySetting = spsRepository.findByPk(request.getStrategyId(), userId, request.getSecId(), true);
        if (userStrategySetting == null) {
            throw new InvalidValueException("strategyId");
        }

        if (userStrategySetting.getLotSize() == 0) {
            throw new GeneralException(ErrorCodeEnums.ORDER_FAILED.name(), "error.order_failed", null);
        }

        UserStrategyOrder userStrategyOrder = userStrategyOrderRepository.
                findByOrderNoAndAccountNumberAndUserIdAndSecuritiesCompanyId(
                        request.getOrderNo(),
                        request.getAccountNumber(),
                        userId,
                        request.getSecId()
                );
        if (userStrategyOrder != null) {
            throw new InvalidValueException("orderNo");
        }
        userStrategyOrder = new UserStrategyOrder();

        userStrategyOrder.setStrategy(userStrategySetting.getStrategy());
        userStrategyOrder.setUser(user);
        userStrategyOrder.setSecuritiesCompany(userStrategySetting.getSecuritiesCompany());
        userStrategyOrder.setPrice(request.getPrice());
        userStrategyOrder.setAccountNumber(request.getAccountNumber());
        userStrategyOrder.setOrderNo(request.getOrderNo());
        userStrategyOrder.setOrderAction(SignalEnum.valueOf(request.getOrderAction()));
        userStrategyOrder.setOrderType(OrderTypeEnum.valueOf(request.getOrderType()));
        userStrategyOrder.setQuantity(request.getQuantity());
        userStrategyOrder.setAutoYn(1);
        if (request.isTempYn()) {
            userStrategyOrder.setSignalTempId(request.getSignalId());
            StrategySignalData strategySignalData = strategySignalDataRepository.findByTempId(request.getSignalId());
            if (strategySignalData != null) {
                log.info("update signal_id for order, user_strategy_order.id: {}, signal_id: {}, signal_temp_id: {}", userStrategyOrder.getId(), strategySignalData.getId(), request.getSignalId());
                userStrategyOrder.setSignalId(strategySignalData.getId());
            }
        } else {
            userStrategyOrder.setSignalId(request.getSignalId());
        }
        userStrategyOrder.setTradingAt(new Timestamp(System.currentTimeMillis()));

        userStrategyOrderRepository.save(userStrategyOrder);

        return StrategyPurchaseStatisticResponse.from(userStrategySetting);

    }

    @Transactional
    public void updateOrder(long userId, UpdateOrderRequest request) {
        log.info("userId: {}, updateOrder {}", userId, request);
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new InvalidValueException("userId");
        }
        UserStrategyOrder userStrategyOrder = userStrategyOrderRepository.
                findByOrderNoAndAccountNumberAndUserId(
                        request.getActualOrderNo(),
                        request.getAccountNumber(),
                        userId
                );
        if (userStrategyOrder == null) {
            throw new InvalidValueException("orderNo");
        }

        if (userStrategyOrder.getUser().getId() != userId) {
            throw new InvalidValueException("userId");
        }

        userStrategyOrder.setFilledPrice(request.getFilledPrice());
        userStrategyOrder.setFilledQuantity(request.getFilledQuantity());
    }

    @Transactional
    public void importOrder(long userId, ImportOrderRequest request) {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new InvalidValueException("userId");
        }

        SecuritiesCompany securitiesCompany = securitiesCompanyRepository.findById(request.getSecId()).orElse(null);
        if (securitiesCompany == null) {
            throw new InvalidValueException("secId");
        }

        for (int i = 0; i < request.getOrderRequests().length; i++) {
            OrderRequest orderRequest = request.getOrderRequests()[i];
            UserStrategyOrder userStrategyOrder = userStrategyOrderRepository.findByOrderNoAndAccountNumberAndUserIdAndSecuritiesCompanyId(orderRequest.getOrderNo(), orderRequest.getAccountNumber(), userId, request.getSecId());
            if (userStrategyOrder == null) {
                userStrategyOrder = new UserStrategyOrder();
                userStrategyOrder.setUser(user);
                userStrategyOrder.setSecuritiesCompany(securitiesCompany);
                userStrategyOrder.setAccountNumber(orderRequest.getAccountNumber());
                userStrategyOrder.setOrderNo(orderRequest.getOrderNo());
                userStrategyOrder.setOrderAction(SignalEnum.valueOf(orderRequest.getOrderAction()));
                if (orderRequest.getOrderType() != null) {
                    userStrategyOrder.setOrderType(OrderTypeEnum.valueOf(orderRequest.getOrderType()));
                }
                userStrategyOrder.setAutoYn(0);
            }
            userStrategyOrder.setPrice(orderRequest.getPrice());
            userStrategyOrder.setQuantity(orderRequest.getQuantity());
            userStrategyOrder.setFilledPrice(orderRequest.getFilledPrice());
            userStrategyOrder.setFilledQuantity(orderRequest.getFilledQuantity());
            userStrategyOrder.setTradingAt(ConvertUtils.fromString(orderRequest.getTradingAt()));

            userStrategyOrderRepository.save(userStrategyOrder);
        }

    }

    public Page<UserAutoOrderResponse> find(List<QueryParam> queryParams, Pageable pageable) throws ParseException {
        return ConvertUtils.convert(queryParamRepository.find(queryParams, pageable, UserAutoOrder.class), UserAutoOrderResponse::from);
    }

}
