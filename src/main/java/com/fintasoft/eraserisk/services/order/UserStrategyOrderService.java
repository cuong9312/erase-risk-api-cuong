package com.fintasoft.eraserisk.services.order;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintasoft.eraserisk.constances.ReportStatusEnum;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.response.PageExt;
import com.fintasoft.eraserisk.model.api.response.TradeAutoReportResponse;
import com.fintasoft.eraserisk.model.api.response.TradeReportResponse;
import com.fintasoft.eraserisk.model.api.response.UserStrategyOrderResponse;
import com.fintasoft.eraserisk.model.db.ReportRequest;
import com.fintasoft.eraserisk.model.db.UserStrategyOrder;
import com.fintasoft.eraserisk.model.query.TradeAutoReport;
import com.fintasoft.eraserisk.model.query.TradeReport;
import com.fintasoft.eraserisk.repositories.ReportRequestRepository;
import com.fintasoft.eraserisk.repositories.impl.UserStrategyOrderRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;

@Service
public class UserStrategyOrderService {

    @Autowired
    UserStrategyOrderRepositoryImpl userStrategyOrderRepository;
    
    @Autowired
    ReportRequestRepository reportRequestRepository;

    public PageExt<TradeReportResponse, TradeReport.Summary> queryTradeReport(
            List<Long> secIds,
            List<Long> providerIds,
            Timestamp minTradingAt,
            Timestamp maxTradingAt,
            String username,
            Pageable pageable
    ) {
        PageExt<TradeReport, TradeReport.Summary> page = userStrategyOrderRepository.queryTradeReport(secIds, providerIds, minTradingAt, maxTradingAt, username, pageable);
        List<TradeReportResponse> results = new ArrayList<>();
        page.getContent().forEach(t -> results.add(TradeReportResponse.from(t)));

        return new PageExt<>(results, pageable, page.getTotalElements(), page.getExt());
    }
    
    public long exportTradeReport(
            List<Long> secIds,
			List<Long> providerIds,
            Timestamp minTradingAt,
            Timestamp maxTradingAt,
            String username,
			String createdByUserId
    ) throws JsonProcessingException {
    	ObjectMapper mapper = new ObjectMapper();
		Map<Object, Object> params = new HashMap<>();
		if (secIds!= null)
			params.put("secIds", secIds);
		if (providerIds!= null)
			params.put("providerIds", providerIds);
		if (minTradingAt!= null)
			params.put("minTradingAt", minTradingAt);
		if (maxTradingAt!= null)
			params.put("maxTradingAt", maxTradingAt);
		if (username!= null)
			params.put("username", username);
		
		String json = mapper.writeValueAsString(params);
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setSqlParam(json);
		
		reportRequest.setRootClass(TradeReport.class.getCanonicalName());

		reportRequest.setResponseClass(TradeReport.class.getCanonicalName());
		reportRequest.setStatus(ReportStatusEnum.PENDING);
		reportRequest.setCreatedByUserId(createdByUserId);

		Map<String, String> exportFields = new LinkedHashMap<String, String>();
		
		
		exportFields.put("fullName", "Username");
		exportFields.put("loginTime", "Login Time");
		exportFields.put("logoutTime", "Logout Time");
		exportFields.put("totalTrade", "Total Trades");
		exportFields.put("totalManualTrade", "Total Manual Trades");
		exportFields.put("totalAutoTrade", "Total Auto Trades");
		exportFields.put("securityName", "Securities Firm");
		
		reportRequest.setExportField(mapper.writeValueAsString(exportFields));

		reportRequestRepository.save(reportRequest);
		
		return reportRequest.getId();
    }

    public PageExt<TradeAutoReportResponse, TradeAutoReport.Summary> queryTradeAutoReport(
            List<Long> secIds,
            List<Long> providerIds,
            Timestamp minTradingAt,
            Timestamp maxTradingAt,
            String username,
            String strategyName,
            Pageable pageable
    ) {
        PageExt<TradeAutoReport, TradeAutoReport.Summary> page = userStrategyOrderRepository.queryTradeAutoReport(
                secIds, providerIds, minTradingAt, maxTradingAt, username, strategyName, pageable);
        List<TradeAutoReportResponse> results = new ArrayList<>();
        page.getContent().forEach(t -> results.add(TradeAutoReportResponse.from(t)));

        return new PageExt<>(results, pageable, page.getTotalElements(), page.getExt());
    }
    
    public long exportTradeAutoReport(
    		String createdByUserId,
    		List<QueryParam> params
    ) throws JsonProcessingException {
    	ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(params);
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setSqlParam(json);
		reportRequest.setRootClass(UserStrategyOrder.class.getCanonicalName());
		
		reportRequest.setStatus(ReportStatusEnum.PENDING);
		
		Map<String, String> exportFields = new LinkedHashMap<String, String>();

		reportRequest.setResponseClass(UserStrategyOrderResponse.class.getCanonicalName());
		exportFields.put("username", "Username");
		exportFields.put("tradingAt", "Trading Time");
		exportFields.put("quantity", "Quantity");
		exportFields.put("strategyName", "Strategy Name");
		exportFields.put("providerName", "Signal Provider");
		exportFields.put("secName", "Securities Firm");

		reportRequest.setExportField(mapper.writeValueAsString(exportFields));
		reportRequest.setCreatedByUserId(createdByUserId);
		reportRequestRepository.save(reportRequest);
		
		return reportRequest.getId();
    }
}
