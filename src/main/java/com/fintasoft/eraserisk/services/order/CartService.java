package com.fintasoft.eraserisk.services.order;

import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.exceptions.StrategyStateInactiveException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.request.CartRequest;
import com.fintasoft.eraserisk.model.api.request.UpdateCartRequest;
import com.fintasoft.eraserisk.model.api.response.CartResponse;
import com.fintasoft.eraserisk.model.api.response.PageExt;
import com.fintasoft.eraserisk.model.db.*;
import com.fintasoft.eraserisk.repositories.CartRepository;
import com.fintasoft.eraserisk.repositories.SecuritiesCompanyRepository;
import com.fintasoft.eraserisk.repositories.StrategyRepository;
import com.fintasoft.eraserisk.repositories.UserRepository;
import com.fintasoft.eraserisk.repositories.impl.QueryParamRepository;
import com.fintasoft.eraserisk.services.strategy.StrategyStatusService;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CartService {

	@Autowired
	private CartRepository cartRepository;
	
	@Autowired
	private StrategyRepository strategyRepository;
	
	@Autowired
	private SecuritiesCompanyRepository securitiesCompanyRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private QueryParamRepository queryParamRepository;

	@Autowired
	private StrategyStatusService strategyStatusService;

	public PageExt<CartResponse, CartResponse.CartSummary> findByUserId(long userId, String category,
																		Pageable pageable, List<Long> cartIds) throws ParseException {
		List<QueryParam> queryParams = new ArrayList<>();
		queryParams.add(QueryParam.create("userId", userId, Long.class));
		if (!CollectionUtils.isEmpty(cartIds)) {
			queryParams.add(QueryParam.create("id", cartIds));
		}
		queryParams.add(QueryParam.create("strategy.status", strategyStatusService.getActivated().getId() + "", Long.class));
		if (!StringUtils.isEmpty(category)) {
			QueryParam.create(
					QueryParam.create("strategy.strategyPositions.pk.category", category).leftJoin(1),
					QueryParam.create("strategy.strategyPositions.pk.category", "").leftJoin(1).nullValue()
			);
			Sort sort = Sort.by(Sort.Direction.DESC, "strategy.strategyPositions.positionNo");
			if (pageable == null) {
				pageable = PageRequest.of(0, 20, sort);
			} else {
				pageable = ConvertUtils.addMoreSort(pageable, sort, true);
			}
		}
		Page<Cart> carts = queryParamRepository.find(queryParams, pageable, Cart.class);
				
		List<CartResponse> list = new ArrayList<>();
		carts.getContent().forEach((up) -> list.add(CartResponse.from(up)));
		PageExt<CartResponse, CartResponse.CartSummary> result = new PageExt(list, carts.getPageable(), carts.getTotalElements());
		Double totalPrice = cartRepository.queryTotalPriceByUser(userId);
		result.setExt(new CartResponse.CartSummary(totalPrice == null ? 0d : totalPrice));
		return result;
	}
	
	@com.fintasoft.eraserisk.annotations.Transactional
    public CartResponse addToCart(long userId, CartRequest cartRequest) {
		Strategy strategy = strategyRepository.findById(cartRequest.getStrategyId()).orElse(null);
		if (strategy == null) {
			throw new InvalidValueException("strategyId");
		}
		if (strategyStatusService.getActivated().getId() != strategy.getStatus()) {
			throw new StrategyStateInactiveException(cartRequest.getStrategyId());
		}
		SecuritiesCompany securitiesCompany = securitiesCompanyRepository.findById(cartRequest.getSecId()).orElse(null);
		if (securitiesCompany == null) {
			throw new InvalidValueException("secId");
		}

		User user = userRepository.findById(userId).orElse(null);
		if (user == null) {
			throw new InvalidValueException("userId");
		}
				
		Cart cart = cartRepository.findByUserIdAndStrategyIdAndSecuritiesCompanyIdAndPriceAndAutoYn(
																				userId, 
																				strategy.getId(), 
																				securitiesCompany.getId(),
																				cartRequest.getAutoYn()==0?strategy.getManualPrice():strategy.getAutoPrice(),
																				cartRequest.getAutoYn());

		if (cart == null) {
			cart = Cart.builder()
		            .autoYn(cartRequest.getAutoYn())
		            .price(cartRequest.getAutoYn()==0?strategy.getManualPrice():strategy.getAutoPrice())
		            .quantity(cartRequest.getQuantity())
		            .securitiesCompany(securitiesCompany)
		            .strategy(strategy)
		            .user(user)
		            .build();
			cart.setStrategy(cart.getStrategy());
			cart.setUser(cart.getUser());
			cart.setSecuritiesCompany(cart.getSecuritiesCompany());
		} else {
			cart.setQuantity(cart.getQuantity() + cartRequest.getQuantity());
		}


		for (StrategySecuritiesCompany ssc : strategy.getSecuritiesCompanies()) {
			if (cartRequest.getAutoYn() > 0) {
				if (ssc.getSecuritiesCompany().getId() == securitiesCompany.getId()) {
					if (ssc.getRemainingSellingUnit() < cart.getQuantity()) {
						throw new InvalidValueException("quantity");
					}
				}
			}
		}

		if (cartRequest.getAutoYn() > 0) {
			if (strategy.getRemainingAuto() < cart.getQuantity()){
				throw new InvalidValueException("quantity");
			}
		} else {
			if (strategy.getRemainingManual() < cart.getQuantity()){
				throw new InvalidValueException("quantity");
			}
		}

		cartRepository.save(cart);
        return CartResponse.from(cart);
    }

    @com.fintasoft.eraserisk.annotations.Transactional
    public void removeItemsInCart(long userId, List<Long> cartIds) {
		cartIds.forEach(cId -> cartRepository.deleteByUserIdAndId(userId, cId));
	}
    
    @com.fintasoft.eraserisk.annotations.Transactional
    public CartResponse updateCart(long userId, UpdateCartRequest updateCartRequest) {
    	Cart cart = cartRepository.findById(updateCartRequest.getCartId()).orElse(null);
    	if (cart == null) {
			throw new InvalidValueException("cartId");
		}
    	
    	if (cart.getUser().getId() != userId){
    		throw new InvalidValueException("userId", "error.invalid_cart", null);
    	}
    	
    	if (updateCartRequest.getQuantity() <= 0 ){
    		throw new InvalidValueException("quantity");
    	}
    	
    	cart.setQuantity(updateCartRequest.getQuantity());

    	Strategy strategy = cart.getStrategy();

		for (StrategySecuritiesCompany ssc : strategy.getSecuritiesCompanies()) {
			if (cart.getAutoYn() > 0) {
				if (ssc.getSecuritiesCompany().getId() == cart.getSecuritiesCompany().getId()) {
					if (ssc.getRemainingSellingUnit() < cart.getQuantity()) {
						throw new InvalidValueException("quantity");
					}
				}
			}
		}

		if (cart.getAutoYn() > 0) {
			if (strategy.getRemainingAuto() < cart.getQuantity()){
				throw new InvalidValueException("quantity");
			}
		} else {
			if (strategy.getRemainingManual() < cart.getQuantity()){
				throw new InvalidValueException("quantity");
			}
		}
    	
    	cartRepository.save(cart);
    	
    	return CartResponse.from(cart);
	}
}
