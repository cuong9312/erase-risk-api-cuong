package com.fintasoft.eraserisk.services.order;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.NotificationMethodEnum;
import com.fintasoft.eraserisk.constances.PaymentStatusEnum;
import com.fintasoft.eraserisk.constances.ReportStatusEnum;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.exceptions.TargetNotFoundException;
import com.fintasoft.eraserisk.model.UserStrategySetting;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.request.ApproveRejectPaymentRequest;
import com.fintasoft.eraserisk.model.api.response.PageExt;
import com.fintasoft.eraserisk.model.api.response.RevenueResponse;
import com.fintasoft.eraserisk.model.api.response.UserPurchaseHistoryResponse;
import com.fintasoft.eraserisk.model.api.response.UserPurchasePaymentResponse;
import com.fintasoft.eraserisk.model.db.ReportRequest;
import com.fintasoft.eraserisk.model.db.StrategySecuritiesCompany;
import com.fintasoft.eraserisk.model.db.UserPurchaseHistory;
import com.fintasoft.eraserisk.model.db.UserPurchasePayment;
import com.fintasoft.eraserisk.model.kafka.NotificationMessage;
import com.fintasoft.eraserisk.model.query.RevenueByDay;
import com.fintasoft.eraserisk.model.query.RevenueByMonth;
import com.fintasoft.eraserisk.model.query.RevenueSummary;
import com.fintasoft.eraserisk.repositories.*;
import com.fintasoft.eraserisk.repositories.impl.QueryParamRepository;
import com.fintasoft.eraserisk.repositories.impl.UserPurchaseRepositoryImpl;
import com.fintasoft.eraserisk.services.queue.RequestResponseHandler;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;

@Service
public class UserPurchaseHistoryService {
	private static final Logger log = LoggerFactory.getLogger(UserPurchaseHistoryService.class);

	@Autowired
	private UserPurchaseHistoryRepository userPurchaseHistoryRepository;
	
	@Autowired
	private UserPurchaseRepositoryImpl userPurchaseRepositoryImpl;

	@Autowired
	private QueryParamRepository queryParamRepository;

	@Autowired
	private UserPurchasePaymentRepository userPaymentRepository;
	
	@Autowired
    private ReportRequestRepository reportRequestRepository;

	@Autowired
    private StrategySecuritiesCompanyRepository sscr;

	@Autowired
    private UserStrategySettingRepository spsRepository;

	@Autowired
    private RequestResponseHandler requestHandler;

	@Autowired
    private AppConf appConf;

	public Page<UserPurchaseHistoryResponse> findByUserId(long userId, String type, Pageable pageable) {
		Page<UserPurchaseHistory> userPurchaseHistories = null;
		if ("all".equals(type)) {
			userPurchaseHistories = userPurchaseHistoryRepository.findByUserId(userId, pageable);
    	} else if ("valid_only".equals(type)){  		
    		userPurchaseHistories = userPurchaseRepositoryImpl.findValidOnly(userId, pageable);  
    	} else {
    		throw new InvalidValueException("type[" + type + "]");
    	}
				
		List<UserPurchaseHistoryResponse> list = new ArrayList<>();
        userPurchaseHistories.getContent().forEach((up) -> list.add(UserPurchaseHistoryResponse.from(up)));
        Page<UserPurchaseHistoryResponse> result = new PageImpl(list, userPurchaseHistories.getPageable(), userPurchaseHistories.getTotalElements());
		return result;
	}

	public Page<UserPurchaseHistoryResponse> searchPurchaseHistories(List<QueryParam> params, Pageable pageable) throws ParseException {
		Page<UserPurchaseHistory> purchaseHistories = userPurchaseRepositoryImpl.findPurchaseHistories(params, pageable);
		List<UserPurchaseHistoryResponse> list = new ArrayList<>();
		purchaseHistories.getContent().forEach((up) -> list.add(UserPurchaseHistoryResponse.from(up)));
        Page<UserPurchaseHistoryResponse> result = new PageImpl(list, purchaseHistories.getPageable(), purchaseHistories.getTotalElements());
		return result;
	}

	public Long exportPurchasePayments(List<QueryParam> params, String createdByUserId) throws ParseException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(params);
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setSqlParam(json);
		reportRequest.setCreatedByUserId(createdByUserId);
		reportRequest.setRootClass(UserPurchasePayment.class.getCanonicalName());

		reportRequest.setStatus(ReportStatusEnum.PENDING);

		Map<String, String> exportFields = new LinkedHashMap<String, String>();

		reportRequest.setResponseClass(UserPurchasePaymentResponse.class.getCanonicalName());
		exportFields.put("purchaseDate", "Transaction Time");
		exportFields.put("id", "Order No");
		exportFields.put("paidAmount", "Paid Amount");
		exportFields.put("paymentMethodName", "Payment Method");
		exportFields.put("customerName", "Customer Name");

		reportRequest.setExportField(mapper.writeValueAsString(exportFields));
		reportRequestRepository.save(reportRequest);

		return reportRequest.getId();
	}

	public Page<UserPurchasePaymentResponse> searchPurchasePayment(List<QueryParam> params, Pageable pageable) throws ParseException, JsonProcessingException {
		Page<UserPurchasePayment> purchaseHistories = queryParamRepository.find(params, pageable, UserPurchasePayment.class);
		List<UserPurchasePaymentResponse> list = new ArrayList<>();
		purchaseHistories.getContent().forEach((up) -> list.add(UserPurchasePaymentResponse.from(up)));
		Page<UserPurchasePaymentResponse> result = new PageImpl(list, purchaseHistories.getPageable(), purchaseHistories.getTotalElements());
		return result;
	}

	public UserPurchasePaymentResponse searchPurchasePayment(Long paymentId) {
		UserPurchasePayment purchasePayment = userPaymentRepository.findById(paymentId).orElse(null);
		if (purchasePayment == null) {
			throw new TargetNotFoundException();
		}
		return UserPurchasePaymentResponse.from(purchasePayment);
	}

	public Long exportRevenueByDay(
			boolean isDay,
			List<Long> paymentMethodIds,
			List<Long> providerIds,
			List<Long> secIds,
			Timestamp minPurchaseDate,
			Timestamp maxPurchaseDate,
            String createdByUserId
	) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        Map<Object, Object> params = new HashMap<>();
        if (paymentMethodIds!= null)
            params.put("paymentMethodIds", paymentMethodIds);
        if (providerIds!= null)
            params.put("providerIds", providerIds);
        if (secIds!= null)
            params.put("secIds", secIds);
        if (minPurchaseDate!= null)
            params.put("minPurchaseDate", minPurchaseDate);
        if (maxPurchaseDate!= null)
            params.put("maxPurchaseDate", maxPurchaseDate);

        String json = mapper.writeValueAsString(params);
        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setCreatedByUserId(createdByUserId);
        reportRequest.setSqlParam(json);
        if (isDay) {
            reportRequest.setRootClass(RevenueByDay.class.getCanonicalName());
        } else {
            reportRequest.setRootClass(RevenueByMonth.class.getCanonicalName());
        }

        reportRequest.setResponseClass(UserPurchasePaymentResponse.class.getCanonicalName());
        reportRequest.setStatus(ReportStatusEnum.PENDING);

        Map<String, String> exportFields = new LinkedHashMap<String, String>();

        if (isDay) {
            exportFields.put("purchaseDate", "Date");
        } else {
            exportFields.put("purchaseDate", "Month");
        }
        exportFields.put("countAuto", "Auto Subscribe");
        exportFields.put("countManual", "Manual Subscribe");
        exportFields.put("countTotal", "Total Subscribe");
        exportFields.put("amount", "Amount");

        reportRequest.setExportField(mapper.writeValueAsString(exportFields));

        reportRequestRepository.save(reportRequest);

        return reportRequest.getId();
    }

	public PageExt<?, RevenueSummary> queryRevenueByDay(
			boolean isDay,
			List<Long> paymentMethodIds,
			List<Long> providerIds,
			List<Long> secIds,
			Timestamp minPurchaseDate,
			Timestamp maxPurchaseDate,
			Pageable pageable
	) {
        List<RevenueResponse> list = new ArrayList<>();
        long totalElements = 0;
        RevenueSummary summary = null;
        if (isDay) {
            PageExt<RevenueByDay, RevenueSummary> queryResult = userPurchaseRepositoryImpl.queryRevenueByDay(paymentMethodIds, providerIds, secIds, minPurchaseDate, maxPurchaseDate, pageable);
            queryResult.getContent().forEach(a -> list.add(RevenueResponse.from(a)));
            totalElements = queryResult.getTotalElements();
            summary = queryResult.getExt();
        } else {
            PageExt<RevenueByMonth, RevenueSummary> queryResult = userPurchaseRepositoryImpl.queryRevenueByMonth(paymentMethodIds, providerIds, secIds, minPurchaseDate, maxPurchaseDate, pageable);
            queryResult.getContent().forEach(a -> list.add(RevenueResponse.from(a)));
            totalElements = queryResult.getTotalElements();
            summary = queryResult.getExt();
        }
        return new PageExt<>(list, pageable, totalElements, summary);
	}

	@Transactional
	public UserPurchasePaymentResponse approveRejectPayment(long paymentId, ApproveRejectPaymentRequest request){
		UserPurchasePayment userPurchasePayment = userPaymentRepository.findById(paymentId).orElse(null);
		if (userPurchasePayment == null) {
			throw new TargetNotFoundException();
		}
		
		if (request.isApprove()){
			userPurchasePayment.setPurchaseDate(new Timestamp(new Date().getTime()));
			userPurchasePayment.setStatus(PaymentStatusEnum.COMPLETE);
			
			this.completePayment(userPurchasePayment);
		} else {
			userPurchasePayment.setStatus(PaymentStatusEnum.CANCELLED);
		}
		
		userPaymentRepository.save(userPurchasePayment);
		
		return UserPurchasePaymentResponse.from(userPurchasePayment);
	}

	public void completePayment(UserPurchasePayment userPurchasePayment) {
		log.info("complete payment id: {}, transactionNo: {}", userPurchasePayment.getId(), userPurchasePayment.getTransactionNo());
		NotificationMessage notificationMessage = new NotificationMessage();
		notificationMessage.setMethod(NotificationMethodEnum.KAKAO);
		notificationMessage.setTemplate("purchase_complete");
		notificationMessage.setTemplateDatas(new ArrayList<>());
		String username = userPurchasePayment.getUser().getUserInfo().getFullName();

		List<UserPurchaseHistory> userPurchaseHistories = userPurchasePayment.getItems();
		for (UserPurchaseHistory item : userPurchaseHistories) {
			item.setPurchaseDate(userPurchasePayment.getPurchaseDate());
			item.setExpirationDate(ConvertUtils.addDay(item.getPurchaseDate(), item.getValidPeriod()));
			userPurchaseHistoryRepository.save(item);

			UserStrategySetting userStrategySetting = spsRepository.findByPk(item.getStrategy().getId(), item.getUser().getId(), item.getSecuritiesCompany().getId(), item.isAuto());
			if (userStrategySetting == null) {
				userStrategySetting = new UserStrategySetting(item.getStrategy().getId(), item.getUser().getId(), item.getSecuritiesCompany().getId(), item.isAuto());
				userStrategySetting.setLotSize(item.getQuantity());
				spsRepository.save(userStrategySetting);
			}

			notificationMessage.getTemplateDatas().add(new HashMap(){{
				put("customer", username);
				put("strategyName", item.getStrategy().getStrategyName());
				put("beginningDate", ConvertUtils.fromTime(new Timestamp(System.currentTimeMillis())));
				put("endingDate", ConvertUtils.fromTime(item.getExpirationDate()));
			}});
		}

		notificationMessage.setConfiguration(new HashMap(){{
			put("to", userPurchasePayment.getUser().getUserInfo().getPhoneNumber());
		}});

		requestHandler.sendRequestNoResponseSafe(appConf.getQueue().getTopics().getNotification(), "", notificationMessage);
	}

	@Transactional
	public void cancelPayment(UserPurchasePayment userPurchasePayment) {
		List<UserPurchaseHistory> userPurchaseHistories = userPurchasePayment.getItems();
		for (UserPurchaseHistory item : userPurchaseHistories) {
			item.setExpirationDate(null);
			if (userPurchasePayment.getStatus() == PaymentStatusEnum.COMPLETE) {
				UserStrategySetting userStrategySetting = spsRepository.findByPk(item.getStrategy().getId(), item.getUser().getId(), item.getSecuritiesCompany().getId(), item.isAuto());
				if (userStrategySetting != null) {
					userStrategySetting.setLotSize(userStrategySetting.getLotSize() - item.getQuantity());
				}
			}
		}
		userPurchasePayment.setStatus(PaymentStatusEnum.CANCELLED);
		userPurchasePayment.setPurchaseDate(null);
	}

	@Transactional
	public void expirePurchases() {
		userPurchaseHistoryRepository.findExpired().forEach(this::expirePurchase);
	}

	@Transactional
	public void expirePurchase(UserPurchaseHistory userPurchaseHistory) {
		StrategySecuritiesCompany ssc = sscr.findByPkStrategyIdAndPkSecId(userPurchaseHistory.getStrategyId(), userPurchaseHistory.getSecId());
		if (userPurchaseHistory.getAutoYn() == 1) {
			ssc.setRemainingSellingUnit(ssc.getRemainingSellingUnit() + userPurchaseHistory.getQuantity());
			userPurchaseHistory.getStrategy().setRemainingAuto(userPurchaseHistory.getStrategy().getRemainingAuto()
					+ userPurchaseHistory.getQuantity());
		} else {
			userPurchaseHistory.getStrategy().setRemainingManual(userPurchaseHistory.getStrategy().getRemainingManual()
					+ userPurchaseHistory.getQuantity());
		}
		userPurchaseHistory.setSettleExpiry(true);
		UserStrategySetting userStrategySetting = spsRepository.findByPk(userPurchaseHistory.getStrategy().getId(), userPurchaseHistory.getUser().getId(), userPurchaseHistory.getSecuritiesCompany().getId(), userPurchaseHistory.isAuto());
		if (userStrategySetting != null) {
			userStrategySetting.setLotSize(userStrategySetting.getLotSize() - userPurchaseHistory.getQuantity());
		}
	}
}
