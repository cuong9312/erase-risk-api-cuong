package com.fintasoft.eraserisk.services.order;

import com.fintasoft.eraserisk.model.api.response.PaymentMethodResponse;
import com.fintasoft.eraserisk.model.db.PaymentMethod;
import com.fintasoft.eraserisk.repositories.PaymentMethodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PaymentMethodService {
    @Autowired
    PaymentMethodRepository paymentMethodRepository;

    public List<PaymentMethodResponse> findAll() {
        List<PaymentMethod> paymentMethods = paymentMethodRepository.findAll();
        return paymentMethods.stream().map(pm -> PaymentMethodResponse.from(pm)).collect(Collectors.toList());
    }
}
