package com.fintasoft.eraserisk.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.model.db.FaqGroup;
import com.fintasoft.eraserisk.repositories.FaqRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fintasoft.eraserisk.model.api.response.FaqGroupResponse;
import com.fintasoft.eraserisk.repositories.FaqGroupRepository;

@Service
public class FaqService {
    @Autowired
    FaqGroupRepository faqGroupRepository;

    @Autowired
    FaqRepository faqRepository;

    @Transactional
    public List<FaqGroupResponse> getAllFaqGroup(String lang) {
        List<FaqGroupResponse> result = new ArrayList<>();
        try (Stream<FaqGroup> sf = faqGroupRepository.findByLanguage(lang)){
            sf.forEach(s -> result.add(FaqGroupResponse.from(s)));
        }
        return result;
    }
}
