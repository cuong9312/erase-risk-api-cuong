package com.fintasoft.eraserisk.services.strategy;

import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.model.api.response.StrategyPositionResponse;
import com.fintasoft.eraserisk.model.db.StrategyPosition;
import com.fintasoft.eraserisk.repositories.StrategyPositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StrategyPositionService {
    @Autowired
    StrategyPositionRepository strategyPositionRepository;

    public StrategyPositionResponse getSafely(long strategyId, String category) {
        try {
            return get(strategyId, category);
        } catch (InvalidValueException e) {
            return null;
        }
    }

    public StrategyPositionResponse get(long strategyId, String category) {
        StrategyPosition strategyPosition = strategyPositionRepository.findById(new StrategyPosition.Pk(strategyId, category)).orElse(null);
        if (strategyPosition == null) {
            throw new InvalidValueException("strategyId");
        }

        return StrategyPositionResponse.from(strategyPosition);
    }
}
