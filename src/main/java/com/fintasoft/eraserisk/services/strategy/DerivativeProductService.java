package com.fintasoft.eraserisk.services.strategy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.ReportStatusEnum;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.exceptions.NotFoundException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.request.DerivativeProductPlatformRequest;
import com.fintasoft.eraserisk.model.api.request.DerivativeProductRequest;
import com.fintasoft.eraserisk.model.api.request.DerivativeProductSecRequest;
import com.fintasoft.eraserisk.model.api.response.DerivativeProductResponse;
import com.fintasoft.eraserisk.model.common.UpdateProduct;
import com.fintasoft.eraserisk.model.common.UpdateProductSec;
import com.fintasoft.eraserisk.model.db.*;
import com.fintasoft.eraserisk.repositories.*;
import com.fintasoft.eraserisk.repositories.impl.QueryParamRepository;
import com.fintasoft.eraserisk.services.queue.RequestResponseHandler;
import com.fintasoft.eraserisk.services.queue.StrategyPriceService;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;

@Service
public class DerivativeProductService {	
	@Autowired
	private QueryParamRepository queryParamRepository;
	
	@Autowired
	private DerivativeProductRepository derivativeProductRepository;
	
	@Autowired
	private DerivativeProductSecRepository derivativeProductSecRepository;
	
	@Autowired
	private DerivativeProductPlatformRepository derivativeProductPlatformRepository;
	
	@Autowired
	private SecuritiesCompanyRepository securitiesCompanyRepository;
	
	@Autowired
	private SignalPlatformRepository signalPlatformRepository;
	
	@Autowired
    private ReportRequestRepository reportRequestRepository;

	@Autowired
	private RequestResponseHandler queueHandler;

	@Autowired
	private StrategyPriceService strategyPriceService;

	@Autowired
	private StrategyRepository strategyRepository;

	@Autowired
	private AppConf appConf;

	public Long exportDerivativeProducts(List<QueryParam> params, String createdByUserId) throws ParseException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(params);
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setSqlParam(json);
		reportRequest.setCreatedByUserId(createdByUserId);
		reportRequest.setRootClass(DerivativeProduct.class.getCanonicalName());

		reportRequest.setStatus(ReportStatusEnum.PENDING);

		Map<String, String> exportFields = new LinkedHashMap<String, String>();
		reportRequest.setResponseClass(DerivativeProductResponse.class.getCanonicalName());
		exportFields.put("derivativeCode", "Derivative Code");
		exportFields.put("derivativeName", "Derivative Name");
		exportFields.put("tickValue", "Tick Size");
		exportFields.put("minMove", "Min Move");
		exportFields.put("expiryDate", "Expiration Date");
		exportFields.put("createdAt", "Registration Date");
		exportFields.put("margin", "Margin");
		exportFields.put("fee", "Fee");

		reportRequest.setExportField(mapper.writeValueAsString(exportFields));
		reportRequestRepository.save(reportRequest);

		return reportRequest.getId();
	}

	public Page<DerivativeProductResponse> findDerivativeProducts(List<QueryParam> params, Pageable pageable) throws ParseException, JsonProcessingException {
		Page<DerivativeProduct> derivativeProducts = queryParamRepository.find(params, pageable, DerivativeProduct.class);
		List<DerivativeProductResponse> list = new ArrayList<>();
		Map<Long, DerivativeProductResponse> map = new HashMap<>();
		List<Long> ids = new ArrayList<>();
		derivativeProducts.getContent().forEach(up -> {
			DerivativeProductResponse response = DerivativeProductResponse.from(up);
			map.put(response.getId(), response);
			ids.add(response.getId());
			list.add(response);
		});
		Map<Long, Timestamp> productLastUpdatedTime = new HashMap<>();
		strategyRepository.findByProductIdIn(ids).forEach(strategy -> {
			Timestamp time = strategyPriceService.getUpdatedTime(strategy.getId());
			if (time != null && (!productLastUpdatedTime.containsKey(strategy.getProductId()) || time.after(productLastUpdatedTime.get(strategy.getProductId())))) {
				productLastUpdatedTime.put(strategy.getProductId(), time);
			}
		});
		productLastUpdatedTime.forEach((productId, time) -> map.get(productId).setLastUpdatedPrice(ConvertUtils.fromTime(time)));
		Page<DerivativeProductResponse> result = new PageImpl(list, derivativeProducts.getPageable(), derivativeProducts.getTotalElements());
		return result;
	}
	
	@com.fintasoft.eraserisk.annotations.Transactional
    public DerivativeProductResponse registerProduct(Long derivativeProductId, DerivativeProductRequest derivativeProductRequest) {
		DerivativeProduct derivativeProduct = new DerivativeProduct();
		if (derivativeProductId != null) {
			derivativeProduct = derivativeProductRepository.findById(derivativeProductId).orElse(null);
			if (derivativeProduct == null) {
				throw new NotFoundException("DerivativeProduct");
			}
		}

		List<DerivativeProduct> sameNameOrCodeProducts = derivativeProductRepository.findByDerivativeNameOrDerivativeCode(derivativeProductRequest.getDerivativeName(), derivativeProductRequest.getDerivativeCode());
		if (!CollectionUtils.isEmpty(sameNameOrCodeProducts)) {
			for (int i = 0; i < sameNameOrCodeProducts.size(); i++) {
				DerivativeProduct item = sameNameOrCodeProducts.get(i);
				if (derivativeProductId == null || item.getId() != derivativeProductId) {
					if (derivativeProductRequest.getDerivativeName().equals(item.getDerivativeName())) {
						throw new InvalidValueException("derivativeName", "error.already_in_use", new String[]{ "Derivative Name" });
					}
					if (derivativeProductRequest.getDerivativeCode().equals(item.getDerivativeCode())) {
						throw new InvalidValueException("derivativeCode", "error.already_in_use", new String[]{ "Derivative Code" });
					}
				}
			}

		}

		derivativeProduct.setDerivativeName(derivativeProductRequest.getDerivativeName());
		derivativeProduct.setDerivativeCode(derivativeProductRequest.getDerivativeCode());
		
		Date parsedDate = ConvertUtils.toDate(derivativeProductRequest.getExpiryDate(), "expiryDate");
        if (parsedDate != null) {
        	derivativeProduct.setExpiryDate(new java.sql.Date(parsedDate.getTime()));
        }
        
		derivativeProduct.setMargin(derivativeProductRequest.getMargin());
		derivativeProduct.setMddRate(derivativeProductRequest.getMddRate());
		derivativeProduct.setTickValue(derivativeProductRequest.getTickValue());
		derivativeProduct.setUnitValue(derivativeProductRequest.getUnitValue());
		derivativeProduct.setFee(derivativeProductRequest.getFee());
		derivativeProduct.setMinMove(derivativeProductRequest.getMinMove());
		derivativeProduct.setTimezone(derivativeProductRequest.getTimezone());

		derivativeProduct = derivativeProductRepository.save(derivativeProduct);

		if (derivativeProductId != null) {
			derivativeProductSecRepository.deleteAll(derivativeProduct.getSecuritiesCompanies());
			derivativeProductPlatformRepository.deleteAll(derivativeProduct.getSignalPlatforms());
		}

		List<UpdateProductSec> secs = new ArrayList<>();
		List<DerivativeProductSec> securitiesCompanies = new ArrayList<>();
		if (derivativeProductRequest.getSecuritiesCompanies() != null) {
			DerivativeProductSecRequest[] derivativeProductSecRequests = derivativeProductRequest.getSecuritiesCompanies();
			for (int i = 0; i < derivativeProductSecRequests.length; i++) {
				SecuritiesCompany securitiesCompany = securitiesCompanyRepository.findById(derivativeProductSecRequests[i].getId()).orElse(null);
				if (securitiesCompany != null) {
					DerivativeProductSec derivativeProductSec = new DerivativeProductSec();
					derivativeProductSec.setDerivativeProduct(derivativeProduct);
					derivativeProductSec.setSecuritiesCompany(securitiesCompany);
					derivativeProductSec.setClientCode(derivativeProductSecRequests[i].getClientCode());
					securitiesCompanies.add(derivativeProductSec);
					secs.add(new UpdateProductSec(securitiesCompany.getId(), securitiesCompany.getSecCode(), derivativeProductSec.getClientCode()));
				} else {
					throw new InvalidValueException("secIds[" + i + "]");
				}
			}
		}

		derivativeProductSecRepository.saveAll(securitiesCompanies);
		
		List<DerivativeProductPlatform> signaPlatforms = new ArrayList<>();
		if (derivativeProductRequest.getSignalPlatforms() != null) {
			DerivativeProductPlatformRequest[] derivativeProductPlatformRequests = derivativeProductRequest.getSignalPlatforms();
			for (int i = 0; i < derivativeProductPlatformRequests.length; i++) {
				SignalPlatform signalPlatform = signalPlatformRepository.findById(derivativeProductPlatformRequests[i].getId()).orElse(null);
				if (signalPlatform != null) {
					DerivativeProductPlatform derivativeProductPlatform = new DerivativeProductPlatform();
					derivativeProductPlatform.setDerivativeProduct(derivativeProduct);
					derivativeProductPlatform.setSignalPlatform(signalPlatform);
					derivativeProductPlatform.setTradingCode(derivativeProductPlatformRequests[i].getTradingCode());
					signaPlatforms.add(derivativeProductPlatform);
				} else {
					throw new InvalidValueException("providerIds[" + i + "]");
				}
			}
		}

		derivativeProductPlatformRepository.saveAll(signaPlatforms);
		if (derivativeProductId != null) {
			UpdateProduct data = new UpdateProduct(derivativeProduct.getId(), derivativeProduct.getDerivativeCode(), secs);
			queueHandler.sendMessageSafe(
					appConf.getQueue().getTopics().getDerivativeProductUpdate(),
					"UPDATE_PRODUCT_SEC",
					data);
		}
		
		return DerivativeProductResponse.from(derivativeProduct);
	}
}
