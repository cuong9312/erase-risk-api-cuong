package com.fintasoft.eraserisk.services.strategy;


import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.response.DailyProfitsResponse;
import com.fintasoft.eraserisk.model.db.DailyProfit;
import com.fintasoft.eraserisk.repositories.DailyProfitRepository;
import com.fintasoft.eraserisk.repositories.impl.QueryParamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service
public class DailyProfitService {
    @Autowired
    private DailyProfitRepository dailyProfitRepository;
    @Autowired
    private QueryParamRepository queryParamRepository;

    public List<DailyProfitsResponse> getDailyProfitsIn(List<Long> strategyIds, Timestamp start, Timestamp end,
                                                        boolean profit, boolean roi, boolean trade,
                                                        boolean count) throws ParseException {
        List<QueryParam> queryParams = new ArrayList<>();
        if (!CollectionUtils.isEmpty(strategyIds)) {
            queryParams.add(QueryParam.create("pk.strategyId", strategyIds, Long.class));
        }
        if (start != null || end != null) {
            QueryParam.createMinMaxDate(queryParams, "pk.date", start, end);
        }

        Sort sort = Sort.by(Sort.Direction.ASC, "pk.strategyId", "pk.date");

        Stream<DailyProfit> dps = queryParamRepository.find(queryParams, sort, DailyProfit.class);
        List<DailyProfitsResponse> results = new ArrayList<>();
        dps.forEach(dp -> add(results, dp, profit, roi, trade, count));
        return results;
    }

    public void add(List<DailyProfitsResponse> reponses, DailyProfit dailyProfit, boolean profit, boolean roi, boolean trade,
                    boolean count) {
        DailyProfitsResponse last = null;
        if (!reponses.isEmpty()) {
            last = reponses.get(reponses.size() - 1);
        }
        if (last == null || dailyProfit.getPk().getStrategyId() != last.getStrategyId()) {
            last = new DailyProfitsResponse(dailyProfit.getPk().getStrategyId());
            last.setProfit(profit);
            last.setRoi(roi);
            last.setTrade(trade);
            last.setCount(count);
            reponses.add(last);
        }
        last.add(dailyProfit);
    }
}
