package com.fintasoft.eraserisk.services.strategy;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fintasoft.eraserisk.model.api.response.SecuritiesCompanyResponse;
import com.fintasoft.eraserisk.repositories.SecuritiesCompanyRepository;

@Service
public class SecuritiesCompanyService {
    @Autowired
    SecuritiesCompanyRepository securitiesCompanyRepository;

    public List<SecuritiesCompanyResponse> getAll() {
        List<SecuritiesCompanyResponse> result = new ArrayList<>();
        securitiesCompanyRepository.findAll().forEach(s -> result.add(SecuritiesCompanyResponse.from(s)));
        return result;
    }

    public List<SecuritiesCompanyResponse> getAll(List<Long> ids) {
        List<SecuritiesCompanyResponse> result = new ArrayList<>();
        securitiesCompanyRepository.findByIdIn(ids).forEach(s -> result.add(SecuritiesCompanyResponse.from(s)));
        return result;
    }
    
    public List<SecuritiesCompanyResponse> findByStrategyId(long strategyId) {
        List<SecuritiesCompanyResponse> result = new ArrayList<>();
        securitiesCompanyRepository.findByStrategiesStrategyId(strategyId).forEach(s -> result.add(SecuritiesCompanyResponse.from(s)));
        return result;
    }
}
