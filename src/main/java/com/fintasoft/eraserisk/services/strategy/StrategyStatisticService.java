package com.fintasoft.eraserisk.services.strategy;


import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.SignalEnum;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.model.api.response.DailyProfitResponse;
import com.fintasoft.eraserisk.model.api.response.QueryDailyProfitResponse;
import com.fintasoft.eraserisk.model.api.response.StrategyStatisticRangeResponse;
import com.fintasoft.eraserisk.model.db.DailyProfit;
import com.fintasoft.eraserisk.model.db.DerivativeProduct;
import com.fintasoft.eraserisk.model.db.StrategySignalData;
import com.fintasoft.eraserisk.repositories.DailyProfitRepository;
import com.fintasoft.eraserisk.repositories.StrategyRepository;
import com.fintasoft.eraserisk.repositories.StrategySignalDataRepository;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StrategyStatisticService {

    @Autowired
    private StrategySignalDataRepository strategySignalDataRepository;

    @Autowired
    private StrategyRepository strategyRepository;

    @Autowired
    private AppConf appConf;

    @Autowired
    private DailyProfitRepository dailyProfitRepository;

    public Map<Long, StrategyStatisticRangeResponse> calculateByRange(List<Long> strategyIds, Timestamp minTime, Timestamp maxTime) {
        Map<Long, StrategyStatisticRangeResponse> responseMap = new HashMap<>();
        strategyIds.forEach(id -> {
            if (id == null) throw new InvalidValueException("strategyIds");
            responseMap.put(id, this.calculateByRange(id, minTime, maxTime));
        });
        return responseMap;
    }

    public StrategyStatisticRangeResponse calculateByRange(long strategyId, Timestamp minTime, Timestamp maxTime) {
        List<StrategySignalData> signalDatas = strategySignalDataRepository.findByStrategyIdAndTradingAtBetweenAndDeletedFalseOrderByTradingAtAsc(strategyId, minTime, maxTime);
        int noOfSignal = signalDatas.size();
        double cumulativeProfit = 0;
        double highestCumulativeProfit = 0;
        double maxDrawDown = 0;
        int noOfNotExit = 0;
        for (int i = 0; i < signalDatas.size(); i ++) {
            StrategySignalData signalData = signalDatas.get(i);
            if (signalData.getSignal() == SignalEnum.Long || signalData.getSignal() == SignalEnum.Short) {
                noOfNotExit++;
            }
            if (i == 0) continue;
            cumulativeProfit = signalData.getCumulativeProfit();
            if (cumulativeProfit > highestCumulativeProfit) {
                highestCumulativeProfit = cumulativeProfit;
            }
            double drawDown = highestCumulativeProfit - cumulativeProfit;
            if (drawDown > maxDrawDown) {
                maxDrawDown = drawDown;
            }
        }

        double rangeProfit = cumulativeProfit;
        DerivativeProduct derivativeProduct = strategyRepository.findById(strategyId).get().getDerivativeProduct();
        double requiredCapital = derivativeProduct.getMargin() + maxDrawDown * derivativeProduct.getMddRate();
        double roi = rangeProfit / requiredCapital;

        return StrategyStatisticRangeResponse.builder()
                .lastCumulativeProfit(cumulativeProfit)
                .profit(rangeProfit)
                .maxDrawDown(maxDrawDown)
                .requiredCapital(requiredCapital)
                .roi(roi)
                .noOfSignal(noOfSignal)
                .noOfNotExit(noOfNotExit)
                .build();
    }

    public List<QueryDailyProfitResponse> queryDailyProfit(List<Long> strategyIds, Timestamp start, Timestamp end) {
        List<DailyProfit> dailyProfits = dailyProfitRepository.findByPkStrategyIdInAndPkDateBetweenOrderByPkDateAsc(strategyIds, start, end);
        List<QueryDailyProfitResponse> result = new ArrayList<>();
        Date currentDate = null;
        QueryDailyProfitResponse current = new QueryDailyProfitResponse();
        for (DailyProfit dailyProfit : dailyProfits) {
            if (currentDate != null && currentDate.getTime() != dailyProfit.getPk().getDate().getTime()) {
                // move to another date
                current.setDate(ConvertUtils.fromDate(currentDate));
                result.add(current);
                current = new QueryDailyProfitResponse();
            }
            currentDate = dailyProfit.getPk().getDate();
            current.getStrategies().add(DailyProfitResponse.from(dailyProfit));
        }
        if (currentDate != null) {
            current.setDate(ConvertUtils.fromDate(currentDate));
            result.add(current);
        }
        return result;
    }
}
