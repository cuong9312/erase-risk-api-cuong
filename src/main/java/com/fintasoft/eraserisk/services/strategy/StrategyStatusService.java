package com.fintasoft.eraserisk.services.strategy;

import java.util.ArrayList;
import java.util.List;

import com.fintasoft.eraserisk.model.db.StrategyStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fintasoft.eraserisk.model.api.response.StrategyStatusResponse;
import com.fintasoft.eraserisk.repositories.StrategyStatusRepository;

@Service
public class StrategyStatusService {	
	@Autowired
	private StrategyStatusRepository strategyStatusRepository;

    private List<StrategyStatusResponse> all = null;
    private List<StrategyStatus> allEntities = null;
    private StrategyStatusResponse  activated;
    private StrategyStatus  activatedEntity;

	public StrategyStatusResponse getActivated() {
	    if (activated == null) {
	        for (StrategyStatusResponse status : this.getAll()) {
	            if (status.getStatus().equalsIgnoreCase("ACTIVATED")) {
	                activated = status;
	                break;
                }
            }
        }
        return activated;
    }

	public StrategyStatus getStatus(long statusId) {
	    this.getAll();
	    return this.allEntities.stream().filter(s -> s.getId() == statusId).findFirst().orElse(null);
    }

	public StrategyStatus getActivatedEntity() {
	    if (activatedEntity == null) {
	        this.getAll();
	        for (StrategyStatus status : this.allEntities) {
	            if (status.getStatus().equalsIgnoreCase("ACTIVATED")) {
	                activatedEntity = status;
	                break;
                }
            }
        }
        return activatedEntity;
    }

	public List<StrategyStatusResponse> getAll() {
	    if (all == null) {
	        all = new ArrayList<>();
	        allEntities = new ArrayList<>();
            strategyStatusRepository.findAll().forEach(s -> {
                all.add(StrategyStatusResponse.from(s));
                allEntities.add(s);
            });
        }

        return all;
    }
}
