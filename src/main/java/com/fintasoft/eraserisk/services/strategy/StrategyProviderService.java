package com.fintasoft.eraserisk.services.strategy;

import java.util.ArrayList;
import java.util.List;

import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.exceptions.TargetNotFoundException;
import com.fintasoft.eraserisk.model.api.request.StrategyProviderRequest;
import com.fintasoft.eraserisk.model.db.StrategyProvider;
import com.fintasoft.eraserisk.model.db.User;
import com.fintasoft.eraserisk.repositories.UserRepository;
import com.fintasoft.eraserisk.services.queue.StrategyPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fintasoft.eraserisk.model.api.response.StrategyProviderResponse;
import com.fintasoft.eraserisk.repositories.StrategyProviderRepository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Service
public class StrategyProviderService {	
	@Autowired
	private StrategyProviderRepository strategyProviderRepository;
	@Autowired
	private StrategyPriceService strategyPriceService;
	@Autowired
	private UserRepository userRepository;

	public List<StrategyProviderResponse> getAll() {
        return getAll(true);
    }

	public List<StrategyProviderResponse> getAll(boolean isShort) {
        List<StrategyProviderResponse> result = new ArrayList<>();
        strategyProviderRepository.findAll().forEach(s -> result.add(
                isShort ? StrategyProviderResponse.shortFrom(s) : StrategyProviderResponse.from(s)
        ));
        return result;
    }

	public StrategyProviderResponse getProvider(Long providerId) {
        StrategyProvider provider = strategyProviderRepository.findById(providerId).orElse(null);
        if (provider == null) {
            throw new TargetNotFoundException();
        }
        return StrategyProviderResponse.from(provider);
    }

	public List<StrategyProviderResponse> getAll(List<Long> providerIds) {
        return getAll(providerIds, true);
    }

	public List<StrategyProviderResponse> getAll(List<Long> providerIds, boolean isShort) {
        List<StrategyProviderResponse> result = new ArrayList<>();
        strategyProviderRepository.findByIdIn(providerIds).forEach(s -> result.add(
                isShort ? StrategyProviderResponse.shortFrom(s) : StrategyProviderResponse.from(s)));
        return result;
    }

	public StrategyProviderResponse get(Long providerId) {
        StrategyProvider strategyProvider = strategyProviderRepository.findById(providerId).orElse(null);
        if (strategyProvider == null) {
            throw new TargetNotFoundException();
        }

        return StrategyProviderResponse.detailFrom(strategyProvider);
    }

    @Transactional
    public StrategyProviderResponse register(Long id, StrategyProviderRequest request) {
	    StrategyProvider provider = new StrategyProvider();
	    if (id == null && StringUtils.isEmpty(request.getCompanyImage())) {
	        throw new InvalidValueException("companyImage");
        }

	    if (id != null) {
            provider = strategyProviderRepository.findById(id).orElse(null);
            if (provider == null) {
                throw new InvalidValueException("providerId");
            }
        }
        List<StrategyProvider> listProvidersByCompanyName = strategyProviderRepository.findByCompanyName(request.getCompanyName());
	    if (!CollectionUtils.isEmpty(listProvidersByCompanyName)) {
	        for (int i = 0; i < listProvidersByCompanyName.size(); i++) {
	            if (id == null || id .longValue() != listProvidersByCompanyName.get(i).getId()) {
                    throw new InvalidValueException("companyName", "error.already_in_use", new String[]{ "Company Name" });
                }
            }
        }
        List<StrategyProvider> listProvidersByCompanyNameKr = strategyProviderRepository.findByCompanyNameKr(request.getCompanyNameKr());
	    if (!CollectionUtils.isEmpty(listProvidersByCompanyNameKr)) {
	        for (int i = 0; i < listProvidersByCompanyNameKr.size(); i++) {
	            if (id == null || id .longValue() != listProvidersByCompanyNameKr.get(i).getId()) {
                    throw new InvalidValueException("companyNameKr", "error.already_in_use", new String[]{ "Company Name Kr" });
                }
            }
        }
        if (!StringUtils.isEmpty(request.getCompanyImage())) {
            provider.setCompanyImage(request.getCompanyImage());
        }
        provider.setCompanyName(request.getCompanyName());
        provider.setCompanyNameKr(request.getCompanyNameKr());
        provider.setLongDescription(request.getLongDescription());
        provider.setLongDescriptionKr(request.getLongDescriptionKr());
        provider.setShortDescription(request.getShortDescription());
        provider.setShortDescriptionKr(request.getShortDescriptionKr());
        provider.setNote(request.getNote());
        provider.setNoteKr(request.getNoteKr());
        strategyProviderRepository.save(provider);
        if (request.getCreatedByUserId() != null) {
            User user = userRepository.findById(request.getCreatedByUserId()).orElse(null);
            if (user == null) {
                throw new InvalidValueException("createdByUserId");
            }
            if (id == null) {
                user.getAdminStrategyProviders().add(provider);
                userRepository.save(user);
            }
        }
        return StrategyProviderResponse.from(provider);
    }
}
