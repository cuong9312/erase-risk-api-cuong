package com.fintasoft.eraserisk.services.strategy;

import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import com.fintasoft.eraserisk.constances.PositionTypeEnums;
import com.fintasoft.eraserisk.constances.SignalEnum;
import com.fintasoft.eraserisk.exceptions.GeneralException;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.exceptions.TargetNotFoundException;
import com.fintasoft.eraserisk.model.UserStrategySetting;
import com.fintasoft.eraserisk.model.api.request.PurchaseAutoRequest;
import com.fintasoft.eraserisk.model.api.response.*;
import com.fintasoft.eraserisk.model.api.response.ComplexUserStrategySettingResponse;
import com.fintasoft.eraserisk.model.db.Strategy;
import com.fintasoft.eraserisk.model.db.User;
import com.fintasoft.eraserisk.model.query.UserStrategySettingQuery;
import com.fintasoft.eraserisk.repositories.*;
import com.fintasoft.eraserisk.repositories.impl.UserStrategyPurchaseSettingRepositoryImpl;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import com.fintasoft.eraserisk.utils.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.stream.Collectors;

@Service
public class UiMyStrategyService {

    @Autowired
    UserStrategyPurchaseSettingRepositoryImpl uspsRepository;

    @Autowired
    UserStrategySettingRepository ussRepository;

    @Autowired
    StrategyRepository strategyRepository;

    @Autowired
    DailyProfitRepository dailyProfitRepository;

    @Autowired
    SecuritiesCompanyRepository securitiesCompanyRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserStrategySettingRepository userStrategySettingRepository;


    @Transactional
    public PureUserStrategySettingResponse updateAutoOnOff(long userId, long strategyId, long secId, boolean isAuto, PurchaseAutoRequest request) throws ParseException {
        com.fintasoft.eraserisk.model.UserStrategySetting userStrategySetting = ussRepository.findByPk(strategyId, userId, secId, isAuto);
        if (userStrategySetting == null) throw new TargetNotFoundException();
        if (request == null || request.getAutoDisabled() == null) {
            userStrategySetting.setAutoDisabled(!userStrategySetting.isAutoDisabled());
        } else {
            userStrategySetting.setAutoDisabled(request.getAutoDisabled().booleanValue());
            if (!userStrategySetting.isAutoDisabled()) {
                if (request.getPositionType() != null) {
                    userStrategySetting.setPositionType(PositionTypeEnums.valueOf(request.getPositionType()));
                    userStrategySetting.setBalance(request.getBalance());
                } else {
                    userStrategySetting.setPositionType(PositionTypeEnums.None);
                    userStrategySetting.setBalance(0);
                }
            }
        }
        return PureUserStrategySettingResponse.from(userStrategySetting);
    }

    @Transactional
    public PureUserStrategySettingResponse updateLotSize(long userId, long strategyId, long secId, boolean isAuto, int lotSize) {
        com.fintasoft.eraserisk.model.UserStrategySetting result = ussRepository.findByPk(strategyId, userId, secId, isAuto);
        if (result == null) throw new TargetNotFoundException();
        result.setLotSize(lotSize);
        return PureUserStrategySettingResponse.from(result);
    }

    @Transactional
    public PureUserStrategySettingResponse updateUserStrategySetting(long userId, long strategyId, long secId, String orderAction) {
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new InvalidValueException("userId");
        }

        UserStrategySetting userStrategySetting = userStrategySettingRepository.findByPk(strategyId, userId, secId, true);
        if (userStrategySetting == null) {
            throw new InvalidValueException("strategyId");
        }

        if (userStrategySetting.getLotSize() == 0) {
            throw new GeneralException(ErrorCodeEnums.ORDER_FAILED.name(), "error.order_failed", null);
        }

        if ((orderAction.equalsIgnoreCase(SignalEnum.ExitLong.name()) && !userStrategySetting.getPositionType().equals(PositionTypeEnums.Long))
                || (orderAction.equalsIgnoreCase(SignalEnum.ExitShort.name()) && !userStrategySetting.getPositionType().equals(PositionTypeEnums.Short))) {
            return PureUserStrategySettingResponse.from(userStrategySetting);
        }

        if (SignalEnum.ExitLong.name().equals(orderAction) || SignalEnum.ExitShort.name().equals(orderAction)) {
            userStrategySetting.setPositionType(PositionTypeEnums.None);
            userStrategySetting.setBalance(0);
        } else {
            userStrategySetting.setPositionType(PositionTypeEnums.valueOf(orderAction));
            userStrategySetting.setBalance(userStrategySetting.getLotSize());
        }
        return PureUserStrategySettingResponse.from(userStrategySettingRepository.save(userStrategySetting));
    }

    public Page<ComplexUserStrategySettingResponse> findSetting(long userId, Pageable pageable) {
        Page<UserStrategySettingQuery> data = uspsRepository.queryActive(userId, pageable);
        return convert(data);
    }

    public Page<ComplexUserStrategySettingResponse> findExpiredSetting(long userId, Pageable pageable) {
        Page<UserStrategySettingQuery> data = uspsRepository.queryExpired(userId, pageable);
        return convert(data);
    }


    public Page<UserStrategyPurchase> findMyStrategies(long userId, int dailyProfitLength, Pageable pageable) {
        Page<UserStrategySettingQuery> data = uspsRepository.queryActive(userId, pageable);
        Timestamp dailyProfit = new Timestamp(TimeUtils.getStartOfDate(new Date(
                System.currentTimeMillis() - dailyProfitLength * TimeUtils.MILISECONDS_OF_A_DAY)).getTime());
        return convert(data, dailyProfit);
    }


    public Page<UserStrategyPurchase> findMyExpiredStrategies(long userId, int dailyProfitLength, Pageable pageable) {
        Page<UserStrategySettingQuery> data = uspsRepository.queryExpired(userId, pageable);
        Timestamp dailyProfit = new Timestamp(TimeUtils.getStartOfDate(new Date(
                System.currentTimeMillis() - dailyProfitLength * TimeUtils.MILISECONDS_OF_A_DAY)).getTime());
        return convert(data, dailyProfit);
    }

    private Page<ComplexUserStrategySettingResponse> convert(Page<UserStrategySettingQuery> data) {
        return data.map(u -> {
            ComplexUserStrategySettingResponse r = u.to();
            Strategy strategy = strategyRepository.findById(u.getStrategyId()).get();
            r.setStrategy(StrategyResponse.fromBasic(null, strategy));
            r.setStrategyProvider(StrategyProviderResponse.from(strategy.getStrategyProvider()));
            r.setProduct(DerivativeProductResponse.from(strategy.getDerivativeProduct()));
            r.setSecuritiesCompany(SecuritiesCompanyResponse.from(securitiesCompanyRepository.getOne(u.getSecId())));

            return r;
        });
    }

    private Page<UserStrategyPurchase> convert(Page<UserStrategySettingQuery> data, Timestamp dailyProfit) {
        return data.map(u -> {
            UserStrategyPurchase i = new UserStrategyPurchase();
            i.setAvailableQuantity(u.getAvailableQuantity());
            i.setMaxExpiredDate(ConvertUtils.fromTime(u.getExpiredTime()));
            Strategy strategy = strategyRepository.findById(u.getStrategyId()).get();
            i.setDailyProfits(
                    dailyProfitRepository.
                            findByPkStrategyIdAndPkDateBetweenOrderByPkDateAsc(
                                    u.getStrategyId(),
                                    dailyProfit,
                                    new Timestamp(System.currentTimeMillis())
                            ).stream().map(d -> DailyProfitResponse.from(d)).collect(Collectors.toList())
            );
            i.setStrategy(StrategyResponse.fromBasic(null, strategy));
            i.setStrategyStatistic(StrategyStatisticResponse.from(strategy.getStrategyStatistic()));
            i.setProduct(DerivativeProductResponse.fromBasic(null, strategy.getDerivativeProduct()));
            i.setProvider(StrategyProviderResponse.shortFrom(strategy.getStrategyProvider()));
            i.setSecuritiesCompany(SecuritiesCompanyResponse.from(securitiesCompanyRepository.getOne(u.getSecId())));
            i.setUserId(u.getUserId());
            i.setAuto(u.isAuto());

            return i;
        });
    }
}
