package com.fintasoft.eraserisk.services.strategy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.*;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.exceptions.NotFoundException;
import com.fintasoft.eraserisk.exceptions.TargetNotFoundException;
import com.fintasoft.eraserisk.job.DailyProfitCalculateJob;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.request.StrategyData;
import com.fintasoft.eraserisk.model.api.request.StrategyRegister;
import com.fintasoft.eraserisk.model.api.request.StrategySecuritiesCompanyRequest;
import com.fintasoft.eraserisk.model.api.request.StrategySignalDataRequest;
import com.fintasoft.eraserisk.model.api.response.DailyProfitResponse;
import com.fintasoft.eraserisk.model.api.response.StrategyPositionResponse;
import com.fintasoft.eraserisk.model.api.response.StrategyResponse;
import com.fintasoft.eraserisk.model.api.response.StrategySignalDataResponse;
import com.fintasoft.eraserisk.model.db.*;
import com.fintasoft.eraserisk.repositories.*;
import com.fintasoft.eraserisk.repositories.impl.StrategySignalDataRepositoryImpl;
import com.fintasoft.eraserisk.repositories.impl.StrategyRepositoryImpl;
import com.fintasoft.eraserisk.services.queue.RequestResponseHandler;
import com.fintasoft.eraserisk.utils.CalculateUtils;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import com.fintasoft.eraserisk.utils.DoubleUtils;
import com.fintasoft.eraserisk.utils.TimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class StrategyService {
	private final static Logger log = LoggerFactory.getLogger(StrategyService.class);

	@Autowired
	private StrategyRepository strategyRepository;

	@Autowired
	private StrategyRepositoryImpl strategyRepositoryImpl;
	
	@Autowired
	private StrategyProviderRepository strategyProviderRepository;
	
	@Autowired
	private SignalPlatformRepository signalPlatformRepository;
	
	@Autowired
	private StrategyStatusRepository strategyStatusRepository;
	
	@Autowired
	private SecuritiesCompanyRepository securitiesCompanyRepository;

	@Autowired
	private StrategySecuritiesCompanyRepository sscRepository;
	
	@Autowired
	private StrategySignalDataRepository signalDataRepository;
	
	@Autowired
	private StrategyStatisticRepository statisticRepository;
	
	@Autowired
	private DerivativeProductRepository derivativeProductRepository;

	@Autowired
	private StrategySignalDataRepository ssdRepository;

	@Autowired
	private StrategySignalDataRepositoryImpl ssdRepositoryImpl;
	
	@Autowired
    private ReportRequestRepository reportRequestRepository;

	@Autowired
    private DailyProfitCalculateJob dailyProfitCalculateJob;

	@Autowired
    private DailyProfitRepository dailyProfitRepository;

	@Autowired
    private StrategyPositionRepository strategyPositionRepository;

    @Autowired
    private RequestResponseHandler queueHandler;

    @Autowired
    private StrategyStatisticRepository strategyStatisticRepository;

	@Autowired
    private AppConf appConf;

	@Autowired
    private StrategyStatusService strategyStatusService;

	public Page<StrategyResponse> findAll(Long strategyProviderId, String strategyName, Pageable pageable) {
		Page<Strategy> strategies = strategyRepositoryImpl.searchStrategies(strategyProviderId, strategyName, pageable);
				
		List<StrategyResponse> list = new ArrayList<>();
		Long[] ids = new Long[strategies.getContent().size()];
		Map<Long, StrategyResponse> map = new HashMap<>();
		for (int i = 0; i < strategies.getContent().size(); i ++) {
			Strategy strategy = strategies.getContent().get(i);
			ids[i] = strategy.getId();
			StrategyResponse strategyResponse = StrategyResponse.from(strategy, true);
			strategyResponse.setSignalDatas(new ArrayList<>());
			list.add(strategyResponse);
			map.put(strategy.getId(), strategyResponse);
		}
		if (ids.length > 0) {
			List<StrategySignalData> signalDatas = ssdRepositoryImpl.findByStrategyIdAndLimit(7, ids);
			for (int i = 0; i < signalDatas.size(); i ++) {
				StrategySignalData signalData = signalDatas.get(i);
				map.get(signalData.getStrategyId()).getSignalDatas().add(StrategySignalDataResponse.from(signalData));
			}
		}
        Page<StrategyResponse> result = new PageImpl(list, strategies.getPageable(), strategies.getTotalElements());
		return result;
	}

	public Page<StrategyResponse> findStrategiesWithSignalDatas(List<QueryParam> params, Pageable pageable) throws ParseException, JsonProcessingException {
		Page<StrategyResponse> result = this.findStrategies(params, pageable);

		Long[] ids = new Long[result.getContent().size()];
		Map<Long, StrategyResponse> map = new HashMap<>();
		for (int i = 0; i < result.getContent().size(); i ++) {
			StrategyResponse strategyResponse = result.getContent().get(i);
			ids[i] = strategyResponse.getId();
			map.put(ids[i], strategyResponse);
			strategyResponse.setSignalDatas(new ArrayList<>());
		}
		if (ids.length > 0) {
			List<StrategySignalData> signalDatas = ssdRepositoryImpl.findByStrategyIdAndLimit(7, ids);
			for (int i = 0; i < signalDatas.size(); i ++) {
				StrategySignalData signalData = signalDatas.get(i);
				map.get(signalData.getStrategyId()).getSignalDatas().add(StrategySignalDataResponse.from(signalData));
			}
		}
		return result;
	}

	@Transactional
	public Page<StrategyResponse> findStrategiesWithDailyProfits(
			int dailyProfitsRange,
			List<QueryParam> params,
			String orderCategory,
			Pageable pageable
	) throws ParseException, JsonProcessingException {
		Integer limitDays = null;
		if (!StringUtils.isEmpty(orderCategory)) {
			if (orderCategory.equals(Constants.ROI_YEAR_TO_DAY)) {
				Calendar calendar = Calendar.getInstance();
				limitDays = calendar.get(Calendar.DAY_OF_YEAR);
			} else {
				limitDays = Integer.parseInt(orderCategory.substring((4)));
			}
		}
		Page<StrategyResponse> result = this.findStrategies(params, pageable, limitDays);
		return updateStrategyResponse(result, dailyProfitsRange, orderCategory);
	}

	@Transactional
	public Page<StrategyResponse> updateStrategyResponse(Page<StrategyResponse> result,
									   Integer dailyProfitsRange,
									   String orderCategory) {
		Map<Long, StrategyResponse> strategyMap = result.getContent().stream().collect(Collectors.toMap(s -> s.getId(), s -> s));
		if (dailyProfitsRange != null) {
			Timestamp now = new Timestamp(System.currentTimeMillis());
			Timestamp start = new Timestamp(TimeUtils.getStartOfDate(new Date(
					System.currentTimeMillis() - (long) dailyProfitsRange * TimeUtils.MILISECONDS_OF_A_DAY)).getTime());
			List<DailyProfit> dailyProfits = dailyProfitRepository.findByPkStrategyIdInAndPkDateBetweenOrderByPkStrategyIdAscPkDateDesc(strategyMap.keySet(), start, now);
			Long currentId = null;
			List<DailyProfitResponse> strategyDailyProfits = new ArrayList<>();

			for (int i = dailyProfits.size() - 1; i >= -1; i--) {
				if (currentId != null && (i == -1 || currentId != dailyProfits.get(i).getPk().getStrategyId())) {
					strategyMap.get(currentId).setDailyProfits(strategyDailyProfits);
					strategyDailyProfits = new ArrayList<>();
				}
				if (i == -1) break;
				currentId = dailyProfits.get(i).getPk().getStrategyId();
				strategyDailyProfits.add(DailyProfitResponse.from(dailyProfits.get(i)));
			}
		}

		if (!StringUtils.isEmpty(orderCategory)) {
			try (Stream<StrategyPosition> sp = strategyPositionRepository.findByPkCategoryAndPkStrategyIdIn(orderCategory, strategyMap.keySet())) {
				sp.forEachOrdered(p -> {
					strategyMap.get(p.getPk().getStrategyId()).setOrderedPosition(StrategyPositionResponse.from(p));
				});
			}
		}

		return result;
	}

	public Page<StrategyResponse> findStrategies(List<QueryParam> params, Pageable pageable) throws ParseException, JsonProcessingException {
		return findStrategies(params, pageable, null);
	}

	public Page<StrategyResponse> findStrategies(List<QueryParam> params, Pageable pageable, Integer limitDays) throws ParseException, JsonProcessingException {
		return findStrategies(params, pageable, limitDays, s -> StrategyResponse.from(s, true, limitDays));
	}

	public Page<StrategyResponse> findStrategies(List<QueryParam> params, Pageable pageable, Integer limitDays, Function<Strategy, StrategyResponse> transform) throws ParseException, JsonProcessingException {
			Page<Strategy> strategies = strategyRepositoryImpl.findStrategies(params, pageable);
	
			List<StrategyResponse> list = new ArrayList<>();
			strategies.getContent().forEach((up) -> list.add(transform.apply(up)));
	        return new PageImpl(list, strategies.getPageable(), strategies.getTotalElements());
	}

	public Long exportStrategies(List<QueryParam> params, String createdBy) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(params);
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setSqlParam(json);
		reportRequest.setCreatedByUserId(createdBy);
		reportRequest.setRootClass(Strategy.class.getCanonicalName());

		reportRequest.setStatus(ReportStatusEnum.PENDING);

		Map<String, String> exportFields = new LinkedHashMap<String, String>();

		reportRequest.setResponseClass(StrategyResponse.class.getCanonicalName());
		exportFields.put("strategyCode", "Strategy Code");
		exportFields.put("strategyName", "Strategy Name");
		exportFields.put("strategyProviderName", "Signal Provider");
		exportFields.put("autoPrice", "Auto Price");
		exportFields.put("autoSellingUnit", "Auto Quantity");
		exportFields.put("manualPrice", "Manual Price");
		exportFields.put("manualSellingUnit", "Manual Quantity");
		exportFields.put("status", "Status");
		exportFields.put("createdAt", "Registration Date");

		reportRequest.setExportField(mapper.writeValueAsString(exportFields));
		reportRequestRepository.save(reportRequest);
		return reportRequest.getId();
	}
	
	public StrategyResponse findByStrategyId(long strategyId) {
		Strategy strategy = strategyRepository.findById(strategyId).orElse(null);
        if (strategy == null) {
        	throw new TargetNotFoundException();
        }
        
        return StrategyResponse.from(strategy, true);
	}

	public List<StrategyResponse> findByStrategyIds(Integer dailyProfitsRange, List<Long> strategyIds) {
		List<Strategy> strategies = strategyRepository.findByIdIn(strategyIds);
		return updateDailyProfits(strategies.stream().map(s -> StrategyResponse.from(s, true)).collect(Collectors.toList()), dailyProfitsRange);
	}

	private List<StrategyResponse> updateDailyProfits(List<StrategyResponse> responses, Integer dailyProfitsRange) {
		if (dailyProfitsRange == null) {
			return responses;
		}
		Map<Long, StrategyResponse> strategyMap = responses.stream().collect(Collectors.toMap(s -> s.getId(), s -> s));
		Timestamp now = new Timestamp(System.currentTimeMillis());
		Timestamp start = new Timestamp(TimeUtils.getStartOfDate(new Date(
				System.currentTimeMillis() - (long)dailyProfitsRange * TimeUtils.MILISECONDS_OF_A_DAY)).getTime());
		List<DailyProfit> dailyProfits = dailyProfitRepository.findByPkStrategyIdInAndPkDateBetweenOrderByPkStrategyIdAscPkDateDesc(strategyMap.keySet(), start, now);
		Long currentId = null;
		List<DailyProfitResponse> strategyDailyProfits = new ArrayList<>();

		for (int i = dailyProfits.size() - 1; i >= -1; i--) {
			if (currentId != null && (i == -1 || currentId != dailyProfits.get(i).getPk().getStrategyId())) {
				strategyMap.get(currentId).setDailyProfits(strategyDailyProfits);
				strategyDailyProfits = new ArrayList<>();
			}
			if (i == -1) break;
			currentId = dailyProfits.get(i).getPk().getStrategyId();
			strategyDailyProfits.add(DailyProfitResponse.from(dailyProfits.get(i)));
		}
		return responses;
	}

	public List<DailyProfitResponse> getDailyProfits(long strategyId, int dateRange) throws ParseException {
		Timestamp start = new Timestamp(TimeUtils.getStartOfDate(new Date(System.currentTimeMillis() - (long)dateRange * TimeUtils.MILISECONDS_OF_A_DAY)).getTime());
		Timestamp end = new Timestamp(TimeUtils.getStartOfDate(new Date(System.currentTimeMillis())).getTime());
		return this.getDailyProfits(strategyId, start, end);
	}

	public List<DailyProfitResponse> getDailyProfits(long strategyId, Timestamp start, Timestamp end) throws ParseException {
		List<DailyProfit> dailyProfits = dailyProfitRepository.findByPkStrategyIdAndPkDateBetweenOrderByPkDateAsc(strategyId, start, end);
		return dailyProfits.stream().map(d -> DailyProfitResponse.from(d)).collect(Collectors.toList());
	}

	public Page<StrategySignalDataResponse> getSignalDatas(long strategyId, int maxNumber) throws ParseException {
		Pageable pageable = PageRequest.of(0, maxNumber, Sort.Direction.DESC, "tradingAt");
		return this.getSignalDatas(strategyId, new ArrayList<>(), pageable);
	}

	public Page<StrategySignalDataResponse> getSignalDatasforPc(long strategyId, List<QueryParam> queryParams, Pageable pageable) throws ParseException {
		queryParams.add(QueryParam.create("strategy.id", strategyId + "", Long.class));
		return getSignalDatas(queryParams, pageable, true);
	}

	public Page<StrategySignalDataResponse> getSignalDatas(long strategyId, List<QueryParam> queryParams, Pageable pageable) throws ParseException {
		queryParams.add(QueryParam.create("strategy.id", strategyId + "", Long.class));
		return getSignalDatas(queryParams, pageable);
	}

	public Page<StrategySignalDataResponse> getSignalDatas(List<QueryParam> queryParams, Pageable pageable) throws ParseException {
		return getSignalDatas(queryParams, pageable, false);
	}

	public Page<StrategySignalDataResponse> getSignalDatas(List<QueryParam> queryParams, Pageable pageable, boolean isFull) throws ParseException {
		Page<StrategySignalData> datas = ssdRepositoryImpl.find(queryParams, pageable);
		List<StrategySignalDataResponse> result = new ArrayList<>();
		datas.forEach(t -> {
			if (isFull)
				result.add(StrategySignalDataResponse.fromForPc(t));
			else
				result.add(StrategySignalDataResponse.from(t));
		});
		return new PageImpl<>(result, pageable, datas.getTotalElements());
	}

	@com.fintasoft.eraserisk.annotations.Transactional
    public StrategyResponse registerStrategy(Long strategyId, StrategyRegister strategyRegister) {
		Strategy strategy = new Strategy();
		boolean statusUpdate = true;
		if (strategyId != null) {
			strategy = strategyRepository.findById(strategyId).orElse(null);
			if (strategy == null) {
				throw new NotFoundException("Strategy");
			}
			statusUpdate = strategy.getStatus() != strategyRegister.getStrategyStatusId();
		}

		List<Strategy> sameNameOrCodeStrategies = strategyRepository.findByStrategyNameOrStrategyCode
				(strategyRegister.getStrategyName(), strategyRegister.getStrategyCode());
		if (!CollectionUtils.isEmpty(sameNameOrCodeStrategies)) {
			for (int i = 0; i < sameNameOrCodeStrategies.size(); i++) {
				Strategy item = sameNameOrCodeStrategies.get(i);
				if (strategyId == null || item.getId() != strategyId) {
					if (item.getStrategyName().equalsIgnoreCase(strategyRegister.getStrategyName())) {
						throw new InvalidValueException("strategyName", "error.already_in_use", new String[]{ "Strategy Name" });
					}
					if (item.getStrategyCode().equalsIgnoreCase(strategyRegister.getStrategyCode())) {
						throw new InvalidValueException("strategyCode", "error.already_in_use", new String[]{ "Strategy Code" });
					}
				}
			}

		}
		
		if (strategyProviderRepository.findById(strategyRegister.getStrategyProviderId()).orElse(null) == null) {
			throw new InvalidValueException("strategyProviderId[" + strategyRegister.getStrategyProviderId() + "]");
        }
		
		if (signalPlatformRepository.findById(strategyRegister.getSignalPlatformId()).orElse(null) == null) {
			throw new InvalidValueException("signalPlatformId[" + strategyRegister.getSignalPlatformId() + "]");
        }
		
		if (strategyStatusRepository.findById(strategyRegister.getStrategyStatusId()).orElse(null) == null) {
			throw new InvalidValueException("status[" + strategyRegister.getStrategyStatusId() + "]");
        }

        if (strategyRegister.getProductId() != null) {
			DerivativeProduct derivativeProduct = derivativeProductRepository.findById(strategyRegister.getProductId()).orElseThrow(() -> new InvalidValueException("productid"));
			strategy.setDerivativeProduct(derivativeProduct);
		}

		strategy.setStrategyName(strategyRegister.getStrategyName());
		strategy.setStrategyCode(strategyRegister.getStrategyCode());
		strategy.setDescription(strategyRegister.getDescription());
		strategy.setDescriptionKr(strategyRegister.getDescriptionKr());
		strategy.setAutoPrice(strategyRegister.getAutoPrice());
		strategy.setManualPrice(strategyRegister.getManualPrice());

		strategy.setStatus(strategyRegister.getStrategyStatusId());
		strategy.setStrategyProviderId(strategyRegister.getStrategyProviderId());
		strategy.setSignalPlatformId(strategyRegister.getSignalPlatformId());
		strategy.setStyle(TradingStyleEnum.valueOf(strategyRegister.getStyle()));
		strategy.setTradingDuration(TradingDurationEnum.valueOf(strategyRegister.getTradingDuration()));
		strategy.setSignalFrequency(strategyRegister.getSignalFrequency());
		strategy.setFrequencyType(FrequencyTypeEnums.valueOf(strategyRegister.getFrequencyType()));

		// Validate selling unit with remaining unit
		if (strategyId == null) {
			strategy.setRemainingAuto(strategyRegister.getAutoSellingUnit());
			strategy.setRemainingManual(strategyRegister.getManualSellingUnit());
		} else {
			strategy.setRemainingAuto(strategy.getRemainingAuto() + strategyRegister.getAutoSellingUnit()
					- strategy.getAutoSellingUnit());
			if (strategy.getRemainingAuto()<0){
				throw new InvalidValueException("autoSellingUnit["+ strategyRegister.getAutoSellingUnit() + "]");
			}
			
			strategy.setRemainingManual(strategy.getRemainingManual() + strategyRegister.getManualSellingUnit()
					- strategy.getManualSellingUnit());
			if (strategy.getRemainingManual()<0){
				throw new InvalidValueException("manualSellingUnit["+ strategyRegister.getManualSellingUnit() + "]");
			}
		}
		
		strategy.setAutoSellingUnit(strategyRegister.getAutoSellingUnit());
		strategy.setManualSellingUnit(strategyRegister.getManualSellingUnit());

		strategy = strategyRepository.save(strategy);
		
		int totalAutoSellingUnit = 0;

		Map<Long, StrategySecuritiesCompany> secMap = strategyId == null ? new HashMap<>()
				: strategy.getSecuritiesCompanies().stream().collect(Collectors.toMap(s -> s.getPk().getSecId(), s -> s));
		if (strategyRegister.getSecuritiesCompanies() != null) {
			StrategySecuritiesCompanyRequest[] strategySecuritiesCompanyRequests = strategyRegister.getSecuritiesCompanies();
			for (int i = 0; i < strategySecuritiesCompanyRequests.length; i++) {
				totalAutoSellingUnit += strategySecuritiesCompanyRequests[i].getAutoSellingUnit();
				SecuritiesCompany securitiesCompany = securitiesCompanyRepository.findById(strategySecuritiesCompanyRequests[i].getId()).orElse(null);
				if (securitiesCompany != null) {
					StrategySecuritiesCompany strategySecuritiesCompany = secMap.get(strategySecuritiesCompanyRequests[i].getId());
					secMap.remove(strategySecuritiesCompanyRequests[i].getId());
					if (strategySecuritiesCompany == null) {
						strategySecuritiesCompany = new StrategySecuritiesCompany();
						strategySecuritiesCompany.setStrategy(strategy);
						strategySecuritiesCompany.setSecuritiesCompany(securitiesCompany);
						strategy.getSecuritiesCompanies().add(strategySecuritiesCompany);
					}
					strategySecuritiesCompany.setRemainingSellingUnit(strategySecuritiesCompany.getRemainingSellingUnit()
																	- strategySecuritiesCompany.getAutoSellingUnit()
																	+ strategySecuritiesCompanyRequests[i].getAutoSellingUnit());
					if (strategySecuritiesCompany.getRemainingSellingUnit() < 0){
						throw new InvalidValueException("remainingSellingUnit[" + strategySecuritiesCompany.getRemainingSellingUnit() + "]");
					}
					strategySecuritiesCompany.setAutoSellingUnit(strategySecuritiesCompanyRequests[i].getAutoSellingUnit());

				} else {
					throw new InvalidValueException("secIds[" + i + "]");
				}
			}
			for (StrategySecuritiesCompany deleting : secMap.values()) {
				strategy.getSecuritiesCompanies().remove(deleting);
			}

			if (totalAutoSellingUnit > strategy.getAutoSellingUnit()){
				throw new InvalidValueException("autoSellingUnit");
			}
			
		} else if (strategyId != null) {
			strategy.getSecuritiesCompanies().clear();
		}

		if (strategyId == null || strategy.getStrategyStatistic() == null) {
			StrategyStatistic strategyStatistic = new StrategyStatistic();
			strategyStatistic.setStrategy(strategy);
			strategyStatisticRepository.save(strategyStatistic);
			strategy.setStrategyStatistic(strategyStatistic);
		}
		strategy.setStrategyStatus(strategyStatusService.getStatus(strategyRegister.getStrategyStatusId()));
		StrategyResponse response = StrategyResponse.from(strategy);
		final boolean statusUpdated = statusUpdate;
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
			@Override
			public void afterCommit() {
				if (statusUpdated) {
					queueHandler.sendMessageSafe(
							appConf.getQueue().getTopics().getStrategyStatusUpdate(),
							strategyId == null ? "NEW_STRATEGY" : "UPDATE_STRATEGY",
							response);
				}
				// send message to live component
				queueHandler.sendMessageSafe(
						appConf.getQueue().getTopics().getStrategyUpdate(),
						"REFRESH_STRATEGY_DATA",
						Arrays.asList(strategyId));
			}
		});
		return response;
	}
	
	@Transactional
	public void uploadData(StrategyData strategyData) {
		Strategy strategy = strategyRepository.findById(strategyData.getStrategyId()).orElse(null);
		if ( strategy == null) {
			throw new NotFoundException("Strategy");
		}
		
		signalDataRepository.markDeleteByStrategyId(strategyData.getStrategyId());
		strategy.setFirstSignalTradingAt(null);
		
		List<StrategySignalData> signalDatas = new ArrayList<>();
		
		StrategyStatistic strategyStatistic = strategy.getStrategyStatistic();
		if (strategyStatistic == null){
			strategyStatistic = new StrategyStatistic();
			strategyStatistic.setStrategy(strategy);
		} else {
			strategyStatistic.resetData();
		}
		
				
		StrategySignalDataRequest[] strategySignalDatas = strategyData.getCsv();
		
		StrategySignalData lastSignal = null;
		
		DerivativeProduct derivativeProduct = strategy.getDerivativeProduct();

		Double highestCumulativeProfit = null;
		List<StrategySignalData> openSignals = new ArrayList<>();
		for (int i = 0; i < strategySignalDatas.length; i++) {
			if (derivativeProduct == null){
				derivativeProduct = derivativeProductRepository.findByDerivativeCode(strategySignalDatas[i].getProductCode());
				strategy.setDerivativeProduct(derivativeProduct);
				strategyRepository.save(strategy);
			} else {
				if (!derivativeProduct.getDerivativeCode().equals(strategySignalDatas[i].getProductCode())){
					throw new InvalidValueException("derivativeCode[" + strategySignalDatas[i].getProductCode() + "]");
				}
			}
			
			StrategySignalData strategySignalData = new StrategySignalData();
			strategySignalData.setStrategy(strategy);
			strategySignalData.setProductCode(strategySignalDatas[i].getProductCode());
			strategySignalData.setSignal(SignalEnum.valueOf(strategySignalDatas[i].getSignal()));
			strategySignalData.setOrderType(OrderTypeEnum.valueOf(strategySignalDatas[i].getOrderType()));
			strategySignalData.setPrice(strategySignalDatas[i].getPrice());

			Timestamp tradingAt = ConvertUtils.convertDateTime(strategySignalDatas[i].getDate(), strategySignalDatas[i].getTime());
			strategySignalData.setTradingAt(tradingAt);
			signalDataRepository.save(strategySignalData);
			
			/* Calculation Part*/
			CalculateUtils.calculateProfit(openSignals, strategySignalData, lastSignal, derivativeProduct.getUnitValue());

			if (highestCumulativeProfit == null || highestCumulativeProfit < strategySignalData.getCumulativeProfit()) {
				highestCumulativeProfit = strategySignalData.getCumulativeProfit();
			}

			strategyStatistic.setTotalCount(strategyStatistic.getTotalCount() + 1);
			if (strategySignalData.getCloseForId() != null){
				strategyStatistic.setTotalTrade(strategyStatistic.getTotalTrade() + 1);
				if (strategySignalData.getSafetyProfit() > 0){
					strategyStatistic.setWinCount(strategyStatistic.getWinCount() + 1);
				}
			}

			if (strategyStatistic.getTotalTrade() > 0) {
				strategyStatistic.setWinRate((((double)strategyStatistic.getWinCount()/strategyStatistic.getTotalTrade()))*100);
			}
			
			strategyStatistic.setNetProfit(strategySignalData.getCumulativeProfit());
			strategyStatistic.setBestTrade(DoubleUtils.maxOrZero(strategyStatistic.getBestTrade(), strategySignalData.getProfit()));
			strategyStatistic.setWorstTrade(DoubleUtils.minOrZero(strategyStatistic.getWorstTrade(), strategySignalData.getProfit()));
			strategyStatistic.setMdd(DoubleUtils.maxOrZero(strategyStatistic.getMdd(), strategySignalData.getDrawdown()));
			strategyStatistic.setRequiredCapital(derivativeProduct.getMargin() + Math.abs(strategyStatistic.getMdd())*derivativeProduct.getMddRate());
			strategyStatistic.setRoi((strategyStatistic.getNetProfit()/strategyStatistic.getRequiredCapital())*100);

			/* End Calculation Part*/
			
			lastSignal = strategySignalData;

			signalDatas.add(strategySignalData);
		}
		if (!signalDatas.isEmpty()) {
			strategy.setFirstSignalTradingAt(signalDatas.get(0).getTradingAt());
		}

		statisticRepository.save(strategyStatistic);

		dailyProfitRepository.deleteByPkStrategyId(strategy.getId());
		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
			@Override
			public void afterCommit() {
				// send message to live component
				queueHandler.sendMessageSafe(
						appConf.getQueue().getTopics().getStrategyUpdate(),
						"REFRESH_STRATEGY_DATA",
						Arrays.asList(strategyData.getStrategyId()));
			}
		});
	}

	private boolean sameDay(Timestamp t1, Timestamp t2) {
		return TimeUtils.getStartOfDate(new Date(t1.getTime())).getTime() == TimeUtils.getStartOfDate(new Date(t2.getTime())).getTime();
	}
	
	public Page<StrategyResponse> findByUser(long userId, Pageable pageable) throws ParseException {
		Page<Strategy> strategies = strategyRepositoryImpl.getAllStrategiesBelongToUser(userId, pageable);
				
		List<StrategyResponse> list = new ArrayList<>();
		Long[] ids = new Long[strategies.getContent().size()];
		Map<Long, StrategyResponse> map = new HashMap<>();
		for (int i = 0; i < strategies.getContent().size(); i ++) {
			Strategy strategy = strategies.getContent().get(i);
			ids[i] = strategy.getId();
			StrategyResponse strategyResponse = StrategyResponse.from(strategy, true);
			strategyResponse.setSignalDatas(new ArrayList<>());
			list.add(strategyResponse);
			map.put(strategy.getId(), strategyResponse);
		}
		if (ids.length > 0) {
			List<StrategySignalData> signalDatas = ssdRepositoryImpl.findByStrategyIdAndLimit(7, ids);
			long currentId = -1;
			for (int i = 0; i < signalDatas.size(); i ++) {
				StrategySignalData signalData = signalDatas.get(i);
				map.get(signalData.getStrategyId()).getSignalDatas().add(StrategySignalDataResponse.from(signalData));
			}
		}
        Page<StrategyResponse> result = new PageImpl(list, strategies.getPageable(), strategies.getTotalElements());
		return result;
	}
	
	
	public Long exportSignalData(Long strategyId) throws JsonProcessingException{
		Strategy strategy = strategyRepository.findById(strategyId).orElse(null);
		if ( strategy == null) {
			throw new NotFoundException("Strategy");
		}
		
		ObjectMapper mapper = new ObjectMapper();
		List<QueryParam> params = new ArrayList<>();
		params.add(QueryParam.create("strategy.id", strategyId.toString()));
		String json = mapper.writeValueAsString(params);
		ReportRequest reportRequest = new ReportRequest();
		reportRequest.setSqlParam(json);
		reportRequest.setRootClass(StrategySignalData.class.getCanonicalName());
		
		reportRequest.setStatus(ReportStatusEnum.PENDING);
		
		Map<String, String> exportFields = new LinkedHashMap<String, String>();
		
		reportRequest.setResponseClass(StrategySignalDataResponse.class.getCanonicalName());
		exportFields.put("productCode", "Product Code");
		exportFields.put("signal", "Signal");
		exportFields.put("orderType", "Order Type");
		exportFields.put("price", "Price");
		exportFields.put("profit", "Profit");
		exportFields.put("cumulativeProfit", "Cumulative Profit");
		exportFields.put("highestProfit", "Highest Profit");
		exportFields.put("drawdown", "Drawdown");
		exportFields.put("tradingAt", "Trading At");
		
		    		
		reportRequest.setExportField(mapper.writeValueAsString(exportFields));
		reportRequestRepository.save(reportRequest);
		
		return reportRequest.getId();
	}

	public List<Long> getAllStrategyIdsBelongToUser(long userId) {
		List<Strategy> strategies = strategyRepositoryImpl.getAllStrategiesBelongToUser(userId);

		List<Long> list = new ArrayList<>();
		strategies.forEach(s -> list.add(s.getId()));
		return list;
	}

	@Transactional
	public StrategyResponse lastUpdatedStatus(Long strategyId) {
		Strategy strategy = strategyRepository.findById(strategyId).orElse(null);
		if (strategy == null) {
			throw new TargetNotFoundException();
		}
		strategy.setLastUpdatedStatus(new Timestamp(new Date().getTime()));
		return StrategyResponse.fromBasic(null, strategyRepository.save(strategy));
	}
}
