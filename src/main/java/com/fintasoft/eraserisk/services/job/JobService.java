package com.fintasoft.eraserisk.services.job;


import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

@Service
public class JobService {
//
//    @Autowired
//    CalculateStrategyPositionJob calculateStrategyPositionJob;
//
//    @Autowired
//    DailyProfitJob dailyProfitJob;

//    @Autowired
//    RealTimeStrategyOrderJob realTimeStrategyOrderJob;

//    public void doCalculateStrategyPosition() {
//        new Thread(new JobThread(() -> calculateStrategyPositionJob.doCalculate())).start();
//    }
//
//    public void doCalculateDailyProfit(Timestamp start, Timestamp end) {
//        dailyProfitJob.setStart(start);
//        dailyProfitJob.setEnd(end);
//        new Thread(new JobThread(dailyProfitJob)).start();
//    }

//    public void updateStrategyPos() {
//        new Thread(new JobThread(realTimeStrategyOrderJob)).start();
//    }

    @Data
    @AllArgsConstructor
    public static class JobThread implements Runnable {
        private JobWorker jobWorker;

        @Override
        public void run() {
            jobWorker.doJob();
        }
    }

    public interface JobWorker {
        void doJob();
    }
}
