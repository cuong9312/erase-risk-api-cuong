package com.fintasoft.eraserisk.services.signal;

import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.response.TradingHistoryResponse;
import com.fintasoft.eraserisk.model.api.response.UserTradingHistoryResponse;
import com.fintasoft.eraserisk.model.db.StrategySignalData;
import com.fintasoft.eraserisk.model.db.UserStrategyOrder;
import com.fintasoft.eraserisk.repositories.UserStrategyOrderRepository;
import com.fintasoft.eraserisk.repositories.impl.StrategySignalDataRepositoryImpl;
import com.fintasoft.eraserisk.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TradingHistoryService {
    public static QueryParam.ManualExpression<StrategySignalData> SIGNAL_FROM_ACTIVE_STRATEGY =
            (field, join, cb, q, r, p, rs) -> cb.greaterThanOrEqualTo(
                    QueryParam.getFieldExpression(field, join, "strategy.userPurchaseHistories.expirationDate", Timestamp.class, r, rs),
                    cb.currentTimestamp()
            );

    @Autowired
    private StrategySignalDataRepositoryImpl queryParamRepository;

    @Autowired
    private UserStrategyOrderRepository userStrategyOrderRepository;

    private List<QueryParam> basic() {
        return basic(null);
    }

    private List<QueryParam> basic(List<QueryParam> queryParams) {
        if (queryParams == null) queryParams = new ArrayList<>();
        queryParams.add(QueryParam.create((field, join, cb, q, r, p, rs) -> cb.isNotNull(r.get("closeForId"))));
        return queryParams;
    }

    @Transactional
    public Page<TradingHistoryResponse> queryTradingHistory (
            long strategyId,
            Timestamp min,
            Timestamp max,
            Pageable pageable
    ) throws ParseException {
        List<QueryParam> queryParams = basic();
        queryParams.add(QueryParam.create("strategy.id", strategyId, Long.class));
        if (min != null || max != null) {
            QueryParam.createMinMaxDate(queryParams, "tradingAt", min, max);
        }

        pageable = processPage(pageable);

        return query(queryParams, pageable, false);
    }

    @Transactional
    public Page<UserTradingHistoryResponse> queryTradingHistoryByUser (long userId, List<QueryParam> queryParams, Pageable pageable) throws ParseException {
        queryParams = basic(queryParams);
        // bought by user
        queryParams.add(QueryParam.create("strategy.userPurchaseHistories.user.id", userId, Long.class));
        // is confirmed as bought
        queryParams.add(QueryParam.create("strategy.userPurchaseHistories.payment.status", "COMPLETE"));
        // is still active
        queryParams.add(QueryParam.create(SIGNAL_FROM_ACTIVE_STRATEGY));

        pageable = processPage(pageable);

        return query(queryParams, pageable, true);
    }

    private Pageable processPage(Pageable pageable) {
        if (pageable == null) {
            return PageRequest.of(0, 10, Sort.by(Sort.Direction.ASC, "tradingAt"));
        } else {
            Iterator<Sort.Order> it = pageable.getSort().iterator();
            boolean canAdd = true;
            while(it.hasNext()) {
                if (it.next().getProperty().equals("tradingAt")) {
                    canAdd = false;
                    break;
                }
            }
            if (canAdd) {
                return ConvertUtils.addMoreSort(pageable, Sort.by(Sort.Direction.ASC, "tradingAt"), true);
            }
        }
        return pageable;
    }

    @Transactional
    public <T extends TradingHistoryResponse> Page<T> query (
            List<QueryParam> queryParams,
            Pageable pageable,
            boolean checkUserOrder
    ) throws ParseException {
        Page<StrategySignalData> closeSignals = queryParamRepository.find(queryParams, pageable);
        List<Long> openSignalIds = closeSignals.stream().map(s -> s.getCloseForId()).filter(id -> id != null).collect(Collectors.toList());
        try (Stream<StrategySignalData> openSignals = queryParamRepository.find(com.fintasoft.eraserisk.utils.Collections.toList(QueryParam.create("id", openSignalIds, Long.class)))) {
            Map<Long, StrategySignalData> openSignalMap = openSignals.collect(Collectors.toMap(StrategySignalData::getId, s -> s));
            List<T> responseList = new ArrayList<>();
            for (StrategySignalData close : closeSignals.getContent()) {
                if (checkUserOrder) {
                    responseList.add((T) UserTradingHistoryResponse.from(close.getCloseForId() != null ? openSignalMap.get(close.getCloseForId()) : null, close));
                } else {
                    responseList.add((T) TradingHistoryResponse.from(null, close.getCloseForId() != null ? openSignalMap.get(close.getCloseForId()) : null, close));
                }
            }

            if (checkUserOrder) {
                openSignalIds.addAll(closeSignals.getContent().stream().map(s -> s.getId()).collect(Collectors.toList()));
                try (Stream<UserStrategyOrder> stream = userStrategyOrderRepository.findBySignalIdInAndSignalDataDeletedIsFalse(openSignalIds)) {
                    Map<Long, Boolean> map = stream.collect(Collectors.toMap(uso -> uso.getSignalId(), uso -> true));
                    for (T trading : responseList) {
                        UserTradingHistoryResponse u = (UserTradingHistoryResponse) trading;
                        u.setCloseSignalHasOrder(map.containsKey(u.getCloseSignal().getSignalId()));
                        u.setOpenSignalHasOrder(map.containsKey(u.getOpenSignal().getSignalId()));
                    }
                }
            }
            return new PageImpl<>(responseList, pageable, closeSignals.getTotalElements());
        }
    }
}
