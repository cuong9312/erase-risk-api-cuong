package com.fintasoft.eraserisk.services.signal;

import com.fintasoft.eraserisk.annotations.NewTransactional;
import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.NotificationMethodEnum;
import com.fintasoft.eraserisk.constances.SignalEnum;
import com.fintasoft.eraserisk.model.api.response.StrategySignalDataResponse;
import com.fintasoft.eraserisk.model.db.*;
import com.fintasoft.eraserisk.model.kafka.NotificationMessage;
import com.fintasoft.eraserisk.repositories.StrategyRepository;
import com.fintasoft.eraserisk.repositories.StrategySignalDataRepository;
import com.fintasoft.eraserisk.repositories.UserRepository;
import com.fintasoft.eraserisk.services.queue.RequestResponseHandler;
import com.fintasoft.eraserisk.utils.CalculateUtils;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import com.fintasoft.eraserisk.utils.DoubleUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

@Component
public class UpdateASignalDataService {
    @Autowired
    private StrategySignalDataRepository signalDataRepository;
    @Autowired
    private RequestResponseHandler requestResponseHandler;
    @Autowired
    private AppConf appConf;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StrategyRepository strategyRepository;



    @NewTransactional
    public void updateAfterCreateSignal(Long id) {
        StrategySignalData newSignal = signalDataRepository.getOne(id);
        SignalEnum openSignalType = SignalEnum.Long;
        if (newSignal.getSignal() == SignalEnum.ExitShort || newSignal.getSignal() == SignalEnum.Long) {
            openSignalType = SignalEnum.Short;
        }
        StrategySignalData openSignal = signalDataRepository.findTopByCloseForIdIsNullAndCloseByIdIsNullAndStrategyIdAndSignalAndDeletedFalseOrderByIdAsc(newSignal.getStrategyId(), openSignalType);
        List<StrategySignalData> openSignals = new ArrayList<>();
        if (openSignal != null) {
            openSignals.add(openSignal);
        }

        StrategySignalData lastSignal = signalDataRepository.findLastSignalBefore(id, newSignal.getStrategyId());
        DerivativeProduct derivativeProduct = newSignal.getStrategy().getDerivativeProduct();
        CalculateUtils.calculateProfit(
                openSignals, newSignal,
                lastSignal,
                derivativeProduct.getUnitValue()
        );

        StrategyStatistic strategyStatistic = newSignal.getStrategy().getStrategyStatistic();

        strategyStatistic.setTotalCount(strategyStatistic.getTotalCount() + 1);
        if (newSignal.getProfit() != null) {
            strategyStatistic.setTotalTrade(strategyStatistic.getTotalTrade() + 1);
        }

        if (newSignal.getSafetyProfit() > 0) {
            strategyStatistic.setWinCount(strategyStatistic.getWinCount() + 1);
        }

        if (strategyStatistic.getTotalTrade() > 0) {
            strategyStatistic.setWinRate(((double) strategyStatistic.getWinCount() / strategyStatistic.getTotalTrade()) * 100);
        }

        strategyStatistic.setNetProfit(newSignal.getCumulativeProfit());
        strategyStatistic.setBestTrade(DoubleUtils.maxOrZero(strategyStatistic.getBestTrade(), newSignal.getProfit()));
        strategyStatistic.setWorstTrade(DoubleUtils.minOrZero(strategyStatistic.getWorstTrade(), newSignal.getProfit()));
        strategyStatistic.setMdd(DoubleUtils.maxOrZero(strategyStatistic.getMdd(), newSignal.getDrawdown()));
        strategyStatistic.setRequiredCapital(derivativeProduct.getMargin() + Math.abs(strategyStatistic.getMdd()) * derivativeProduct.getMddRate());

        strategyStatistic.setRoi((strategyStatistic.getNetProfit() / strategyStatistic.getRequiredCapital()) * 100);
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
            @Override
            public void afterCommit() {
                requestResponseHandler.sendMessageSafe(
                        appConf.getQueue().getTopics().getStrategyUpdate(),
                        "REFRESH_STRATEGY", Arrays.asList(newSignal.getStrategyId()));
            }
        });
    }

    @Transactional
    @Async
    public void sendNotificationAfterCreateSignal(StrategySignalDataResponse response) {
        Strategy strategy = strategyRepository.findById(response.getStrategyId()).get();

        try (Stream<User> users = userRepository.findUserBoughtStrategy(response.getStrategyId())) {
            users.forEach(user -> {
                NotificationMessage notificationMessage = new NotificationMessage();
                notificationMessage.setMethod(NotificationMethodEnum.KAKAO);
                notificationMessage.setTemplate("create_signal");
                notificationMessage.setTemplateDatas(new ArrayList<>());

                notificationMessage.getTemplateDatas().add(new HashMap(){{
                    put("userName", user.getUserInfo().getFullName());
                    put("strategyName", strategy.getStrategyName());
                    put("productCode", strategy.getDerivativeProduct().getDerivativeCode());
                    put("orderType", response.getSignal());
                    put("price", DoubleUtils.formatPrice(response.getPrice()));
                    put("time", response.getTradingAt());
                }});
                notificationMessage.setConfiguration(new HashMap(){{
                    put("to", user.getUserInfo().getPhoneNumber());
                }});
                requestResponseHandler.sendRequestNoResponseSafe(appConf.getQueue().getTopics().getNotification(), "", notificationMessage);
            });
        }
    }
}
