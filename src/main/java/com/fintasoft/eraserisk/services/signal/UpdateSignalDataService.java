package com.fintasoft.eraserisk.services.signal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class UpdateSignalDataService {
    private static final Logger log = LoggerFactory.getLogger(UpdateSignalDataService.class);
    private final List<Long> signalIds = new ArrayList<>();
    private boolean isRunning = false;
    @Autowired
    private UpdateASignalDataService updateASignalDataService;

    @Async
    public void updateAfterCreateSignal(Long signalId) {
        Long[] ids;
        synchronized (this.signalIds) {
            if (signalId != null) {
                this.signalIds.add(signalId);
            }
            if (this.isRunning) {
                return;
            }
            this.isRunning = true;
            ids = this.signalIds.toArray(new Long[this.signalIds.size()]);
            this.signalIds.clear();
        }
        for (Long id : ids) {
            try {
                updateASignalDataService.updateAfterCreateSignal(id);
            } catch (Exception e) {
                log.error("fail to update signal " + id, e);
            }
        }
        synchronized (this.signalIds) {
            this.isRunning = false;
            if (!this.signalIds.isEmpty()) {
                this.updateAfterCreateSignal(null);
            }
        }
    }
}
