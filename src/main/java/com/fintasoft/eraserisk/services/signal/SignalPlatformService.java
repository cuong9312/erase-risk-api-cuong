package com.fintasoft.eraserisk.services.signal;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fintasoft.eraserisk.model.api.response.SignalPlatformResponse;
import com.fintasoft.eraserisk.repositories.SignalPlatformRepository;

@Service
public class SignalPlatformService {	
	@Autowired
	private SignalPlatformRepository signalPlatformRepository;

	public List<SignalPlatformResponse> getAll() {
        List<SignalPlatformResponse> result = new ArrayList<>();
        signalPlatformRepository.findAll().forEach(s -> result.add(SignalPlatformResponse.from(s)));
        return result;
    }
}
