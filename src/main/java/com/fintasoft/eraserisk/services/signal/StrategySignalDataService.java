package com.fintasoft.eraserisk.services.signal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.OrderTypeEnum;
import com.fintasoft.eraserisk.constances.ReportStatusEnum;
import com.fintasoft.eraserisk.constances.SignalEnum;
import com.fintasoft.eraserisk.exceptions.InvalidParamsValueException;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.exceptions.UnAuthorizedException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.request.SignalDataRequest;
import com.fintasoft.eraserisk.model.api.request.StrategySignalDataRequest;
import com.fintasoft.eraserisk.model.api.response.StrategySignalDataResponse;
import com.fintasoft.eraserisk.model.db.*;
import com.fintasoft.eraserisk.repositories.*;
import com.fintasoft.eraserisk.repositories.impl.StrategySignalDataRepositoryImpl;
import com.fintasoft.eraserisk.services.queue.RequestResponseHandler;
import com.fintasoft.eraserisk.services.queue.StrategyPriceService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class StrategySignalDataService {
    private static final Logger log = LoggerFactory.getLogger(StrategySignalDataService.class);

    @Autowired
    private StrategySignalDataRepository signalDataRepository;

    @Autowired
    private StrategyRepository strategyRepository;

    @Autowired
    private StrategyPriceService strategyPriceService;

    @Autowired
    private DerivativeProductPlatformRepository derivativeProductPlatformRepository;

    @Autowired
    private RequestResponseHandler requestResponseHandler;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AppConf appConf;

    @Autowired
    private UpdateASignalDataService updateASignalDataService;

    @Autowired
    private StrategySignalDataRepositoryImpl customSignalDataRepository;

    @Autowired
    private ReportRequestRepository reportRequestRepository;

    @Autowired
    private StrategySignalDataTempRepository signalDataTempRepository;

    @Autowired
    private SignalBroadcastRepository signalBroadcastRepository;


    @Transactional
    public StrategySignalDataResponse createSignal(SignalDataRequest request) {
        return createSignal(null, request);
    }

    /**
     * when receive a new signal:
     * 1. request price and update price
     * 2. create signla and insert db
     * 3. send signal to ws-api through kafka
     * 4. calculate open position in another thread
     * 5. calculate signal data
     *
     * @param request
     * @return
     */
    @Transactional
    public StrategySignalDataResponse createSignal(Long userId, SignalDataRequest request) {
        log.warn("create signal {}", request);

        Strategy strategy = strategyRepository.findByStrategyCode(request.getStrategyCode());
        if (strategy == null) {
            throw new InvalidValueException("strategyCode");
        }

        List<DerivativeProductPlatform> derivativeProductPlatforms = derivativeProductPlatformRepository.findByPkSignalPlatformIdAndTradingCode(
                strategy.getSignalPlatformId(), request.getProductCode());

        if (CollectionUtils.isEmpty(derivativeProductPlatforms)) {
            throw new InvalidValueException("productCode");
        }

        Double price = request.getPrice();
        if (price == null || price == 0) {
            price = strategyPriceService.getPrice(strategy.getId());
        }

        if (price == null) {
            throw new InvalidValueException("price");
        }

        if (userId != null) {
            User user = userRepository.findById(userId).orElse(null);
            if (user == null) {
                throw new InvalidValueException("userId");
            }
            user.getAdminStrategyProviders().stream().filter(p -> p.getId() == strategy.getStrategyProviderId()).findFirst().orElseThrow(UnAuthorizedException::new);
        }

        if (!request.isAllowMultipleBuy()) {
            StrategySignalData lastSignal = signalDataRepository.findLastSignal(strategy.getId());
            String signalType = request.getSignalType();

            if (lastSignal != null) {
                switch (lastSignal.getSignal()) {
                    case Long:
                        if (!signalType.equalsIgnoreCase(SignalEnum.ExitLong.name()) && !signalType.equalsIgnoreCase(SignalEnum.Short.name())) {
                            throw new InvalidParamsValueException("signalType", lastSignal.getSignal(), signalType);
                        }
                        break;
                    case Short:
                        if (!signalType.equalsIgnoreCase(SignalEnum.ExitShort.name()) && !signalType.equalsIgnoreCase(SignalEnum.Long.name())) {
                            throw new InvalidParamsValueException("signalType", lastSignal.getSignal(), signalType);
                        }
                        break;
                    case ExitLong:
                        if (!signalType.equalsIgnoreCase(SignalEnum.Long.name()) && !signalType.equalsIgnoreCase(SignalEnum.Short.name())) {
                            throw new InvalidParamsValueException("signalType", lastSignal.getSignal(), signalType);
                        }
                        break;
                    case ExitShort:
                        if (!signalType.equalsIgnoreCase(SignalEnum.Long.name()) && !signalType.equalsIgnoreCase(SignalEnum.Short.name())) {
                            throw new InvalidParamsValueException("signalType", lastSignal.getSignal(), signalType);
                        }
                        break;
                    default:
                        break;
                }
            }

        } else {
            return createSignalTemp(request);
        }

        DerivativeProductPlatform derivativeProductPlatform = derivativeProductPlatforms.get(0);
        if (derivativeProductPlatforms.size() > 1 && !StringUtils.isEmpty(request.getPlatform())) {
            try {
                Long id = Long.parseLong(request.getPlatform());
                derivativeProductPlatform = derivativeProductPlatforms.stream().
                        filter(p -> p.getPk().getSignalPlatformId().equals(id)).findFirst().orElse(derivativeProductPlatform);
            } catch (Exception e) {
                derivativeProductPlatform = derivativeProductPlatforms.stream().
                        filter(p -> p.getSignalPlatform().getPlatformName().equalsIgnoreCase(request.getPlatform())).
                        findFirst().orElse(derivativeProductPlatform);
            }
        }

        StrategySignalData signalData = StrategySignalData.builder()
                .productCode(derivativeProductPlatform.getDerivativeProduct().getDerivativeCode())
                .strategy(strategy)
                .strategyId(strategy.getId())
                .price(price)
                .orderType(OrderTypeEnum.valueOf(request.getOrderType()))
                .signal(SignalEnum.valueOf(request.getSignalType()))
                .build();

        signalDataRepository.save(signalData);
        if (strategy.getFirstSignalTradingAt() == null) {
            strategy.setFirstSignalTradingAt(signalData.getTradingAt());
        }

        StrategySignalDataResponse response = StrategySignalDataResponse.fromForPc(signalData);
        response.setTempYn(false);

        SignalBroadcast signalBroadcast = SignalBroadcast.builder()
                .signalId(response.getSignalId())
                .tempYn(false)
                .broadcast(false)
                .build();
        signalBroadcastRepository.save(signalBroadcast);

        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
            @Override
            public void afterCommit() {
                requestResponseHandler.sendMessageSafe(
                        appConf.getQueue().getTopics().getStrategySignalUpdate(),
                        "NEW_SIGNAL", response);
                updateASignalDataService.sendNotificationAfterCreateSignal(response);
                updateASignalDataService.updateAfterCreateSignal(response.getSignalId());
            }
        });
        return response;
    }

    @Transactional
    public Page<StrategySignalDataResponse> query(List<QueryParam> queryParams, Pageable pageable) throws ParseException {
        Page<StrategySignalData> signals = customSignalDataRepository.find(queryParams, pageable);
        return signals.map(StrategySignalDataResponse::from);
    }


    @Transactional
    public Long exportSignals(List<QueryParam> params, String createdByUserId) throws JsonProcessingException {
        params.add(QueryParam.create("deleted", "false", Boolean.class));
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(params);
        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setSqlParam(json);
        reportRequest.setCreatedByUserId(createdByUserId);
        reportRequest.setRootClass(StrategySignalData.class.getCanonicalName());

        reportRequest.setStatus(ReportStatusEnum.PENDING);

        Map<String, String> exportFields = new LinkedHashMap<>();
        reportRequest.setResponseClass(StrategySignalDataRequest.class.getCanonicalName());
        exportFields.put("date", "Date");
        exportFields.put("time", "Time");
        exportFields.put("productCode", "Product Code");
        exportFields.put("signal", "Signal");
        exportFields.put("price", "Price");
        exportFields.put("orderType", "Order Type");

        reportRequest.setExportField(mapper.writeValueAsString(exportFields));
        reportRequest.setOutputType("csv");
        reportRequestRepository.save(reportRequest);

        return reportRequest.getId();
    }

    @Transactional
    public StrategySignalDataResponse createSignalTemp(SignalDataRequest request) {
        StrategySignalDataTemp strategySignalDataTemp = StrategySignalDataTemp.builder()
                .productCode(request.getProductCode())
                .strategyCode(request.getStrategyCode())
                .signalType(request.getSignalType())
                .price(request.getPrice())
                .orderType(request.getOrderType())
                .platform(request.getPlatform())
                .allowMultipleBuy(request.isAllowMultipleBuy())
                .build();

        StrategySignalDataResponse response = StrategySignalDataResponse.from(signalDataTempRepository.save(strategySignalDataTemp));
        response.setTempYn(true);
        Strategy strategy = strategyRepository.findByStrategyCode(request.getStrategyCode());
        response.setStrategyId(strategy.getId());

        SignalBroadcast signalBroadcast = SignalBroadcast.builder()
                .signalId(response.getSignalId())
                .tempYn(true)
                .broadcast(false)
                .build();
        signalBroadcastRepository.save(signalBroadcast);

        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronization() {
            @Override
            public void afterCommit() {
                requestResponseHandler.sendMessageSafe(
                        appConf.getQueue().getTopics().getStrategySignalUpdate(),
                        "NEW_SIGNAL", response);
                updateASignalDataService.sendNotificationAfterCreateSignal(response);
            }
        });

        return response;
    }
}
