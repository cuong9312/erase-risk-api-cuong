package com.fintasoft.eraserisk.services.signal;

import com.fintasoft.eraserisk.annotations.Transactional;
import com.fintasoft.eraserisk.constances.SignalEnum;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.response.OpenPositionResponse;
import com.fintasoft.eraserisk.model.api.response.UserOpenPositionResponse;
import com.fintasoft.eraserisk.model.db.StrategySignalData;
import com.fintasoft.eraserisk.model.db.UserStrategyOrder;
import com.fintasoft.eraserisk.repositories.UserStrategyOrderRepository;
import com.fintasoft.eraserisk.repositories.impl.StrategySignalDataRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class OpenPositionService {
    @Autowired
    private StrategySignalDataRepositoryImpl queryParamRepository;

    @Autowired
    private UserStrategyOrderRepository userStrategyOrderRepository;

    public Page<OpenPositionResponse> queryOpenPosition (long strategyId, Pageable pageable) throws ParseException {
        List<QueryParam> queryParams = basicQueryParam();
        queryParams.add(QueryParam.create("strategy.id", strategyId + "", Long.class));
        Page<StrategySignalData> signalStream = queryParamRepository.find(queryParams, pageable);
        return signalStream.map(s -> OpenPositionResponse.from(null, s));
    }

    public Page<OpenPositionResponse> queryOpenPosition (long userId, long strategyId, Pageable pageable) throws ParseException {
        List<QueryParam> queryParams = basicQueryParam();
        queryParams.add(QueryParam.create("strategy.id", strategyId + "", Long.class));
        // bought by user
        queryParams.add(QueryParam.create("strategy.userPurchaseHistories.user.id", userId+ "", Long.class));
        // is confirmed as bought
        queryParams.add(QueryParam.create("strategy.userPurchaseHistories.payment.status", "COMPLETE"));
        // is still active
        queryParams.add(QueryParam.create(TradingHistoryService.SIGNAL_FROM_ACTIVE_STRATEGY));
        Page<StrategySignalData> signalStream = queryParamRepository.find(queryParams, pageable);
        return signalStream.map(s -> OpenPositionResponse.from(null, s));
    }

    @Transactional
    public Page<UserOpenPositionResponse> queryOpenPositions (Long userId, List<QueryParam> queryParams, Pageable pageable) throws ParseException {
        queryParams = basicQueryParam(queryParams);
        // bought by user
        queryParams.add(QueryParam.create("strategy.userPurchaseHistories.user.id", userId+ "", Long.class));
        // is confirmed as bought
        queryParams.add(QueryParam.create("strategy.userPurchaseHistories.payment.status", "COMPLETE"));
        // is still active
        queryParams.add(QueryParam.create(TradingHistoryService.SIGNAL_FROM_ACTIVE_STRATEGY));
        Page<StrategySignalData> signals = queryParamRepository.find(queryParams, pageable);
        List<Long> signalIds = signals.stream().map(StrategySignalData::getId).collect(Collectors.toList());
        try (Stream<UserStrategyOrder> stream = userStrategyOrderRepository.findBySignalIdInAndSignalDataDeletedIsFalse(signalIds)) {
            Map<Long, Boolean> map = stream.collect(Collectors.toMap(UserStrategyOrder::getSignalId, uso -> true));
            Page<UserOpenPositionResponse> response = signals.map(s -> UserOpenPositionResponse.from(null, s));
            response.forEach(op -> op.setHasUserOrder(map.containsKey(op.getOpenSignal().getSignalId())));
            return response;
        }
    }

    private List<QueryParam> basicQueryParam() {
        return basicQueryParam(null);
    }

    private List<QueryParam> basicQueryParam(List<QueryParam> queryParams) {
        if (queryParams == null) queryParams = new ArrayList<>();
        List<SignalEnum> signals = Arrays.asList(
                SignalEnum.Long,
                SignalEnum.Short
        );
        queryParams.add(QueryParam.create("signal", signals, SignalEnum.class));
        queryParams.add(QueryParam.create("closeById", "").nullValue());
        queryParams.add(QueryParam.create("closeForId", "").nullValue());
        return queryParams;
    }

}
