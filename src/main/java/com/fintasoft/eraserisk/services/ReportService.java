package com.fintasoft.eraserisk.services;

import static com.fintasoft.eraserisk.utils.LambdaExceptionUtil.rethrowConsumer;
import static com.fintasoft.eraserisk.utils.LambdaExceptionUtil.rethrowFunction;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.fintasoft.eraserisk.annotations.NewTransactional;
import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.s3.S3Service;
import com.fintasoft.eraserisk.utils.FileUtils;
import com.fintasoft.eraserisk.utils.lambda.RethrowTripleFunction;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fintasoft.eraserisk.constances.ReportStatusEnum;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.response.ReportRequestResponse;
import com.fintasoft.eraserisk.model.api.response.UserStrategyOrderResponse;
import com.fintasoft.eraserisk.model.db.ReportRequest;
import com.fintasoft.eraserisk.model.query.ExportRevenue;
import com.fintasoft.eraserisk.model.query.RevenueByDay;
import com.fintasoft.eraserisk.model.query.TradeReport;
import com.fintasoft.eraserisk.repositories.ReportRequestRepository;
import com.fintasoft.eraserisk.repositories.impl.UserPurchaseRepositoryImpl;
import com.fintasoft.eraserisk.repositories.impl.UserStrategyOrderRepositoryImpl;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import com.fintasoft.eraserisk.utils.CriterialHelper;

@Service
public class ReportService {
	private final Logger log = LoggerFactory.getLogger(ReportService.class);

	@PersistenceContext
    EntityManager entityManager;
	
    @Autowired
    ReportRequestRepository reportRequestRepository;
    
    @Autowired
    UserPurchaseRepositoryImpl userPurchaseRepositoryImpl;
    
    @Autowired
    UserStrategyOrderRepositoryImpl userStrategyOrderRepositoryImpl;

    @Autowired
	AppConf appConf;

    @Autowired
	S3Service s3Service;

    public ReportRequestResponse findReport(long reportId){
    	return ReportRequestResponse.from(reportRequestRepository.findById(reportId).orElse(null));
    }

    private ReportRequestResponse commonProcess(ReportRequest request, RethrowTripleFunction<String, List<QueryParam>, Map<String, String>, String, Exception> process) {
		return this.commonProcess(request, process, null);
	}

    private ReportRequestResponse commonProcess(ReportRequest request,
												RethrowTripleFunction<String, List<QueryParam>, Map<String, String>, String, Exception> process,
												RethrowTripleFunction<String, Map<Object, Object>, Map<String, String>, String, Exception> processMapParam
	) {
		String date = ConvertUtils.fromDate(new Date());
		String fileDir = appConf.getReport().getReportDir() + date;
		String uuid = UUID.randomUUID().toString();
		String file = fileDir + "/" + "report_" + request.getId()
				+ "_" + uuid + "_" + ConvertUtils.fromDate(new Date())
				+ ("csv".equalsIgnoreCase(request.getOutputType()) ? ".csv" : ".xls");
		new File(fileDir).mkdirs();
		String url = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			Map<String, String> exportFields = mapper.readValue(request.getExportField(), LinkedHashMap.class);
			if (process != null) {
				List<QueryParam> queryParams = mapper.readValue(request.getSqlParam(),
						TypeFactory.defaultInstance().constructCollectionType(List.class, QueryParam.class));
				url = process.apply(file, queryParams, exportFields);
			} else {
				HashMap<Object, Object> params = mapper.readValue(request.getSqlParam(),
						TypeFactory.defaultInstance().constructMapType(HashMap.class, Object.class, Object.class));
				url = processMapParam.apply(file, params, exportFields);
			}
			request.setFilePath(url);
			request.setStatus(ReportStatusEnum.COMPLETED);
		} catch (Exception e) {
			request.setStatus(ReportStatusEnum.FAILED);
			log.error(e.getMessage(), e);
		}
		reportRequestRepository.save(request);
		return ReportRequestResponse.from(request).fullFilePath(url);

	}


	@NewTransactional()
    public ReportRequestResponse generate(ReportRequest request) {
    	if ("csv".equalsIgnoreCase(request.getOutputType())) {
			return generateCsvReport(request);
		}
		return generateReport(request);
	}

    private ReportRequestResponse generateReport(ReportRequest request) {
    	return commonProcess(request, (path, queryParams, exportFields) -> {
			Stream<Object> results = CriterialHelper.findEntityByParam(entityManager, queryParams, Class.forName(request.getRootClass()));
			try (HSSFWorkbook workbook = new HSSFWorkbook()) {
				HSSFSheet sheet = workbook.createSheet("data");
				HSSFRow rowhead = sheet.createRow((short) 0);
				short i = 0;
				for (Map.Entry<String, String> entry : exportFields.entrySet()) {
					rowhead.createCell(i).setCellValue(entry.getValue());
					i++;
				}

				AtomicInteger counter = new AtomicInteger(1);
				Method method = Class.forName(request.getResponseClass()).getMethod("fromReport", Class.forName(request.getRootClass()));

				results.forEach(rethrowConsumer(object -> {
					int iIdx = 0;
					Object objectResponse = method.invoke(null, object);
					BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(objectResponse);
					HSSFRow row = sheet.createRow(counter.getAndIncrement());
					for (Map.Entry<String, String> entry : exportFields.entrySet()) {
						Object value = wrapper.getPropertyValue(entry.getKey());
						if (value != null && value.getClass() == Long.class) {
							row.createCell(iIdx).setCellValue(value == null ? 0 : Long.parseLong(value.toString()));
						} else {
							row.createCell(iIdx).setCellValue(value == null ? "" : value.toString());
						}
						iIdx++;
					}

					entityManager.detach(object);
				}));

				if (UserStrategyOrderResponse.class.getCanonicalName().equals(request.getResponseClass())){
					HSSFRow row = sheet.createRow(counter.get());
					row.createCell(0).setCellValue("Summary");
					sheet.addMergedRegion(new CellRangeAddress((counter.get()), (counter.get()), 0,1));
					row.createCell(2).setCellFormula("SUM(E2:E" + (counter.get()) + ")");
				}

				return s3Service.uploadFile(workbook, path, false);
			}
		});
    }


    private ReportRequestResponse generateCsvReport(ReportRequest request) {
		return commonProcess(request, (path, queryParams, exportFields) -> {
			String[] headers = new String[exportFields.size()];
			int index = 0;
			for (Map.Entry<String, String> entry : exportFields.entrySet()) {
				headers[index] = entry.getValue();
				index++;
			}
			return FileUtils.processTempFile(rethrowFunction(file -> {
				try (
						CSVPrinter csvPrinter = new CSVPrinter(new FileWriter(file), CSVFormat.DEFAULT.withHeader(headers));
						Stream results = CriterialHelper.findEntityByParam(entityManager, queryParams, Class.forName(request.getRootClass()));
				) {
					Method method = Class.forName(request.getResponseClass()).getMethod("fromReport", Class.forName(request.getRootClass()));
					results.forEach(rethrowConsumer(object -> {
						Object objectResponse = method.invoke(null, object);
						BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(objectResponse);
						for (Map.Entry<String, String> entry : exportFields.entrySet()) {
							Object value = wrapper.getPropertyValue(entry.getKey());
							if (value instanceof Number) {
								csvPrinter.print(value);
							} else {
								csvPrinter.print(value == null ? "" : value);
							}
						}
						csvPrinter.println();
						entityManager.detach(object);
					}));
					csvPrinter.flush();
					return s3Service.uploadFile(file, path, false);
				}
			}));
		});
    }


	@NewTransactional
    public ReportRequestResponse generateRevenueReport(ReportRequest request) {
		return commonProcess(request, null, (path, params, exportFields) -> {
			List<Integer> tmpPaymentMethodIds = null;
			List<Integer> tmpProviderIds = null;
			List<Integer> tmpSecIds = null;
			List<Long> paymentMethodIds = new ArrayList<Long>();
			List<Long> providerIds = new ArrayList<Long>();
			List<Long> secIds = new ArrayList<Long>();

			Timestamp minPurchaseDate = null;
			Timestamp maxPurchaseDate = null;
			if (params.containsKey("paymentMethodIds")) {
				tmpPaymentMethodIds = (List<Integer>) params.get("paymentMethodIds");
				for (int i = 0; i < tmpPaymentMethodIds.size(); i++) {
					paymentMethodIds.add(tmpPaymentMethodIds.get(i).longValue());
				}
			}
			if (params.containsKey("providerIds")) {
				tmpProviderIds = (List<Integer>) params.get("providerIds");
				for (int i = 0; i < tmpProviderIds.size(); i++) {
					providerIds.add(tmpProviderIds.get(i).longValue());
				}
			}
			if (params.containsKey("secIds")) {
				tmpSecIds = (List<Integer>) params.get("secIds");
				for (int i = 0; i < tmpSecIds.size(); i++) {
					secIds.add(tmpSecIds.get(i).longValue());
				}
			}
			if (params.containsKey("minPurchaseDate"))
				minPurchaseDate = (Timestamp) params.get("minPurchaseDate");
			if (params.containsKey("maxPurchaseDate"))
				maxPurchaseDate = (Timestamp) params.get("maxPurchaseDate");

			List<ExportRevenue> results = null;
			if (RevenueByDay.class.getCanonicalName().equals(request.getRootClass())){
				results = userPurchaseRepositoryImpl.exportRevenueByDay(paymentMethodIds, providerIds, secIds, minPurchaseDate, maxPurchaseDate);
			} else {
				results = userPurchaseRepositoryImpl.exportRevenueByMonth(paymentMethodIds, providerIds, secIds, minPurchaseDate, maxPurchaseDate);
			}

			try (HSSFWorkbook workbook = new HSSFWorkbook()) {
				HSSFSheet sheet = workbook.createSheet("data");
				HSSFRow rowhead = sheet.createRow((short) 0);
				short i = 0;
				for (Map.Entry<String, String> entry : exportFields.entrySet()) {
					rowhead.createCell(i).setCellValue(entry.getValue());
					i++;
				}

				for (int j = 0; j < results.size(); j++) {
					ExportRevenue exportRevenue = results.get(j);
					HSSFRow row = sheet.createRow(j + 1);
					int iIdx = 0;
					for (Map.Entry<String, String> entry : exportFields.entrySet()) {
						Field field = ExportRevenue.class.getDeclaredField(entry.getKey());
						field.setAccessible(true);
						Object value = field.get(exportRevenue);
						if (field.getType() == String.class){
							row.createCell(iIdx).setCellValue(value == null ? "" : value.toString());
						} else if (field.getType() == Long.class){
							row.createCell(iIdx).setCellValue(Long.parseLong(value.toString()));
						} else if (field.getType() == Double.class){
							row.createCell(iIdx).setCellValue(Double.parseDouble(value.toString()));
						}
						iIdx++;
					}
				}

				HSSFRow row = sheet.createRow(results.size() + 1);
				row.createCell(0).setCellValue("Summary");
				row.createCell(1).setCellFormula("SUM(B2:B" + (results.size() + 1) + ")");
				row.createCell(2).setCellFormula("SUM(C2:C" + (results.size() + 1) + ")");
				row.createCell(3).setCellFormula("SUM(D2:D" + (results.size() + 1) + ")");
				return s3Service.uploadFile(workbook, path, false);
			}
		});
    }



	@NewTransactional
    public ReportRequestResponse generateTradeReport(ReportRequest request) {
		return commonProcess(request, null, (path, params, exportFields) -> {
			List<Integer> tmpSecIds = null;
			List<Long> secIds = new ArrayList<>();
			Timestamp minTradingAt = null;
			Timestamp maxTradingAt = null;
			if (params.containsKey("secIds")) {
				tmpSecIds = (List<Integer>) params.get("secIds");
				for (int i = 0; i < tmpSecIds.size(); i++) {
					secIds.add(tmpSecIds.get(i).longValue());
				}
			}
			if (params.containsKey("minTradingAt"))
				minTradingAt = (Timestamp) params.get("minTradingAt");
			if (params.containsKey("maxTradingAt"))
				maxTradingAt = (Timestamp) params.get("maxTradingAt");
			String username = (String) params.get("username");

			List<TradeReport> results = userStrategyOrderRepositoryImpl
					.exportTradeReport(secIds, minTradingAt, maxTradingAt, username);

			try (HSSFWorkbook workbook = new HSSFWorkbook()) {
				HSSFSheet sheet = workbook.createSheet("data");
				HSSFRow rowhead = sheet.createRow((short) 0);
				short i = 0;
				for (Map.Entry<String, String> entry : exportFields.entrySet()) {
					rowhead.createCell(i).setCellValue(entry.getValue());
					i++;
				}

				for (int j = 0; j < results.size(); j++) {
					TradeReport tradeReport = results.get(j);
					HSSFRow row = sheet.createRow(j + 1);
					int iIdx = 0;
					for (Map.Entry<String, String> entry : exportFields.entrySet()) {
						Field field = TradeReport.class.getDeclaredField(entry.getKey());
						Object value = field.get(tradeReport);
						if (field.getType() == String.class){
							row.createCell(iIdx).setCellValue(value == null ? "" : value.toString());
						} else if (field.getType() == Long.class){
							row.createCell(iIdx).setCellValue(Long.parseLong(value.toString()));
						} else if (field.getType() == Integer.class){
							row.createCell(iIdx).setCellValue(Integer.parseInt(value.toString()));
						} else if (field.getType() == Double.class){
							row.createCell(iIdx).setCellValue(Double.parseDouble(value.toString()));
						}
						iIdx++;
					}
				}

				HSSFRow row = sheet.createRow(results.size() + 1);
				row.createCell(0).setCellValue("Summary");
				sheet.addMergedRegion(new CellRangeAddress((results.size() + 1), (results.size() + 1), 0,2));
				row.createCell(3).setCellFormula("SUM(E2:E" + (results.size() + 1) + ")");
				row.createCell(4).setCellFormula("SUM(F2:F" + (results.size() + 1) + ")");
				row.createCell(5).setCellFormula("SUM(G2:G" + (results.size() + 1) + ")");
				return s3Service.uploadFile(workbook, path, false);
			}
		});
    }
    
    private String objectToCSV(Object object, Class clazz, String[] exportField) {
    	List<String> fields = new ArrayList<>();
    	BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(object);
    	for (int i = 0; i < exportField.length; i++) {
    		Object value = wrapper.getPropertyValue(exportField[i]);
    		fields.add("" + value.toString()!=null?value.toString():"");
		}
    	
		return String.join(",", fields);
	}
}
