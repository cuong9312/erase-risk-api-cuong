package com.fintasoft.eraserisk.model.common;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateProductSec {
    private Long secId;
    private String secCode;
    private String clientCode;
}
