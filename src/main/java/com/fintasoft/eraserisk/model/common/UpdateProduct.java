package com.fintasoft.eraserisk.model.common;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class UpdateProduct {
    private Long productId;
    private String productCode;
    private List<UpdateProductSec> secs;

    public UpdateProduct(Long productId, String productCode) {
        this.productId = productId;
        this.productCode = productCode;
        this.secs = new ArrayList<>();
    }
}
