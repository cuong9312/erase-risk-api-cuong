package com.fintasoft.eraserisk.model.db;

import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "strategy_provider")
@Data
public class StrategyProvider {
	@Id
    @GeneratedValue
    private long id;
    @Column
    private String companyName;
    @Column
    private String companyNameKr;
    @Column
    private String companyImage;
    @Column
    private String shortDescription;
    @Column
    private String longDescription;
    @Column
    private String note;
    @Column
    private String shortDescriptionKr;
    @Column
    private String longDescriptionKr;
    @Column
    private String noteKr;
    
    @OneToMany(mappedBy = "strategyProvider")
    public List<Strategy> strategies;
}
