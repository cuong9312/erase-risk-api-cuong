package com.fintasoft.eraserisk.model.db;

import lombok.Data;

import javax.persistence.*;

import com.fintasoft.eraserisk.constances.PaymentStatusEnum;

import java.sql.Timestamp;
import java.util.List;

@Data
@Entity
@Table(name = "user_purchase_payment")
public class UserPurchasePayment {
    @Id
    @GeneratedValue
    @Column
    private long id;
    @Column
    private double paidAmount;
    @Column
    private String note;
    @Column
    @Enumerated(EnumType.STRING)
    private PaymentStatusEnum status;
    @Column
    private String transactionNo;
    @Column
    private String payUrl;
    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
    @Column
    private Timestamp purchaseDate;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
    public User user;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="payment_method")
    public PaymentMethod paymentMethod;

    @OneToMany(fetch=FetchType.LAZY, mappedBy = "payment")
    public List<UserPurchaseHistory> items;
}
