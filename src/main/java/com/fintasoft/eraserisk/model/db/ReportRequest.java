package com.fintasoft.eraserisk.model.db;

import java.sql.Timestamp;

import javax.persistence.*;

import com.fintasoft.eraserisk.constances.ReportStatusEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "report_request")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@NamedStoredProcedureQueries({
@NamedStoredProcedureQuery(name = "ReportRequest.getNextReportId",
		procedureName = "query_report",
		parameters = {
				@StoredProcedureParameter(mode = ParameterMode.OUT, name = "reportId", type = Long.class)
		})
})
public class ReportRequest {
	@Id
	@GeneratedValue
    private long id;
	@Column
	@Enumerated(EnumType.STRING)
    private ReportStatusEnum status;
	@Column
	private String filePath;
	@Column
	private String sqlParam;
	@Column
	private String rootClass;
	@Column
	private String responseClass;
	@Column
	private String exportField;
    @Column
    private String outputType;
	@Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
    @Column
    private String createdByUserId;
}
