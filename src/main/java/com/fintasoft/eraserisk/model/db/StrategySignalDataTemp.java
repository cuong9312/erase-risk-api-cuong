package com.fintasoft.eraserisk.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "strategy_signal_data_temp")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StrategySignalDataTemp {
	@Id
	@GeneratedValue
    private long id;
    @Column
    private String productCode;
    @Column
    private String strategyCode;
    @Column
    private String signalType;
    @Column
    private Double price;
    @Column
    private String orderType;
    @Column
    private String platform;
    @Column
    private boolean allowMultipleBuy;
    @Column
    private Timestamp createdAt;
	@Column
    public boolean deleted = false;
}
