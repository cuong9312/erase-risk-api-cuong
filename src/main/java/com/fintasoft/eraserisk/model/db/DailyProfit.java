package com.fintasoft.eraserisk.model.db;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Data
@Entity
@Table(name = "daily_profit")
public class DailyProfit {

    @EmbeddedId
    private Pk pk;

    @Column
    private Double profit;
    @Column
    private Double roi;
    @Column
    private Double mdd;
    @Column
    private Double requiredCapital;
    @Column
    private Double bestTrade;
    @Column
    private Double worstTrade;
    @Column
    private Double winRate;
    @Column
    private int totalWin;
    @Column
    private int totalCount;
    @Column
    private int totalBreakEven;
    @Column
    private int totalTrade;
    @ManyToOne
    @JoinColumn(name = "strategy_id", insertable = false, updatable = false)
    private Strategy strategy;

    public DailyProfit() {
    }

    public DailyProfit(long strategyId, Date date, Double profit, Double roi, Double mdd) {
        this.pk = new Pk();
        this.pk.date = date;
        this.pk.strategyId = strategyId;
        this.profit = profit;
        this.roi = roi;
        this.mdd = mdd;
    }

    @Embeddable
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Pk implements Serializable {
        @Column
        private Date date;
        @Column(name = "strategy_id")
        private long strategyId;
    }
}
