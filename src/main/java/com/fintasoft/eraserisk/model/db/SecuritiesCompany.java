package com.fintasoft.eraserisk.model.db;

import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "securities_company")
@Data
public class SecuritiesCompany {
	@Id
    @GeneratedValue
    private long id;
    @Column
    private String secCode;
    @Column
    private String secName;
    @Column
    private String secImage;
    @Column
    private String secUrl;
    @Column
    private String note;
    @Column
    private String noteKr;

    @ManyToMany(mappedBy="securitiesCompanies")
    public List<User> users;
    
    @OneToMany(mappedBy="securitiesCompany")
    private List<StrategySecuritiesCompany> strategies;
}
