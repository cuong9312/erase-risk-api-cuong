package com.fintasoft.eraserisk.model.db;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "derivative_product")
@Data
public class DerivativeProduct {
	@Id
	@GeneratedValue
    private long id;
	@Column
	private String derivativeName;
	@Column
	private String derivativeCode;
	@Column
	private double margin;
	@Column
	private double tickValue;
	@Column
	private Date expiryDate;
	@Column
	private Double mddRate;
	@Column
	private Double unitValue;
	@Column
	private double fee;
	@Column
	private Double minMove;
	@Column
	private String timezone = "+0000";
	@Column(updatable = false, insertable = false)
    private Timestamp createdAt;
	@Column(updatable = false, insertable = false)
	private Timestamp updatedAt;

    @OneToMany(mappedBy="derivativeProduct")
    private List<DerivativeProductSec> securitiesCompanies;
    
    @OneToMany(mappedBy="derivativeProduct")
    private List<DerivativeProductPlatform> signalPlatforms;

    public double getMddRate() {
    	return this.mddRate == null ? 1d : this.mddRate.doubleValue();
	}
}
