package com.fintasoft.eraserisk.model.db;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "faq_group")
@Data
public class FaqGroup {
	@Id
    private long id;
    @Column
    private String faqGroup;
    @Column
    private String language;
   
    @OneToMany(mappedBy = "faqGroup")
    public List<Faq> faqs;
}
