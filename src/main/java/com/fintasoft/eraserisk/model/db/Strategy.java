package com.fintasoft.eraserisk.model.db;

import com.fintasoft.eraserisk.Application;
import com.fintasoft.eraserisk.constances.FrequencyTypeEnums;
import com.fintasoft.eraserisk.constances.TradingDurationEnum;
import com.fintasoft.eraserisk.constances.TradingStyleEnum;
import com.fintasoft.eraserisk.model.UserStrategySetting;
import com.fintasoft.eraserisk.services.strategy.StrategyStatusService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "strategy")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Strategy {
	@Id
    @GeneratedValue
    @Column
    private long id;
	@Column
	private String strategyCode;
    @Column
    private String strategyName;
    @Column
    private String description;
    @Column
    private String descriptionKr;
    @Column
    @Enumerated(EnumType.STRING)
    private TradingStyleEnum style;
    @Column
    @Enumerated(EnumType.STRING)
    private TradingDurationEnum tradingDuration;
    @Column
    private long strategyProviderId;
    @Column
    private long signalPlatformId;
    @Column
    private long validPeriod = 30;
    @Column
    private double autoPrice;
    @Column
    private double manualPrice;
    @Column
    private long autoSellingUnit;
    @Column
    private long manualSellingUnit;
    @Column
    private long status;
    @Column
    private long remainingAuto;
    @Column
    private long remainingManual;
    @Column(name="productId")
    private long productId;
    @Column
    private int signalFrequency;
    @Column(name = "first_signal_trading_at")
    private Timestamp firstSignalTradingAt;
    @Column
    @Enumerated(EnumType.STRING)
    private FrequencyTypeEnums frequencyType = FrequencyTypeEnums.MINUTES;
    @Column
    private int rank;
    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
    @Column
    private Timestamp lastUpdatedStatus;
    @Column
    private boolean monitorYn;

    @OneToMany(mappedBy = "strategy")
    private List<Cart> carts;

    @ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name="strategyProviderId", updatable=false, insertable=false)
    private StrategyProvider strategyProvider;
    
    @ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name="productId", insertable = false, updatable = false)
    private DerivativeProduct derivativeProduct;
    
    @ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name="signalPlatformId", updatable=false, insertable=false)
    private SignalPlatform signalPlatform;
    
    @OneToMany(mappedBy="strategy", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<StrategySecuritiesCompany> securitiesCompanies = new ArrayList<>();
    
    @ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name="status", updatable=false, insertable=false)
    private StrategyStatus strategyStatus;
    
    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private StrategyStatistic strategyStatistic;
    
    @OneToMany(mappedBy="strategy", fetch=FetchType.LAZY)
    private List<UserPurchaseHistory> userPurchaseHistories;

    @OneToMany(mappedBy="strategy", fetch=FetchType.LAZY)
    private List<StrategyPosition> strategyPositions;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "strategy")
    private List<UserStrategySetting> userStrategySettings;

    public boolean isExpired() {
        return Application.applicationContext.getBean(StrategyStatusService.class).getActivated().getId() != this.getStatus();
    }

    public void setDerivativeProduct(DerivativeProduct derivativeProduct) {
        this.derivativeProduct = derivativeProduct;
        this.productId = derivativeProduct == null ? null : derivativeProduct.getId();
    }

    public void setStrategyStatus(StrategyStatus strategyStatus) {
        this.strategyStatus = strategyStatus;
        this.status = strategyStatus == null ? 0l : strategyStatus.getId();
    }
}
