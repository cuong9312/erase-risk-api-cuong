package com.fintasoft.eraserisk.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "system_conf")
@Data
@NoArgsConstructor
public class SystemConf {

    @EmbeddedId
    private Pk pk;
    @Column
    private String description;
    @Column
    private String value;
    @Column
    private String dataType;

    @Data
    @Embeddable
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Pk implements Serializable {
        @Column(name = "name")
        private String name;
        @Column(name = "lang")
        private String lang;
    }

}
