package com.fintasoft.eraserisk.model.db;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "strategy_securities_company")
@Data
public class StrategySecuritiesCompany implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5734524627279245502L;

	@EmbeddedId
	private Pk pk;

	@ManyToOne
	@JoinColumn(name = "strategyId", updatable = false, insertable = false, referencedColumnName = "id")
	private Strategy strategy;

	@ManyToOne
	@JoinColumn(name = "secId", updatable = false, insertable = false, referencedColumnName = "id")
	private SecuritiesCompany securitiesCompany;

	public void setSecuritiesCompany(SecuritiesCompany securitiesCompany) {
		this.securitiesCompany = securitiesCompany;
		if (this.pk == null) {
			this.pk = new Pk();
		}

		this.pk.secId = securitiesCompany == null ? null : securitiesCompany.getId();
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
		if (this.pk == null) {
			this.pk = new Pk();
		}

		this.pk.strategyId = strategy == null ? null : strategy.getId();
	}

	@Column
	private long autoSellingUnit;
	
	@Column
	private long remainingSellingUnit;

	@Embeddable
	@Data
	public static class Pk implements Serializable {
		@Column
		private Long strategyId;
		@Column
		private Long secId;
	}
}
