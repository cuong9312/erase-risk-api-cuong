package com.fintasoft.eraserisk.model.db;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fintasoft.eraserisk.constances.OrderTypeEnum;
import com.fintasoft.eraserisk.constances.SignalEnum;

import lombok.Data;

@Entity
@Table(name = "user_auto_order")
@Data
public class UserAutoOrder {
	@Id
	@GeneratedValue
    private long id;
    @Column
    private String accountNumber;
    @Column
    private String orderNo;
    @Column
    private double price;
    @Column
    private long quantity;
    @Column
    private Double filledPrice;
    @Column
    private Long filledQuantity;
    @Column
    @Enumerated(EnumType.STRING)
    private SignalEnum orderAction;
    @Column
    @Enumerated(EnumType.STRING)
    private OrderTypeEnum orderType;
    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="purchase_id")
    public UserPurchaseHistory userPurchaseHistory;
}
