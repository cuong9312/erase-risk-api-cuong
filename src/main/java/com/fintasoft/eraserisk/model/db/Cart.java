package com.fintasoft.eraserisk.model.db;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "cart")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Cart {
	@Id
	@GeneratedValue
    private long id;
    @Column
    private int autoYn;
    @Column
    private long quantity;
    @Column(name = "user_id")
    private long userId;
    @Column(name = "strategy_id")
    private long strategyId;
    @Column(name = "sec_id")
    private long secId;
    @Column
    private double price;
    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id", updatable = false, insertable = false)
    public User user;

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="strategy_id", updatable = false, insertable = false)
    public Strategy strategy;

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
        this.strategyId = strategy.getId();
    }
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sec_id", updatable = false, insertable = false)
    public SecuritiesCompany securitiesCompany;

    public void setSecuritiesCompany(SecuritiesCompany securitiesCompany) {
        this.securitiesCompany = securitiesCompany;
        this.secId = securitiesCompany.getId();
    }

    public boolean isAuto() {
        return this.getAutoYn() > 0;
    }
}
