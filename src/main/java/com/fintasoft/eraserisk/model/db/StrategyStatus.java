package com.fintasoft.eraserisk.model.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "strategy_status")
@Data
public class StrategyStatus {
	public static int STRATEGY_STATUS__STANDBY = 0;
	public static int STRATEGY_STATUS__ACTIVATED = 1;
	public static int STRATEGY_STATUS__DISABLED = 2;
	
	
	@Id
    private long id;
	@Column
	private String status;
    @Column
    private String description;
}
