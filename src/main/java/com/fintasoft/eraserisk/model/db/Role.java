package com.fintasoft.eraserisk.model.db;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "role")
@Data
public class Role {
	@Id
    private long id;
    @Column
    private String roleName;
    @Column
    private String description;
    
    @ManyToMany(mappedBy="roles")
    public List<User> users;
}
