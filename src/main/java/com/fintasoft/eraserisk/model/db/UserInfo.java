package com.fintasoft.eraserisk.model.db;

import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Data;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "user_info")
@Data
public class UserInfo {
	@Id
    private long id;
    @Column
    private String fullName;
    @Column
    private String phoneNumber;
    @Column
    private Date birthday;
    @Column
    private Integer emailNotification = 0;
    @Column
    private Integer mobileNotification = 0;
    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
    
    @MapsId 
    @OneToOne(mappedBy = "userInfo")
    @JoinColumn(name = "id")
    public User user;

    public String getLocalPhoneNumber() {
        return ConvertUtils.fromIntlToLocal(this.phoneNumber);
    }
}
