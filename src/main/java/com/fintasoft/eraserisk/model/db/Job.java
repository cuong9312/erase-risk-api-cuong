package com.fintasoft.eraserisk.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

/*
DROP PROCEDURE IF EXISTS `query_jobs`;
DELIMITER ||

CREATE PROCEDURE query_jobs (IN type VARCHAR(32), IN nodeId VARCHAR(256), IN expiryMS INT, OUT startedAt Timestamp)
BEGIN
DECLARE startedAt Timestamp;
DECLARE bDone INT;
DECLARE curs CURSOR FOR  select j.started_at as id from jobs as j where j.`type` = type and (j.state = 0 or j.state_expiry_at is null or j.state_expiry_at < now());
DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;
OPEN curs;
SET bDone = 0;
REPEAT
FETCH curs INTO startedAt;
Update jobs SET state = 1, started_at = NOW(), state_expiry_at = TIMESTAMPADD(SECOND, 60, NOW()), running_on_node = nodeId where `type` = type;
UNTIL bDone END REPEAT;
SELECT startedAt;
CLOSE curs;
END||

DELIMITER ;
*/

@Entity
@Table(name = "jobs")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "Job.queryJob",
                procedureName = "query_jobs",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "type", type = String.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "nodeId", type = String.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "expiryMS", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.OUT, name = "reportId", type = Timestamp.class)
                })
})
public class Job {
	@Id
    private String type;
    @Column
    private String name;
    @Column
    private Timestamp startedAt;
    @Column
    private int state;
    @Column
    private Timestamp stateExpiryAt;
    @Column
    private String runningOnNode;
}
