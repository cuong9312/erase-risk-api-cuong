package com.fintasoft.eraserisk.model.db;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "signal_broadcast")
@Data
@Builder
public class SignalBroadcast {
    @Id
    @GeneratedValue
    private long id;
    @Column
    private Long signalId;
    @Column
    private boolean tempYn;
    @Column
    private boolean broadcast;
    @Column(updatable = false, insertable = false)
    private Timestamp createdAt;
    @Column(updatable = false, insertable = false)
    private Timestamp updatedAt;
    @Column
    private boolean deleted;
}
