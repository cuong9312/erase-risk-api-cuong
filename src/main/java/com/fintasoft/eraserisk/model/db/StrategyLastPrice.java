package com.fintasoft.eraserisk.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "strategy_last_price")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StrategyLastPrice {
	@Id
    @Column
    private Long strategyId;
	@Column
	private Double price;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
}
