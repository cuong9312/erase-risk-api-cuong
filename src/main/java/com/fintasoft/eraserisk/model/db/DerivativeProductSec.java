package com.fintasoft.eraserisk.model.db;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "derivative_product_sec")
@Data
public class DerivativeProductSec implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1882606167891124012L;

	@EmbeddedId
	private Pk pk;

	@ManyToOne
	@JoinColumn(name = "product_id", updatable = false, insertable = false)
	private DerivativeProduct derivativeProduct;

	@ManyToOne
	@JoinColumn(name = "sec_id", updatable = false, insertable = false)
	private SecuritiesCompany securitiesCompany;

	public void setSecuritiesCompany(SecuritiesCompany securitiesCompany) {
		this.securitiesCompany = securitiesCompany;
		if (this.pk == null) {
			this.pk = new Pk();
		}

		this.securitiesCompany = securitiesCompany;
		this.pk.secId = securitiesCompany == null ? null : securitiesCompany.getId();
	}

	public void setDerivativeProduct(DerivativeProduct derivativeProduct) {
		this.derivativeProduct = derivativeProduct;
		if (this.pk == null) {
			this.pk = new Pk();
		}

		this.derivativeProduct = derivativeProduct;
		this.pk.productId = derivativeProduct == null ? null : derivativeProduct.getId();
	}

	@Column
	private String clientCode;

	@Embeddable
	@Data
	public static class Pk implements Serializable {
		@Column(name = "product_id")
		private Long productId;
		@Column(name = "sec_id")
		private Long secId;
	}
}
