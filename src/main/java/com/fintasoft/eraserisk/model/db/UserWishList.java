package com.fintasoft.eraserisk.model.db;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "user_wishlist")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserWishList {
	@Id
	@GeneratedValue
    private long id;
    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
    @Column(name="user_id")
    private Long userId;
    @Column(name="strategy_id")
    private Long strategyId;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id", updatable = false, insertable = false)
    public User user;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="strategy_id", updatable = false, insertable = false)
    public Strategy strategy;

    public void setUser(User user) {
        this.user = user;
        this.userId = user == null ? null : user.getId();
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
        this.strategyId = strategy == null ? null : strategy.getId();
    }
}
