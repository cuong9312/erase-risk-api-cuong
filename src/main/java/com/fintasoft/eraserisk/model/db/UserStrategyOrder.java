package com.fintasoft.eraserisk.model.db;

import com.fintasoft.eraserisk.constances.OrderTypeEnum;
import com.fintasoft.eraserisk.constances.SignalEnum;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "user_strategy_order")
@Data
public class UserStrategyOrder {
    @Id
    @GeneratedValue
    private long id;
    @Column
    private String accountNumber;
    @Column
    private String orderNo;
    @Column
    private int autoYn;
    @Column
    private double price;
    @Column
    private long quantity;
    @Column
    private Double filledPrice;
    @Column
    private Long filledQuantity;
    @Enumerated(EnumType.STRING)
    private SignalEnum orderAction;
    @Column
    @Enumerated(EnumType.STRING)
    private OrderTypeEnum orderType;
    @Column
    private Timestamp tradingAt;
    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
    @Column(name = "signal_id")
    private Long signalId;
    @Column(name = "signal_temp_id")
    private Long signalTempId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    public User user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_id")
    public SecuritiesCompany securitiesCompany;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "strategy_id")
    public Strategy strategy;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "signal_id", insertable = false, updatable = false)
    public StrategySignalData signalData;
}
