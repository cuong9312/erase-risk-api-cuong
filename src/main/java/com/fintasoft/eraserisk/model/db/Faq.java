package com.fintasoft.eraserisk.model.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "faq")
@Data
public class Faq {
	@Id
    private long id;
    @Column
    private String question;
    @Column
    private String answer;
    @Column
    private long groupId;
    
    @ManyToOne(fetch=FetchType.LAZY)
   	@JoinColumn(name="groupId", updatable=false, insertable=false)
    public FaqGroup faqGroup;
}
