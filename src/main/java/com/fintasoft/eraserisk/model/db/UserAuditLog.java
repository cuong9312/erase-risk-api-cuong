package com.fintasoft.eraserisk.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "user_audit_log")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserAuditLog {
	public static int USER_STATUS__INACTIVE = 0;
	public static int USER_STATUS__ACTIVE = 10;
	
	@Id
    @GeneratedValue
    private long id;
    @Column
    private long userId;
    @Column
    private long secId;
    @Column
    private Timestamp loginTime;
    @Column
    private Timestamp logoutTime;
}
