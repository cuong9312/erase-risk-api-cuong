package com.fintasoft.eraserisk.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "user")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
	public static final int USER_STATUS__INACTIVE = 0;
	public static final int USER_STATUS__ACTIVE = 10;
	
	@Id
    @GeneratedValue
    private long id;
    @Column
    private String username;
    @Column
    private String authKey;
    @Column
    private String passwordHash;
    @Column
    private String passwordResetToken;
    @Column
    private String email;
    @Column
    private String verifyEmailToken;
    @Column
    private String resetPasswordToken;
    @Column
    private int status;
    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;

    
    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private UserInfo userInfo;
    
    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private AdminInfo adminInfo;
    
    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(
			name = "user_securities_company",
			joinColumns = {@JoinColumn(name = "user_id", updatable = false, insertable = false)},
			inverseJoinColumns = {@JoinColumn(name="sec_id", updatable = false, insertable = false)}
	)
    private List<SecuritiesCompany> securitiesCompanies = new ArrayList<>();
    
    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(
			name = "user_role",
			joinColumns = {@JoinColumn(name = "user_id", updatable = false, insertable = false)},
			inverseJoinColumns = {@JoinColumn(name="role_id", updatable = false, insertable = false)}
	)
    private List<Role> roles = new ArrayList<>();
    
    @ManyToMany(cascade=CascadeType.ALL)
    @JoinTable(
			name = "user_authority",
			joinColumns = {@JoinColumn(name = "user_id", updatable = false, insertable = false)},
			inverseJoinColumns = {@JoinColumn(name="authority_id", updatable = false, insertable = false)}
	)
    private List<Authority> authorities = new ArrayList<>();
    
    @ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinTable(
			name = "admin_securities_company",
			joinColumns = {@JoinColumn(name = "user_id", updatable = false, insertable = false)},
			inverseJoinColumns = {@JoinColumn(name="sec_id", updatable = false, insertable = false)}
	)
    private List<SecuritiesCompany> adminSecuritiesCompanies = new ArrayList<>();
    
    @ManyToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    @JoinTable(
			name = "admin_strategy_provider",
			joinColumns = {@JoinColumn(name = "user_id", updatable = false, insertable = false)},
			inverseJoinColumns = {@JoinColumn(name="strategy_provider_id", updatable = false, insertable = false)}
	)
    private List<StrategyProvider> adminStrategyProviders = new ArrayList<>();

    @OneToMany(fetch=FetchType.LAZY, mappedBy = "user")
    private List<UserPurchaseHistory> purchases = new ArrayList<>();
}
