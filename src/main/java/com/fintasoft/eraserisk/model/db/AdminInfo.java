package com.fintasoft.eraserisk.model.db;

import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Data;

import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "admin_info")
@Data
public class AdminInfo {
	@Id
    private long id;
    @Column
    private String fullName;
    @Column
    private String phoneNumber;
    @Column
    private String cellNumber;
    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
    
    @MapsId 
    @OneToOne(mappedBy = "adminInfo")
    @JoinColumn(name = "id")
    public User user;
    
    public String getLocalPhoneNumber() {
        return ConvertUtils.fromIntlToLocal(this.phoneNumber);
    }
}
