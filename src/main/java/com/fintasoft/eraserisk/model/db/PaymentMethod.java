package com.fintasoft.eraserisk.model.db;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "payment_method")
public class PaymentMethod {
    @Id
    @Column
    private long id;
    @Column
    private String paymentMethod;
    @Column
    private String methodCode;
}
