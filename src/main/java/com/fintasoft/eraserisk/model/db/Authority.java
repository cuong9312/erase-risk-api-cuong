package com.fintasoft.eraserisk.model.db;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "authority")
@Data
public class Authority {
	@Id
    private long id;
    @Column
    private String authority;
    @Column
    private String description;
    
    @ManyToMany(mappedBy="authorities")
    public List<User> users;
}
