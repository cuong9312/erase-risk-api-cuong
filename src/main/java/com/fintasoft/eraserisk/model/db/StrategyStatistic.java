package com.fintasoft.eraserisk.model.db;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "strategy_statistic")
@Data
public class StrategyStatistic {
	@Id
    private long id;
	@Column
	private double requiredCapital;
	@Column
	private double netProfit;
	@Column
	private double worstTrade;
	@Column
	private double bestTrade;
	@Column
	private double roi;
	@Column
	private double mdd;
    @Column
    private long winCount;
    @Column
    private long totalCount;
    @Column
    private double winRate;
    @Column
    private long totalTrade;
    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
    @Column
    private Integer totalDailyProfitRecords;
    
    @MapsId 
    @OneToOne(mappedBy = "strategyStatistic")
    @JoinColumn(name = "id")
    public Strategy strategy;

    public int getTotalDailyProfitRecords() {
    	return this.totalDailyProfitRecords == null ? 0 : this.totalDailyProfitRecords;
	}
    
    public void resetData(){
    	this.setRequiredCapital(0);
    	this.setNetProfit(0);
    	this.setBestTrade(0);
    	this.setWorstTrade(0);
    	this.setMdd(0);
    	this.setWinCount(0);
    	this.setTotalCount(0);
    	this.setTotalTrade(0);
    	this.setRoi(0);
    }
}
