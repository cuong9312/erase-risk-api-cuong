package com.fintasoft.eraserisk.model.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "strategy_positions")
@Data
public class StrategyPosition {
	@EmbeddedId
    private Pk pk;
    @Column
    private long positionNo;
    @Column
    private String value;
    @Column(precision = 20, scale = 4)
    private Double profit;
    @Column(precision = 20, scale = 4)
    private Double mdd;
    @Column(precision = 20, scale = 4)
    private Double roi;
    @Column
    private Double requiredCapital;
    @Column
    private Double netProfit;
    @Column
    private Double bestTrade;
    @Column
    private Double worstTrade;
    @Column
    private Double winRate;
    @Column
    private int totalWin;
    @Column
    private int totalBreakEven;
    @Column
    private int totalCount;
    @Column
    private int totalTrade;

    public StrategyPosition() {
    }

    public StrategyPosition(long strategyId, String category, long positionNo, String value,
                            Double roi, Double profit, Double mdd
    ) {
        this.pk = new Pk();
        this.pk.strategyId = strategyId;
        this.pk.category = category;
        this.positionNo = positionNo;
        this.value = value;
        this.roi = roi;
        this.profit = profit;
        this.mdd = mdd;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="strategy_id", updatable=false, insertable=false)
    private Strategy strategy;

    @Embeddable
    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Pk implements Serializable {
        @Column(name = "strategy_id")
        private long strategyId;
        @Column
        private String category;
    }
}
