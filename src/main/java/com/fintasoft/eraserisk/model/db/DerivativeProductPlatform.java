package com.fintasoft.eraserisk.model.db;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "derivative_product_platform")
@Data
public class DerivativeProductPlatform implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4839685044863137290L;

	@EmbeddedId
	private Pk pk;

	@ManyToOne
	@JoinColumn(name = "productId", updatable = false, insertable = false, referencedColumnName = "id")
	private DerivativeProduct derivativeProduct;

	@ManyToOne
	@JoinColumn(name = "signalPlatformId", updatable = false, insertable = false, referencedColumnName = "id")
	private SignalPlatform signalPlatform;

	public void setSignalPlatform(SignalPlatform signalPlatform) {
		this.signalPlatform = signalPlatform;
		if (this.pk == null) {
			this.pk = new Pk();
		}

		this.pk.signalPlatformId = signalPlatform == null ? null : signalPlatform.getId();
	}

	public void setDerivativeProduct(DerivativeProduct derivativeProduct) {
		this.derivativeProduct = derivativeProduct;
		if (this.pk == null) {
			this.pk = new Pk();
		}

		this.pk.productId = derivativeProduct == null ? null : derivativeProduct.getId();
	}

	@Column
	private String tradingCode;

	@Embeddable
	@Data
	public static class Pk implements Serializable {
		@Column
		private Long productId;
		@Column
		private Long signalPlatformId;
	}
}
