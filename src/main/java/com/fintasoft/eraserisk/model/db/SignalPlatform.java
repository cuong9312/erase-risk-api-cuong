package com.fintasoft.eraserisk.model.db;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "signal_platform")
@Data
public class SignalPlatform {
	@Id
    private long id;
    @Column
    private String platformName;

    @OneToMany(mappedBy = "signalPlatform")
    public List<DerivativeProductPlatform> derivativeProductPlatforms;
}
