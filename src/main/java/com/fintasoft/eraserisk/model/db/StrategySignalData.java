package com.fintasoft.eraserisk.model.db;

import com.fintasoft.eraserisk.constances.OrderTypeEnum;
import com.fintasoft.eraserisk.constances.SignalEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "strategy_signal_data")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StrategySignalData {
	@Id
	@GeneratedValue
	private long id;
	@Column
	private String productCode;
	@Column(name = "`signal`")
	@Enumerated(EnumType.STRING)
	private SignalEnum signal;
	@Column
	private double price;
	@Column
	private Double profit;
	@Column
	private double cumulativeProfit;
	@Column
	private double highestProfit;
	@Column
	private double drawdown;
	@Column
	@Enumerated(EnumType.STRING)
	private OrderTypeEnum orderType;
	@Column
	private Timestamp tradingAt;
	@Column(insertable = false, updatable = false)
	private Timestamp createdAt;
	@Column(name = "close_by_id")
	private Long closeById;
	@Column(name = "close_for_id")
	private Long closeForId;

	@Column(name = "strategy_id")
	public long strategyId;

	@Column(name = "deleted")
	public boolean deleted = false;
	@Column(name = "autoGenerate")
	public boolean autoGenerate = false;
	@Column
	public Long tempId;


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "strategy_id", insertable = false, updatable = false)
	public Strategy strategy;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "close_by_id", insertable = false, updatable = false)
	public StrategySignalData closeBySignal;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "close_for_id", insertable = false, updatable = false)
	public StrategySignalData closeForSignal;

	public void setCloseBySignal(StrategySignalData closeBySignal) {
		this.closeBySignal = closeBySignal;
		this.closeById = this.closeBySignal == null ? null : this.closeBySignal.getId();
		if (closeBySignal != null) {
			closeBySignal.setCloseForId(this.getId());
		}
	}

	public void setStrategy(Strategy strategy) {
		if (strategy == null) throw new IllegalArgumentException();
		this.strategy = strategy;
		this.strategyId = this.strategy.getId();
	}

	public double getSafetyProfit() {
		return this.profit == null ? 0d : this.profit;
	}
}
