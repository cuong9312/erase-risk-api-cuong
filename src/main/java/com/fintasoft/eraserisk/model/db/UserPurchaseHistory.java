package com.fintasoft.eraserisk.model.db;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fintasoft.eraserisk.constances.PaymentStatusEnum;
import lombok.Data;

@Entity
@Table(name = "user_purchase_history")
@Data
public class UserPurchaseHistory {
	@Id
	@GeneratedValue
    private long id;
    @Column
    private int autoYn;
    @Column
    private long quantity;
    @Column
    private double unitPrice;
    @Column
    private double totalPrice;
    @Column
    private long validPeriod;
    @Column
    private long lotSize;
    @Column
    private long remainingQuantity;
    @Column
    private Timestamp purchaseDate;
    @Column
    private Timestamp expirationDate;
    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;
    @Column(insertable = false, updatable = false)
    private Timestamp updatedAt;
    @Column(name="strategy_id")
    private Long strategyId;
    @Column(name="sec_id")
    private Long secId;
    @Column
    private boolean settleExpiry;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
    public User user;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="strategy_id", updatable = false, insertable = false)
    public Strategy strategy;
    
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="sec_id", updatable = false, insertable = false)
    public SecuritiesCompany securitiesCompany;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="payment_id")
    public UserPurchasePayment payment;

    public boolean isExpired() {
        return this.expirationDate != null? (this.expirationDate.before(new Timestamp(System.currentTimeMillis())) && payment.getStatus()== PaymentStatusEnum.PENDING) : false;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
        this.strategyId = this.strategy == null ? null : this.strategy.getId();
    }

    public void setSecuritiesCompany(SecuritiesCompany securitiesCompany) {
        this.securitiesCompany = securitiesCompany;
        this.secId = this.securitiesCompany == null ? null : this.securitiesCompany.getId();
    }

    public boolean isAuto() {
        return this.autoYn > 0;
    }
}
