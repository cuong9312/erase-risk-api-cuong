package com.fintasoft.eraserisk.model.query;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
public class TradeAutoReport {
    private Integer userId;
    private String username;
    private String fullName;
    private Timestamp tradingAt;
    private Integer strategyId;
    private String strategyName;
    private Integer providerId;
    private String providerName;
    private Integer secId;
    private String secName;
    private Integer quantity;

    @Data
    @AllArgsConstructor
    public static class Summary {
        private int totalQuantity;
    }
}
