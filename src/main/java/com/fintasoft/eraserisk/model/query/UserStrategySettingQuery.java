package com.fintasoft.eraserisk.model.query;

import com.fintasoft.eraserisk.model.api.response.ComplexUserStrategySettingResponse;
import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;

@Data
public class UserStrategySettingQuery {
    private long userId;
    private long strategyId;
    private String strategyName;
    private boolean auto;
    private Timestamp expiredTime;
    private int availableQuantity;
    private boolean autoDisabled;
    private int lotSize;
    private int balance;
    private String positionType;
    private long secId;

    public static UserStrategySettingQuery from(Object[] row) {
        UserStrategySettingQuery instance = new UserStrategySettingQuery();
        instance.strategyName = (String) row[0];
        instance.userId = (Integer) row[1];
        instance.strategyId = (Integer) row[2];
        instance.auto = (Boolean) row[3];
        instance.autoDisabled = (Boolean) row[4];
        instance.positionType = (String) row[5];
        instance.balance = (Integer) row[6];
        instance.lotSize = (Integer) row[7];
        instance.availableQuantity = ((BigDecimal) row[8]).intValue();
        instance.expiredTime = (Timestamp) row[9];
        instance.secId = (Integer) row[10];

        return instance;
    }

    public ComplexUserStrategySettingResponse to() {
        ComplexUserStrategySettingResponse instance = new ComplexUserStrategySettingResponse();
        instance.setAvailableQuantity(availableQuantity);
        instance.setAuto(auto);
        instance.setAutoDisabled(autoDisabled);
        instance.setLotSize(lotSize);
        instance.setBalance(balance);
        instance.setPositionType(positionType);
        return instance;
    }
}
