package com.fintasoft.eraserisk.model.query;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

@Data
@AllArgsConstructor
public class RevenueByDay implements Serializable {
    private Date purchaseDate;
    private long countAuto;
    private long countTotal;
    private double amount;
}
