package com.fintasoft.eraserisk.model.query;

import lombok.Data;

import java.io.Serializable;

@Data
public class ExportRevenue implements Serializable {
    private String purchaseDate;
    private Long countAuto;
    private Long countTotal;
    private Double amount;
    private Long countManual;

    public ExportRevenue(String purchaseDate, Long countAuto, Long countTotal, Double amount) {
        this.purchaseDate = purchaseDate;
        this.countAuto = countAuto;
        this.countTotal = countTotal;
        this.amount = amount;
        this.countManual = this.countTotal - this.countAuto;
    }
}
