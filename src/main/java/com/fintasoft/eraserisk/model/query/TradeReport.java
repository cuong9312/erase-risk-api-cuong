package com.fintasoft.eraserisk.model.query;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class TradeReport {
    public long userId;
    public long secId;
    public Timestamp loginTime;
    public Timestamp logoutTime;
    public Integer totalTrade;
    public Integer totalAutoTrade;
    public String username;
    public String fullName;
    public String securityName;
    public Integer totalManualTrade;

    public TradeReport(long userId, long secId, Timestamp loginTime, Timestamp logoutTime, int totalTrade, int totalAutoTrade, String username, String fullName, String securityName) {
        this.userId = userId;
        this.secId = secId;
        this.loginTime = loginTime;
        this.logoutTime = logoutTime;
        this.totalTrade = totalTrade;
        this.totalAutoTrade = totalAutoTrade;
        this.username = username;
        this.fullName = fullName;
        this.securityName = securityName;
        this.totalManualTrade = totalTrade - totalAutoTrade;
    }

    @Data
    public static class Summary {
        private int totalTrade;
        private int totalAutoTrade;
        private int totalManualTrade;

        public Summary(int totalTrade, int totalAutoTrade) {
            this.totalTrade = totalTrade;
            this.totalAutoTrade = totalAutoTrade;
            this.totalManualTrade = totalTrade - totalAutoTrade;
        }
    }
}
