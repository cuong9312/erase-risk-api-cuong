package com.fintasoft.eraserisk.model.query;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class RevenueSummary implements Serializable {
    private Long countAuto;
    private Long countTotal;
    private Double amount;
}
