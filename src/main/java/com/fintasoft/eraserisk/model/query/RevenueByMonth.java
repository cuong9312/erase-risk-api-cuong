package com.fintasoft.eraserisk.model.query;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.sql.Date;

@Data
@AllArgsConstructor
public class RevenueByMonth implements Serializable {
    private Integer purchaseDate;
    private long countAuto;
    private long countTotal;
    private double amount;
}
