package com.fintasoft.eraserisk.model.kafka;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.Status;
import lombok.Data;

import java.io.IOException;
import java.util.UUID;

@Data
public class Message {
    public static final String SOURCE_ID = "EAPI";
    private MessageTypeEnum messageType;
    private String sourceId;
    private String messageId;
    private String uri;
    private ResponseDestination responseDestination;
    private Object data;

    public <T> T getData(ObjectMapper objectMapper, Class<T> clazz) throws IOException {
        return objectMapper.readValue(objectMapper.writeValueAsBytes(data), clazz);
    }

    public <T> T getData(ObjectMapper objectMapper, TypeReference typeReference) throws IOException {
        return objectMapper.readValue(objectMapper.writeValueAsBytes(data), typeReference);
    }

    public static Message create(String uri, Object data) {
        Message message = new Message();
        message.setMessageType(MessageTypeEnum.MESSAGE);
        message.setSourceId(SOURCE_ID);
        message.setMessageId(UUID.randomUUID().toString());
        message.setUri(uri);
        message.setResponseDestination(null);
        message.setData(new Response(data));

        return message;
    }

    public static Message createError(String uri, String errorCode) {
        Message message = new Message();
        message.setMessageType(MessageTypeEnum.MESSAGE);
        message.setSourceId(SOURCE_ID);
        message.setMessageId(UUID.randomUUID().toString());
        message.setUri(uri);
        message.setResponseDestination(null);
        Status status = new Status(errorCode, "");
        message.setData(new Response(status));

        return message;
    }
}
