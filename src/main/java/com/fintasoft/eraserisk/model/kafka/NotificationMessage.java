package com.fintasoft.eraserisk.model.kafka;


import com.fintasoft.eraserisk.constances.NotificationMethodEnum;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class NotificationMessage {
    private NotificationMethodEnum method;
    private String template;
    private List<Map<String, String>> templateDatas;
    private Map<String, String> configuration;
}
