package com.fintasoft.eraserisk.model.kafka;


public enum MessageTypeEnum {
    REQUEST, RESPONSE, MESSAGE;
}
