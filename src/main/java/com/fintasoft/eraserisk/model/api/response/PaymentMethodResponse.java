package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.PaymentMethod;
import lombok.Data;

@Data
public class PaymentMethodResponse {
    private long id;
    private String name;
    private String code;

    public static PaymentMethodResponse from(PaymentMethod method) {
        PaymentMethodResponse paymentMethodResponse = new PaymentMethodResponse();
        paymentMethodResponse.setId(method.getId());
        paymentMethodResponse.setName(method.getPaymentMethod());
        paymentMethodResponse.setCode(method.getMethodCode());

        return paymentMethodResponse;
    }
}
