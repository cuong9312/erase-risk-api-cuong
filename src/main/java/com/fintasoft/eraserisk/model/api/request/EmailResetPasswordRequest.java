package com.fintasoft.eraserisk.model.api.request;


import lombok.Data;

@Data
public class EmailResetPasswordRequest extends UpdatePassword {
    private String token;
}
