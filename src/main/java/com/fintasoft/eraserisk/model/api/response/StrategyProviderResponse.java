package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.StrategyProvider;

import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Data;

@Data
public class StrategyProviderResponse {
    private long id;
    private String companyName;
    private String companyNameKr;
    private String companyImage;
    private String shortDescription;
    private String longDescription;
    private String note;
    private String shortDescriptionKr;
    private String longDescriptionKr;
    private String noteKr;

    public static StrategyProviderResponse from(StrategyProvider strategyProvider) {
        if (strategyProvider == null) return null;
    	StrategyProviderResponse strategyProviderResponse = StrategyProviderResponse.shortFrom(strategyProvider);
    	strategyProviderResponse.setLongDescription(strategyProvider.getLongDescription());
    	strategyProviderResponse.setLongDescriptionKr(strategyProvider.getLongDescriptionKr());

        return strategyProviderResponse;
    }

    public static StrategyProviderResponse detailFrom(StrategyProvider strategyProvider) {
        if (strategyProvider == null) return null;
    	return StrategyProviderResponse.from(strategyProvider);
    }
    
    public static StrategyProviderResponse shortFrom(StrategyProvider strategyProvider) {
        if (strategyProvider == null) return null;
    	StrategyProviderResponse strategyProviderResponse = new StrategyProviderResponse();
    	strategyProviderResponse.setId(strategyProvider.getId());
    	strategyProviderResponse.setCompanyName(strategyProvider.getCompanyName());
    	strategyProviderResponse.setCompanyNameKr(strategyProvider.getCompanyNameKr());
    	strategyProviderResponse.setCompanyImage(ConvertUtils.translateResource(strategyProvider.getCompanyImage()));
    	strategyProviderResponse.setShortDescription(strategyProvider.getShortDescription());
    	strategyProviderResponse.setShortDescriptionKr(strategyProvider.getShortDescriptionKr());
        strategyProviderResponse.setNote(strategyProvider.getNote());
        strategyProviderResponse.setNoteKr(strategyProvider.getNoteKr());
        return strategyProviderResponse;
    }
}
