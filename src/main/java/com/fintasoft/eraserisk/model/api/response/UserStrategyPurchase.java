package com.fintasoft.eraserisk.model.api.response;

import lombok.Data;

import java.util.List;

@Data
public class UserStrategyPurchase {
    private StrategyResponse strategy;
    private DerivativeProductResponse product;
    private StrategyStatisticResponse strategyStatistic;
    private StrategyProviderResponse provider;
    private SecuritiesCompanyResponse securitiesCompany;
    private int availableQuantity;
    private String maxExpiredDate;
    private List<DailyProfitResponse> dailyProfits;
    private long userId;
    private boolean auto;
}
