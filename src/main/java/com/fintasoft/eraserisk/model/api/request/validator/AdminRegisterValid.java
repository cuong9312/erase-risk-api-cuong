package com.fintasoft.eraserisk.model.api.request.validator;

import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.fintasoft.eraserisk.model.api.request.AdminRegister;
import com.fintasoft.eraserisk.utils.ValidatorHelper;


public class AdminRegisterValid implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return AdminRegister.class.equals(aClass);
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        AdminRegister adminRegister = (AdminRegister) o;
        new ValidatorHelper("fullName", adminRegister.getFullName(), errors).notBlank();
        new ValidatorHelper("username", adminRegister.getUsername(), errors).notBlank();
        new ValidatorHelper("password", adminRegister.getPassword(), errors).notBlank();
        new ValidatorHelper("email", adminRegister.getEmail(), errors).notBlank();
        new ValidatorHelper("phoneNumber", adminRegister.getPhoneNumber(), errors).notBlank();
        new ValidatorHelper("email", adminRegister.getEmail(), errors).formatCheck(new EmailValidator());
        new ValidatorHelper("username", adminRegister.getUsername(), errors).formatCheck(new UsernameValidator());
    }
}
