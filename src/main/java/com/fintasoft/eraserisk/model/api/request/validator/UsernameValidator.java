package com.fintasoft.eraserisk.model.api.request.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fintasoft.eraserisk.utils.ValidatorHelper.IValidate;

public class UsernameValidator implements IValidate {

	@Override
	public boolean isValid(Object value) {
		if (null != value.toString()) {
            String regex = "^[A-Za-z_]\\w{7,29}$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(value.toString());
            if (!matcher.matches()) {
                return false;
            }
        }
		return true;
	}
	
}
