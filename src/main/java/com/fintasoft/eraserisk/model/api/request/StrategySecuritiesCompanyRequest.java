package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StrategySecuritiesCompanyRequest {
    private long id;
    private long autoSellingUnit;
}
