package com.fintasoft.eraserisk.model.api.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fintasoft.eraserisk.model.db.DailyProfit;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DailyProfitsResponse {
    private Long strategyId;
    private List<DailyProfitResponse> dailyProfits;
    @JsonIgnore
    private boolean profit;
    @JsonIgnore
    private boolean roi;
    @JsonIgnore
    private boolean trade;
    @JsonIgnore
    private boolean count;

    public DailyProfitsResponse(Long strategyId) {
        this.strategyId = strategyId;
        this.dailyProfits = new ArrayList<>();
    }

    public DailyProfitsResponse add(DailyProfit dp) {
        this.dailyProfits.add(DailyProfitResponse.from(dp, profit, roi, trade, count));
        return this;
    }
}
