package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SystemConfUpdate {
    private String value;
    private String description;
    private String dataType;
}
