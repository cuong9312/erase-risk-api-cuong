package com.fintasoft.eraserisk.model.api.request;

import lombok.Data;

@Data
public class Login {
    private String username;
    private String password;
}
