package com.fintasoft.eraserisk.model.api.response;

import lombok.Data;

@Data
public class StrategyLiveResponse {
    private long strategyId;
    private double currentPrice;
    private DailyProfitResponse currentDailyProfit;
}
