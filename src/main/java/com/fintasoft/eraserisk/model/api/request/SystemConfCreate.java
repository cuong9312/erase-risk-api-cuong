package com.fintasoft.eraserisk.model.api.request;

import com.fintasoft.eraserisk.model.db.SystemConf;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SystemConfCreate {
    private String name;
    private String lang;
    private String value;
    private String description;
    private String dataType;

    public SystemConf toSystemConf() {
        SystemConf systemConf = new SystemConf();
        systemConf.setPk(new SystemConf.Pk(this.name, this.lang));
        systemConf.setValue(this.value);
        systemConf.setDescription(this.description);
        systemConf.setDataType(this.dataType);
        return systemConf;
    }
}
