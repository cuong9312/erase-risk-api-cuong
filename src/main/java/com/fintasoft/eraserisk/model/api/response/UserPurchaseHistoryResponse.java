package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.UserPurchaseHistory;
import com.fintasoft.eraserisk.utils.ConvertUtils;

import lombok.Data;

@Data
public class UserPurchaseHistoryResponse {
	private long id;
    private int autoYn;
    private long quantity;
    private double unitPrice;
    private double totalPrice;
    private long validPeriod;
    private boolean isExpired;
    private boolean autoDisabled;
    private String positionType;
    private long balance;
    private String purchaseDate;
    private String expirationDate;
    private long lotSize;
    private long remainingQuantity;
    private SecuritiesCompanyResponse securitiesCompany;
    private StrategyResponse strategy;
    private UserPurchasePaymentResponse payment;

    public static UserPurchaseHistoryResponse from(UserPurchaseHistory userPurchaseHistory) {
    	UserPurchaseHistoryResponse userPurchaseHistoryResponse = new UserPurchaseHistoryResponse();
    	userPurchaseHistoryResponse.setId(userPurchaseHistory.getId());
    	userPurchaseHistoryResponse.setAutoYn(userPurchaseHistory.getAutoYn());
    	userPurchaseHistoryResponse.setPurchaseDate(ConvertUtils.fromTime(userPurchaseHistory.getPurchaseDate()));
    	userPurchaseHistoryResponse.setStrategy(StrategyResponse.from(userPurchaseHistory.getStrategy()));
    	userPurchaseHistoryResponse.setUnitPrice(userPurchaseHistory.getUnitPrice());
    	userPurchaseHistoryResponse.setTotalPrice(userPurchaseHistory.getTotalPrice());
    	userPurchaseHistoryResponse.setQuantity(userPurchaseHistory.getQuantity());
    	userPurchaseHistoryResponse.setValidPeriod(userPurchaseHistory.getValidPeriod());  	
    	userPurchaseHistoryResponse.setExpirationDate(ConvertUtils.fromTime(userPurchaseHistory.getExpirationDate()));
        userPurchaseHistoryResponse.setSecuritiesCompany(SecuritiesCompanyResponse.from(userPurchaseHistory.getSecuritiesCompany()));
        userPurchaseHistoryResponse.setPayment(UserPurchasePaymentResponse.fromItem(userPurchaseHistory.getPayment()));
//        userPurchaseHistoryResponse.setAutoDisabled(userPurchaseHistory.isAutoDisabled());
//        userPurchaseHistoryResponse.setPositionType(userPurchaseHistory.getPositionType().toString());
//        userPurchaseHistoryResponse.setBalance(userPurchaseHistory.getBalance());
//        userPurchaseHistoryResponse.setLotSize(userPurchaseHistory.getLotSize());
        userPurchaseHistoryResponse.setRemainingQuantity(userPurchaseHistory.getRemainingQuantity());
        userPurchaseHistoryResponse.setExpired(userPurchaseHistory.isExpired());

        return userPurchaseHistoryResponse;
    }
}
