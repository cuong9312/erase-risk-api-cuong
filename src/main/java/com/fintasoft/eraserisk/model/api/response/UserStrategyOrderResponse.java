package com.fintasoft.eraserisk.model.api.response;


import com.fintasoft.eraserisk.model.db.UserStrategyOrder;
import com.fintasoft.eraserisk.utils.ConvertUtils;

import lombok.Data;

@Data
public class UserStrategyOrderResponse {
    private String username;
    private String tradingAt;
    private Long quantity;
    private String strategyName;
    private String providerName;
    private String secName;

    public static UserStrategyOrderResponse fromReport (UserStrategyOrder userStrategyOrder) {
        UserStrategyOrderResponse response = new UserStrategyOrderResponse();
        response.setUsername(userStrategyOrder.getUser().getUserInfo().getFullName());
        response.setTradingAt(ConvertUtils.fromTime(userStrategyOrder.getTradingAt()));
        response.setQuantity(userStrategyOrder.getQuantity());
        if (userStrategyOrder.getStrategy() != null) {
        	response.setStrategyName(userStrategyOrder.getStrategy().getStrategyName());
        	response.setProviderName(userStrategyOrder.getStrategy().getStrategyProvider().getCompanyName());
        	response.setSecName(userStrategyOrder.getSecuritiesCompany().getSecName());
    	}

        return response;
    }
}
