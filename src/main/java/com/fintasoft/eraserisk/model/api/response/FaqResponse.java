package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.Faq;

import lombok.Data;

@Data
public class FaqResponse {
    private long id;
    private String question;
    private String answer;

    public static FaqResponse from(Faq faq) {
        if (faq == null) return null;
    	FaqResponse faqResponse = new FaqResponse();
    	faqResponse.setId(faq.getId());
    	faqResponse.setQuestion(faq.getQuestion());
    	faqResponse.setAnswer(faq.getAnswer());
        return faqResponse;
    }
}
