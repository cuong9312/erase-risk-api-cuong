package com.fintasoft.eraserisk.model.api.request.validator;

import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.fintasoft.eraserisk.model.api.request.Login;
import com.fintasoft.eraserisk.utils.ValidatorHelper;


public class LoginValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Login.class.equals(aClass);
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        Login login = (Login) o;
        new ValidatorHelper("username", login.getUsername(), errors).notBlank();
        new ValidatorHelper("password", login.getPassword(), errors).notBlank();
    }
}
