package com.fintasoft.eraserisk.model.api.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class QueryDailyProfitResponse {
    private String date;
    private List<DailyProfitResponse> strategies = new ArrayList<>();
}
