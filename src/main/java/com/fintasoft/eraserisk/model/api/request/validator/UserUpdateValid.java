package com.fintasoft.eraserisk.model.api.request.validator;

import com.fintasoft.eraserisk.model.api.request.UserUpdate;
import com.fintasoft.eraserisk.utils.ValidatorHelper;
import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public class UserUpdateValid implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return UserUpdate.class.equals(aClass);
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        UserUpdate userUpdate = (UserUpdate) o;
        new ValidatorHelper("fullName", userUpdate.getFullName(), errors).notBlank();
        new ValidatorHelper("phoneNumber", userUpdate.getPhoneNumber(), errors).notBlank();
    }
}
