package com.fintasoft.eraserisk.model.api.request.validator;

import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.fintasoft.eraserisk.model.api.request.AdminUpdate;
import com.fintasoft.eraserisk.utils.ValidatorHelper;


public class AdminUpdateValid implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return AdminUpdate.class.equals(aClass);
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        AdminUpdate adminUpdate = (AdminUpdate) o;
        new ValidatorHelper("fullName", adminUpdate.getFullName(), errors).notBlank();
        new ValidatorHelper("phoneNumber", adminUpdate.getPhoneNumber(), errors).notBlank();
        new ValidatorHelper("email", adminUpdate.getEmail(), errors).notBlank();
        new ValidatorHelper("email", adminUpdate.getEmail(), errors).formatCheck(new EmailValidator());
    }
}
