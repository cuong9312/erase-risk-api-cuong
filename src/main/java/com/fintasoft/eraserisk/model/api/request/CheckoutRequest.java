package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class CheckoutRequest {
    private String note;
    private long[] cartIds;
    private String paymentMethod;
    private Map<String, String> feedbackParams;
    private Map<String, String> returnParams;
}
