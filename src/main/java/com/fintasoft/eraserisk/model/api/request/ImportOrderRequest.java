package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ImportOrderRequest {
	private long secId;
	private OrderRequest[] orderRequests;
}
