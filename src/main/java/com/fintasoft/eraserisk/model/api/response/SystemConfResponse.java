package com.fintasoft.eraserisk.model.api.response;


import com.fintasoft.eraserisk.model.db.SystemConf;
import lombok.Data;

@Data
public class SystemConfResponse {
    private String name;
    private String lang;
    private String value;
    private String description;
    private String dataType;

    public static SystemConfResponse from(SystemConf systemConf) {
        SystemConfResponse result = new SystemConfResponse();
        result.setName(systemConf.getPk().getName());
        result.setLang(systemConf.getPk().getLang());
        result.setValue(systemConf.getValue());
        result.setDescription(systemConf.getDescription());
        result.setDataType(systemConf.getDataType());
        return result;
    }
}
