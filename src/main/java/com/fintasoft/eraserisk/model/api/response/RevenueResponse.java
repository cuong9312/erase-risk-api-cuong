package com.fintasoft.eraserisk.model.api.response;


import com.fintasoft.eraserisk.model.query.RevenueByDay;
import com.fintasoft.eraserisk.model.query.RevenueByMonth;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Data;

@Data
public class RevenueResponse {
    private String day;
    private String month;
    private long countAuto;
    private long countTotal;
    private double amount;

    public static RevenueResponse from(RevenueByDay revenueByDay) {
        RevenueResponse response = new RevenueResponse();
        response.setAmount(revenueByDay.getAmount());
        response.setCountAuto(revenueByDay.getCountAuto());
        response.setCountTotal(revenueByDay.getCountTotal());
        response.setDay(ConvertUtils.fromDate(revenueByDay.getPurchaseDate()));
        return response;
    }

    public static RevenueResponse from(RevenueByMonth revenueByMonth) {
        RevenueResponse response = new RevenueResponse();
        response.setAmount(revenueByMonth.getAmount());
        response.setCountAuto(revenueByMonth.getCountAuto());
        response.setCountTotal(revenueByMonth.getCountTotal());
        response.setDay(revenueByMonth.getPurchaseDate() == null ? null : revenueByMonth.getPurchaseDate().toString());
        return response;
    }
}
