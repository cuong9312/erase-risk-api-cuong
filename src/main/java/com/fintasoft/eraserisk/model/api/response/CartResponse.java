package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.Cart;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class CartResponse {
	private long id;
	private long quantity;
	private double price;
    private int autoYn;
    private UserResponse user;
    private StrategyResponse strategy;
    private SecuritiesCompanyResponse securitiesCompany;

    public static CartResponse from(Cart cart) {
    	CartResponse cartResponse = new CartResponse();
    	cartResponse.setId(cart.getId());
    	cartResponse.setPrice(cart.getPrice());
    	cartResponse.setQuantity(cart.getQuantity());
    	cartResponse.setAutoYn(cart.getAutoYn());
    	cartResponse.setStrategy(StrategyResponse.from(cart.getStrategy()));
    	cartResponse.setSecuritiesCompany(SecuritiesCompanyResponse.from(cart.getSecuritiesCompany()));

        return cartResponse;
    }

    @Data
	@AllArgsConstructor
    public static class CartSummary {
    	public double totalPrices;
	}
}
