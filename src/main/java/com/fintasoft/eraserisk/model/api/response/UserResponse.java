package com.fintasoft.eraserisk.model.api.response;

import java.util.ArrayList;
import java.util.List;

import com.fintasoft.eraserisk.model.db.AdminInfo;
import com.fintasoft.eraserisk.model.db.User;
import com.fintasoft.eraserisk.model.db.UserInfo;
import com.fintasoft.eraserisk.utils.ConvertUtils;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserResponse {
    private long id;
    private String username;
    private String email;
    private int status;
    private String createdAt;
    private String updatedAt;
    private List<SecuritiesCompanyResponse> securitiesCompanies;
    private String fullName;
    private String birthday;
    private String phoneNumber;
    private String cellNumber;
    private Integer emailNotification;
    private Integer mobileNotification;
    private List<StrategyProviderResponse> strategyProviders;
    private List<AuthorityResponse> authorities;
    private String secNames;

    public static UserResponse fromCustomer(User user) {
        if (user == null) return null;
        List<SecuritiesCompanyResponse> securitiesCompanies = new ArrayList<>();
        user.getSecuritiesCompanies().forEach(s -> securitiesCompanies.add(SecuritiesCompanyResponse.from(s)));
        
        UserInfo userInfo = user.getUserInfo();
        
        return UserResponse.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .status(user.getStatus())
                .createdAt(ConvertUtils.fromTime(user.getCreatedAt()))
                .updatedAt(ConvertUtils.fromTime(user.getUpdatedAt()))
                .securitiesCompanies(securitiesCompanies)
                .fullName(userInfo.getFullName())
        		.phoneNumber(userInfo.getLocalPhoneNumber())
        		.birthday(ConvertUtils.fromDate(userInfo.getBirthday()))
        		.emailNotification(userInfo.getEmailNotification())
        		.mobileNotification(userInfo.getMobileNotification())
                .build();
    }
    
    public static UserResponse fromAdmin(User user) {
        if (user == null) return null;
        List<SecuritiesCompanyResponse> securitiesCompanies = new ArrayList<>();
        user.getAdminSecuritiesCompanies().forEach(s -> securitiesCompanies.add(SecuritiesCompanyResponse.from(s)));
        
        List<StrategyProviderResponse> strategyProviders = new ArrayList<>();
        user.getAdminStrategyProviders().forEach(s -> strategyProviders.add(StrategyProviderResponse.from(s)));
        
        List<AuthorityResponse> authorities = new ArrayList<>();
        user.getAuthorities().forEach(s -> authorities.add(AuthorityResponse.from(s)));
        
        AdminInfo adminInfo = user.getAdminInfo();
        
        return UserResponse.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .status(user.getStatus())
                .createdAt(ConvertUtils.fromTime(user.getCreatedAt()))
                .updatedAt(ConvertUtils.fromTime(user.getUpdatedAt()))
                .securitiesCompanies(securitiesCompanies)
                .strategyProviders(strategyProviders)
                .fullName(adminInfo.getFullName())
        		.phoneNumber(adminInfo.getLocalPhoneNumber())
        		.cellNumber(adminInfo.getCellNumber())
        		.authorities(authorities)
                .build();
    }
    
    public static UserResponse fromReport(User user) {
    	if (user == null) return null;
        List<String> securitiesCompanies = new ArrayList<>();
        user.getSecuritiesCompanies().forEach(s -> securitiesCompanies.add(s.getSecName()));
        
        UserInfo userInfo = user.getUserInfo();
        
        return UserResponse.builder()
                .username(user.getUsername())
                .email(user.getEmail())
                .createdAt(ConvertUtils.fromTime(user.getCreatedAt()))
                .secNames(String.join(", ", securitiesCompanies))
        		.phoneNumber(userInfo.getLocalPhoneNumber())
                .build();
    }
}
