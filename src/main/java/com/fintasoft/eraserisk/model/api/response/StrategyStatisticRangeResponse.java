package com.fintasoft.eraserisk.model.api.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StrategyStatisticRangeResponse {
    int noOfSignal;
    int noOfNotExit;
    double lastCumulativeProfit;
    double profit;
    double maxDrawDown;
    double requiredCapital;
    double roi;
}
