package com.fintasoft.eraserisk.model.api.request;

import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdminRegister {
    private String fullName;
    private String username;
    private String password;
    private String email;
    private String phoneNumber;
    private String cellNumber;
    private long[] secIds;
    private long[] authIds;
    private long[] providerIds;

    public String getPhoneNumber() {
        return ConvertUtils.fromLocalToInt(this.phoneNumber);
    }
}
