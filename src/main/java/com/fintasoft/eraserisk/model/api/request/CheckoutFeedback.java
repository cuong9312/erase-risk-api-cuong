package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CheckoutFeedback {
    private String accountId;
    private String linkKey;
    private String linkVal;
    private double price;
    private int payState;
    private long paymentId;
    private String mulNo;
}
