package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.StrategySignalData;
import lombok.Data;
import org.springframework.lang.Nullable;

@Data
public class TradingHistoryResponse extends OpenPositionResponse {
    protected StrategySignalDataResponse closeSignal;
    protected Double profit;
    protected Double netProfit;

    public static TradingHistoryResponse from (
            TradingHistoryResponse response,
            StrategySignalData open,
            @Nullable StrategySignalData close
    ) {
        if (open == null) throw new IllegalArgumentException();
        if (response == null) response = new TradingHistoryResponse();
        response = (TradingHistoryResponse) OpenPositionResponse.from(response, open);
        if (close != null) {
            response.setCloseSignal(StrategySignalDataResponse.from(close));
            response.setProfit(close.getSafetyProfit() - open.getSafetyProfit());
            response.setNetProfit(close.getSafetyProfit() - open.getSafetyProfit() + open.getStrategy().getDerivativeProduct().getMargin());
        }

        return response;
    }
}
