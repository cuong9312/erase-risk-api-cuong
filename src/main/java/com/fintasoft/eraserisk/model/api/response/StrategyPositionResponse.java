package com.fintasoft.eraserisk.model.api.response;


import com.fintasoft.eraserisk.model.db.StrategyPosition;
import com.fintasoft.eraserisk.utils.DoubleUtils;
import lombok.Data;

@Data
public class StrategyPositionResponse {
    private Long strategyId;
    private String category;
    private double profit;
    private double roi;
    private double mdd;
    private long positionNo;
    
    private double requiredCapital;
    private double netProfit;
    private double bestTrade;
    private double worstTrade;
    private double winRate;
    private int totalWin;
    private int totalSignal;
    private int totalTrade;
    private int totalEven;
    private int totalLose;

    public static StrategyPositionResponse from(StrategyPosition position) {
        StrategyPositionResponse result = new StrategyPositionResponse();
        result.setRoi(DoubleUtils.defaultZero(position.getRoi()));
        result.setMdd(DoubleUtils.defaultZero(position.getMdd()));
        result.setProfit(DoubleUtils.defaultZero(position.getProfit()));
        result.setCategory(position.getPk().getCategory());
        result.setStrategyId(position.getPk().getStrategyId());
        result.setPositionNo(position.getPositionNo());

        result.setRequiredCapital(DoubleUtils.defaultZero(position.getRequiredCapital()));
        result.setNetProfit(DoubleUtils.defaultZero(position.getNetProfit()));
        result.setBestTrade(DoubleUtils.defaultZero(position.getBestTrade()));
        result.setWorstTrade(DoubleUtils.defaultZero(position.getWorstTrade()));
        result.setWinRate(DoubleUtils.defaultZero(position.getWinRate()));
        result.setTotalWin(position.getTotalWin());
        result.setTotalSignal(position.getTotalCount());
        result.setTotalTrade(position.getTotalTrade());
        result.setTotalEven(position.getTotalBreakEven());
        result.setTotalLose(position.getTotalTrade() - position.getTotalWin() - position.getTotalBreakEven());
        return result;
    }
}
