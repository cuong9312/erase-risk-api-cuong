package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DerivativeProductSecRequest {
    private long id;
    private String clientCode;
}
