package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.UserStrategySetting;
import lombok.Data;

@Data
public class PureUserStrategySettingResponse {
    private long strategyId;
    private long userId;
    private boolean isAuto;
    private boolean isAutoDisabled;
    private int lotSize;
    private double balance;
    private String positionType;
    private long secId;

    public static PureUserStrategySettingResponse from (UserStrategySetting userStrategySetting) {
        PureUserStrategySettingResponse response = new PureUserStrategySettingResponse();
        response.setAutoDisabled(userStrategySetting.isAutoDisabled());
        response.setAuto(userStrategySetting.getPk().isAuto());
        response.setBalance(userStrategySetting.getBalance());
        response.setLotSize((int) userStrategySetting.getLotSize());
        response.setPositionType(userStrategySetting.getPositionType().name());
        response.setStrategyId(userStrategySetting.getPk().getStrategyId());
        response.setUserId(userStrategySetting.getPk().getUserId());
        response.setSecId(userStrategySetting.getPk().getSecId());
        return response;
    }
}
