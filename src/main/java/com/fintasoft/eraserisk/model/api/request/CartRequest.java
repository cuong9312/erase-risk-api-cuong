package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CartRequest {
    private long strategyId;
    private long secId;
    private int autoYn;
    private long quantity;
}
