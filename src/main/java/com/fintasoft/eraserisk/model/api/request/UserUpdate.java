package com.fintasoft.eraserisk.model.api.request;

import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserUpdate {
    private String fullName;
    private String password;
    private String phoneNumber;
    private String birthday;
    private long[] secIds;
    private Integer emailNotification;
    private Integer mobileNotification;
    private Integer status;

    public String getPhoneNumber() {
        return ConvertUtils.fromLocalToInt(this.phoneNumber);
    }
}
