package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.Authority;

import lombok.Data;

@Data
public class AuthorityResponse {
    private long id;
    private String authority;
    private String description;

    public static AuthorityResponse from(Authority authority) {
        if (authority == null) return null;
    	AuthorityResponse authorityResponse = new AuthorityResponse();
    	authorityResponse.setId(authority.getId());
    	authorityResponse.setAuthority(authority.getAuthority());
    	authorityResponse.setDescription(authority.getDescription());
        return authorityResponse;
    }
}
