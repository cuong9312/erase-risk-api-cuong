package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DerivativeProductPlatformRequest {
    private long id;
    private String tradingCode;
}
