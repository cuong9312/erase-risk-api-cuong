package com.fintasoft.eraserisk.model.api.request.validator;

import com.fintasoft.eraserisk.model.api.EmailMarketingRequest;
import com.fintasoft.eraserisk.utils.ValidatorHelper;
import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public class EmailMarketingRequestValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return EmailMarketingRequest.class.equals(aClass);
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        EmailMarketingRequest request = (EmailMarketingRequest) o;
        new ValidatorHelper("subject", request.getSubject(), errors).notBlank();
        new ValidatorHelper("content", request.getContent(), errors).notBlank();
    }
}
