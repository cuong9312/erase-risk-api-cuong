package com.fintasoft.eraserisk.model.api.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fintasoft.eraserisk.Application;
import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.ReportStatusEnum;
import com.fintasoft.eraserisk.model.db.ReportRequest;
import com.fintasoft.eraserisk.utils.ConvertUtils;

import lombok.Data;

@Data
public class ReportRequestResponse {
    private long id;
    private String status;
	private String filePath;
	@JsonIgnore
	private String fullFilePath;
	private String createdAt;
    private String createdByUserId;

	public ReportRequestResponse fullFilePath(String fullFilePath) {
	    this.fullFilePath = fullFilePath;
	    return this;
    }

    public boolean isSuccess() {
	    return ReportStatusEnum.COMPLETED.name().equals(this.getStatus());
    }
	
	public static ReportRequestResponse from(ReportRequest reportRequest) {
        if (reportRequest == null) return null;
        ReportRequestResponse reportRequestResponse = new ReportRequestResponse();
        reportRequestResponse.setId(reportRequest.getId());
        reportRequestResponse.setStatus(reportRequest.getStatus().toString());
        reportRequestResponse.setFilePath(Application.applicationContext.getBean(AppConf.class).getResources().get("report") + reportRequest.getFilePath());
        reportRequestResponse.setCreatedAt(ConvertUtils.fromTime(reportRequest.getCreatedAt()));
        reportRequestResponse.setCreatedByUserId(reportRequest.getCreatedByUserId());

        return reportRequestResponse;
    }
}
