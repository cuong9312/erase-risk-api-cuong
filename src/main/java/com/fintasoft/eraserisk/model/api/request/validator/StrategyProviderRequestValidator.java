package com.fintasoft.eraserisk.model.api.request.validator;

import com.fintasoft.eraserisk.model.api.request.StrategyProviderRequest;
import com.fintasoft.eraserisk.model.api.request.UserUpdate;
import com.fintasoft.eraserisk.utils.ValidatorHelper;
import com.fintasoft.eraserisk.utils.validator.StringValidatorBuilder;
import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public class StrategyProviderRequestValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return StrategyProviderRequest.class.equals(aClass);
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        StrategyProviderRequest request = (StrategyProviderRequest) o;
        new StringValidatorBuilder(errors,"companyName", request.getCompanyName()).empty().doCheck();
        new StringValidatorBuilder(errors,"companyNameKr", request.getCompanyNameKr()).empty().doCheck();
        new StringValidatorBuilder(errors,"longDescription", request.getLongDescription()).empty().doCheck();
        new StringValidatorBuilder(errors,"longDescriptionKr", request.getLongDescriptionKr()).empty().doCheck();
        new StringValidatorBuilder(errors,"shortDescription", request.getShortDescription()).empty().doCheck();
        new StringValidatorBuilder(errors,"shortDescriptionKr", request.getShortDescriptionKr()).empty().doCheck();
    }
}
