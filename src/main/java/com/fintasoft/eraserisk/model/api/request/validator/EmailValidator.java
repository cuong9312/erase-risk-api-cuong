package com.fintasoft.eraserisk.model.api.request.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fintasoft.eraserisk.utils.ValidatorHelper.IValidate;

public class EmailValidator implements IValidate {

	@Override
	public boolean isValid(Object value) {
		if (null != value.toString()) {
            String regex = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))?$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(value.toString());
            if (!matcher.matches()) {
                return false;
            }
        }
		return true;
	}
	
}
