package com.fintasoft.eraserisk.model.api;

import com.fintasoft.eraserisk.exceptions.InvalidFormatException;
import com.fintasoft.eraserisk.utils.TimeUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.util.Pair;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.*;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class QueryParam<T> {
    public static final Logger log = LoggerFactory.getLogger(QueryParam.class);
    public static final String CURRENT_TIMESTAMP = "CURRENT_TIMESTAMP";

    private String fieldName;
    private String minValue;
    private String maxValue;
    private String value;
    private List<T> values; // does not support functional yet
    private boolean extract = false;
    private boolean functionParam = false;
    private ManualExpression manual = null;
    private boolean leftJoin = false;
    private int[] leftJoinIndexes = new int[0];
    private Class<T> dataClass;
    private List<QueryParam> childs;
    private boolean isOr = true;
    private boolean isNull = false;

    public QueryParam() {
    }

    public QueryParam(String fieldName, String minValue, String maxValue, String value, boolean extract, Class<T> dataClass) {
        this.fieldName = fieldName;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.value = value;
        this.extract = extract;
        this.dataClass = dataClass;
    }

    public static <T> QueryParam<T> create(QueryParam... params) {
        return new QueryParam<T>().childs(params);
    }

    public static <T> QueryParam<T> create(String fieldName, String value) {
        return new QueryParam(fieldName, null, null, value, false, String.class);
    }

    public static <T> QueryParam<T> create(String fieldName, List<T> values) {
        QueryParam queryParam = new QueryParam(fieldName, null, null, null, false, String.class);
        queryParam.setValues(values);
        return queryParam;
    }

    public static <T> QueryParam<T> create(String fieldName, List<T> values, Class<T> dataClass) {
        QueryParam queryParam = new QueryParam(fieldName, null, null, null, false, dataClass);
        queryParam.setValues(values);
        return queryParam;
    }

    public static <T> QueryParam<T> create(String fieldName, String value, Class<T> dataClass) {
        return new QueryParam(fieldName, null, null, value, false, dataClass);
    }

    public static <T> QueryParam<T> create(String fieldName, Object value, Class<T> dataClass) {
        return new QueryParam(fieldName, null, null, value == null ? null : value.toString(), false, dataClass);
    }

    public static <T> QueryParam<T> createExtract(String fieldName, String value, Class<T> dataClass) {
        return new QueryParam(fieldName, null, null, value, true, dataClass);
    }

    public static <T> QueryParam<T> create(String fieldName, String minValue, String maxValue, Class<T> dataClass) {
        return new QueryParam(fieldName, minValue, maxValue, null, true, dataClass);
    }

    public static <T> QueryParam<T> create(ManualExpression manual) {
        return new QueryParam().manual(manual);
    }

    public static <T> Expression<T> getFieldExpression(Map<String, Expression> fieldCache, Map<String, Join> joinCache, String fieldName, Root root, Class clazz, boolean leftJoin, int... leftJoinIndexes) {
        String[] parts = fieldName.split("\\.");
        Join join = null;
        Integer leftJoinIndex = null;
        String fieldKey = fieldName;
        if (leftJoin) {
            fieldKey += ":left";
        } else if (leftJoinIndexes != null && leftJoinIndexes.length > 0) {
            fieldKey += ":left" + Arrays.toString(leftJoinIndexes);
            leftJoinIndex = 0;
        }
        if (fieldCache.containsKey(fieldKey)) {
            return fieldCache.get(fieldKey);
        }
        if (parts.length > 1) {
            String currentKey = "";
            String originalKey = "";
            for (int i = 0; i < parts.length - 1 ; i++) {
                currentKey += "." + parts[i];
                originalKey += originalKey.isEmpty() ? parts[i] : ("." + parts[i]);
                if (leftJoin) {
                    currentKey += ":left";
                    if (joinCache.containsKey(currentKey)) {
                        join = joinCache.get(currentKey);
                    } else {
                        join = join == null ? root.join(parts[i], JoinType.LEFT) : join.join(parts[i], JoinType.LEFT);
                        joinCache.put(currentKey, join);
                    }
                } else {
                    if (leftJoinIndex != null && leftJoinIndex < leftJoinIndexes.length && i == leftJoinIndexes[leftJoinIndex]) {
                        currentKey = currentKey + ":left";
                        if (joinCache.containsKey(currentKey)) {
                            join = joinCache.get(currentKey);
                        } else {
                            join = join == null ? root.join(parts[i], JoinType.LEFT) : join.join(parts[i], JoinType.LEFT);
                            leftJoinIndex++;
                            joinCache.put(currentKey, join);
                        }
                    } else {
                        if (joinCache.containsKey(currentKey)) {
                            join = joinCache.get(currentKey);
                        } else {
                            join = join == null ? root.join(parts[i]) : join.join(parts[i]);
                            joinCache.put(currentKey, join);
                        }
                    }
                }
                joinCache.put("origin:" + originalKey, join);
            }
        }
        Expression e;
        if (join != null) {
            e = join.get(parts[parts.length - 1]).as(clazz);
        } else {
            e = root.get(fieldName).as(clazz);
        }
        fieldCache.put("origin:" + fieldName, e);
        fieldCache.put(fieldKey, e);
        return e;
    }

    public static <T> Expression<T> getFieldExpression(Map<String, Expression> fieldCache, Map<String, Join> joinCache, String fieldName, Class clazz, Root root, Root ...others) {
        return getFieldExpression(fieldCache, joinCache, fieldName, clazz, false, root, others);
    }

    public static <T> Expression<T> getFieldExpression(Map<String, Expression> fieldCache, Map<String, Join> joinCache, String fieldName, Class clazz, boolean leftJoin, Root root, Root ...others) {
        for (Root r : others) {
            getFieldExpression(fieldCache, joinCache, fieldName, r, clazz, leftJoin);
        }
        return getFieldExpression(fieldCache, joinCache, fieldName, root, clazz, leftJoin);
    }

    public Expression<T> getFieldExpression(Map<String, Expression> fieldCache, Map<String, Join> joinCache, Root root) {
        return getFieldExpression(fieldCache, joinCache, this.fieldName, root, this.dataClass, this.leftJoin, this.leftJoinIndexes);
    }

    public Expression<T> getValueExpresssion(CriteriaBuilder cb) throws ParseException {
        if (this.functionParam) {
            String value = this.value;
            if (StringUtils.isEmpty(value)) {
                value = this.minValue;
            }
            if (StringUtils.isEmpty(value)) {
                value = this.maxValue;
            }

            if (CURRENT_TIMESTAMP.equals(value)) {
                return (Expression<T>) cb.currentTimestamp();
            }

            throw new IllegalArgumentException(value);
        }
        ParameterExpression<T> parameter = cb.parameter(this.dataClass);
        return parameter;
    }

    public Object getObject(String value) throws ParseException {
        if (this.dataClass.isEnum()) {
            try {
                return (T) this.dataClass.getMethod("valueOf", String.class).invoke(null, value);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return null;
            }
        } else if (Timestamp.class.equals(this.dataClass)) {
            return (T) new Timestamp(TimeUtils.dateFormat.parse(value).getTime());
        } else if (Integer.class.equals(this.dataClass)) {
            return (T) Integer.valueOf(value);
        } else if (Double.class.equals(this.dataClass)) {
            return (T) Double.valueOf(value);
        } else if (Long.class.equals(this.dataClass)) {
            return (T) Long.valueOf(value);
        } else if (Boolean.class.equals(this.dataClass)) {
            return (T) Boolean.valueOf(value);
        }

        return (T) value;
    }

    public QueryParam<T> extract() {
        return this.extract(true);
    }

    public QueryParam<T> extract(boolean extract) {
        this.extract = extract;
        return this;
    }

    public QueryParam<T> functionParam() {
        this.functionParam = true;
        return this;
    }

    public QueryParam<T> leftJoin() {
        this.leftJoin = true;
        return this;
    }

    public QueryParam<T> leftJoin(int... indexes) {
        this.leftJoin = false;
        this.leftJoinIndexes = indexes;
        return this;
    }

    public QueryParam<T> childs(QueryParam... queryParams) {
        if (this.childs == null) this.childs = new ArrayList<>();
        this.childs.addAll(Arrays.asList(queryParams));
        return this;
    }

    public QueryParam<T> or() {
        this.isOr = true;
        return this;
    }

    public QueryParam<T> and() {
        this.isOr = false;
        return this;
    }

    public QueryParam<T> nullValue() {
        this.isNull = true;
        return this;
    }

    public QueryParam<T> manual(ManualExpression manual) {
        this.manual = manual;
        return this;
    }


    /**
     * helper methods
     */


    public static void createMinMaxDate(List<QueryParam> queryParams, String fieldPath, String minDate, String maxDate, String minDateQueryName, String maxDateQueryName) {
        try {
            String min = TimeUtils.convertMinDate(minDate);
            try {
                String max = TimeUtils.convertMaxDate(maxDate);
                queryParams.add(QueryParam.create(fieldPath, min, max, Timestamp.class));
            } catch (ParseException e) {
                throw new InvalidFormatException(maxDateQueryName);
            }
        } catch (ParseException e) {
            throw new InvalidFormatException(minDateQueryName);
        }
    }

    public static void createMinMaxDate(List<QueryParam> queryParams, String fieldPath, Timestamp minDate, Timestamp maxDate) {
        String min = minDate == null ? null : TimeUtils.dateFormat.format(minDate);
        String max = maxDate == null ? null : TimeUtils.dateFormat.format(maxDate);
        queryParams.add(QueryParam.create(fieldPath, min, max, Timestamp.class));
    }

    public interface ManualExpression<T> {
        Predicate create(
                Map<String, Expression> fieldCache,
                Map<String, Join> joinCache,
                CriteriaBuilder cb,
                QueryParam param,
                Root<T> root,
                List<Pair<ParameterExpression, Object>> parameters,
                Root<T>... others
        );
    }
}
