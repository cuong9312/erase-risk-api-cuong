package com.fintasoft.eraserisk.model.api.request;

import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdminUpdate {
    private String fullName;
    private String email;
    private String password;
    private String phoneNumber;
    private String cellNumber;
    private long[] secIds;
    private long[] authIds;
    private long[] providerIds;
    private boolean isSelf;
    private Integer status;

    public String getPhoneNumber() {
        return ConvertUtils.fromLocalToInt(this.phoneNumber);
    }
}
