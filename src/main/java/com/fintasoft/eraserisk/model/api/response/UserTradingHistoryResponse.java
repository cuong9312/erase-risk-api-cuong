package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.configurations.AppContextStatic;
import com.fintasoft.eraserisk.model.db.StrategySignalData;
import lombok.Data;
import org.springframework.lang.Nullable;

@Data
public class UserTradingHistoryResponse extends TradingHistoryResponse {
    protected boolean openSignalHasOrder;
    protected boolean closeSignalHasOrder;

    public static UserTradingHistoryResponse from (
            StrategySignalData open,
            @Nullable StrategySignalData close
    ) {
        return (UserTradingHistoryResponse) TradingHistoryResponse.from(new UserTradingHistoryResponse(), open, close);
    }
}
