package com.fintasoft.eraserisk.model.api;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class EmailMarketingRequest {

    // filter user
    private String minCreatedAt;
    private String maxCreatedAt;
    private String username;
    private String phoneNumber;
    private String email;
    private List<Long> companyIds;

    // email content
    private String subject;
    private String content;

    private List<String> additionEmails;

}
