package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.DailyProfit;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import com.fintasoft.eraserisk.utils.DoubleUtils;
import lombok.Data;

@Data
public class DailyProfitResponse {
    private Long strategyId;
    private String date;
    private Double profit;
    private Double roi;
    private Double mdd;
    private Double requiredCapital;
    private Double netProfit;
    private Double bestTrade;
    private Double worstTrade;
    private Double winRate;
    private Integer totalWin;
    private Integer totalSignal;
    private Integer totalTrade;
    private Integer totalEven;
    private Integer totalLose;

    public static DailyProfitResponse from(DailyProfit dailyProfit, boolean profit, boolean roi, boolean trade, boolean count) {
        DailyProfitResponse instance = new DailyProfitResponse();
        instance.setDate(ConvertUtils.fromDate(dailyProfit.getPk().getDate()));
        instance.setStrategyId(dailyProfit.getPk().getStrategyId());
        if (profit) {
            instance.setProfit(DoubleUtils.defaultZero(dailyProfit.getProfit()));
        }
        if (roi) {
            instance.setRoi(DoubleUtils.defaultZero(dailyProfit.getRoi()));
            instance.setMdd(DoubleUtils.defaultZero(dailyProfit.getMdd()));
            instance.setRequiredCapital(DoubleUtils.defaultZero(dailyProfit.getRequiredCapital()));
        }
        if (trade) {
            instance.setBestTrade(DoubleUtils.defaultZero(dailyProfit.getBestTrade()));
            instance.setWorstTrade(DoubleUtils.defaultZero(dailyProfit.getWorstTrade()));
        }
        if (count) {
            instance.setWinRate(DoubleUtils.defaultZero(dailyProfit.getWinRate()));
            instance.setTotalSignal(dailyProfit.getTotalCount());
            instance.setTotalTrade(dailyProfit.getTotalTrade());
            instance.setTotalWin(dailyProfit.getTotalWin());
            instance.setTotalEven(dailyProfit.getTotalBreakEven());
            instance.setTotalLose(dailyProfit.getTotalTrade() - dailyProfit.getTotalWin() - dailyProfit.getTotalBreakEven());
        }

        return instance;
    }

    public static DailyProfitResponse from(DailyProfit dailyProfit) {
        return from(dailyProfit, true, true, true, true);
    }
}
