package com.fintasoft.eraserisk.model.api.request;

import com.fintasoft.eraserisk.model.db.StrategySignalData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StrategySignalDataRequest {
    String date;
    String time;
    String productCode;
    String signal;
    String orderType;
    double price;

    public static StrategySignalDataRequest fromReport(StrategySignalData signal) {
        StrategySignalDataRequest request = new StrategySignalDataRequest();
        request.setDate(new SimpleDateFormat("yyyy-MM-dd").format(signal.getTradingAt()));
        request.setTime(new SimpleDateFormat("HH:mm").format(signal.getTradingAt()));
        request.setProductCode(signal.getProductCode());
        request.setSignal(signal.getSignal().name());
        request.setOrderType(signal.getOrderType().name());
        request.setPrice(signal.getPrice());
        return request;
    }
}
