package com.fintasoft.eraserisk.model.api;

import lombok.Data;

@Data
public class EmailSupportRequest {
    private String subject;
    private String content;
}
