package com.fintasoft.eraserisk.model.api.request;

import lombok.Data;

@Data
public class SignalDataRequest {
    private String productCode;
    private String strategyCode;
    private String signalType;
    private Double price;
    private String orderType;
    private String platform;
    private boolean allowMultipleBuy;

    public SignalDataRequest clone() {
        SignalDataRequest signalDataRequest = new SignalDataRequest();
        signalDataRequest.setProductCode(this.getProductCode());
        signalDataRequest.setStrategyCode(this.getStrategyCode());
        signalDataRequest.setSignalType(this.getSignalType());
        signalDataRequest.setPrice(this.getPrice());
        signalDataRequest.setOrderType(this.getOrderType());
        signalDataRequest.setPlatform(this.getPlatform());
        signalDataRequest.setAllowMultipleBuy(this.isAllowMultipleBuy());
        return signalDataRequest;
    }
}
