package com.fintasoft.eraserisk.model.api.request;

import lombok.Data;

@Data
public class StrategyProviderRequest {
    private String companyName;
    private String companyNameKr;
    private String companyImage;
    private String shortDescription;
    private String shortDescriptionKr;
    private String longDescription;
    private String longDescriptionKr;
    private String note;
    private String noteKr;
    private Long createdByUserId;
}
