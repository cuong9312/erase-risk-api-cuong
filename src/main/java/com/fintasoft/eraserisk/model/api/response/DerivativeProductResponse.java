package com.fintasoft.eraserisk.model.api.response;

import java.util.ArrayList;
import java.util.List;

import com.fintasoft.eraserisk.model.db.DerivativeProduct;
import com.fintasoft.eraserisk.utils.ConvertUtils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DerivativeProductResponse {
	private long id;
	private String derivativeName;
	private String derivativeCode;
	private double margin;
	private double tickValue;
	private String expiryDate;
	private String createdAt;
	private String lastUpdatedPrice;
	private double mddRate;
	private double unitValue;
	private double minMove;
	private double fee;
	private String timezone;
    private List<SecuritiesCompanyResponse> securitiesCompanies;
    private List<SignalPlatformResponse> signalPlatforms;


    public static DerivativeProductResponse fromBasic(DerivativeProductResponse response, DerivativeProduct derivativeProduct) {
		if (derivativeProduct == null) return null;
		if (response == null) response = new DerivativeProductResponse();
		response.setDerivativeName(derivativeProduct.getDerivativeName());
		response.setDerivativeCode(derivativeProduct.getDerivativeCode());
		response.setMargin(derivativeProduct.getMargin());
		response.setTickValue(derivativeProduct.getTickValue());
		response.setUnitValue(derivativeProduct.getUnitValue());
		response.setMddRate(derivativeProduct.getMddRate());
		response.setMinMove(derivativeProduct.getMinMove());
		response.setFee(derivativeProduct.getFee());
		response.setTimezone(derivativeProduct.getTimezone());
		response.setId(derivativeProduct.getId());
		response.setExpiryDate(ConvertUtils.fromDate(derivativeProduct.getExpiryDate()));
		response.setCreatedAt(ConvertUtils.fromTime(derivativeProduct.getCreatedAt()));
		return response;
	}

    public static DerivativeProductResponse from(DerivativeProduct derivativeProduct) {
    	if (derivativeProduct == null) {
    		return null;
		}
        DerivativeProductResponse derivativeProductResponse = fromBasic(null, derivativeProduct);
        if (derivativeProduct.getSecuritiesCompanies() != null) {
    	    derivativeProductResponse.securitiesCompanies = new ArrayList<>();
    	    derivativeProduct.getSecuritiesCompanies().forEach(sc -> derivativeProductResponse.securitiesCompanies.add(SecuritiesCompanyResponse.fromDerivativeProduct(sc)));
        }
        
        if (derivativeProduct.getSignalPlatforms() != null) {
    	    derivativeProductResponse.signalPlatforms = new ArrayList<>();
    	    derivativeProduct.getSignalPlatforms().forEach(sp -> derivativeProductResponse.signalPlatforms.add(SignalPlatformResponse.fromDerivativeProduct(sp)));
        }
        
        return derivativeProductResponse;  
    }
    
    public static DerivativeProductResponse fromReport(DerivativeProduct derivativeProduct) {
        if (derivativeProduct == null) return null;
        DerivativeProductResponse derivativeProductResponse = DerivativeProductResponse.builder()
        		.derivativeName(derivativeProduct.getDerivativeName())
        		.derivativeCode(derivativeProduct.getDerivativeCode())
        		.margin(derivativeProduct.getMargin())
        		.tickValue(derivativeProduct.getTickValue())
        		.unitValue(derivativeProduct.getUnitValue())
        		.mddRate(derivativeProduct.getMddRate())
        		.minMove(derivativeProduct.getMinMove())
        		.fee(derivativeProduct.getFee())
        		.id(derivativeProduct.getId())
        		.expiryDate(ConvertUtils.fromDate(derivativeProduct.getExpiryDate()))
        		.createdAt(ConvertUtils.fromTime(derivativeProduct.getCreatedAt()))
        		.build();
        return derivativeProductResponse;  
    }
}
