package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.UserPurchasePayment;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Data;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserPurchasePaymentResponse {
    private long id;
    private double paidAmount;
    private String note;
    private String status;
    private String createdAt;
    private String updatedAt;
    private String purchaseDate;
    private PaymentMethodResponse paymentMethod;
    private List<UserPurchaseHistoryResponse> items;
    private UserResponse user;
    private String payUrl;
    private String paymentMethodName;
    private String customerName;

    public static UserPurchasePaymentResponse from(UserPurchasePayment payment) {
        if (payment == null) return null;
        UserPurchasePaymentResponse response = new UserPurchasePaymentResponse();
        response.setId(payment.getId());
        response.setPaidAmount(payment.getPaidAmount());
        response.setPaymentMethod(PaymentMethodResponse.from(payment.getPaymentMethod()));
        response.setUser(UserResponse.fromCustomer(payment.getUser()));
        response.setNote(payment.getNote());
        response.setStatus(payment.getStatus().toString());
        response.setCreatedAt(ConvertUtils.fromTime(payment.getCreatedAt()));
        response.setUpdatedAt(ConvertUtils.fromTime(payment.getUpdatedAt()));
        response.setPurchaseDate(ConvertUtils.fromTime(payment.getPurchaseDate()));
        response.setPayUrl(payment.getPayUrl());
        response.setItems(new ArrayList<>());

        if (!CollectionUtils.isEmpty(payment.getItems())) {
            payment.getItems().forEach(u -> response.getItems().add(UserPurchaseHistoryResponse.from(u)));
        }

        return response;
    }
    
    public static UserPurchasePaymentResponse fromItem(UserPurchasePayment payment) {
        if (payment == null) return null;
        UserPurchasePaymentResponse response = new UserPurchasePaymentResponse();
        response.setId(payment.getId());
        response.setPaidAmount(payment.getPaidAmount());
        response.setPaymentMethod(PaymentMethodResponse.from(payment.getPaymentMethod()));
        response.setUser(UserResponse.fromCustomer(payment.getUser()));
        response.setNote(payment.getNote());
        response.setStatus(payment.getStatus().toString());
        response.setCreatedAt(ConvertUtils.fromTime(payment.getCreatedAt()));
        response.setUpdatedAt(ConvertUtils.fromTime(payment.getUpdatedAt()));
        response.setPurchaseDate(ConvertUtils.fromTime(payment.getPurchaseDate()));
        response.setPayUrl(payment.getPayUrl());
        return response;
    }
    
    public static UserPurchasePaymentResponse fromReport(UserPurchasePayment payment) {
        if (payment == null) return null;
        UserPurchasePaymentResponse response = new UserPurchasePaymentResponse();
        response.setId(payment.getId());
        response.setPaidAmount(payment.getPaidAmount());
        response.setPaymentMethodName(payment.getPaymentMethod().getPaymentMethod());
        response.setCustomerName(payment.getUser().getUserInfo().getFullName());
        response.setPurchaseDate(ConvertUtils.fromTime(payment.getPurchaseDate()));
        return response;
    }
}
