package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.StrategySignalData;
import lombok.Data;

@Data
public class OpenPositionResponse {
    protected StrategyResponse strategy;
    protected DerivativeProductResponse derivativeProduct;
    protected StrategySignalDataResponse openSignal;

    public static OpenPositionResponse from (OpenPositionResponse response, StrategySignalData open) {
        if (response == null) response = new OpenPositionResponse();
        response.setStrategy(StrategyResponse.addProvider(StrategyResponse.fromBasic(null, open.getStrategy()), open.getStrategy()));
        response.setDerivativeProduct(DerivativeProductResponse.fromBasic(null, open.getStrategy().getDerivativeProduct()));
        response.setOpenSignal(StrategySignalDataResponse.from(open));

        return response;
    }
}
