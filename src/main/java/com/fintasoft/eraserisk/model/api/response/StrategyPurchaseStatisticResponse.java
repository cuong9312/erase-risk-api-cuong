package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.UserStrategySetting;
import com.fintasoft.eraserisk.model.db.UserPurchaseHistory;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Data;

@Data
public class StrategyPurchaseStatisticResponse {
    private long strategyId;
    private long secId;
    private long userId;
    private boolean auto;
    private int availableQuantity;
    private boolean autoDisabled;
    private long balance;
    private long lotSize;
    private String positionType;

    public static StrategyPurchaseStatisticResponse from(UserStrategySetting from) {
    	StrategyPurchaseStatisticResponse to = new StrategyPurchaseStatisticResponse();
    	to.setStrategyId(from.getPk().getStrategyId());
    	to.setSecId(from.getPk().getSecId());
    	to.setUserId(from.getPk().getUserId());
    	to.setAutoDisabled(from.isAutoDisabled());
    	to.setAuto(from.getPk().isAuto());
    	to.setBalance(from.getBalance());
    	to.setLotSize(from.getLotSize());
    	to.setPositionType(from.getPositionType().name());
        return to;
    }
}
