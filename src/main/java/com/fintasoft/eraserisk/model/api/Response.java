package com.fintasoft.eraserisk.model.api;

import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import com.fintasoft.eraserisk.exceptions.GeneralException;
import com.fintasoft.eraserisk.exceptions.SubErrorsException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.HashMap;
import java.util.Locale;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response<T> {
    private T data;
    private Status status;

    public Response(T data) {
        this.data = data;
    }

    public Response(Status status) {
        this.status = status;
    }

    public static Response<?> from(GeneralException e, MessageSource msg) {
        Locale locale = LocaleContextHolder.getLocale();
        if (e instanceof SubErrorsException) {
            return new Response<Object>(from((SubErrorsException) e, msg, locale));
        }
        return new Response<Object>(from(e, msg, locale));
    }

    public static Status from(GeneralException e, MessageSource msg, Locale locale) {
        return new Status(
                e.getCode(),
                msg.getMessage(e.getMessage(), e.getMessageParams(), locale)
        );
    }

    public static Status from(SubErrorsException e, MessageSource msg, Locale locale) {
        Status status = new Status(
                e.getCode(),
                msg.getMessage(e.getMessage(), e.getMessageParams(), locale)
        );
        e.getErrors().forEach(err -> status.add(
            new Error(
                    err.getCode(),
                    err.getParam(),
                    msg.getMessage(err.getMessage(), err.getMessageParams(), locale)
            )
        ));
        return status;
    }

    public static Response<?> from(MethodArgumentNotValidException e, MessageSource msg) {
        return new Response<Object>(from(e, msg, LocaleContextHolder.getLocale()));
    }

    public static Status from(MethodArgumentNotValidException e, MessageSource msg, Locale locale) {
        Status status = new Status(
                ErrorCodeEnums.INVALID_PARAMETER_ERROR.name(),
                msg.getMessage("error.invalid.parameter", null, locale)
        );
        e.getBindingResult().getFieldErrors().forEach(err -> status.add(
            new Error(
                    err.getCode(),
                    err.getField(),
                    msg.getMessage(err.getDefaultMessage(), err.getArguments(), locale)
            )
        ));
        return status;
    }

    /**
     * this method return an empty success response.
     *
     * @return
     */
    public static Response<HashMap<String, String>> empty() {
        return new Response<HashMap<String, String>>(new HashMap<String, String>());
    }
}
