package com.fintasoft.eraserisk.model.api.request;

import lombok.Data;

@Data
public class PurchaseLotSizeRequest {
    private long lotSize;
}
