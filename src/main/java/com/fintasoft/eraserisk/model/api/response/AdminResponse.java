package com.fintasoft.eraserisk.model.api.response;

import java.util.ArrayList;
import java.util.List;

import com.fintasoft.eraserisk.model.db.AdminInfo;
import com.fintasoft.eraserisk.model.db.User;
import com.fintasoft.eraserisk.utils.ConvertUtils;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdminResponse {
    private long id;
    private String username;
    private String createdAt;
    private String updatedAt;
    private String fullName;
    private String phoneNumber;
    private String cellNumber;
    private String secNames;
    private String providerNames;

    
    public static AdminResponse fromReport(User user) {
    	if (user == null) return null;
        List<String> securitiesCompanies = new ArrayList<>();
        user.getAdminSecuritiesCompanies().forEach(s -> securitiesCompanies.add(s.getSecName()));
        
        List<String> providers = new ArrayList<>();
        user.getAdminStrategyProviders().forEach(sp -> providers.add(sp.getCompanyName()));
        
        AdminInfo adminInfo = user.getAdminInfo();
        
        return AdminResponse.builder()
                .username(user.getUsername())
                .fullName(adminInfo.getFullName())
                .cellNumber(adminInfo.getCellNumber())
                .createdAt(ConvertUtils.fromTime(user.getCreatedAt()))
                .secNames(String.join(", ", securitiesCompanies))
                .providerNames(String.join(", " , providers))
        		.phoneNumber(adminInfo.getLocalPhoneNumber())
                .build();
    }
}
