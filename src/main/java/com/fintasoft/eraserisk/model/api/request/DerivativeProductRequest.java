package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DerivativeProductRequest {
    private String derivativeCode;
    private String derivativeName;
    private String expiryDate;
    private double margin;
    private double fee;
    private double tickValue;
    private double unitValue;
    private double mddRate;
    private double minMove;
    private String timezone;
    private DerivativeProductSecRequest[] securitiesCompanies;
    private DerivativeProductPlatformRequest[] signalPlatforms;
}
