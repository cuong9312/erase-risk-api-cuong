package com.fintasoft.eraserisk.model.api.response;

import java.util.ArrayList;
import java.util.List;

import com.fintasoft.eraserisk.model.db.FaqGroup;

import lombok.Data;

@Data
public class FaqGroupResponse {
    private long id;
    private String faqGroup;
    private List<FaqResponse> faqs = new ArrayList<>();

    public static FaqGroupResponse from(FaqGroup faqGroup) {
        if (faqGroup == null) return null;
    	FaqGroupResponse faqGroupResponse = new FaqGroupResponse();
    	faqGroupResponse.setId(faqGroup.getId());
    	faqGroupResponse.setFaqGroup(faqGroup.getFaqGroup());
    	faqGroup.getFaqs().forEach(f -> faqGroupResponse.getFaqs().add(FaqResponse.from(f)));
        return faqGroupResponse;
    }
}
