package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StrategyRegister {
    private String strategyName;
    private long strategyProviderId;
    private long signalPlatformId;
    private String strategyCode;
    private double autoPrice;
    private double manualPrice;
    private long autoSellingUnit;
    private long manualSellingUnit;
    private StrategySecuritiesCompanyRequest[] securitiesCompanies;
    private long strategyStatusId;
    private Long productId;
    private String description;
    private String descriptionKr;
    private String style;
    private String tradingDuration;
    private Integer signalFrequency;
    private String frequencyType;
    private boolean monitorYn;
}
