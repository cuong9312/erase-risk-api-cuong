package com.fintasoft.eraserisk.model.api.request;

import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserRegister {
    private String fullName;
    private String password;
    private String email;
    private String phoneNumber;
    private String birthday;
    private long[] secIds;
    private Integer emailNotification;
    private Integer mobileNotification;

    public String getPhoneNumber() {
        return ConvertUtils.fromLocalToInt(this.phoneNumber);
    }
}
