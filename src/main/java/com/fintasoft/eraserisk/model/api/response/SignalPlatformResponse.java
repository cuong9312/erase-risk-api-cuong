package com.fintasoft.eraserisk.model.api.response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.fintasoft.eraserisk.model.db.DerivativeProductPlatform;
import com.fintasoft.eraserisk.model.db.SignalPlatform;

import lombok.Data;

@Data
public class SignalPlatformResponse {
    private long id;
    private String platformName;
    private String tradingCode;
    private List<DerivativeProductPlatformResponse> derivativeProductPlatformResponses = new ArrayList<>();
    
    public static SignalPlatformResponse from(SignalPlatform signalPlatform) {
        return from(signalPlatform, null);
    }

    public static SignalPlatformResponse from(SignalPlatform signalPlatform, Long productId) {
        if (signalPlatform == null) return null;
    	SignalPlatformResponse signalPlatformResponse = new SignalPlatformResponse();
    	signalPlatformResponse.setId(signalPlatform.getId());
        signalPlatformResponse.setPlatformName(signalPlatform.getPlatformName());
        if (productId == null) {
            signalPlatformResponse.setDerivativeProductPlatformResponses(
                    signalPlatform.getDerivativeProductPlatforms().stream().
                            map(DerivativeProductPlatformResponse::from).
                            collect(Collectors.toList())
            );
        } else {
            DerivativeProductPlatformResponse r = signalPlatform.getDerivativeProductPlatforms().stream().
                    filter(p -> p.getPk().getProductId().equals(productId)).
                    map(DerivativeProductPlatformResponse::from).
                    findFirst().orElse(null);
            if (r != null) {
                signalPlatformResponse.setDerivativeProductPlatformResponses(Arrays.asList(r));
            }
        }
        
    	
        return signalPlatformResponse;
    }

    public static SignalPlatformResponse fromDerivativeProduct(DerivativeProductPlatform signalPlatform) {
        if (signalPlatform == null) return null;
        SignalPlatformResponse signalPlatformResponse = SignalPlatformResponse.from(signalPlatform.getSignalPlatform());
        signalPlatformResponse.setTradingCode(signalPlatform.getTradingCode());
        return signalPlatformResponse;
    }
}
