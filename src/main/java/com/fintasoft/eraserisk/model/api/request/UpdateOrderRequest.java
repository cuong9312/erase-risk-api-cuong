package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdateOrderRequest {
    private String orderNo;
    private double filledPrice;
    private long filledQuantity;
    private String accountNumber;

    public String getActualOrderNo() {
        return String.format("%010d", Long.parseLong(this.orderNo));
    }
}
