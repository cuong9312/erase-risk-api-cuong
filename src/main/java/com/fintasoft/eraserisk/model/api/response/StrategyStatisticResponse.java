package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.StrategyStatistic;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StrategyStatisticResponse {
	private double requiredCapital;
	private double netProfit;
	private double worstTrade;
	private double bestTrade;
	private double roi;
	private double mdd;
    private double winRate;
    private long totalTrade;
    private int totalDailyProfitRecords;

    public static StrategyStatisticResponse from(StrategyStatistic strategyStatistic) {
        if (strategyStatistic == null) return null;
        
        return StrategyStatisticResponse.builder()
                .requiredCapital(strategyStatistic.getRequiredCapital())
                .netProfit(strategyStatistic.getNetProfit())
                .worstTrade(strategyStatistic.getWorstTrade())
                .bestTrade(strategyStatistic.getBestTrade())
                .roi(strategyStatistic.getRoi())
                .mdd(strategyStatistic.getMdd())
                .winRate(strategyStatistic.getWinRate())
                .totalTrade(strategyStatistic.getTotalTrade())
                .totalDailyProfitRecords(strategyStatistic.getTotalDailyProfitRecords())
                .build();
    }
}
