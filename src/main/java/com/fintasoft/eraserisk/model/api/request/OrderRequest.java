package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderRequest {
	private String derivativeCode;
	private Double price;
	private Long quantity;
	private Double filledPrice;
	private Long filledQuantity;
	private String tradingAt;
	private String orderNo;
	private String accountNumber;
	private String orderType;
	private String orderAction;
}
