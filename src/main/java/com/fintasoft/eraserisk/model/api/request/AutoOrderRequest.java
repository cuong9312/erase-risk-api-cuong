package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AutoOrderRequest {
	private String orderNo;
	private long strategyId;
	private String derivativeCode;
	private long secId;
	private String orderType;
	private String orderAction;
	private double price;
	private long quantity;
	private String accountNumber;
	private long signalId;
	private boolean tempYn;
}
