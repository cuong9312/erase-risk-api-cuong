package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.StrategyStatus;

import lombok.Data;

@Data
public class StrategyStatusResponse {
    private long id;
	private String status;
    private String description;

    public static StrategyStatusResponse from(StrategyStatus strategyStatus) {
        if (strategyStatus == null) return null;
    	StrategyStatusResponse strategyStatusResponse = new StrategyStatusResponse();
    	strategyStatusResponse.setId(strategyStatus.getId());
    	strategyStatusResponse.setStatus(strategyStatus.getStatus());
    	strategyStatusResponse.setDescription(strategyStatus.getDescription());
        return strategyStatusResponse;
    }
}
