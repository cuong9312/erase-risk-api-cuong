package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.DerivativeProductSec;
import com.fintasoft.eraserisk.model.db.SecuritiesCompany;
import com.fintasoft.eraserisk.model.db.StrategySecuritiesCompany;
import lombok.Data;

@Data
public class SecuritiesCompanyResponse {
    private long id;
    private String secCode;
    private String secName;
    private String secImage;
    private String secUrl;
    private long autoSellingUnit;
    private long remainingSellingUnit;
    private String clientCode;
    private String note;
    private String noteKr;

    public static SecuritiesCompanyResponse from(SecuritiesCompany securitiesCompany) {
        if (securitiesCompany == null) return null;
    	SecuritiesCompanyResponse securitiesCompanyResponse = new SecuritiesCompanyResponse();
    	securitiesCompanyResponse.setId(securitiesCompany.getId());
    	securitiesCompanyResponse.setSecCode(securitiesCompany.getSecCode());
    	securitiesCompanyResponse.setSecName(securitiesCompany.getSecName());
    	securitiesCompanyResponse.setSecImage(securitiesCompany.getSecImage());
    	securitiesCompanyResponse.setSecUrl(securitiesCompany.getSecUrl());
    	securitiesCompanyResponse.setNote(securitiesCompany.getNote());
    	securitiesCompanyResponse.setNoteKr(securitiesCompany.getNoteKr());
        return securitiesCompanyResponse;
    }
    
    public static SecuritiesCompanyResponse fromStrategy(StrategySecuritiesCompany securitiesCompany) {
        if (securitiesCompany == null) return null;
    	SecuritiesCompanyResponse securitiesCompanyResponse = SecuritiesCompanyResponse.from(securitiesCompany.getSecuritiesCompany());
    	securitiesCompanyResponse.setAutoSellingUnit(securitiesCompany.getAutoSellingUnit());
    	securitiesCompanyResponse.setRemainingSellingUnit(securitiesCompany.getRemainingSellingUnit());
        return securitiesCompanyResponse;
    }
    
    public static SecuritiesCompanyResponse fromDerivativeProduct(DerivativeProductSec securitiesCompany) {
        if (securitiesCompany == null) return null;
    	SecuritiesCompanyResponse securitiesCompanyResponse = SecuritiesCompanyResponse.from(securitiesCompany.getSecuritiesCompany());
    	securitiesCompanyResponse.setClientCode(securitiesCompany.getClientCode());
        return securitiesCompanyResponse;
    }
}
