package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.StrategySignalData;
import lombok.Data;

@Data
public class UserOpenPositionResponse extends OpenPositionResponse {
    protected boolean hasUserOrder;

    public static UserOpenPositionResponse from (UserOpenPositionResponse response, StrategySignalData open) {
        if (response == null) response = new UserOpenPositionResponse();
        return (UserOpenPositionResponse) OpenPositionResponse.from(response, open);
    }
}
