package com.fintasoft.eraserisk.model.api.message;

import lombok.Data;

@Data
public class PriceUpdate {
    private Long id;
    private Double price;
}
