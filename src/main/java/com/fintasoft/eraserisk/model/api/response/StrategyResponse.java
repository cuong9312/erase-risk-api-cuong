package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.Strategy;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import com.fintasoft.eraserisk.utils.TimeUtils;
import lombok.Data;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
public class StrategyResponse {
    private long id;
    private String strategyCode;
    private String strategyName;
    private String description;
    private Long validPeriod;
    private double autoPrice;
    private double manualPrice;
    private long autoSellingUnit;
    private long manualSellingUnit;
    private String style;
    private String tradingDuration;
    private String createdAt;
    private String updatedAt;
    private StrategyProviderResponse strategyProvider;
    private SignalPlatformResponse signalPlatform;
    private List<SecuritiesCompanyResponse> securitiesCompanies;
    private StrategyStatusResponse strategyStatus;
    private StrategyStatisticResponse strategyStatistic;
    private DerivativeProductResponse derivativeProduct;
    private long tradingDays;
    private List<StrategySignalDataResponse> signalDatas;
    private List<DailyProfitResponse> dailyProfits;
    private long remainingAuto;
    private long remainingManual;
    private int signalFrequency;
    private String frequencyType;
    private String strategyProviderName;
    private String status;
    private boolean isExpired;
    private int rank;
    private StrategyPositionResponse orderedPosition;
    private String descriptionKr;
    private Double lastPrice;
    private List<Long> purchaseUserIds;
    private String lastUpdatedStatus;
    private boolean monitorYn;

    public StrategyResponse lastPrice(Double lastPrice) {
        this.lastPrice = lastPrice;
        return this;
    }

    public static StrategyResponse from(Strategy strategy) {
        return from(strategy, false);
    }

    public static StrategyResponse addProvider(StrategyResponse strategyResponse, Strategy strategy) {
        strategyResponse.setStrategyProvider(StrategyProviderResponse.from(strategy.getStrategyProvider()));
        return strategyResponse;
    }

    public static StrategyResponse fromBasic(StrategyResponse strategyResponse, Strategy strategy) {
        if (strategyResponse == null) strategyResponse = new StrategyResponse();
        strategyResponse.setId(strategy.getId());
        strategyResponse.setStrategyCode(strategy.getStrategyCode());
        strategyResponse.setStrategyName(strategy.getStrategyName());
        strategyResponse.setDescription(strategy.getDescription());
        strategyResponse.setDescriptionKr(strategy.getDescriptionKr());
        strategyResponse.setValidPeriod(strategy.getValidPeriod());
        strategyResponse.setAutoPrice(strategy.getAutoPrice());
        strategyResponse.setManualPrice(strategy.getManualPrice());
        strategyResponse.setAutoSellingUnit(strategy.getAutoSellingUnit());
        strategyResponse.setRemainingAuto(strategy.getRemainingAuto());
        strategyResponse.setRemainingManual(strategy.getRemainingManual());
        strategyResponse.setStyle(strategy.getStyle().name());
        strategyResponse.setTradingDuration(strategy.getTradingDuration().name());
        strategyResponse.setManualSellingUnit(strategy.getManualSellingUnit());
        strategyResponse.setExpired(strategy.isExpired());
        strategyResponse.setSignalFrequency(strategy.getSignalFrequency());
        strategyResponse.setFrequencyType(strategy.getFrequencyType().name());
        strategyResponse.setStatus(strategy.getStrategyStatus().getStatus());
        strategyResponse.setLastUpdatedStatus(ConvertUtils.fromTime(strategy.getLastUpdatedStatus()));
        strategyResponse.setMonitorYn(strategy.isMonitorYn());
        if (strategy.getCreatedAt() != null) {
            strategyResponse.setTradingDays((System.currentTimeMillis() -
                    strategy.getCreatedAt().getTime()) / (TimeUtils.MILISECONDS_OF_A_DAY));
        }
        return strategyResponse;
    }

    public static StrategyResponse fromBasic(StrategyResponse strategyResponse, Strategy strategy, Integer limitDays) {
        strategyResponse = fromBasic(strategyResponse, strategy);
        if (strategy.getCreatedAt() != null) {
            strategyResponse.setTradingDays((System.currentTimeMillis() -
                    strategy.getCreatedAt().getTime()) / (TimeUtils.MILISECONDS_OF_A_DAY));
            if (limitDays != null && strategyResponse.getTradingDays() > limitDays) {
                strategyResponse.setTradingDays(limitDays);
            }
        }

        return strategyResponse;
    }

    public static StrategyResponse from(Strategy strategy, boolean includeSecurityCompany) {
        return from(strategy, includeSecurityCompany, null);
    }

    public static StrategyResponse from(Strategy strategy, boolean includeSecurityCompany, Integer limitDays) {
        StrategyResponse strategyResponse = fromBasic(null, strategy);
        strategyResponse.setStrategyProvider(StrategyProviderResponse.shortFrom(strategy.getStrategyProvider()));
        strategyResponse.setSignalPlatform(SignalPlatformResponse.from(strategy.getSignalPlatform(), strategy.getProductId()));
        strategyResponse.setStrategyStatistic(StrategyStatisticResponse.from(strategy.getStrategyStatistic()));
        strategyResponse.setSignalFrequency(strategy.getSignalFrequency());
        if (includeSecurityCompany && strategy.getSecuritiesCompanies() != null) {
            strategyResponse.securitiesCompanies = new ArrayList<>();
            strategy.getSecuritiesCompanies().forEach(sc -> strategyResponse.securitiesCompanies.add(SecuritiesCompanyResponse.fromStrategy(sc)));
        }
        strategyResponse.setDerivativeProduct(DerivativeProductResponse.from(strategy.getDerivativeProduct()));
        strategyResponse.setStrategyStatus(StrategyStatusResponse.from(strategy.getStrategyStatus()));
        strategyResponse.setCreatedAt(ConvertUtils.fromTime(strategy.getCreatedAt()));
        strategyResponse.setUpdatedAt(ConvertUtils.fromTime(strategy.getUpdatedAt()));
        strategyResponse.setLastUpdatedStatus(ConvertUtils.fromTime(strategy.getLastUpdatedStatus()));
        strategyResponse.setMonitorYn(strategy.isMonitorYn());
        strategyResponse.setRank(strategy.getRank());
        strategyResponse.setStatus(strategy.getStrategyStatus().getStatus());
        Timestamp start = ConvertUtils.coalesce(strategy.getFirstSignalTradingAt(), strategy.getCreatedAt());
        if (start != null) {
            strategyResponse.setTradingDays((System.currentTimeMillis() - start.getTime()) / (TimeUtils.MILISECONDS_OF_A_DAY));
            if (limitDays != null && strategyResponse.getTradingDays() > limitDays) {
                strategyResponse.setTradingDays(limitDays);
            }
        }

        return strategyResponse;
    }

    public static StrategyResponse fromReport(Strategy strategy) {
        StrategyResponse strategyResponse = new StrategyResponse();
        strategyResponse.setStrategyCode(strategy.getStrategyCode());
        strategyResponse.setStrategyName(strategy.getStrategyName());
        strategyResponse.setAutoPrice(strategy.getAutoPrice());
        strategyResponse.setManualPrice(strategy.getManualPrice());
        strategyResponse.setAutoSellingUnit(strategy.getAutoSellingUnit());
        strategyResponse.setManualSellingUnit(strategy.getManualSellingUnit());
        strategyResponse.setStrategyProviderName(strategy.getStrategyProvider().getCompanyName());
        strategyResponse.setStatus(strategy.getStrategyStatus().getStatus());
        strategyResponse.setCreatedAt(ConvertUtils.fromTime(strategy.getCreatedAt()));
        strategyResponse.setLastUpdatedStatus(ConvertUtils.fromTime(strategy.getLastUpdatedStatus()));
        strategyResponse.setMonitorYn(strategy.isMonitorYn());
        strategyResponse.setSignalFrequency(strategy.getSignalFrequency());
        strategyResponse.setFrequencyType(strategy.getFrequencyType().name());
        strategyResponse.setExpired(strategy.isExpired());
        strategyResponse.setRank(strategy.getRank());

        return strategyResponse;
    }
}
