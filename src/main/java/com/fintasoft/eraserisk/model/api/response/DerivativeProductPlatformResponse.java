package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.DerivativeProductPlatform;
import lombok.Data;

@Data
public class DerivativeProductPlatformResponse {
    private Long productId;
    private Long signalPlatformId;
    private String tradingCode;

    public static DerivativeProductPlatformResponse from(DerivativeProductPlatform derivativeProductPlatform) {
        DerivativeProductPlatformResponse result = new DerivativeProductPlatformResponse();
        result.setProductId(derivativeProductPlatform.getPk().getProductId());
        result.setSignalPlatformId(derivativeProductPlatform.getPk().getSignalPlatformId());
        result.setTradingCode(derivativeProductPlatform.getTradingCode());

        return result;
    }
}
