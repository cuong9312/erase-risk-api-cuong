package com.fintasoft.eraserisk.model.api.request.validator;

import com.fintasoft.eraserisk.model.api.request.SystemConfCreate;
import com.fintasoft.eraserisk.model.db.SystemConf;
import com.fintasoft.eraserisk.utils.ValidatorHelper;
import com.fintasoft.eraserisk.utils.validator.StringValidatorBuilder;
import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public class SystemConfCreateValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return SystemConfCreate.class.equals(aClass);
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        SystemConfCreate request = (SystemConfCreate) o;
        new ValidatorHelper("name", request.getName(), errors).notBlank();
        new ValidatorHelper("lang", request.getLang(), errors).notBlank();
    }
}
