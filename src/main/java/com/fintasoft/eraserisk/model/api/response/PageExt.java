package com.fintasoft.eraserisk.model.api.response;

import lombok.Data;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;


@Data
public class PageExt<T, E> extends PageImpl<T> {
    private E ext;

    public PageExt(List<T> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    public PageExt(List<T> content, Pageable pageable, long total, E ext) {
        super(content, pageable, total);
        this.ext = ext;
    }
}
