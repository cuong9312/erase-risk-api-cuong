package com.fintasoft.eraserisk.model.api.response;

import lombok.Data;

@Data
public class ComplexUserStrategySettingResponse {
    private StrategyResponse strategy;
    private StrategyProviderResponse strategyProvider;
    private DerivativeProductResponse product;
    private SecuritiesCompanyResponse securitiesCompany;
    private String strategyName;
    private int availableQuantity;
    private boolean isAuto;
    private boolean isAutoDisabled;
    private int lotSize;
    private double balance;
    private String positionType;
    private long userId;
}
