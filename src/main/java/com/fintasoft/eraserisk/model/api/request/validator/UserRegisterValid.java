package com.fintasoft.eraserisk.model.api.request.validator;

import com.fintasoft.eraserisk.model.api.request.UserRegister;
import com.fintasoft.eraserisk.utils.ValidatorHelper;

import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


public class UserRegisterValid implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return UserRegister.class.equals(aClass);
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        UserRegister userRegister = (UserRegister) o;
        new ValidatorHelper("fullName", userRegister.getFullName(), errors).notBlank();
        new ValidatorHelper("password", userRegister.getPassword(), errors).notBlank();
        new ValidatorHelper("email", userRegister.getEmail(), errors).notBlank();
        new ValidatorHelper("phoneNumber", userRegister.getPhoneNumber(), errors).notBlank();
        new ValidatorHelper("email", userRegister.getEmail(), errors).formatCheck(new EmailValidator());
    }
}
