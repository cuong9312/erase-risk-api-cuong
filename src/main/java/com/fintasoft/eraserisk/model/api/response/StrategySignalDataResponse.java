package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.StrategySignalData;
import com.fintasoft.eraserisk.model.db.StrategySignalDataTemp;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Builder
public class StrategySignalDataResponse {
    private Long strategyId;
    private Long signalId;
    private String productCode;
    private String signal;
    private double price;
    private double profit;
    private double cumulativeProfit;
    private double highestProfit;
    private double drawdown;
    private String tradingAt;
    private String orderType;
    private boolean tempYn;

    public static StrategySignalDataResponse fromForPc(StrategySignalData strategySignalData) {
        return StrategySignalDataResponse.builder()
                .productCode(strategySignalData.getProductCode())
                .signal(strategySignalData.getSignal().toString())
                .price(strategySignalData.getPrice())
                .profit(strategySignalData.getSafetyProfit())
                .cumulativeProfit(strategySignalData.getCumulativeProfit())
                .highestProfit(strategySignalData.getHighestProfit())
                .drawdown(strategySignalData.getDrawdown())
                .tradingAt(ConvertUtils.fromTime(strategySignalData.getTradingAt()))
                .strategyId(strategySignalData.getStrategyId())
                .orderType(strategySignalData.getOrderType().name())
                .signalId(strategySignalData.getId())
                .build();

    }

    public static StrategySignalDataResponse from(StrategySignalDataTemp strategySignalDataTemp) {
        return StrategySignalDataResponse.builder()
                .productCode(strategySignalDataTemp.getProductCode())
                .signal(strategySignalDataTemp.getSignalType())
                .price(strategySignalDataTemp.getPrice())
                .orderType(strategySignalDataTemp.getOrderType())
                .signalId(strategySignalDataTemp.getId())
                .tradingAt(ConvertUtils.fromTime(new Timestamp(new java.util.Date().getTime())))
                .build();
    }

    public static StrategySignalDataResponse from(StrategySignalData strategySignalData) {
        return StrategySignalDataResponse.builder()
                .productCode(strategySignalData.getProductCode())
                .price(strategySignalData.getPrice())
                .profit(strategySignalData.getSafetyProfit())
                .signal(strategySignalData.getSignal().name())
                .cumulativeProfit(strategySignalData.getCumulativeProfit())
                .highestProfit(strategySignalData.getHighestProfit())
                .drawdown(strategySignalData.getDrawdown())
                .tradingAt(ConvertUtils.fromTime(strategySignalData.getTradingAt()))
                .strategyId(strategySignalData.getStrategyId())
                .signalId(strategySignalData.getId())
                .build();
    }

    public static StrategySignalDataResponse fromReport(StrategySignalData strategySignalData) {
        return StrategySignalDataResponse.builder()
                .productCode(strategySignalData.getProductCode())
                .signal(strategySignalData.getSignal().toString())
                .price(strategySignalData.getPrice())
                .profit(strategySignalData.getSafetyProfit())
                .cumulativeProfit(strategySignalData.getCumulativeProfit())
                .highestProfit(strategySignalData.getHighestProfit())
                .drawdown(strategySignalData.getDrawdown())
                .tradingAt(ConvertUtils.fromTime(strategySignalData.getTradingAt()))
                .strategyId(strategySignalData.getStrategyId())
                .signalId(strategySignalData.getId())
                .orderType(strategySignalData.getOrderType().name())
                .build();

    }
}
