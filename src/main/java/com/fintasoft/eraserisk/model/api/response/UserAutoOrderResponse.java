package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.constances.OrderTypeEnum;
import com.fintasoft.eraserisk.constances.SignalEnum;
import com.fintasoft.eraserisk.model.db.UserAutoOrder;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Data;

@Data
public class UserAutoOrderResponse {
    private long id;
    private String accountNumber;
    private String orderNo;
    private double price;
    private long quantity;
    private Double filledPrice;
    private Long filledQuantity;
    private SignalEnum orderAction;
    private OrderTypeEnum orderType;
    private String createdAt;
    private String updatedAt;

    public static UserAutoOrderResponse from(UserAutoOrder from) {
        UserAutoOrderResponse result  =  new UserAutoOrderResponse();
        result.setId(from.getId());
        result.setAccountNumber(from.getAccountNumber());
        result.setOrderNo(from.getOrderNo());
        result.setPrice(from.getPrice());
        result.setQuantity(from.getQuantity());
        result.setFilledPrice(from.getFilledPrice());
        result.setFilledQuantity(from.getFilledQuantity());
        result.setOrderAction(from.getOrderAction());
        result.setOrderType(from.getOrderType());
        result.setCreatedAt(ConvertUtils.fromTime(from.getCreatedAt()));
        result.setUpdatedAt(ConvertUtils.fromTime(from.getUpdatedAt()));

        return result;
    }
}
