package com.fintasoft.eraserisk.model.api.response;


import com.fintasoft.eraserisk.model.query.TradeAutoReport;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Data;

@Data
public class TradeAutoReportResponse {
    private Integer userId;
    private String username;
    private String fullName;
    private String tradingAt;
    private Integer strategyId;
    private String strategyName;
    private Integer providerId;
    private String providerName;
    private Integer secId;
    private String secName;
    private Integer quantity;

    public static TradeAutoReportResponse from (TradeAutoReport tradeReport) {
        TradeAutoReportResponse response = new TradeAutoReportResponse();
        response.setUserId(tradeReport.getUserId());
        response.setUsername(tradeReport.getUsername());
        response.setFullName(tradeReport.getFullName());
        response.setTradingAt(ConvertUtils.fromTime(tradeReport.getTradingAt()));
        response.setStrategyId(tradeReport.getStrategyId());
        response.setStrategyName(tradeReport.getStrategyName());
        response.setProviderId(tradeReport.getProviderId());
        response.setProviderName(tradeReport.getProviderName());
        response.setSecId(tradeReport.getSecId());
        response.setSecName(tradeReport.getSecName());
        response.setQuantity(tradeReport.getQuantity());

        return response;
    }
}
