package com.fintasoft.eraserisk.model.api.request;

import lombok.Data;

@Data
public class PurchaseAutoRequest {
    private Boolean autoDisabled;
    private String positionType;
    private long balance;
}
