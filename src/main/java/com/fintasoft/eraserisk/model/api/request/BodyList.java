package com.fintasoft.eraserisk.model.api.request;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class BodyList<T> extends ArrayList<T> {
    public List<T> getItems() {
        return this;
    }

    public void setItems(List<T> l) {
        this.clear();
        if (l != null) {
            l.forEach(i -> this.add(i));
        }
    }
}
