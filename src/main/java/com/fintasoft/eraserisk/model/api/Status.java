package com.fintasoft.eraserisk.model.api;

import com.fintasoft.eraserisk.utils.ResponseUtil;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Status {
    private String id;
    private String code;
    private String message;
    private List<Error> errors = new ArrayList<>();

    public Status(String code, String message) {
        this.id = ResponseUtil.generateTransactionId();
        this.code = code;
        this.message = message;
    }

    public Status add(Error error) {
        this.errors.add(error);
        return this;
    }
}
