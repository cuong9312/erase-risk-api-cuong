package com.fintasoft.eraserisk.model.api.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StrategyData {
    private long strategyId;
    private StrategySignalDataRequest[] csv;
}
