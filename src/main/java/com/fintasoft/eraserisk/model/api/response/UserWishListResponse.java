package com.fintasoft.eraserisk.model.api.response;

import com.fintasoft.eraserisk.model.db.UserWishList;

import lombok.Data;

@Data
public class UserWishListResponse {
	private long id;
    private UserResponse user;
    private StrategyResponse strategy;

    public static UserWishListResponse from(UserWishList userWishList) {
    	UserWishListResponse userWishListResponse = new UserWishListResponse();
    	userWishListResponse.setId(userWishList.getId());
    	userWishListResponse.setStrategy(StrategyResponse.from(userWishList.getStrategy(), true));

        return userWishListResponse;
    }
}
