package com.fintasoft.eraserisk.model.api.response;


import com.fintasoft.eraserisk.model.query.TradeReport;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class TradeReportResponse {
    private long userId;
    private long secId;
    private String loginTime;
    private String logoutTime;
    private int totalTrade;
    private int totalAutoTrade;
    private String username;
    private String fullName;
    private String securityName;
    private int totalManualTrade;

    public static TradeReportResponse from (TradeReport tradeReport) {
        TradeReportResponse response = new TradeReportResponse();
        response.setSecId(tradeReport.getSecId());
        response.setUserId(tradeReport.getUserId());
        response.setLoginTime(ConvertUtils.fromTime(tradeReport.getLoginTime()));
        response.setLogoutTime(ConvertUtils.fromTime(tradeReport.getLogoutTime()));
        response.setTotalTrade(tradeReport.getTotalTrade());
        response.setTotalAutoTrade(tradeReport.getTotalAutoTrade());
        response.setTotalManualTrade(tradeReport.getTotalManualTrade());
        response.setUsername(tradeReport.getUsername());
        response.setFullName(tradeReport.getFullName());
        response.setSecurityName(tradeReport.getSecurityName());

        return response;
    }
}
