package com.fintasoft.eraserisk.model;

import com.fintasoft.eraserisk.constances.PositionTypeEnums;
import com.fintasoft.eraserisk.model.db.SecuritiesCompany;
import com.fintasoft.eraserisk.model.db.Strategy;
import com.fintasoft.eraserisk.model.db.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "user_strategy_setting")
public class UserStrategySetting {
    @EmbeddedId
    private Pk pk;
    @Column
    private boolean autoDisabled;
    @Column
    private long balance;
    @Column
    private long lotSize;
    @Column
    @Enumerated(EnumType.STRING)
    private PositionTypeEnums positionType = PositionTypeEnums.None;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "strategy_id", insertable = false, updatable = false)
    private Strategy strategy;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sec_id", insertable = false, updatable = false)
    private SecuritiesCompany securitiesCompany;

    public UserStrategySetting(){}

    public UserStrategySetting(long strategyId, long userId, long secId, boolean auto) {
        pk = new Pk(strategyId, userId, secId, auto);
    }

    @Data
    @Embeddable
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Pk implements Serializable {
        @Column(name = "strategy_id")
        private long strategyId;
        @Column(name = "user_id")
        private long userId;
        @Column(name = "sec_id")
        private long secId;
        @Column
        private boolean auto;
    }
}
