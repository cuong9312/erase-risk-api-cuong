package com.fintasoft.eraserisk.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.fintasoft.eraserisk.utils.FileUtils;
import com.fintasoft.eraserisk.utils.LambdaExceptionUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
public class S3Service {
    private static final Logger log = LoggerFactory.getLogger(S3Service.class);

    @Autowired
    private AmazonS3 s3Client;
    @Autowired
    private S3Configuration s3Configuration;

    public String uploadFile(InputStream inputStream, String path, boolean privateAccess) throws IOException {
        return FileUtils.processTempFile(LambdaExceptionUtil.rethrowFunction(scratchFile -> {
            FileCopyUtils.copy(inputStream, new FileOutputStream(scratchFile));
            return this.uploadFile(scratchFile, path, privateAccess);
        }));
    }

    public String uploadFile(File file, String path, boolean privateAccess) {
        this.s3Client.putObject(new PutObjectRequest(s3Configuration.getBucketName(), path, file)
                .withCannedAcl(privateAccess ? CannedAccessControlList.Private : CannedAccessControlList.PublicRead));
        return s3Client.getUrl(s3Configuration.getBucketName(), path).toString();
    }

    public String uploadFile(HSSFWorkbook workbook, String path, boolean privateAccess) throws IOException {
        String filename=path.substring(path.lastIndexOf("/")+1);
        File file = new File(filename);
        workbook.write(file);
        log.info("Write report file: " + file.getAbsolutePath());
        this.s3Client.putObject(new PutObjectRequest(s3Configuration.getBucketName(), path, file)
                .withCannedAcl(privateAccess ? CannedAccessControlList.Private : CannedAccessControlList.PublicRead));
        return s3Client.getUrl(s3Configuration.getBucketName(), path).toString();
    }
}
