package com.fintasoft.eraserisk.s3;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class S3Configuration {
    @Value("${app.s3.region}")
    private String region;
    @Value("${app.s3.bucketName}")
    private String bucketName;
    @Value("${app.s3.accessKey}")
    private String accessKey;
    @Value("${app.s3.privateKey}")
    private String privateKey;

    public String getBucketName() {
        return bucketName;
    }

    @Bean
    public AmazonS3 s3Client() {
        AWSCredentials credentials = new BasicAWSCredentials(accessKey, privateKey);
        Regions regions = Regions.fromName(region);
        return AmazonS3ClientBuilder.standard().
                withCredentials(new AWSStaticCredentialsProvider(credentials)).
                withRegion(regions).
                build();
    }
}
