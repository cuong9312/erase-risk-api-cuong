package com.fintasoft.eraserisk.controllers.frontend;

import java.util.List;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.response.FaqGroupResponse;
import com.fintasoft.eraserisk.services.FaqService;

@RestController
public class FaqController {

    @Autowired
    private FaqService faqService;

    @GetMapping("/api/v1/faq-groups")
    @ApiOperation(value = "get faq group", tags = "FAQ")
    public ResponseEntity<Response<List<FaqGroupResponse>>> getAllFaqGroup(
            @RequestParam(name = "lang", defaultValue = "EN") String language
    ) {
        return ResponseEntity.ok(new Response(faqService.getAllFaqGroup(language)));
    }
}
