package com.fintasoft.eraserisk.controllers.frontend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.services.strategy.DerivativeProductService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private DerivativeProductService derivativeProductService;

    @GetMapping("/api/v1/products")
    @ApiOperation(value = "find products", tags = "Products")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response> findProduct(
        @RequestParam(value = "minExpiryDate", required = false) String minExpiryDate,
        @RequestParam(value = "maxExpiryDate", required = false) String maxExpiryDate,
        Pageable pageable
    ) throws ParseException, JsonProcessingException {
        List<QueryParam> queryParams = new ArrayList<>();
        QueryParam.createMinMaxDate(
                queryParams,
                "expiryDate",
                minExpiryDate,
                maxExpiryDate,
                "minExpiryDate",
                "maxExpiryDate"
        );
        return ResponseEntity.ok(new Response<>(derivativeProductService.findDerivativeProducts(queryParams, pageable)));
    }
}
