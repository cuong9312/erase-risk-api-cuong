package com.fintasoft.eraserisk.controllers.frontend.purchase;


import com.fintasoft.eraserisk.constances.PaymentStatusEnum;
import com.fintasoft.eraserisk.exceptions.TargetNotFoundException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.PurchaseAutoRequest;
import com.fintasoft.eraserisk.model.api.request.PurchaseLotSizeRequest;
import com.fintasoft.eraserisk.model.api.response.StrategyPurchaseStatisticResponse;
import com.fintasoft.eraserisk.model.api.response.UserPurchasePaymentResponse;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import com.fintasoft.eraserisk.model.api.response.UserPurchaseHistoryResponse;
import com.fintasoft.eraserisk.services.order.UserPurchaseHistoryService;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class UserPurchaseHistoryController {

    @Autowired
    private UserPurchaseHistoryService userPurchaseHistoryService;

    @GetMapping("/api/v1/users/{userId}/purchase-histories")
    @ApiOperation(value = "find purchase histories", tags = "Purchase History")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<UserPurchaseHistoryResponse>>> searchProducts(
            @PathVariable("userId") long userId,
            @RequestParam(value = "minExpiryDate", required = false) String minExpiryDate,
            @RequestParam(value = "maxExpiryDate", required = false) String maxExpiryDate,
            @RequestParam(value = "paymentStatuses", required = false) List<String> paymentStatuses,
            Pageable pageable
    ) throws ParseException {
        List<QueryParam> queryParams = new ArrayList<>();
        queryParams.add(QueryParam.create("user.id", userId + "", Long.class));
        QueryParam.createMinMaxDate(
                queryParams,
                "expirationDate",
                minExpiryDate,
                maxExpiryDate,
                "minExpiryDate",
                "maxExpiryDate"
        );

        if (!CollectionUtils.isEmpty(paymentStatuses)) {
            List<PaymentStatusEnum> statuses = new ArrayList<>();
            paymentStatuses.forEach(s -> statuses.add(PaymentStatusEnum.valueOf(s)));
            queryParams.add(QueryParam.create("payment.status", statuses, PaymentStatusEnum.class));
        }


        return ResponseEntity.ok(new Response(userPurchaseHistoryService.searchPurchaseHistories(
                queryParams,
                pageable
        )));
    }

    @GetMapping("/api/v1/users/{userId}/purchase-history")
    @ApiOperation(value = "find purchase histories", tags = "Purchase History")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<UserPurchaseHistoryResponse>>> findByUserId(
            @PathVariable("userId") long userId,
            @RequestParam(value = "type", required = false) String type,
            Pageable pageable
    ) {
        return ResponseEntity.ok(new Response(userPurchaseHistoryService.findByUserId(
                userId,
                type,
                pageable
        )));
    }

    @GetMapping("/api/v1/users/{userId}/payment-histories/{paymentId}")
    @ApiOperation(value = "find a payment by ID", tags = "Purchase History")
    public ResponseEntity<Response<UserPurchasePaymentResponse>> findPurchasePayment(
            @PathVariable(value = "userId") Long userId,
            @PathVariable(value = "paymentId") Long paymentId
            ) throws ParseException {
        UserPurchasePaymentResponse response = userPurchaseHistoryService.searchPurchasePayment(paymentId);
        if (response.getUser()== null || userId == null || response.getUser().getId() != userId) {
            throw new TargetNotFoundException();
        }
        return ResponseEntity.ok(new Response<>(response));
    }
}
