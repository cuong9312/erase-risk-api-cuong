package com.fintasoft.eraserisk.controllers.frontend.strategy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fintasoft.eraserisk.constances.TradingDurationEnum;
import com.fintasoft.eraserisk.constances.TradingStyleEnum;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.response.DailyProfitResponse;
import com.fintasoft.eraserisk.model.api.response.StrategyResponse;
import com.fintasoft.eraserisk.model.api.response.StrategySignalDataResponse;
import com.fintasoft.eraserisk.model.db.UserPurchaseHistory;
import com.fintasoft.eraserisk.repositories.impl.UserPurchaseRepositoryImpl;
import com.fintasoft.eraserisk.services.queue.StrategyPriceService;
import com.fintasoft.eraserisk.services.strategy.StrategyPositionService;
import com.fintasoft.eraserisk.services.strategy.StrategyService;
import com.fintasoft.eraserisk.services.strategy.StrategyStatusService;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import com.fintasoft.eraserisk.utils.TimeUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RestController
public class StrategyController {
    @Autowired
    private StrategyService strategyService;

    @Autowired
    private StrategyStatusService strategyStatusService;

    @Autowired
    private StrategyPositionService strategyPositionService;

    @Autowired
    private StrategyPriceService strategyPriceService;


    @Autowired
    private UserPurchaseRepositoryImpl userPurchaseRepository;

    @GetMapping("/api/v1/strategies")
    @ApiOperation(value = "search strategy", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<StrategyResponse>>> findAll(
    		@RequestParam(name = "provider", required = false) Long strategyProviderId,
    		@RequestParam(name = "name", required = false) String strategyName,
    		@RequestParam(name = "tradingStyle", required = false) String tradingStyle,
    		@RequestParam(name = "tradingDuration", required = false) String tradingDuration,
    		@RequestParam(name = "orderCategory", required = false) String orderCategory,
    		@RequestParam(name = "dailyProfitsRange", required = false, defaultValue="14") Integer dailyProfitsRange,
    		@RequestParam(name = "productCode", required = false) String productCode,
    		@RequestParam(name = "activeOnly", required = true, defaultValue="true") boolean activeOnly,
            Pageable p
    ) throws JsonProcessingException, ParseException {
        Pageable pageable = p;
        List<QueryParam> queryparams = new ArrayList<>();
        if (strategyProviderId != null) {
            queryparams.add(QueryParam.create("strategyProviderId", strategyProviderId.toString()));
        }

        if (!StringUtils.isEmpty(strategyName)) {
            queryparams.add(QueryParam.create("strategyName", strategyName));
        }

        if (!StringUtils.isEmpty(tradingStyle)) {
            try {
                TradingStyleEnum.valueOf(tradingStyle);
            } catch (IllegalArgumentException e) {
                throw new InvalidValueException("tradingStyle");
            }
            queryparams.add(QueryParam.create("style", tradingStyle, TradingStyleEnum.class));
        }

        if (!StringUtils.isEmpty(tradingDuration)) {
            try {
                TradingDurationEnum.valueOf(tradingDuration);
            } catch (IllegalArgumentException e) {
                throw new InvalidValueException("tradingDuration");
            }
            queryparams.add(QueryParam.create("tradingDuration", tradingDuration, TradingDurationEnum.class));
        }

        if (!StringUtils.isEmpty(orderCategory)) {
            queryparams.add(QueryParam.create(
                    QueryParam.create("strategyPositions.pk.category", orderCategory).leftJoin(),
                    QueryParam.create("strategyPositions.pk.category", "").leftJoin().nullValue()
            ));
            pageable = ConvertUtils.addMoreSort(pageable, new Sort(Sort.Direction.DESC, "strategyPositions.positionNo"), true);
        }

        if (!StringUtils.isEmpty(productCode)) {
            queryparams.add(QueryParam.create("derivativeProduct.derivativeCode", productCode));
        }

        if (activeOnly) {
            queryparams.add(QueryParam.create("status", strategyStatusService.getActivated().getId(), Long.class));
        }
        return ResponseEntity.ok(new Response(strategyService.findStrategiesWithDailyProfits(dailyProfitsRange, queryparams, orderCategory, pageable)));
    }
    
    @GetMapping("/api/v1/strategies/{strategyId}")
    @ApiOperation(value = "get strategy information", tags = "Strategy")
    public ResponseEntity<Response<Page<StrategyResponse>>> findByStrategyId(
            @PathVariable("strategyId") long strategyId,
            @RequestParam(name = "dailyProfitsRange", required = false, defaultValue="14") Integer dailyProfitsRange,
            @RequestParam(name = "categoryOrder", required = false) String categoryOrder
    ) throws ParseException {
        StrategyResponse strategyResponse = strategyService.findByStrategyId(strategyId);
        strategyResponse.setDailyProfits(strategyService.getDailyProfits(strategyId, dailyProfitsRange));
        if (!StringUtils.isEmpty(categoryOrder)) {
            strategyResponse.setOrderedPosition(strategyPositionService.getSafely(strategyId, categoryOrder));
        }
        strategyResponse.lastPrice(strategyPriceService.getPrice(strategyId));
        return ResponseEntity.ok(new Response(strategyResponse));
    }

    @GetMapping("/api/v1/strategies/{strategyId}/users/{userId}")
    @ApiOperation(value = "get strategy information", tags = "Strategy")
    public ResponseEntity<Response<Page<StrategyResponse>>> findByStrategyId(
            @PathVariable("userId") long userId,
            @PathVariable("strategyId") long strategyId,
            @RequestParam(name = "dailyProfitsRange", required = false, defaultValue="14") Integer dailyProfitsRange,
            @RequestParam(name = "categoryOrder", required = false) String categoryOrder
    ) throws ParseException {
        StrategyResponse strategyResponse = strategyService.findByStrategyId(strategyId);
        strategyResponse.setDailyProfits(strategyService.getDailyProfits(strategyId, dailyProfitsRange));
        if (!StringUtils.isEmpty(categoryOrder)) {
            strategyResponse.setOrderedPosition(strategyPositionService.getSafely(strategyId, categoryOrder));
        }
        strategyResponse.lastPrice(strategyPriceService.getPrice(strategyId));
        Page<UserPurchaseHistory> purchases = userPurchaseRepository.findValidOnly(Arrays.asList(userId), strategyId, PageRequest.of(0, 1));
        strategyResponse.setPurchaseUserIds(purchases.map(p -> p.getUser().getId()).getContent());
        return ResponseEntity.ok(new Response(strategyResponse));
    }

    @GetMapping("/api/v1/strategies/find-by-ids")
    @ApiOperation(value = "get list strategy information by strategy ids", tags = "Strategy")
    public ResponseEntity<Response<List<StrategyResponse>>> findByStrategyIds(
            @RequestParam(name = "strategyIds") List<Long> strategyIds,
            @RequestParam(name = "dailyProfitsRange", required = false, defaultValue="14") Integer dailyProfitsRange
    ) throws ParseException {
        return ResponseEntity.ok(new Response(strategyService.findByStrategyIds(dailyProfitsRange, strategyIds)));
    }

    @GetMapping("/api/v1/strategies/{strategyId}/signal-datas")
    @ApiOperation(value = "get short information of signal datas of a strategy", tags = "Strategy")
    public ResponseEntity<Response<Page<StrategySignalDataResponse>>> getStrategySignalData(
            @PathVariable("strategyId") long strategyId,
            @RequestParam(value = "minCreatedAt", required = false) String minCreatedAt,
            @RequestParam(value = "maxCreatedAt", required = false) String maxCreatedAt,
            @PageableDefault(direction = Sort.Direction.ASC, sort = "tradingAt") Pageable pageable
    ) throws ParseException {
        List<QueryParam> queryParams = new ArrayList<>();
        QueryParam.createMinMaxDate(queryParams, "createdAt", minCreatedAt, maxCreatedAt, "minCreatedAt", "maxCreatedAt");
        return ResponseEntity.ok(new Response(strategyService.getSignalDatas(strategyId, queryParams, pageable)));
    }

    @GetMapping("/api/v1/strategies/{strategyId}/daily-profits")
    @ApiOperation(value = "get information of daily profits of a strategy", tags = "Strategy")
    public ResponseEntity<Response<List<DailyProfitResponse>>> getDailyProfits(
            @PathVariable("strategyId") long strategyId,
            @RequestParam(value = "minCreatedAt", required = true) String minCreatedAt,
            @RequestParam(value = "maxCreatedAt", required = false, defaultValue = "EY") String maxCreatedAt
    ) throws ParseException {
        Timestamp start = new Timestamp(TimeUtils.convertMinDateToDate(minCreatedAt).getTime());
        Timestamp end = new Timestamp(TimeUtils.convertMinDateToDate(maxCreatedAt).getTime());
        return ResponseEntity.ok(new Response(strategyService.getDailyProfits(strategyId, start, end)));
    }

    @GetMapping("/api/v1/strategies/{strategyId}/daily-profits-by-range")
    @ApiOperation(value = "get information of daily profits of a strategy", tags = "Strategy")
    public ResponseEntity<Response<List<DailyProfitResponse>>> getDailyProfits(
            @PathVariable("strategyId") long strategyId,
            @RequestParam(value = "dateRange", required = true) Integer range
    ) throws ParseException {
        return ResponseEntity.ok(new Response(strategyService.getDailyProfits(strategyId, range)));
    }

    @GetMapping("/api/v1/users/{userId}/strategy")
    @ApiOperation(value = "search strategies os a user", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<StrategyResponse>>> findByUser(
    		@PathVariable("userId") long userId,
            Pageable pageable
    ) throws ParseException {
        return ResponseEntity.ok(new Response(strategyService.findByUser(
        		userId,
                pageable
        )));
    }


    @GetMapping("/api/v1/users/{userId}/strategies/{strategyId}/signal-datas")
    @ApiOperation(value = "get signal datas of a strategy with checking with userz", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<StrategySignalDataResponse>>> getStrategySignalData(
            @PathVariable("userId") long userId,
            @PathVariable("strategyId") long strategyId,
            @RequestParam(value = "minTradingAt", required = false) String minTradingAt,
            @RequestParam(value = "maxTradingAt", required = false) String maxTradingAt,
            @PageableDefault(direction = Sort.Direction.DESC, sort = "tradingAt") Pageable pageable
    ) throws ParseException {
        List<QueryParam> queryParams = new ArrayList<>();
        Date maxDate = TimeUtils.convertMaxDateAsDate(maxTradingAt);
        Date yesterday = TimeUtils.convertEndYesterday("EY");
        if (maxDate == null || maxDate.after(yesterday)) {
            List<QueryParam> userPurchaseParams = new ArrayList<>();
            userPurchaseParams.add(QueryParam.create("user.id", userId + "", Long.class));
            userPurchaseParams.add(QueryParam.create("strategy.id", strategyId + "", Long.class));
            try {
                QueryParam.createMinMaxDate(userPurchaseParams, "expirationDate", "now", null, "", "");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            Page<UserPurchaseHistory> results = userPurchaseRepository.findPurchaseHistories(userPurchaseParams, PageRequest.of(0, 1));
            if (CollectionUtils.isEmpty(results.getContent())) {
                throw new InvalidValueException("maxTradingAt");
            }
        }
        QueryParam.createMinMaxDate(queryParams, "tradingAt", minTradingAt, maxTradingAt, "minTradingAt", "maxTradingAt");
        return ResponseEntity.ok(new Response(strategyService.getSignalDatasforPc(strategyId, queryParams, pageable)));
    }

    @GetMapping("/api/v1/users/{userId}/strategy-ids")
    @ApiOperation(value = "all strategy ids of a user", tags = "Strategy")
    public ResponseEntity<Response<List<Long>>> findByUser(
            @PathVariable("userId") long userId
    ) {
        return ResponseEntity.ok(new Response(strategyService.getAllStrategyIdsBelongToUser(
                userId
        )));
    }

    @PutMapping(value = "/api/v1/strategies/{strategyId}/last-updated-status", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update Last-Updated Strategy Status", tags = "Strategy")
    public ResponseEntity<Response<StrategyResponse>> updateStatus(
            @PathVariable("strategyId") long strategyId
    ) {
        return ResponseEntity.ok(new Response<>(strategyService.lastUpdatedStatus(strategyId)));
    }


}
