package com.fintasoft.eraserisk.controllers.frontend;

import javax.validation.Valid;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.exceptions.NotFoundException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.request.*;
import com.fintasoft.eraserisk.services.user.UserAuditLogService;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.validator.LoginValidator;
import com.fintasoft.eraserisk.model.api.request.validator.UserRegisterValid;
import com.fintasoft.eraserisk.model.api.request.validator.UserUpdateValid;
import com.fintasoft.eraserisk.model.api.response.UserResponse;
import com.fintasoft.eraserisk.services.user.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    @InitBinder
    public void initBinderRegister(WebDataBinder binder) {
        if (binder.getObjectName().equalsIgnoreCase("login")) {
            binder.setValidator(new LoginValidator());
        } else if (binder.getObjectName().equalsIgnoreCase("userRegister")) {
            binder.setValidator(new UserRegisterValid());
        } else if (binder.getObjectName().equalsIgnoreCase("userUpdate")) {
        	binder.setValidator(new UserUpdateValid());
        }
    }

    @Autowired
    private UserService userService;

    @Autowired
    private UserAuditLogService userAuditLogService;

    @GetMapping("/api/v1/users/{userId}")
    @ApiOperation(value = "Get User by Id", tags = "Users")
    public ResponseEntity<Response<UserResponse>> findByUserId(
            @PathVariable("userId") Long id
    ) {
        return ResponseEntity.ok(new Response<>(userService.getUserByUserId(id, false)));
    }
    
    @PostMapping(value = "/api/v1/user/register", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Register User", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> registerUser(
            @RequestBody @Valid UserRegister userRegister
    ) throws IOException, TemplateException {
        return ResponseEntity.ok(new Response<>(userService.registerUser(userRegister)));
    }
    
    @PostMapping(value = "/api/v1/user/login", headers = "Content-Type=application/json")
    @ApiOperation(value = "Login", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> loginUser(
            @RequestBody @Valid Login login
    ) {
        return ResponseEntity.ok(new Response<>(userService.loginUser(login)));
    }
    
    @PutMapping(value = "/api/v1/user/{userId}", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update User", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> updateUser(
    		@PathVariable long userId,
            @RequestBody @Valid UserUpdate userUpdate
    ) {
        return ResponseEntity.ok(new Response<>(userService.updateUser(userId, userUpdate, false)));
    }

    @PutMapping(value = "/api/v1/users/{userId}", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update User", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> updateUserInfo(
    		@PathVariable long userId,
            @RequestBody @Valid UserUpdate userUpdate
    ) {
        return ResponseEntity.ok(new Response<>(userService.updateUser(userId, userUpdate, false)));
    }

    @PutMapping(value = "/api/v1/users/{userId}/update-password", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update User Password", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> updateUserPassword(
    		@PathVariable long userId,
            @RequestBody @Valid UpdatePassword updatePassword
    ) {
        return ResponseEntity.ok(new Response<UserResponse>(userService.updatePassword(userId, updatePassword)));
    }

    @PostMapping(value = "/api/v1/users/emailVerify", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update User Password", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> verifyEmail(
            @RequestBody String token
    ) {
        return ResponseEntity.ok(new Response<UserResponse>(userService.verifyUserEmail(token)));
    }

    @GetMapping(value = "/api/v1/users/findByUsername", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update User Password", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> findByUserName(
            @RequestParam("username") String username
    ) {
        return ResponseEntity.ok(new Response<>(userService.findByUserName(username)));
    }

    @GetMapping(value = "/api/v1/users/find", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update User Password", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> findByUserName(
            @RequestParam("param") String paramName,
            @RequestParam("value") String paramValue
    ) throws ParseException, JsonProcessingException {
        if (
                "email".equals(paramName)
                && "phoneNumber".equals(paramName)
//                && "fullName".equals(paramName)
                ) {
            throw new InvalidValueException("param");
        }

        String param = paramName;
        if (!"email".equals(paramName)) param = "userInfo." + param;

        List<QueryParam> queryParams = new ArrayList<>();
        queryParams.add(QueryParam.create(param, paramValue).extract());
        Page<UserResponse> users = userService.findUsers(queryParams, PageRequest.of(0, 1), false);
        if (users.getContent().isEmpty()) {
            throw new NotFoundException("user");
        }
        return ResponseEntity.ok(new Response<>(users.getContent().get(0)));
    }

    @GetMapping(value = "/api/v1/users/findByResetPasswordToken", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Check reset password token", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> findByUserResetPasswordToken(
            @RequestParam("token") String token
    ) {
        return ResponseEntity.ok(new Response<>(userService.findByResetPasswordToken(token)));
    }

    @PostMapping(value = "/api/v1/users/resetPasswordByEmail", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Request to send reset password link through email", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response> requestResetPasswordByEmail(
            @RequestBody long userId
    ) throws IOException, TemplateException {
        userService.sendResetPasswordViaEmail(userId);
        return ResponseEntity.ok(Response.empty());
    }

    @PostMapping(value = "/api/v1/users/resetPassword", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Request to send reset password link through email", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response> resetPasswordByEmail(
            @RequestBody EmailResetPasswordRequest request
        ) {
        if (StringUtils.isEmpty(request.getToken())) {
            throw new InvalidValueException("token");
        }

        UserResponse userResponse = userService.findByResetPasswordToken(request.getToken());
        userService.updatePassword(userResponse.getId(), request, false);
        return ResponseEntity.ok(Response.empty());
    }

    @PostMapping(value = "/api/v1/users/{userId}/securities-companies/{secId}/loginTime", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "update security login time", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<String>> updateSecLoginTime(
            @PathVariable("userId") long userId,
            @PathVariable("secId") long secId
            ) {
        
        return ResponseEntity.ok(new Response<>(userAuditLogService.securityLoginUpdate(userId, secId)));
    }

    @PostMapping(value = "/api/v1/users/{userId}/securities-companies/{secId}/logoutTime", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "update security logout time", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response> updateSecLogoutTime(
            @PathVariable("userId") long userId,
            @PathVariable("secId") long secId
            ) {
        userAuditLogService.securityLogoutUpdate(userId, secId);
        return ResponseEntity.ok(Response.empty());
    }
}
