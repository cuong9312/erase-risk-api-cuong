package com.fintasoft.eraserisk.controllers.frontend;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.response.SecuritiesCompanyResponse;
import com.fintasoft.eraserisk.services.strategy.SecuritiesCompanyService;

import io.swagger.annotations.ApiOperation;

@RestController
public class SecuritiesCompanyController {
    @Autowired
    SecuritiesCompanyService securitiesCompanyService;

    @GetMapping("/api/v1/securities-companies")
    @ApiOperation(value = "All securities companies", tags = "Securities companies")
    public ResponseEntity<Response<List<SecuritiesCompanyResponse>>> findAll() {
        return ResponseEntity.ok(new Response(securitiesCompanyService.getAll()));
    }
    
    @GetMapping("/api/v1/securities-companies/strategy")
    @ApiOperation(value = "All securities companies of 1 strategy", tags = "Securities companies")
    public ResponseEntity<Response<List<SecuritiesCompanyResponse>>> findByStrategyId(
    		 @RequestParam("strategyId") long strategyId) {
        return ResponseEntity.ok(new Response(securitiesCompanyService.findByStrategyId(strategyId)));
    }
}
