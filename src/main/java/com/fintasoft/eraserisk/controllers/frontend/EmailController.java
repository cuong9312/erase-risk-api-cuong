package com.fintasoft.eraserisk.controllers.frontend;

import com.fintasoft.eraserisk.constances.RoleEnums;
import com.fintasoft.eraserisk.model.api.EmailMarketingRequest;
import com.fintasoft.eraserisk.model.api.EmailSupportRequest;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.validator.EmailMarketingRequestValidator;
import com.fintasoft.eraserisk.services.EmailSupportService;
import com.fintasoft.eraserisk.services.user.UserService;
import freemarker.template.TemplateException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class EmailController {
    @Autowired
    EmailSupportService emailSupportService;

    @Autowired
    UserService userService;

    @InitBinder
    private void initBinderRegister(WebDataBinder binder) {
        if (binder.getObjectName().equalsIgnoreCase("emailMarketingRequest")) {
            binder.setValidator(new EmailMarketingRequestValidator());
        }
    }


    @PostMapping("/api/v1/users/{userId}/email-support")
    @ApiOperation(value = "user send email support", tags = "Support")
    public ResponseEntity<Response> findProduct(
            @PathVariable(value = "userId") Long userId,
            @RequestBody EmailSupportRequest request
            ) throws IOException, TemplateException {
        emailSupportService.createEmailSupport(request, userId);
        return ResponseEntity.ok(new Response<>(new HashMap<>()));
    }

    @PostMapping(value = "/api/v1/email-marketing", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "admin send email marketing", tags = "Marketing")
    public ResponseEntity<Response> sendEmailMarketing(
            @RequestBody @Valid EmailMarketingRequest emailMarketingRequest
    ) throws ParseException {
        List<QueryParam> queryParams = new ArrayList<>();
        QueryParam.createMinMaxDate(queryParams, "createdAt", emailMarketingRequest.getMinCreatedAt(), emailMarketingRequest.getMaxCreatedAt(), "minCreatedAt", "maxCreatedAt");
        queryParams.add(QueryParam.create("securitiesCompanies.id", emailMarketingRequest.getCompanyIds()));
        queryParams.add(QueryParam.create("username", emailMarketingRequest.getUsername()));
        queryParams.add(QueryParam.create("email", emailMarketingRequest.getEmail()));
        queryParams.add(QueryParam.create("userInfo.phoneNumber", emailMarketingRequest.getPhoneNumber()));
        queryParams.add(QueryParam.create("roles.roleName", RoleEnums.CUSTOMER.toString()));

        List<String> userEmails = userService.findEmails(queryParams);
        List<String> emails = new ArrayList<>();
        if (emailMarketingRequest.getAdditionEmails() != null) {
            emails.addAll(emailMarketingRequest.getAdditionEmails());
        }
        if (userEmails != null) {
            emails.addAll(userEmails);
        }
        if (!emails.isEmpty()) {
            emailSupportService.createEmailMarketing(emails, emailMarketingRequest.getContent(), emailMarketingRequest.getSubject());
        }
        return ResponseEntity.ok(new Response<>(emails));
    }
}
