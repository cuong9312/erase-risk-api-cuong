package com.fintasoft.eraserisk.controllers.frontend.purchase;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.AutoOrderRequest;
import com.fintasoft.eraserisk.model.api.request.ImportOrderRequest;
import com.fintasoft.eraserisk.model.api.request.UpdateOrderRequest;
import com.fintasoft.eraserisk.model.api.response.StrategyPurchaseStatisticResponse;
import com.fintasoft.eraserisk.model.api.response.UserAutoOrderResponse;
import com.fintasoft.eraserisk.services.order.OrderService;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/api/v1/users/{userId}/auto-order")
    @ApiOperation(value = "Save auto order into server", tags = "Orders")
    public ResponseEntity<Response<StrategyPurchaseStatisticResponse>> saveOrder(
    		@PathVariable long userId,
    		@RequestBody @Valid AutoOrderRequest autoOrderRequest
    ) {
    	
        return ResponseEntity.ok(new Response<>(orderService.saveOrder(userId, autoOrderRequest)));
    }
    
    @PutMapping("/api/v1/users/{userId}/update-order")
    @ApiOperation(value = "Update auto order from TC3", tags = "Orders")
    public ResponseEntity<Response> updateOrder(
    		@PathVariable long userId,
    		@RequestBody @Valid UpdateOrderRequest request
    ) {
    	orderService.updateOrder(userId, request);
        return ResponseEntity.ok(Response.empty());
    }
    
    @PostMapping("/api/v1/users/{userId}/import-order")
    @ApiOperation(value = "Import order from CIDBQ02400", tags = "Orders")
    public ResponseEntity<Response> importOrder(
    		@PathVariable long userId,
    		@RequestBody @Valid ImportOrderRequest request
    ) {
    	orderService.importOrder(userId, request);
        return ResponseEntity.ok(Response.empty());
    }

    @GetMapping("/api/v1/users/{userId}/strategies/{strategyId}/order-history")
    @ApiOperation(value = "Query Order history of a strategy and users", tags = "Orders")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<UserAutoOrderResponse>>> queryOrderHistories(
    		@PathVariable("userId") long userId,
    		@PathVariable("strategyId") long strategyId,
            Pageable pageable
    ) throws ParseException {
        List<QueryParam> queryParams = new ArrayList<>();
        queryParams.add(QueryParam.create("userPurchaseHistory.user.id", userId, Long.class));
        queryParams.add(QueryParam.create("userPurchaseHistory.strategy.id", strategyId, Long.class));
        return ResponseEntity.ok(new Response<>(this.orderService.find(queryParams, pageable)));
    }
    
    
}
