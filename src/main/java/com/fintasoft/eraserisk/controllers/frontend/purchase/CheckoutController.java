package com.fintasoft.eraserisk.controllers.frontend.purchase;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.CheckoutFeedback;
import com.fintasoft.eraserisk.model.api.request.CheckoutRequest;
import com.fintasoft.eraserisk.model.api.response.UserPurchasePaymentResponse;
import com.fintasoft.eraserisk.services.order.CheckoutService;

@RestController
public class CheckoutController {

    @Autowired
    private CheckoutService checkoutService;

    @PostMapping("/api/v1/users/{userId}/checkout")
    public ResponseEntity<Response<UserPurchasePaymentResponse>> checkout(
            @PathVariable("userId") long userId,
            @RequestBody @Valid CheckoutRequest request
    ) {
        return ResponseEntity.ok(new Response(checkoutService.checkout(
                userId,
                request
        )));
    }
    
    @PutMapping("/api/v1/checkout/feedback")
    public ResponseEntity<Response<UserPurchasePaymentResponse>> checkoutFeedback(
            @RequestBody @Valid CheckoutFeedback feedback
    ) {
        return ResponseEntity.ok(new Response(checkoutService.feedback(
                feedback
        )));
    }
}
