package com.fintasoft.eraserisk.controllers.frontend.strategy;


import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.PurchaseAutoRequest;
import com.fintasoft.eraserisk.model.api.response.PureUserStrategySettingResponse;
import com.fintasoft.eraserisk.model.api.response.UserStrategyPurchase;
import com.fintasoft.eraserisk.model.api.response.ComplexUserStrategySettingResponse;
import com.fintasoft.eraserisk.services.strategy.UiMyStrategyService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
public class MyStrategyController {

    @Autowired
    UiMyStrategyService uiMyStrategyService;

    @GetMapping("/api/v1/users/{userId}/my-strategies-setting")
    @ApiOperation(value = "Query my strategies setting", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<ComplexUserStrategySettingResponse>>> forPcApp(
            @PathVariable("userId") long userId,
            Pageable pageable
    ) throws ParseException {
        return ResponseEntity.ok(new Response<>(uiMyStrategyService.findSetting(userId, pageable)));
    }

    @GetMapping("/api/v1/users/{userId}/my-expired-strategies-setting")
    @ApiOperation(value = "Query my strategies setting", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<ComplexUserStrategySettingResponse>>> forPcAppExpired(
            @PathVariable("userId") long userId,
            Pageable pageable
    ) throws ParseException {
        return ResponseEntity.ok(new Response<>(uiMyStrategyService.findExpiredSetting(userId, pageable)));
    }

    @GetMapping("/api/v1/users/{userId}/my-strategies")
    @ApiOperation(value = "Query my strategies setting", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<UserStrategyPurchase>>> forWeb(
            @PathVariable("userId") long userId,
            @RequestParam(value = "dailyProfitLength", defaultValue = "30") int dailyProfitLength,
            Pageable pageable
    ) throws ParseException {
        return ResponseEntity.ok(new Response<>(uiMyStrategyService.findMyStrategies(userId, dailyProfitLength, pageable)));
    }

    @GetMapping("/api/v1/users/{userId}/my-expired-strategies")
    @ApiOperation(value = "Query my strategies setting", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<UserStrategyPurchase>>> forWebExpired(
            @PathVariable("userId") long userId,
            @RequestParam(value = "dailyProfitLength", defaultValue = "30") int dailyProfitLength,
            Pageable pageable
    ) throws ParseException {
        return ResponseEntity.ok(new Response<>(uiMyStrategyService.findMyExpiredStrategies(userId, dailyProfitLength, pageable)));
    }

    @PutMapping("/api/v1/users/{userId}/strategies/{strategyId}/secs/{secId}/settings/{auto}/update-auto")
    @ApiOperation(value = "Query my strategies setting", tags = "Strategy")
    public ResponseEntity<Response<PureUserStrategySettingResponse>> forWebExpired(
            @PathVariable("userId") long userId,
            @PathVariable("strategyId") long strategyId,
            @PathVariable("secId") long secId,
            @PathVariable("auto") boolean auto,
            @RequestBody PurchaseAutoRequest request
    ) throws ParseException {
        return ResponseEntity.ok(new Response(uiMyStrategyService.updateAutoOnOff(userId, strategyId, secId, auto, request)));
    }

    @PutMapping("/api/v1/users/{userId}/strategies/{strategyId}/secs/{secId}/settings/{auto}/update-lot-size")
    @ApiOperation(value = "Query my strategies setting", tags = "Strategy")
    public ResponseEntity<Response<PureUserStrategySettingResponse>> forWebExpired(
            @PathVariable("userId") long userId,
            @PathVariable("strategyId") long strategyId,
            @PathVariable("secId") long secId,
            @PathVariable("auto") boolean auto,
            @RequestBody int lotSize
    ) throws ParseException {
        return ResponseEntity.ok(new Response(uiMyStrategyService.updateLotSize(userId, strategyId, secId, auto, lotSize)));
    }

    @PutMapping("/api/v1/users/{userId}/strategies/{strategyId}/secs/{secId}/settings/update-position-setting")
    @ApiOperation(value = "Update Position setting", tags = "Strategy")
    public ResponseEntity<Response<PureUserStrategySettingResponse>> updateUserStrategySetting(
            @PathVariable("userId") long userId,
            @PathVariable("strategyId") long strategyId,
            @PathVariable("secId") long secId,
            @RequestBody String orderAction
    ) {
        return ResponseEntity.ok(new Response(uiMyStrategyService.updateUserStrategySetting(userId, strategyId, secId, orderAction)));
    }
}
