package com.fintasoft.eraserisk.controllers.frontend.purchase;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.WishListRequest;
import com.fintasoft.eraserisk.model.api.response.UserWishListResponse;
import com.fintasoft.eraserisk.services.order.UserWishListService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

@RestController
public class UserWishListController {

    @Autowired
    private UserWishListService userWishListService;

    @GetMapping("/api/v1/users/{userId}/wish-list")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<UserWishListResponse>>> findByUserId(
            @PathVariable("userId") long userId,
            @RequestParam(value = "category", defaultValue = "ROI_30") String category,
            Pageable pageable
    ) {
        return ResponseEntity.ok(new Response(userWishListService.findByUserId(
                userId,
                pageable
        )));
    }
    
    @PostMapping("/api/v1/users/{userId}/wish-list")
    @ApiOperation(value = "Add To Wish List", tags = "Wish List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<Page<UserWishListResponse>>> addToWishList(
            @PathVariable("userId") long userId,
            @RequestBody @Valid WishListRequest wishListRequest            
    ) {
        return ResponseEntity.ok(new Response(userWishListService.addToWishList(
                userId,
                wishListRequest
        )));
    }
    
    @DeleteMapping("/api/v1/users/{userId}/wish-list")
    @ApiOperation(value = "Delete items in Wish list", tags = "Wish List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response> deleteWishListItem(
            @PathVariable("userId") long userId,
            @RequestBody @Valid WishListRequest wishListRequest
    ) {
    	userWishListService.removeItemInWishList(
                userId,
                wishListRequest
        );
        return ResponseEntity.ok(Response.empty());
    }
}
