package com.fintasoft.eraserisk.controllers.frontend.purchase;

import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.BodyList;
import com.fintasoft.eraserisk.model.api.request.CartRequest;
import com.fintasoft.eraserisk.model.api.request.UpdateCartRequest;
import com.fintasoft.eraserisk.model.api.response.CartResponse;
import com.fintasoft.eraserisk.services.order.CartService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

@RestController
public class CartController {

    @Autowired
    private CartService cartService;

    @GetMapping("/api/v1/users/{userId}/carts")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<CartResponse>>> findByUserId(
            @PathVariable("userId") long userId,
            @RequestParam(value = "cartIds", required = false) List<Long> cartIds,
            @RequestParam(value = "category", required = false) String category,
            Pageable pageable
    ) throws ParseException {
        return ResponseEntity.ok(new Response(cartService.findByUserId(
                userId,
                category,
                pageable,
                cartIds
        )));
    }
    
    @PostMapping("/api/v1/users/{userId}/carts")
    @ApiOperation(value = "Add To Cart", tags = "Carts")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<Page<CartResponse>>> addToCart(
            @PathVariable("userId") long userId,
            @RequestBody @Valid CartRequest cartRequest            
    ) {
        return ResponseEntity.ok(new Response(cartService.addToCart(
                userId,
                cartRequest
        )));
    }

    @DeleteMapping("/api/v1/users/{userId}/carts")
    @ApiOperation(value = "Delete items in Carts", tags = "Carts")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response> deleteCarts(
            @PathVariable("userId") long userId,
            @RequestBody @Valid BodyList<Long> request
    ) {
        cartService.removeItemsInCart(
                userId,
                request.getItems()
        );
        return ResponseEntity.ok(Response.empty());
    }
    
    @PostMapping("/api/v1/users/{userId}/update-cart")
    @ApiOperation(value = "Update item in Carts", tags = "Carts")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response> updateCart(
            @PathVariable("userId") long userId,
            @RequestBody @Valid UpdateCartRequest updateCartRequest
    ) {
        cartService.updateCart(
                userId,
                updateCartRequest
        );
        return ResponseEntity.ok(Response.empty());
    }
}
