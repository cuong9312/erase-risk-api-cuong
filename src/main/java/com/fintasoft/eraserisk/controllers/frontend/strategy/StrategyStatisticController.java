package com.fintasoft.eraserisk.controllers.frontend.strategy;


import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.response.QueryDailyProfitResponse;
import com.fintasoft.eraserisk.model.api.response.StrategyStatisticRangeResponse;
import com.fintasoft.eraserisk.services.strategy.StrategyStatisticService;
import com.fintasoft.eraserisk.utils.TimeUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

@RestController
public class StrategyStatisticController {


    @Autowired
    private StrategyStatisticService strategyStatisticService;

    @GetMapping("/api/v1/strategies/{strategyId}/strategy-statistic")
    @ApiOperation(value = "get short information of signal datas of a strategy", tags = "Strategy Statistic")
    public ResponseEntity<Response<StrategyStatisticRangeResponse>> getStrategyStatistic(
            @PathVariable("strategyId") long strategyId,
            @RequestParam(value = "minTime") String minTime,
            @RequestParam(value = "maxTime") String maxTime
    ) throws ParseException {
        Timestamp min = new Timestamp(TimeUtils.convertMinDateToDate(minTime).getTime());
        Timestamp max = new Timestamp(TimeUtils.convertMaxDateAsDate(maxTime).getTime());
        if (max.after(new Timestamp(System.currentTimeMillis() - TimeUtils.MILISECONDS_OF_A_DAY))) {
            throw new InvalidValueException("maxTime");
        }
        return ResponseEntity.ok(new Response(strategyStatisticService.calculateByRange(strategyId, min, max)));
    }


    @GetMapping("/api/v1/strategies/strategy-statistics")
    @ApiOperation(value = "calculate information of requesting strategy", tags = "Strategy Statistic")
    public ResponseEntity<Response<Map<Long, StrategyStatisticRangeResponse>>> getStrategyStatistic(
            @RequestParam("strategyIds") List<Long> strategyIds,
            @RequestParam(value = "minTime") String minTime,
            @RequestParam(value = "maxTime") String maxTime
    ) throws ParseException {
        Timestamp min = new Timestamp(TimeUtils.convertMinDateToDate(minTime).getTime());
        Timestamp max = new Timestamp(TimeUtils.convertMaxDateAsDate(maxTime).getTime());
        Timestamp endYesterday = new Timestamp(TimeUtils.convertEndYesterday("EY").getTime());
        if (max.after(endYesterday)) {
            throw new InvalidValueException("maxTime");
        }
        return ResponseEntity.ok(new Response(strategyStatisticService.calculateByRange(strategyIds, min, max)));
    }


    @GetMapping("/api/v1/strategies/daily-profits")
    @ApiOperation(value = "calculate information of requesting strategy", tags = "Strategy Statistic")
    public ResponseEntity<Response<List<QueryDailyProfitResponse>>> queryDailyProfits(
            @RequestParam("strategyIds") List<Long> strategyIds,
            @RequestParam(value = "minTime") String minTime,
            @RequestParam(value = "maxTime") String maxTime
    ) throws ParseException {
        Timestamp min = new Timestamp(TimeUtils.convertMinDateToDate(minTime).getTime());
        Timestamp max = new Timestamp(TimeUtils.convertMaxDateAsDate(maxTime).getTime());
        Timestamp endYesterday = new Timestamp(TimeUtils.convertEndYesterday("EY").getTime());
        if (max.after(endYesterday)) {
            throw new InvalidValueException("maxTime");
        }
        return ResponseEntity.ok(new Response(strategyStatisticService.queryDailyProfit(strategyIds, min, max)));
    }



}
