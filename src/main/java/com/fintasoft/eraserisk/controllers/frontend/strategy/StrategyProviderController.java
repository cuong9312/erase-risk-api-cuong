package com.fintasoft.eraserisk.controllers.frontend.strategy;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.response.StrategyProviderResponse;
import com.fintasoft.eraserisk.services.strategy.StrategyProviderService;

import io.swagger.annotations.ApiOperation;

@RestController
public class StrategyProviderController {

    @Autowired
    private StrategyProviderService strategyProviderService;

    @GetMapping("/api/v1/strategy-providers")
    @ApiOperation(value = "All strategy providers", tags = "Strategy Providers")
    public ResponseEntity<Response<List<StrategyProviderResponse>>> getAll() {
        return ResponseEntity.ok(new Response(strategyProviderService.getAll()));
    }

    @GetMapping("/api/v1/strategy-providers/{providerId}")
    @ApiOperation(value = "All strategy providers", tags = "Strategy Providers")
    public ResponseEntity<Response<List<StrategyProviderResponse>>> getProvider(
            @PathVariable("providerId") Long providerId
    ) {
        return ResponseEntity.ok(new Response(strategyProviderService.getProvider(providerId)));
    }

}
