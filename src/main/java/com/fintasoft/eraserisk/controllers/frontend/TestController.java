package com.fintasoft.eraserisk.controllers.frontend;

import com.fintasoft.eraserisk.configurations.AppConf;
import com.fintasoft.eraserisk.constances.ErrorCodeEnums;
import com.fintasoft.eraserisk.exceptions.FieldError;
import com.fintasoft.eraserisk.exceptions.GeneralException;
import com.fintasoft.eraserisk.exceptions.InvalidParameterException;
import com.fintasoft.eraserisk.exceptions.PartialException;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.utils.ResponseUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private AppConf appConf;

    @GetMapping("/api/v1/test_success")
    public ResponseEntity<Response<TestModel>> testSuccess() {
        return ResponseUtil.from(new TestModel("hello world", "name"));
    }

    @GetMapping("/api/v1/test_partial_success")
    public ResponseEntity<Response<TestModel>> testPartialSuccess() {
        throw new PartialException(
                new TestModel("hello world", "name"),
                new GeneralException(ErrorCodeEnums.INVALID_PARAMETER_ERROR.name(), "error.unknown", null)
        );
    }

    @GetMapping("/api/v1/test_exception")
    public ResponseEntity<TestModel> testException() throws Exception {
        throw new Exception("abcd");
    }

    @GetMapping("/api/v1/test_gen_exception")
    public ResponseEntity<TestModel> testGeneralException() throws Exception {
        throw new GeneralException(ErrorCodeEnums.INVALID_PARAMETER_ERROR.name(), "error.unknown", null);
    }

    @GetMapping("/api/v1/test_sub_exception")
    public ResponseEntity<TestModel> testSubErrorException() throws Exception {
        throw new InvalidParameterException().add(
                new FieldError(
                        ErrorCodeEnums.INTERNAL_SERVER_ERRROR.name(),
                        "param1",
                        "error.unknown",
                        null
                )
        );
    }

    @Data
    @AllArgsConstructor
    public static class TestModel {
        private String mesage;
        private String mesageName;
    }
}
