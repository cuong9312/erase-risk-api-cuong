package com.fintasoft.eraserisk.controllers.frontend.strategy;

import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.response.OpenPositionResponse;
import com.fintasoft.eraserisk.model.api.response.UserOpenPositionResponse;
import com.fintasoft.eraserisk.services.signal.OpenPositionService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class OpenPositionController {

    @Autowired
    private OpenPositionService openPositionService;

    @GetMapping("/api/v1/users/{userId}/open-position")
    @ApiOperation(value = "Open Strategy Signal of User", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<UserOpenPositionResponse>>> queryOpenPositionOfUser (
            @PathVariable("userId") long userId,
            @RequestParam(value = "strategyName", required = false) String strategyName,
            @RequestParam(value = "providerName", required = false) String providerName,
            @RequestParam(value = "productName", required = false) String productName,
            @RequestParam(value = "strategyId", required = false) Long strategyId,
            @RequestParam(value = "providerId", required = false) Long providerId,
            @RequestParam(value = "productId", required = false) Long productId,
            Pageable pageable
    ) throws ParseException {
        List<QueryParam> queryParams = new ArrayList<>();
        if (!StringUtils.isEmpty(strategyName))
            queryParams.add(QueryParam.create("strategy.strategyName", strategyName));
        if (!StringUtils.isEmpty(providerName))
            queryParams.add(QueryParam.create("strategy.strategyProvider.companyName", providerName));
        if (!StringUtils.isEmpty(productName))
            queryParams.add(QueryParam.create("strategy.derivativeProduct.derivativeName", productName));
        if (strategyId != null)
            queryParams.add(QueryParam.create("strategyId", strategyId, Long.class));
        if (providerId != null)
            queryParams.add(QueryParam.create("strategy.strategyProviderId", providerId, Long.class));
        if (productId != null)
            queryParams.add(QueryParam.create("strategy.productId", productId, Long.class));
        return ResponseEntity.ok(new Response(openPositionService.queryOpenPositions(userId, queryParams, pageable)));
    }

    @GetMapping("/api/v1/users/{userId}/strategy/{strategyId}/open-position")
    @ApiOperation(value = "Open Strategy Signal of User", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<OpenPositionResponse>>> queryOpenPositionOfUser (
            @PathVariable("userId") long userId,
            @PathVariable("strategyId") long strategyId,
            Pageable pageable
    ) throws ParseException {
        return ResponseEntity.ok(new Response(openPositionService.queryOpenPosition(userId, strategyId, pageable)));
    }

    @GetMapping("/api/v1/strategy/{strategyId}/open-position")
    @ApiOperation(value = "Open Strategy Signal of strategy", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<OpenPositionResponse>>> queryOpenPositionOfStrategy (
            @PathVariable("strategyId") long strategyId,
            Pageable pageable
    ) throws ParseException {
        return ResponseEntity.ok(new Response(openPositionService.queryOpenPosition(strategyId, pageable)));
    }
}
