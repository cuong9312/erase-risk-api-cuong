package com.fintasoft.eraserisk.controllers.frontend.strategy;

import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.SignalDataRequest;
import com.fintasoft.eraserisk.model.api.response.StrategySignalDataResponse;
import com.fintasoft.eraserisk.model.api.response.TradingHistoryResponse;
import com.fintasoft.eraserisk.model.api.response.UserTradingHistoryResponse;
import com.fintasoft.eraserisk.services.signal.StrategySignalDataService;
import com.fintasoft.eraserisk.services.signal.TradingHistoryService;
import com.fintasoft.eraserisk.utils.TimeUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class StrategySignalDataController {

    @Autowired
    StrategySignalDataService strategySignalDataService;

    @Autowired
    TradingHistoryService tradingHistoryService;

    @PostMapping("/api/v1/signals")
    @ApiOperation(value = "Create a signal data", tags = "Signal")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @Deprecated
    public ResponseEntity<Response<StrategySignalDataResponse>> createSignalData(
            @RequestBody SignalDataRequest request
            ) {
        return ResponseEntity.ok(new Response<>(strategySignalDataService.createSignal(request)));
    }

    @GetMapping("/api/v1/strategies/{strategyId}/trading-history")
    @ApiOperation(value = "Trading history of a strategy", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<Page<TradingHistoryResponse>>> queryTradingHistory(
            @PathVariable("strategyId") long strategyId,
            @RequestParam(value = "range", required = false) Integer range,
            Pageable pageable
            ) throws ParseException {
        Timestamp min = null;
        Timestamp max = null;
        if (range != null) {
            min = new Timestamp(TimeUtils.startOfPrevious(range + 1).getTime());
            max = new Timestamp(TimeUtils.yesterdayEnd().getTime());
        }
        return ResponseEntity.ok(new Response<>(tradingHistoryService.queryTradingHistory(strategyId, min, max, pageable)));
    }

    @GetMapping("/api/v1/users/{userId}/trading-history")
    @ApiOperation(value = "Trading history of user", tags = "Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<Page<UserTradingHistoryResponse>>> queryMyTradingHistory(
            @PathVariable("userId") long userId,
            @RequestParam(value = "strategyName", required = false) String strategyName,
            @RequestParam(value = "providerName", required = false) String providerName,
            @RequestParam(value = "productName", required = false) String productName,
            @RequestParam(value = "strategyId", required = false) Long strategyId,
            @RequestParam(value = "providerId", required = false) Long providerId,
            @RequestParam(value = "productId", required = false) Long productId,
            Pageable pageable
            ) throws ParseException {
        List<QueryParam> queryParams = new ArrayList<>();
        if (!StringUtils.isEmpty(strategyName))
            queryParams.add(QueryParam.create("strategy.strategyName", strategyName));
        if (!StringUtils.isEmpty(providerName))
            queryParams.add(QueryParam.create("strategy.strategyProvider.companyName", providerName));
        if (!StringUtils.isEmpty(productName))
            queryParams.add(QueryParam.create("strategy.derivativeProduct.derivativeName", productName));
        if (strategyId != null)
            queryParams.add(QueryParam.create("strategyId", strategyId, Long.class));
        if (providerId != null)
            queryParams.add(QueryParam.create("strategy.strategyProviderId", providerId, Long.class));
        if (productId != null)
            queryParams.add(QueryParam.create("strategy.productId", productId, Long.class));
        return ResponseEntity.ok(new Response<>(tradingHistoryService.queryTradingHistoryByUser(userId, queryParams, pageable)));
    }

}
