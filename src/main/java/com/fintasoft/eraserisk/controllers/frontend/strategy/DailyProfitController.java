package com.fintasoft.eraserisk.controllers.frontend.strategy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.response.StrategyResponse;
import com.fintasoft.eraserisk.services.strategy.DailyProfitService;
import com.fintasoft.eraserisk.utils.TimeUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;

@RestController
public class DailyProfitController {
    @Autowired
    private DailyProfitService dailyProfitService;

    @GetMapping("/api/v1/daily-profits")
    @ApiOperation(value = "search strategy", tags = "Daily Profit")
    public ResponseEntity<Response<Page<StrategyResponse>>> findAll(
    		@RequestParam(name = "strategyIds") List<Long> strategyIds,
    		@RequestParam(name = "minDate", required = false) String minDate,
    		@RequestParam(name = "maxDate", required = false) String maxDate,
    		@RequestParam(name = "includeProfit", required = false, defaultValue = "true") boolean profit,
    		@RequestParam(name = "includeRoi", required = false, defaultValue = "false") boolean roi,
    		@RequestParam(name = "includeTrade", required = false, defaultValue = "false") boolean trade,
    		@RequestParam(name = "includeCount", required = false, defaultValue = "false") boolean count
    ) throws JsonProcessingException, ParseException {
        Timestamp start = StringUtils.isEmpty(minDate) ? null : new Timestamp(TimeUtils.convertMinDateToDate(minDate).getTime());
        Timestamp end = StringUtils.isEmpty(maxDate) ? null : new Timestamp(TimeUtils.convertMaxDateAsDate(maxDate).getTime());
        return ResponseEntity.ok(new Response(dailyProfitService.getDailyProfitsIn(strategyIds, start, end, profit, roi, trade, count)));
    }
}
