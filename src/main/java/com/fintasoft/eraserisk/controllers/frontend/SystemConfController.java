package com.fintasoft.eraserisk.controllers.frontend;

import com.fintasoft.eraserisk.constances.Constants;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.SystemConfCreate;
import com.fintasoft.eraserisk.model.api.request.SystemConfUpdate;
import com.fintasoft.eraserisk.model.api.request.validator.SystemConfCreateValidator;
import com.fintasoft.eraserisk.model.api.response.SystemConfResponse;
import com.fintasoft.eraserisk.services.SystemConfService;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class SystemConfController {

    @Autowired
    private SystemConfService systemConfService;

    @InitBinder
    private void initBinderRegister(WebDataBinder binder) {
        if (binder.getObjectName().equalsIgnoreCase("systemConfCreate")) {
            binder.setValidator(new SystemConfCreateValidator());
        }
    }

    @GetMapping("/api/v1/system-config/{name}/{lang}")
    @ApiOperation(value = "Get System Config By Name and Lang", tags = "System Config")
    public ResponseEntity<Response<SystemConfResponse>> findById(@PathVariable("name") String name, @PathVariable("lang") String lang) {
        return ResponseEntity.ok(new Response(systemConfService.findByPk(name, lang)));

    }

    @GetMapping("/api/v1/system-config/{name}")
    @ApiOperation(value = "Get System Config By Name", tags = "System Config")
    public ResponseEntity<Response<SystemConfResponse>> findByName(@PathVariable("name") String name) {
        return ResponseEntity.ok(new Response(systemConfService.findByPk(name, Constants.SYSTEM_CONFIG_LANG_NONE)));
    }

    @GetMapping("/api/v1/system-config")
    @ApiOperation(value = "Get All System Config", tags = "System Config")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("pk.name,DESC"),
                            @ExampleProperty("pk.lang,ASC"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<SystemConfResponse>>> findAll(@RequestParam(value = "name", required = false) String name, @RequestParam(value = "lang", required = false) String lang, Pageable pageable) {
        return ResponseEntity.ok(new Response(systemConfService.findAll(StringUtils.trimToEmpty(name), StringUtils.trimToEmpty(lang), pageable)));
    }

    @PostMapping(value = "/api/v1/system-config", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Add new System Config", tags = "System Config")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<SystemConfResponse>> save(
            @RequestBody @Valid SystemConfCreate systemConfCreate) {
        return ResponseEntity.ok(new Response<>(systemConfService.save(systemConfCreate.toSystemConf())));
    }


    @PutMapping(value = "/api/v1/system-config/{name}/{lang}", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update System Config", tags = "System Config")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<SystemConfResponse>> update( @PathVariable("name") String name, @PathVariable("lang") String lang,
            @RequestBody @Valid SystemConfUpdate systemConfUpdate) {
        return ResponseEntity.ok(new Response<>(systemConfService.update(name, lang, systemConfUpdate)));
    }
}
