package com.fintasoft.eraserisk.controllers.frontend.strategy;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.response.StrategyResponse;
import com.fintasoft.eraserisk.services.strategy.StrategyService;
import com.fintasoft.eraserisk.services.strategy.StrategyStatusService;
import com.fintasoft.eraserisk.utils.ConvertUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class StrategyWishListController {

    @Autowired
    StrategyService strategyService;
    @Autowired
    StrategyStatusService strategyStatusService;

    @GetMapping("/api/v1/users/{userId}/strategies/categories/{category}/top-wished-items")
    @ApiOperation(value = "Query top strategies has been added to cart by user", tags = "Strategy")
    public ResponseEntity<Response<StrategyResponse>> forWebExpired(
            @PathVariable("userId") long userId,
            @PathVariable("category") String category,
            @RequestParam(value = "activeOnly", defaultValue = "true") boolean activeOnly,
            Pageable pageable
    ) throws JsonProcessingException, ParseException {
        List<QueryParam> queryParams = new ArrayList<>();
        queryParams.add(QueryParam.create("carts.userId", userId, Long.class));

        queryParams.add(QueryParam.create(
                QueryParam.create("strategyPositions.pk.category", category).leftJoin(),
                QueryParam.create("strategyPositions.pk.category", "").leftJoin().nullValue()
        ));
        Sort sort = Sort.by(Sort.Direction.DESC, "strategyPositions.positionNo");
        if (pageable == null) {
            pageable = PageRequest.of(0, 20, sort);
        } else {
            pageable = ConvertUtils.addMoreSort(pageable, sort, true);
        }
        if (activeOnly) {
            queryParams.add(QueryParam.create("status", strategyStatusService.getActivated().getId(), Long.class));
        }

        Page<StrategyResponse> result = strategyService.findStrategies(queryParams, pageable, null, s -> StrategyResponse.fromBasic(null, s));
        strategyService.updateStrategyResponse(result, null, category);
        return ResponseEntity.ok(new Response(result));
    }
}
