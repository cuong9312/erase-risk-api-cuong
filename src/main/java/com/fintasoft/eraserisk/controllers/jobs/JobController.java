package com.fintasoft.eraserisk.controllers.jobs;

import com.fintasoft.eraserisk.exceptions.InvalidValueException;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.services.job.JobService;
import com.fintasoft.eraserisk.services.order.UserPurchaseHistoryService;
import com.fintasoft.eraserisk.utils.TimeUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.Date;


@RestController
public class JobController {
//    @Autowired
//    JobService jobService;
    @Autowired
    UserPurchaseHistoryService userPurchaseHistoryService;
//
//    @PostMapping("/api/v1/internal/jobs/strategy-pos")
//    @ApiOperation(value = "calculate strategy postition by range ", tags = "Jobs")
//    @com.fintasoft.eraserisk.annotations.Transactional
//    public ResponseEntity<Response> calculateStrategyPosition() throws Exception {
//        jobService.doCalculateStrategyPosition();
//        return ResponseEntity.ok(Response.empty());
//    }
//
//    @PostMapping("/api/v1/internal/jobs/daily-profit")
//    @ApiOperation(value = "calculate daily profit job ", tags = "Jobs")
//    @com.fintasoft.eraserisk.annotations.Transactional
//    public ResponseEntity<Response> calculateDailyProfit(
//        @RequestParam("date") String date
//    ) throws Exception {
//        Date d = null;
//        if (!StringUtils.isEmpty(date)) {
//            try {
//                d = TimeUtils.convertMinDateToTime(date);
//            } catch (Exception e) {
//                throw new InvalidValueException("date").source(e);
//            }
//            Timestamp start = new Timestamp(d.getTime());
//            Timestamp end = new Timestamp(TimeUtils.getEndOfDate(d).getTime());
//            jobService.doCalculateDailyProfit(start, end);
//        } else {
//            jobService.doCalculateDailyProfit(null, null);
//        }
//        return ResponseEntity.ok(Response.empty());
//    }
//
//
//
//    @PostMapping("/api/v1/internal/jobs/current-strategy-pos")
//    @ApiOperation(value = "calculate current strategy postition", tags = "Jobs")
//    @com.fintasoft.eraserisk.annotations.Transactional
//    public ResponseEntity<Response> updateStrategyPosition() throws Exception {
//        jobService.updateStrategyPos();
//        return ResponseEntity.ok(Response.empty());
//    }

    @PostMapping("/api/v1/internal/jobs/expire-purchase")
    @ApiOperation(value = "expire purchases", tags = "Jobs")
    @com.fintasoft.eraserisk.annotations.Transactional
    public ResponseEntity<Response> updateStrategyPosition() throws Exception {
        new Thread(() -> userPurchaseHistoryService.expirePurchases()).start();
        return ResponseEntity.ok(Response.empty());
    }
}
