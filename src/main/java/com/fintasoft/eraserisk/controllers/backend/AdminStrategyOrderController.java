package com.fintasoft.eraserisk.controllers.backend;

import com.fintasoft.eraserisk.exceptions.InvalidFormatException;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.response.PageExt;
import com.fintasoft.eraserisk.model.api.response.TradeAutoReportResponse;
import com.fintasoft.eraserisk.model.api.response.TradeReportResponse;
import com.fintasoft.eraserisk.model.query.TradeAutoReport;
import com.fintasoft.eraserisk.model.query.TradeReport;
import com.fintasoft.eraserisk.services.order.UserStrategyOrderService;
import com.fintasoft.eraserisk.utils.TimeUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;

@RestController
public class AdminStrategyOrderController {

    @Autowired
    UserStrategyOrderService userStrategyOrderService;

    @GetMapping(value = "/api/v1/admin/strategy-orders", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "User strategy order service", tags = "Strategy Order")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<PageExt<TradeReportResponse, TradeReport.Summary>>> queryTradeReport(
            @RequestParam(value = "securitiesCompaniesIds", required = false) List<Long> secIds,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> providerIds,
            @RequestParam(value = "minTradingAt", required = false) String minTradingAt,
            @RequestParam(value = "maxTradingAt", required = false) String maxTradingAt,
            @RequestParam(value = "username", required = false) String username,
            Pageable pageable
    ) {
        Timestamp min = null;
        if (!StringUtils.isEmpty(minTradingAt)) {
            try {
                min = new Timestamp(TimeUtils.convertMinDateToDate(minTradingAt).getTime());
            } catch (ParseException e) {
                throw new InvalidFormatException("minTradingAt");
            }
        }

        Timestamp max = null;
        if (!StringUtils.isEmpty(maxTradingAt)) {
            try {
                max = new Timestamp(TimeUtils.convertMaxDateAsDate(maxTradingAt).getTime());
            } catch (ParseException e) {
                throw new InvalidFormatException("minTradingAt");
            }
        }
        return ResponseEntity.ok(new Response<>(userStrategyOrderService.queryTradeReport(secIds, providerIds, min, max, username, pageable)));
    }

    @GetMapping(value = "/api/v1/admin/auto-strategy-orders", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "User strategy order service", tags = "Strategy Order")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<PageExt<TradeAutoReportResponse, TradeAutoReport.Summary>>> queryTradeAutoReport(
            @RequestParam(value = "securitiesCompaniesIds", required = false) List<Long> secIds,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> providerIds,
            @RequestParam(value = "minTradingAt", required = false) String minTradingAt,
            @RequestParam(value = "maxTradingAt", required = false) String maxTradingAt,
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "strategyName", required = false) String strategyName,
            Pageable pageable
    ) {
        Timestamp min = null;
        if (!StringUtils.isEmpty(minTradingAt)) {
            try {
                min = new Timestamp(TimeUtils.convertMinDateToDate(minTradingAt).getTime());
            } catch (ParseException e) {
                throw new InvalidFormatException("minTradingAt");
            }
        }

        Timestamp max = null;
        if (!StringUtils.isEmpty(maxTradingAt)) {
            try {
                max = new Timestamp(TimeUtils.convertMaxDateAsDate(maxTradingAt).getTime());
            } catch (ParseException e) {
                throw new InvalidFormatException("maxTradingAt");
            }
        }
        return ResponseEntity.ok(new Response<>(userStrategyOrderService.queryTradeAutoReport(secIds, providerIds, min, max, username, strategyName, pageable)));
    }
}
