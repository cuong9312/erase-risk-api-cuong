package com.fintasoft.eraserisk.controllers.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.ApproveRejectPaymentRequest;
import com.fintasoft.eraserisk.model.api.response.UserPurchasePaymentResponse;
import com.fintasoft.eraserisk.services.order.UserPurchaseHistoryService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class AdminPaymentController {

    @Autowired
    UserPurchaseHistoryService userPurchaseHistoryService;

    @GetMapping("/api/v1/admin/payment-histories")
    @ApiOperation(value = "Search purchase histories", tags = "Purchase History")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Object>> findPurchaseHistories(
            @RequestParam(value = "paymentMethods", required = false) List<Long> paymentMethodIds,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> providerIds,
            @RequestParam(value = "securitiesCompaniesIds", required = false) List<Long> secIds,
            @RequestParam(value = "minPurchaseDate", required = false) String minPurchaseDate,
            @RequestParam(value = "maxPurchaseDate", required = false) String maxPurchaseDate,
            @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "purchaseDate") Pageable pageable
    ) throws ParseException, JsonProcessingException {
        List<QueryParam> params = new ArrayList<>();
        params.add(QueryParam.create("paymentMethod.id", paymentMethodIds));
        params.add(QueryParam.create("items.strategy.strategyProviderId", providerIds));
        params.add(QueryParam.create("items.securitiesCompany.id", secIds));
        QueryParam.createMinMaxDate(
                params,
                "purchaseDate",
                minPurchaseDate,
                maxPurchaseDate,
                "minPurchaseDate",
                "maxPurchaseDate"
        );
        return ResponseEntity.ok(new Response<>(userPurchaseHistoryService.searchPurchasePayment(params, pageable)));
    }
    
    @PutMapping("/api/v1/admin/payment-histories/{paymentId}")
    @ApiOperation(value = "Approve/Reject Payment", tags = "Purchase Payment")
    public ResponseEntity<Response<UserPurchasePaymentResponse>> approveRejectPayment(
    		@PathVariable long paymentId,
    		@RequestBody ApproveRejectPaymentRequest request
    ) {
        return ResponseEntity.ok(new Response<>(userPurchaseHistoryService.approveRejectPayment(paymentId, request)));
    }
}
