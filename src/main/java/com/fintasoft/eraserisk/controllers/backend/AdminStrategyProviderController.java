package com.fintasoft.eraserisk.controllers.backend;

import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.StrategyProviderRequest;
import com.fintasoft.eraserisk.model.api.request.validator.StrategyProviderRequestValidator;
import com.fintasoft.eraserisk.model.api.response.StrategyProviderResponse;
import com.fintasoft.eraserisk.services.strategy.StrategyProviderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class AdminStrategyProviderController {

    @Autowired
    private StrategyProviderService strategyProviderService;


    @InitBinder
    public void bind(WebDataBinder binder) {
        if (binder.getTarget() instanceof StrategyProviderRequest) {
            binder.addValidators(new StrategyProviderRequestValidator());
        }
    }

    @GetMapping("/api/v1/admin/strategy-providers")
    @ApiOperation(value = "All strategy providers", tags = "Admin")
    public ResponseEntity<Response<List<StrategyProviderResponse>>> findAllStrategyProviders(
            @RequestParam(value = "full", required = false) boolean isFull,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> providerIds
    ) {
        if (CollectionUtils.isEmpty(providerIds))
            return ResponseEntity.ok(new Response(strategyProviderService.getAll(!isFull)));
        return ResponseEntity.ok(new Response(strategyProviderService.getAll(providerIds, !isFull)));
    }

    @PostMapping("/api/v1/admin/strategy-providers")
    @ApiOperation(value = "Register a Strategy Provider", tags = "Admin Strategy Provider")
    public ResponseEntity<Response> registerProvider(
            @RequestBody @Valid StrategyProviderRequest request
            ) {
        return ResponseEntity.ok(new Response(strategyProviderService.register(null, request)));
    }

    @PutMapping("/api/v1/admin/strategy-providers/{providerId}")
    @ApiOperation(value = "Update a Strategy Provider", tags = "Admin Strategy Provider")
    public ResponseEntity<Response> registerProvider(
            @PathVariable("providerId") Long providerId,
            @RequestBody @Valid StrategyProviderRequest request
            ) {
        return ResponseEntity.ok(new Response(strategyProviderService.register(providerId, request)));
    }

    @GetMapping("/api/v1/admin/strategy-providers/{providerId}")
    @ApiOperation(value = "Detail strategy providers", tags = "Admin")
    public ResponseEntity<Response<StrategyProviderResponse>> getStrategyProvider(
            @PathVariable(value = "providerId") long providerId
    ) {
        return ResponseEntity.ok(new Response(strategyProviderService.get(providerId)));
    }
}
