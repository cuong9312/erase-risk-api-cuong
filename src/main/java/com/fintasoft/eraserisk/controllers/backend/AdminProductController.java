package com.fintasoft.eraserisk.controllers.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.DerivativeProductRequest;
import com.fintasoft.eraserisk.model.api.response.DerivativeProductResponse;
import com.fintasoft.eraserisk.services.strategy.DerivativeProductService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class AdminProductController {
    @Autowired
    private DerivativeProductService derivativeProductService;


    @GetMapping("/api/v1/admin/products")
    @ApiOperation(value = "All Derivative Products", tags = "Admin Derivative Product")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<DerivativeProductResponse>>> findAll(
            @RequestParam(value = "derivativeCode", required = false) String derivativeCode,
            @RequestParam(value = "derivativeName", required = false) String derivativeName,
            @PageableDefault(sort = {"createdAt"}, direction = Sort.Direction.DESC) Pageable pageable
    ) throws ParseException, JsonProcessingException {
        List<QueryParam> queryParams = new ArrayList<>();
        queryParams.add(QueryParam.create("derivativeCode", derivativeCode));
        queryParams.add(QueryParam.create("derivativeName", derivativeName));
        return ResponseEntity.ok(new Response(derivativeProductService.findDerivativeProducts(queryParams, pageable)));
    }

    @PostMapping(value = "/api/v1/admin/products", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Register Derivative Product by Admin", tags = "Admin Derivative Product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<DerivativeProductResponse>> registerProduct(
            @RequestBody @Valid DerivativeProductRequest derivativeProductRequest
    ) {
        return ResponseEntity.ok(new Response(derivativeProductService.registerProduct(null, derivativeProductRequest)));
    }

    @PutMapping(value = "/api/v1/admin/products/{productId}", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Register Derivative Product by Admin", tags = "Admin Derivative Product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<DerivativeProductResponse>> updateStrategy(
            @PathVariable("productId") Long productId,
            @RequestBody @Valid DerivativeProductRequest derivativeProductRequest
    ) {
        return ResponseEntity.ok(new Response(derivativeProductService.registerProduct(productId, derivativeProductRequest)));
    }
}
