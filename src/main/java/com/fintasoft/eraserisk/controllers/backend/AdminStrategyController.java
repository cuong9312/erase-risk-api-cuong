package com.fintasoft.eraserisk.controllers.backend;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.StrategyData;
import com.fintasoft.eraserisk.model.api.request.StrategyRegister;
import com.fintasoft.eraserisk.model.api.response.StrategyResponse;
import com.fintasoft.eraserisk.services.strategy.StrategyService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

@RestController
public class AdminStrategyController {
    @Autowired
    private StrategyService strategyService;

    @GetMapping("/api/v1/admin/strategies")
    @ApiOperation(value = "All Strategies", tags = "Admin Strategy")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<StrategyResponse>>> queryStrategies(
            @RequestParam(value = "minCreatedAt", required = false) String minCreatedAt,
            @RequestParam(value = "maxCreatedAt", required = false) String maxCreatedAt,
            @RequestParam(value = "strategyCode", required = false) String strategyCode,
            @RequestParam(value = "strategyName", required = false) String strategyName,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> providerIds,
            @RequestParam(value = "providerName", required = false) String providerName,
            @PageableDefault(sort = {"createdAt"}, direction = Sort.Direction.DESC) Pageable pageable
    ) throws ParseException, JsonProcessingException {
        List<QueryParam> queryParams = new ArrayList<>();
        QueryParam.createMinMaxDate(
                queryParams,
                "createdAt",
                minCreatedAt,
                maxCreatedAt,
                "minCreatedAt",
                "maxCreatedAt"
        );
        queryParams.add(QueryParam.create("strategyCode", strategyCode));
        queryParams.add(QueryParam.create("strategyName", strategyName));
        queryParams.add(QueryParam.create("strategyProviderId", providerIds));
        queryParams.add(QueryParam.create("strategyProvider.companyName", providerName));
        return ResponseEntity.ok(new Response(strategyService.findStrategies(queryParams, pageable)));
    }

    @PostMapping(value = "/api/v1/admin/strategies", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Register Strategy by Admin", tags = "Admin Strategy")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<StrategyResponse>> registerStrategy(
            @RequestBody @Valid StrategyRegister strategyRegister
    ) {
        return ResponseEntity.ok(new Response(strategyService.registerStrategy(null, strategyRegister)));
    }

    @PutMapping(value = "/api/v1/admin/strategies/{strategyId}", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update Strategy by Admin", tags = "Admin Strategy")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<StrategyResponse>> registerStrategy(
            @PathVariable Long strategyId,
            @RequestBody @Valid StrategyRegister strategyRegister
    ) {
        return ResponseEntity.ok(new Response(strategyService.registerStrategy(strategyId, strategyRegister)));
    }
    
    @PostMapping(value = "/api/v1/admin/strategies/upload-data", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Upload data for Strategy by Admin", tags = "Admin Strategy")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response> uploadData(
            @RequestBody @Valid StrategyData strategyData
    ) {
        strategyService.uploadData(strategyData);
        return ResponseEntity.ok(Response.empty());
    }
    
    @PostMapping(value = "/api/v1/admin/strategies/{strategyId}/export-signal", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update Strategy by Admin", tags = "Admin Strategy")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<Long>> exportSignalDat(
            @PathVariable Long strategyId
    ) throws JsonProcessingException {
        return ResponseEntity.ok(new Response(strategyService.exportSignalData(strategyId)));
    }
}
