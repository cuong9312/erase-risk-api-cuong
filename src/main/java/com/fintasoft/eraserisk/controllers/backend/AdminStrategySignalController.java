package com.fintasoft.eraserisk.controllers.backend;

import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.SignalDataRequest;
import com.fintasoft.eraserisk.model.api.response.StrategySignalDataResponse;
import com.fintasoft.eraserisk.services.signal.StrategySignalDataService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


@Controller
public class AdminStrategySignalController {

    @Autowired
    StrategySignalDataService strategySignalDataService;

    @PostMapping("/api/v1/admin/users/{userId}/signals")
    @ApiOperation(value = "Create a signal data", tags = "Signal")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<StrategySignalDataResponse>> createSignalData(
            @PathVariable Long userId,
            @RequestBody SignalDataRequest request
    ) {
        return ResponseEntity.ok(new Response<>(strategySignalDataService.createSignal(userId, request)));
    }


    @GetMapping("/api/v1/admin/signals")
    @ApiOperation(value = "Signal", tags = "Admin Signal")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<StrategySignalDataResponse>>> querySignalDatas(
            @RequestParam(value = "strategyIds", required = false) List<Long> strategyIds,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> strategyProviderIds,
            @PageableDefault(sort = {"createdAt", "id"}, direction = Sort.Direction.DESC) Pageable pageable
    ) throws ParseException {
        List<QueryParam> queryParams = new ArrayList<>();
        queryParams.add(QueryParam.create("strategyId", strategyIds));
        if (!CollectionUtils.isEmpty(strategyProviderIds)) {
            queryParams.add(QueryParam.create("strategy.strategyProviderId", strategyProviderIds, Long.class));
        }
        return ResponseEntity.ok(new Response(strategySignalDataService.query(queryParams, pageable)));
    }
}
