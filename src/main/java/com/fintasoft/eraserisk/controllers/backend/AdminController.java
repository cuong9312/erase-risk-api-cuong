package com.fintasoft.eraserisk.controllers.backend;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fintasoft.eraserisk.constances.RoleEnums;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.AdminRegister;
import com.fintasoft.eraserisk.model.api.request.AdminUpdate;
import com.fintasoft.eraserisk.model.api.request.Login;
import com.fintasoft.eraserisk.model.api.request.UpdatePassword;
import com.fintasoft.eraserisk.model.api.request.UserUpdate;
import com.fintasoft.eraserisk.model.api.request.validator.AdminRegisterValid;
import com.fintasoft.eraserisk.model.api.request.validator.AdminUpdateValid;
import com.fintasoft.eraserisk.model.api.request.validator.LoginValidator;
import com.fintasoft.eraserisk.model.api.request.validator.UserUpdateValid;
import com.fintasoft.eraserisk.model.api.response.AuthorityResponse;
import com.fintasoft.eraserisk.model.api.response.PaymentMethodResponse;
import com.fintasoft.eraserisk.model.api.response.ReportRequestResponse;
import com.fintasoft.eraserisk.model.api.response.SecuritiesCompanyResponse;
import com.fintasoft.eraserisk.model.api.response.SignalPlatformResponse;
import com.fintasoft.eraserisk.model.api.response.StrategyStatusResponse;
import com.fintasoft.eraserisk.model.api.response.UserResponse;
import com.fintasoft.eraserisk.services.user.AuthorityService;
import com.fintasoft.eraserisk.services.order.PaymentMethodService;
import com.fintasoft.eraserisk.services.ReportService;
import com.fintasoft.eraserisk.services.strategy.SecuritiesCompanyService;
import com.fintasoft.eraserisk.services.signal.SignalPlatformService;
import com.fintasoft.eraserisk.services.strategy.StrategyProviderService;
import com.fintasoft.eraserisk.services.strategy.StrategyStatusService;
import com.fintasoft.eraserisk.services.user.UserService;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

@RestController
public class AdminController {

    @InitBinder
    private void initBinderRegister(WebDataBinder binder) {
        if (binder.getObjectName().equalsIgnoreCase("login")) {
            binder.setValidator(new LoginValidator());
        } else if (binder.getObjectName().equalsIgnoreCase("adminRegister")) {
            binder.setValidator(new AdminRegisterValid());
        } else if (binder.getObjectName().equalsIgnoreCase("userUpdate")) {
        	binder.setValidator(new UserUpdateValid());
        } else if (binder.getObjectName().equalsIgnoreCase("adminUpdate")) {
        	binder.setValidator(new AdminUpdateValid());
        }
    }

    @Autowired
    private UserService userService;

    @Autowired
    private StrategyProviderService strategyProviderService;
    
    @Autowired
    private SignalPlatformService signalPlatformService;

    @Autowired
    private StrategyStatusService strategyStatusService;
    
    @Autowired
    SecuritiesCompanyService securitiesCompanyService;
    
    @Autowired
    AuthorityService authorityService;

    @Autowired
    PaymentMethodService paymentMethodService;
    
    @Autowired
    ReportService reportService;

    @GetMapping("/api/v1/admin/users/query")
    public ResponseEntity<Response<UserResponse>> findByUsername(
            @RequestParam("username") String username
    ) {
        return ResponseEntity.ok(new Response(userService.getUserByUsername(username)));
    }

    @GetMapping("/api/v1/admin/users/{userId}")
    @ApiOperation(value = "Get User by Id", tags = "Users")
    public ResponseEntity<Response<UserResponse>> findByUserId(
            @PathVariable("userId") Long id
    ) {
        return ResponseEntity.ok(new Response(userService.getUserByUserId(id, false)));
    }
    
    @GetMapping("/api/v1/admin/admins/{userId}")
    @ApiOperation(value = "Get Admin by Id", tags = "Users")
    public ResponseEntity<Response<UserResponse>> findByAdminId(
            @PathVariable("userId") Long id
    ) {
        return ResponseEntity.ok(new Response(userService.getUserByUserId(id, true)));
    }

    @GetMapping("/api/v1/admin/users")
    @ApiOperation(value = "All Users", tags = "Users")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<UserResponse>>> findAll(
            @RequestParam(value = "companyId", required = false) List<Long> companyIds,
            @RequestParam(value = "minCreatedAt", required = false) String minCreatedAt,
            @RequestParam(value = "maxCreatedAt", required = false) String maxCreatedAt,
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "phoneNumber", required = false) String phoneNumber,
            @RequestParam(value = "email", required = false) String email,
            @PageableDefault(sort = {"createdAt"}, direction = Sort.Direction.DESC) Pageable pageable
    ) throws ParseException, JsonProcessingException {
        List<QueryParam> queryParams = new ArrayList<>();
        QueryParam.createMinMaxDate(queryParams, "createdAt", minCreatedAt, maxCreatedAt, "minCreatedAt", "maxCreatedAt");
        queryParams.add(QueryParam.create("securitiesCompanies.id", companyIds, Long.class));
        queryParams.add(QueryParam.create("username", username));
        queryParams.add(QueryParam.create("email", email));
        queryParams.add(QueryParam.create("userInfo.phoneNumber", phoneNumber));
        queryParams.add(QueryParam.create("roles.roleName", RoleEnums.CUSTOMER.toString()));
        return ResponseEntity.ok(new Response(userService.findUsers(queryParams, pageable, false)));
    }
    
    @GetMapping("/api/v1/admin/admins")
    @ApiOperation(value = "All Admins", tags = "Users")
    @ApiImplicitParams(
            value = {
                    @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                            examples = @Example(@ExampleProperty("1")), paramType = "query"),
                    @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                            examples = @Example(@ExampleProperty("40")), paramType = "query"),
                    @ApiImplicitParam(
                            name = "sort", examples = @Example({
                            @ExampleProperty("contractId,DESC"),
                            @ExampleProperty("merchantId,ASC"),
                            @ExampleProperty("merchantId"),
                    }), dataType = "string", paramType = "query", allowMultiple = true
                    )
            }
    )
    public ResponseEntity<Response<Page<UserResponse>>> findAllAdmin(
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "fullName", required = false) String fullName,
            @RequestParam(value = "phoneNumber", required = false) String phoneNumber,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "providerIds", required = false) List<Long> providerIds,
            @RequestParam(value = "secIds", required = false) List<Long> secIds,
            @PageableDefault(sort = {"createdAt"}, direction = Sort.Direction.DESC) Pageable pageable
    ) throws ParseException, JsonProcessingException {
        List<QueryParam> queryParams = new ArrayList<>();

        queryParams.add(QueryParam.create("username", username));
        queryParams.add(QueryParam.create("email", email));
        queryParams.add(QueryParam.create("userInfo.fullName", fullName));
        queryParams.add(QueryParam.create("userInfo.phoneNumber", phoneNumber));
        queryParams.add(QueryParam.create("roles.roleName", RoleEnums.ADMIN.toString()));
        queryParams.add(QueryParam.create("adminStrategyProviders.id", providerIds));
        queryParams.add(QueryParam.create("securitiesCompanies.id", secIds));
        return ResponseEntity.ok(new Response(userService.findUsers(queryParams, pageable, true)));
    }
    
    @PostMapping(value = "/api/v1/admin/login", headers = "Content-Type=application/json")
    @ApiOperation(value = "Admin Login", tags = "Admin")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<List<UserResponse>>> loginAdmin(
            @RequestBody @Valid Login login
    ) throws Exception {
        return ResponseEntity.ok(new Response(userService.loginAdmin(login)));
    }
    
    @PutMapping(value = "/api/v1/admin/user/{userId}", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update User by Admin", tags = "Admin")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> updateUser(
    		@PathVariable long userId,
            @RequestBody @Valid UserUpdate userUpdate
    ) {
        return ResponseEntity.ok(new Response(userService.updateUser(userId, userUpdate, true)));
    }
    
    @GetMapping("/api/v1/admin/signal-platforms")
    @ApiOperation(value = "All signal platforms", tags = "Admin")
    public ResponseEntity<Response<List<SignalPlatformResponse>>> findAllSignalPlatforms() {
        return ResponseEntity.ok(new Response(signalPlatformService.getAll()));
    }
    
    @GetMapping("/api/v1/admin/strategy-statuses")
    @ApiOperation(value = "All strategy status", tags = "Admin")
    public ResponseEntity<Response<List<StrategyStatusResponse>>> findAllStrategyStatus() {
        return ResponseEntity.ok(new Response(strategyStatusService.getAll()));
    }


    @GetMapping("/api/v1/admin/securities-companies")
    @ApiOperation(value = "All securities companies", tags = "Securities companies")
    public ResponseEntity<Response<List<SecuritiesCompanyResponse>>> findAllSecs(
            @RequestParam(value = "securitiesCompaniesIds", required = false) List<Long> secIds
    ) {
        if (CollectionUtils.isEmpty(secIds))
            return ResponseEntity.ok(new Response(securitiesCompanyService.getAll()));
        return ResponseEntity.ok(new Response(securitiesCompanyService.getAll(secIds)));
    }
    
    @GetMapping("/api/v1/admin/authorities")
    @ApiOperation(value = "All authorities", tags = "Authorities")
    public ResponseEntity<Response<List<AuthorityResponse>>> findAllAuthorities() {
        return ResponseEntity.ok(new Response(authorityService.getAll()));
    }
    
    @PostMapping(value = "/api/v1/admin/register", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Register Admin", tags = "Users")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> registerAdmin(
            @RequestBody @Valid AdminRegister adminRegister
    ) {
        return ResponseEntity.ok(new Response<UserResponse>(userService.registerAdmin(adminRegister)));
    }
    
    @PutMapping(value = "/api/v1/admin/{userId}", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update Admin by Admin", tags = "Admin")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> updateAdmin(
    		@PathVariable long userId,
            @RequestBody @Valid AdminUpdate adminUpdate
    ) {
        return ResponseEntity.ok(new Response(userService.updateAdmin(userId, adminUpdate, true)));
    }
    
    @PutMapping(value = "/api/v1/admin/profile/{userId}/update-profile", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update Admin by Self", tags = "Admin")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> updateAdminProfile(
    		@PathVariable long userId,
            @RequestBody @Valid AdminUpdate adminUpdate
    ) {
        return ResponseEntity.ok(new Response(userService.updateAdmin(userId, adminUpdate, false)));
    }
    
    @PutMapping(value = "/api/v1/admin/profile/{userId}/update-password", consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE}, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "Update Admin by Self", tags = "Admin")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<UserResponse>> updateAdminPassword(
    		@PathVariable long userId,
            @RequestBody @Valid UpdatePassword updatePassword
    ) {
        return ResponseEntity.ok(new Response(userService.updatePassword(userId, updatePassword)));
    }

    @GetMapping("/api/v1/admin/payment-methods")
    @ApiOperation(value = "All payment methods", tags = "Payment Method")
    public ResponseEntity<Response<List<PaymentMethodResponse>>> findAllPaymentMethods() {
        return ResponseEntity.ok(new Response(paymentMethodService.findAll()));
    }
    
    @GetMapping("/api/v1/admin/report")
    @ApiOperation(value = "Get Report", tags = "Report")
    public ResponseEntity<Response<ReportRequestResponse>> findReport(
    		@RequestParam("id") long reportId) {
        return ResponseEntity.ok(new Response(reportService.findReport(reportId)));
    }
}
