package com.fintasoft.eraserisk.controllers.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fintasoft.eraserisk.exceptions.InvalidFormatException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.response.PageExt;
import com.fintasoft.eraserisk.model.api.response.UserPurchaseHistoryResponse;
import com.fintasoft.eraserisk.model.query.RevenueSummary;
import com.fintasoft.eraserisk.services.order.UserPurchaseHistoryService;
import com.fintasoft.eraserisk.utils.TimeUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class AdminPurchaseHistoryController {
    @Autowired
    private UserPurchaseHistoryService userPurchaseHistoryService;

    /**
     * currently not use in UI. If change it to use need to add params strategyProviderIds and securitiesCompaniesIds to secure in Yii
     * @param paymentMethodIds
     * @param providerIds
     * @param minPurchaseDate
     * @param maxPurchaseDate
     * @param pageable
     * @return
     * @throws ParseException
     */
    @GetMapping("/api/v1/admin/purchase-histories")
    @ApiOperation(value = "Search purchase histories", tags = "Purchase History")
    @ApiImplicitParams(
        value = {
            @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                    examples = @Example(@ExampleProperty("1")), paramType = "query"),
            @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                    examples = @Example(@ExampleProperty("40")), paramType = "query"),
            @ApiImplicitParam(
                    name = "sort", examples = @Example({
                    @ExampleProperty("contractId,DESC"),
                    @ExampleProperty("merchantId,ASC"),
                    @ExampleProperty("merchantId"),
            }), dataType = "string", paramType = "query", allowMultiple = true
            )
        }
    )
    public ResponseEntity<Response<Page<UserPurchaseHistoryResponse>>> findPurchaseHistories(
        @RequestParam(value = "paymentMethods", required = false) List<Long> paymentMethodIds,
        @RequestParam(value = "providers", required = false) List<Long> providerIds,
        @RequestParam(value = "minPurchaseDate", required = false) String minPurchaseDate,
        @RequestParam(value = "maxPurchaseDate", required = false) String maxPurchaseDate,
        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "purchaseDate") Pageable pageable
    ) throws ParseException {
        List<QueryParam> params = new ArrayList<>();
        params.add(QueryParam.create("paymentMethodId", paymentMethodIds));
        params.add(QueryParam.create("strategy.strategyProviderId", providerIds));
        QueryParam.createMinMaxDate(
                params,
                "purchaseDate",
                minPurchaseDate,
                maxPurchaseDate,
                "minPurchaseDate",
                "maxPurchaseDate"
        );
        return ResponseEntity.ok(new Response<>(userPurchaseHistoryService.searchPurchaseHistories(params, pageable)));
    }

    @GetMapping("/api/v1/admin/revenues")
    @ApiOperation(value = "revenue report", tags = "Revenue")
    @ApiImplicitParams(
        value = {
            @ApiImplicitParam(name = "page", value = "page number start from 0", dataType = "integer",
                    examples = @Example(@ExampleProperty("1")), paramType = "query"),
            @ApiImplicitParam(name = "size", value = "maximum number of item in page", dataType = "integer",
                    examples = @Example(@ExampleProperty("40")), paramType = "query"),
            @ApiImplicitParam(
                    name = "sort", examples = @Example({
                    @ExampleProperty("contractId,DESC"),
                    @ExampleProperty("merchantId,ASC"),
                    @ExampleProperty("merchantId"),
            }), dataType = "string", paramType = "query", allowMultiple = true
            )
        }
    )
    public ResponseEntity<Response<PageExt<?, RevenueSummary>>> queryRevenue(
        @RequestParam(value = "byDay", required = false, defaultValue = "true") boolean isDay,
        @RequestParam(value = "paymentMethods", required = false) List<Long> paymentMethodIds,
        @RequestParam(value = "strategyProviderIds", required = false) List<Long> providerIds,
        @RequestParam(value = "securitiesCompaniesIds", required = false) List<Long> secIds,
        @RequestParam(value = "minPurchaseDate", required = false) String minPurchaseDate,
        @RequestParam(value = "maxPurchaseDate", required = false) String maxPurchaseDate,
        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "purchaseDate") Pageable pageable
    ) throws ParseException, JsonProcessingException {
        Timestamp minCreated = null;
        Timestamp maxCreated = null;
        try {
            Date minDate = TimeUtils.convertMinDateToTime(minPurchaseDate);
            if (minDate != null) minCreated = new Timestamp(minDate.getTime());
            try {
                Date maxDate = TimeUtils.convertMaxDateToTime(maxPurchaseDate);
                if (maxDate != null) maxCreated = new Timestamp(maxDate.getTime());
            } catch (ParseException e) {
                throw new InvalidFormatException("maxPurchaseDate");
            }
        } catch (ParseException e) {
            throw new InvalidFormatException("minPurchaseDate");
        }

        return ResponseEntity.ok(new Response<>(userPurchaseHistoryService.queryRevenueByDay(isDay,
                paymentMethodIds, providerIds, secIds, minCreated, maxCreated, pageable)));
    }
}
