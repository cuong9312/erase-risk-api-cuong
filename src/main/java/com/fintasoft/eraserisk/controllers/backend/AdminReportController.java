package com.fintasoft.eraserisk.controllers.backend;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fintasoft.eraserisk.constances.RoleEnums;
import com.fintasoft.eraserisk.exceptions.InvalidFormatException;
import com.fintasoft.eraserisk.model.api.QueryParam;
import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.services.order.UserPurchaseHistoryService;
import com.fintasoft.eraserisk.services.order.UserStrategyOrderService;
import com.fintasoft.eraserisk.services.signal.StrategySignalDataService;
import com.fintasoft.eraserisk.services.strategy.DerivativeProductService;
import com.fintasoft.eraserisk.services.strategy.StrategyService;
import com.fintasoft.eraserisk.services.user.UserService;
import com.fintasoft.eraserisk.utils.TimeUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class AdminReportController {

    @Autowired
    private StrategyService strategyService;

    @Autowired
    private UserService userService;

    @Autowired
    UserPurchaseHistoryService userPurchaseHistoryService;

    @Autowired
    DerivativeProductService derivativeProductService;

    @Autowired
    UserStrategyOrderService userStrategyOrderService;

    @Autowired
    StrategySignalDataService strategySignalDataService;

    @PostMapping("/api/v1/admin/reports/strategies")
    @ApiOperation(value = "All Strategies", tags = "Report")
    public ResponseEntity<Response<Long>> exportStrategies(
            @RequestParam(value = "minCreatedAt", required = false) String minCreatedAt,
            @RequestParam(value = "maxCreatedAt", required = false) String maxCreatedAt,
            @RequestParam(value = "strategyCode", required = false) String strategyCode,
            @RequestParam(value = "strategyName", required = false) String strategyName,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> providerIds,
            @RequestParam(value = "providerName", required = false) String providerName,
            @RequestParam(value = "createdByUserId", required = false) String createdByUserId
    ) throws JsonProcessingException {
        List<QueryParam> queryParams = new ArrayList<>();
        QueryParam.createMinMaxDate(
                queryParams,
                "createdAt",
                minCreatedAt,
                maxCreatedAt,
                "minCreatedAt",
                "maxCreatedAt"
        );
        queryParams.add(QueryParam.create("strategyCode", strategyCode));
        queryParams.add(QueryParam.create("strategyName", strategyName));
        queryParams.add(QueryParam.create("strategyProviderId", providerIds));
        queryParams.add(QueryParam.create("strategyProvider.companyName", providerName));
        return ResponseEntity.ok(new Response(strategyService.exportStrategies(queryParams, createdByUserId)));
    }

    @PostMapping("/api/v1/admin/reports/users")
    @ApiOperation(value = "All Users", tags = "Report")
    public ResponseEntity<Response<Long>> exportUsers(
            @RequestParam(value = "minCreatedAt", required = false) String minCreatedAt,
            @RequestParam(value = "maxCreatedAt", required = false) String maxCreatedAt,
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "phoneNumber", required = false) String phoneNumber,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "createdByUserId", required = false) String createdByUserId
    ) throws JsonProcessingException {
        List<QueryParam> queryParams = new ArrayList<>();
        QueryParam.createMinMaxDate(queryParams, "createdAt", minCreatedAt, maxCreatedAt, "minCreatedAt", "maxCreatedAt");
        queryParams.add(QueryParam.create("username", username));
        queryParams.add(QueryParam.create("email", email));
        queryParams.add(QueryParam.create("userInfo.phoneNumber", phoneNumber));
        queryParams.add(QueryParam.create("roles.roleName", RoleEnums.CUSTOMER.toString()));
        return ResponseEntity.ok(new Response(userService.exportUsers(queryParams, false, createdByUserId)));
    }


    @PostMapping("/api/v1/admin/reports/admins")
    @ApiOperation(value = "All Admins", tags = "Report")
    public ResponseEntity<Response<Long>> exportAdmins(
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "fullName", required = false) String fullName,
            @RequestParam(value = "phoneNumber", required = false) String phoneNumber,
            @RequestParam(value = "email", required = false) String email,
            @RequestParam(value = "providerIds", required = false) List<Long> providerIds,
            @RequestParam(value = "secIds", required = false) List<Long> secIds,
            @RequestParam(value = "createdByUserId", required = false) String createdByUserId
    ) throws JsonProcessingException {
        List<QueryParam> queryParams = new ArrayList<>();

        queryParams.add(QueryParam.create("username", username));
        queryParams.add(QueryParam.create("email", email));
        queryParams.add(QueryParam.create("userInfo.fullName", fullName));
        queryParams.add(QueryParam.create("userInfo.phoneNumber", phoneNumber));
        queryParams.add(QueryParam.create("roles.roleName", RoleEnums.ADMIN.toString()));
        queryParams.add(QueryParam.create("adminStrategyProviders.id", providerIds));
        queryParams.add(QueryParam.create("securitiesCompanies.id", secIds));
        return ResponseEntity.ok(new Response(userService.exportUsers(queryParams, true, createdByUserId)));
    }



    @PostMapping("/api/v1/admin/reports/payment-histories")
    @ApiOperation(value = "Search purchase histories", tags = "Report")
    public ResponseEntity<Response<Long>> exportPurchaseHistories(
            @RequestParam(value = "paymentMethods", required = false) List<Long> paymentMethodIds,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> providerIds,
            @RequestParam(value = "securitiesCompaniesIds", required = false) List<Long> secIds,
            @RequestParam(value = "minPurchaseDate", required = false) String minPurchaseDate,
            @RequestParam(value = "maxPurchaseDate", required = false) String maxPurchaseDate,
            @RequestParam(value = "createdByUserId", required = false) String createdByUserId
    ) throws ParseException, JsonProcessingException {
        List<QueryParam> params = new ArrayList<>();
        params.add(QueryParam.create("paymentMethod.id", paymentMethodIds));
        params.add(QueryParam.create("items.strategy.strategyProviderId", providerIds));
        params.add(QueryParam.create("items.securitiesCompany.id", secIds));
        QueryParam.createMinMaxDate(
                params,
                "purchaseDate",
                minPurchaseDate,
                maxPurchaseDate,
                "minPurchaseDate",
                "maxPurchaseDate"
        );
        return ResponseEntity.ok(new Response<>(userPurchaseHistoryService.exportPurchasePayments(params, createdByUserId)));
    }

    @PostMapping("/api/v1/admin/reports/products")
    @ApiOperation(value = "All Derivative Products", tags = "Report")
    public ResponseEntity<Response<Long>> findAll(
            @RequestParam(value = "derivativeCode", required = false) String derivativeCode,
            @RequestParam(value = "derivativeName", required = false) String derivativeName,
            @RequestParam(value = "createdByUserId", required = false) String createdByUserId
    ) throws ParseException, JsonProcessingException {
        List<QueryParam> queryParams = new ArrayList<>();
        queryParams.add(QueryParam.create("derivativeCode", derivativeCode));
        queryParams.add(QueryParam.create("derivativeName", derivativeName));
        return ResponseEntity.ok(new Response(derivativeProductService.exportDerivativeProducts(queryParams, createdByUserId)));
    }


    @PostMapping("/api/v1/admin/reports/revenues")
    @ApiOperation(value = "revenue report", tags = "Report")
    public ResponseEntity<Response<Long>> queryRevenue(
            @RequestParam(value = "byDay", required = false, defaultValue = "true") boolean isDay,
            @RequestParam(value = "paymentMethods", required = false) List<Long> paymentMethodIds,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> providerIds,
            @RequestParam(value = "securitiesCompaniesIds", required = false) List<Long> secIds,
            @RequestParam(value = "minPurchaseDate", required = false) String minPurchaseDate,
            @RequestParam(value = "maxPurchaseDate", required = false) String maxPurchaseDate,
            @RequestParam(value = "createdByUserId", required = false) String createdByUserId
    ) throws ParseException, JsonProcessingException {
        Timestamp minCreated = null;
        Timestamp maxCreated = null;
        try {
            Date minDate = TimeUtils.convertMinDateToTime(minPurchaseDate);
            if (minDate != null) minCreated = new Timestamp(minDate.getTime());
            try {
                Date maxDate = TimeUtils.convertMaxDateToTime(maxPurchaseDate);
                if (maxDate != null) maxCreated = new Timestamp(maxDate.getTime());
            } catch (ParseException e) {
                throw new InvalidFormatException("maxPurchaseDate");
            }
        } catch (ParseException e) {
            throw new InvalidFormatException("minPurchaseDate");
        }
        return ResponseEntity.ok(new Response<>(userPurchaseHistoryService.exportRevenueByDay(isDay,
                paymentMethodIds, providerIds, secIds, minCreated, maxCreated, createdByUserId)));
    }


    @PostMapping(value = "/api/v1/admin/reports/strategy-orders", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "User strategy order service", tags = "Report")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<Long>> exportTradeReport(
            @RequestParam(value = "securitiesCompaniesIds", required = false) List<Long> secIds,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> providerIds,
            @RequestParam(value = "minTradingAt", required = false) String minTradingAt,
            @RequestParam(value = "maxTradingAt", required = false) String maxTradingAt,
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "createdByUserId", required = false) String createdByUserId
    ) throws JsonProcessingException {
        Timestamp min = null;
        if (!StringUtils.isEmpty(minTradingAt)) {
            try {
                min = new Timestamp(TimeUtils.convertMinDateToDate(minTradingAt).getTime());
            } catch (ParseException e) {
                throw new InvalidFormatException("minTradingAt");
            }
        }

        Timestamp max = null;
        if (!StringUtils.isEmpty(maxTradingAt)) {
            try {
                max = new Timestamp(TimeUtils.convertMaxDateAsDate(maxTradingAt).getTime());
            } catch (ParseException e) {
                throw new InvalidFormatException("minTradingAt");
            }
        }
        return ResponseEntity.ok(new Response<>(userStrategyOrderService.exportTradeReport(secIds, providerIds, min, max, username, createdByUserId)));
    }

    @PostMapping(value = "/api/v1/admin/reports/auto-strategy-orders", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ApiOperation(value = "User strategy order service", tags = "Report")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<Long>> exportTradeAutoReport(
            @RequestParam(value = "securitiesCompaniesIds", required = false) List<Long> secIds,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> providerIds,
            @RequestParam(value = "minTradingAt", required = false) String minTradingAt,
            @RequestParam(value = "maxTradingAt", required = false) String maxTradingAt,
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "strategyName", required = false) String strategyName,
            @RequestParam(value = "createdByUserId", required = false) String createdByUserId
    ) throws JsonProcessingException {
        List<QueryParam> queryParams = new ArrayList<>();
        QueryParam.createMinMaxDate(queryParams, "tradingAt", minTradingAt, minTradingAt, "minTradingAt", "maxTradingAt");

        queryParams.add(QueryParam.create("securitiesCompany.id", secIds));
        queryParams.add(QueryParam.create("userPurchaseHistory.strategy.strategyProvider.id", providerIds));
        queryParams.add(QueryParam.create("user.userInfo.fullName", username));
        queryParams.add(QueryParam.create("userPurchaseHistory.strategy.strategyName", strategyName));
        queryParams.add(QueryParam.create("autoYn", "1"));

        return ResponseEntity.ok(new Response<>(userStrategyOrderService.exportTradeAutoReport(createdByUserId, queryParams)));
    }

    @PostMapping("/api/v1/admin/reports/signals")
    @ApiOperation(value = "Report Signal", tags = "Report")
    public ResponseEntity<Response<Long>> querySignalDatas(
            @RequestParam(value = "strategyIds", required = false) List<Long> strategyIds,
            @RequestParam(value = "strategyProviderIds", required = false) List<Long> strategyProviderIds,
            @RequestParam(value = "createdByUserId", required = false) String createdByUserId
    ) throws JsonProcessingException {
        List<QueryParam> queryParams = new ArrayList<>();
        queryParams.add(QueryParam.create("strategyId", strategyIds, Long.class));
        if (!CollectionUtils.isEmpty(strategyProviderIds)) {
            queryParams.add(QueryParam.create("strategy.strategyProviderId", strategyProviderIds, Long.class));
        }
        return ResponseEntity.ok(new Response(strategySignalDataService.exportSignals(queryParams, createdByUserId)));
    }
}
