package com.fintasoft.eraserisk.controllers.internal;

import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.model.api.request.SignalDataRequest;
import com.fintasoft.eraserisk.model.api.response.StrategySignalDataResponse;
import com.fintasoft.eraserisk.services.signal.StrategySignalDataService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StrategySignalController {


    @Autowired
    StrategySignalDataService strategySignalDataService;

    @PostMapping("/api/v1/internal/signals")
    @ApiOperation(value = "Create a signal data", tags = "Signal")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<StrategySignalDataResponse>> createSignalData(
            @RequestBody SignalDataRequest request
    ) {
        return ResponseEntity.ok(new Response<>(strategySignalDataService.createSignal(request)));
    }
}
