package com.fintasoft.eraserisk.controllers.internal;

import com.fintasoft.eraserisk.model.api.Response;
import com.fintasoft.eraserisk.services.queue.StrategyPriceService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class StrategyPriceController {


    @Autowired
    StrategyPriceService strategyPriceService;

    @GetMapping("/api/v1/internal/strategy/live-price")
    @ApiOperation(value = "Create a signal data", tags = "Internal")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad request- validation fail"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Response<Map<Long, Double>>> createSignalData(
            @RequestParam("ids") List<Long> ids
    ) {
        Map<Long, Double> result = new HashMap<>();
        ids.forEach(id -> result.put(id, strategyPriceService.getPrice(id)));
        return ResponseEntity.ok(new Response<>(result));
    }
}
