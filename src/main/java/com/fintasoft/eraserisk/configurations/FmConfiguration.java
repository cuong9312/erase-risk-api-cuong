package com.fintasoft.eraserisk.configurations;

import freemarker.template.TemplateExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FmConfiguration {
    @Autowired
    private CustomTemplateLoader customTemplateLoader;

    private freemarker.template.Configuration fmConfiguration;

    public freemarker.template.Configuration fmConfiguration() {
        if (fmConfiguration == null) {
            fmConfiguration = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_26);
            fmConfiguration.setDefaultEncoding("UTF-8");
            fmConfiguration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            fmConfiguration.setLogTemplateExceptions(true);
            fmConfiguration.setTemplateLoader(customTemplateLoader);
        }
        return fmConfiguration;
    }
}
