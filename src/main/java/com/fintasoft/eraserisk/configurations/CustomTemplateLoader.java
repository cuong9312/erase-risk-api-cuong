package com.fintasoft.eraserisk.configurations;

import freemarker.cache.TemplateLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class CustomTemplateLoader implements TemplateLoader {
    private ResourceLoader resourceLoader;

    @Autowired
    public CustomTemplateLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @Override
    public Object findTemplateSource(String resourceName) throws IOException {
        File f = new File(resourceName);
        if (f.exists()) return f;
        String name = "classpath:" + resourceName;
        Resource resource = this.resourceLoader.getResource(name);
        if (!resource.exists()) {
            throw new RuntimeException("Resource " + resourceName + " does not exist");
        }
        return resource.getFile();
    }

    @Override
    public long getLastModified(Object templateSource) {
        return Long.valueOf(((File)templateSource).lastModified());
    }

    @Override
    public Reader getReader(Object templateSource, String encoding) throws IOException {
        if(!(templateSource instanceof File)) {
            throw new IllegalArgumentException("templateSource wasn\'t a File, but a: " + templateSource.getClass().getName());
        } else {
            return new InputStreamReader(new FileInputStream((File)templateSource), encoding);
        }
    }

    @Override
    public void closeTemplateSource(Object o) throws IOException {

    }
}
