package com.fintasoft.eraserisk.configurations;

import com.fintasoft.eraserisk.Application;
import com.fintasoft.eraserisk.services.queue.RequestResponseHandler;
import com.fintasoft.eraserisk.services.queue.StrategyPriceService;
import com.fintasoft.eraserisk.model.kafka.Message;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RouteConfiguration extends RouteBuilder {
    private static final Logger log = LoggerFactory.getLogger(RouteConfiguration.class);

    @Autowired
    AppConf appConf;

    @Autowired
    StrategyPriceService strategyPriceService;

    @Autowired
    RequestResponseHandler requestResponseHandler;

    @Override
    public void configure() throws Exception {
        errorHandler(loggingErrorHandler(log, LoggingLevel.ERROR));
        this.from(processTopicName(appConf.getQueue().getStrategyPriceListener()))
                .to("log:in")
                .unmarshal().json(JsonLibrary.Jackson, Message.class)
                .process(strategyPriceService)
        ;
        this.from(processTopicName(appConf.getQueue().getRequestListener()))
                .to("log:in")
                .unmarshal().json(JsonLibrary.Jackson, Message.class)
                .process(requestResponseHandler)
        ;
        strategyPriceService.init();
    }

    private String processTopicName(String topic) {
        return topic.replace("{uuid}", Application.getInstanceId()).replace("{listenResponseTopic}", this.getResponseListenerTopic());
    }

    public String getResponseListenerTopic() {
        return appConf.getQueue().getListenResponseTopic().replace("{uuid}", Application.getInstanceId());
    }
}
