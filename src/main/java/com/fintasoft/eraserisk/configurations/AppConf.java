package com.fintasoft.eraserisk.configurations;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;
import java.util.Map;

@Data
@ConfigurationProperties(prefix = "app")
public class AppConf {
    private String emailVerificationTemplate;
    private String emailSupport;
    private String resetPasswordTemplate;
    private Checkout checkout;
    private Report report;
    private Email email;
    private Frontend frontend;
    private List<StrategyPositionCategory> calculateStrategyPosition;
    private Map<String, String> resources;
    private QueueConfig queue;

    @Data
    public static class Checkout {
        private String accountId;
        private String linkKey;
        private String linkValue;
        private String frontUrl;
        private String wsUrl;
        private String feedbackPayapp;
    }

    @Data
    public static class Report {
        private String reportDir;
    }

    @Data
    public static class Email {
        private String endpoint;
        private int port;
        private String username;
        private String smtpUsername;
        private String smtpPassword;
        private String sender;
        private String support;
    }

    @Data
    public static class Frontend {
        private String basePath;
        private String verifyEmail;
        private String resetPassword;
    }

    @Data
    public static class StrategyPositionCategory {
        private String name;
        private int range;
    }

    @Data
    public static class QueueConfig {
        private String strategyPriceListener;
        private String listenResponseTopic;
        private String requestListener;
        private String commonSendingOut;
        private Topics topics;
    }

    @Data
    public static class Topics {
        private String strategyUpdate;
        private String reportUpdate;
        private String strategySignalUpdate;
        private String strategyStatusUpdate;
        private String derivativeProductUpdate;
        private String liveData;
        private String notification;
    }
}
